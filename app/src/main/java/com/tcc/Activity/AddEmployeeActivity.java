package com.tcc.Activity;


import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.tcc.Model.AddEmployeeModel;
import com.tcc.Model.CityModel;
import com.tcc.Model.GetUserType;
import com.tcc.R;
import com.tcc.utils.AppUtil;
import com.tcc.utils.CameraGalleryPermission;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddEmployeeActivity extends AppCompatActivity {

    public static final int REQUEST_IMAGE = 100;
    private static final String TAG = TicketActivity.class.getSimpleName();
    private static final int PROFILE_PIC = 1, AADHARCARD = 2, OFFER_LETTER = 3;
    private final int REQUEST_CODE_DOC = 121;
    private final int REQUEST_CAMERA = 122;
    java.io.File destination;
    ArrayList<GetUserType.Datum> designationList = new ArrayList<>();
    ArrayList<CityModel.Datum> mArrayCityList = new ArrayList<>();
    ArrayList<String> DesignationString = new ArrayList<>();
    ArrayList<String> mArrCity = new ArrayList<>();
    private Activity mActivity;
    private ImageView mImageBack;
    private TextView mTextHeader;
    private ImageView imgProfileEmployeeImagel, addDocEmployee, addOfferletterEmployee;
    private TextView tv_uniformType;
    private Button button_add_customer_submit;
    private int mYear, mMonth, mDay;
    private String mFilePath, CityID = "";
    private String strTitle, choosenTask = "";
    private boolean isDownload = false;
    private EditText mEditFirstName, mEditLastName, mEditMobile, mEditSalary, mEditPassword, mEditCPass, mEditJoiningDate, mEditWokringHours, mEditAddress, mEditBankName, mEditBranchName, mEditAccountNumber, mEditIFSCCode;
    private Spinner mSpinner, spinnerCity;
    private int SelectedDesignationIndex = 0, mSelectedCityIndex = 0, mCityId;
    private String DesignationID = "";
    private int IMAGE_STATUS = PROFILE_PIC;

    private String ProfilePicPath, AadharCardPath, OfferLetterPath;

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.Spinner_Designation:
                    SelectedDesignationIndex = adapterView.getSelectedItemPosition();

                    if (SelectedDesignationIndex != 0) {
                        DesignationID = String.valueOf((Integer.parseInt(designationList.get(SelectedDesignationIndex - 1).getUsertypeID())));
                        String Name = (designationList.get(SelectedDesignationIndex - 1).getUsertype());
                        Common.insertLog("mStateId::> " + DesignationID);
                        Common.insertLog("mStateName::> " + Name);
                    }
                    break;
                case R.id.spinner_register_city:
                    mSelectedCityIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedCityIndex != 0) {
                        CityID = String.valueOf((Integer.parseInt(mArrayCityList.get(mSelectedCityIndex - 1).getCityID())));
                        String mCityName = (mArrayCityList.get(mSelectedCityIndex - 1).getCityName());
                        Common.insertLog("mCityId::> " + CityID);
                        Common.insertLog("mCityName::> " + mCityName);
                    }
                    break;


            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer1);

        mActivity = AddEmployeeActivity.this;
        getIds();
        setData();
        setRegister();
        getDesignation();
        callToCityAPI(-1);

        ImagePickerActivity.clearCache(this);

        Date c = Calendar.getInstance().getTime();
        String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c);

        mEditJoiningDate.setText(formattedDate);

        imgProfileEmployeeImagel.setImageDrawable(Common.setLabeledImageView(AddEmployeeActivity.this, "John", "Carter"));

        button_add_customer_submit = findViewById(R.id.button_add_customer_submit);

        button_add_customer_submit.setOnClickListener(v -> {

            if (checkValidation()) {
                AddEmployee();
            }
        });

        tv_uniformType.setOnClickListener(v -> {
            Intent intent = new Intent(AddEmployeeActivity.this, AddUniformActivity.class);
            startActivity(intent);
        });
    }

    private boolean checkValidation() {

        boolean status = true;

        SessionManager manager = new SessionManager(mActivity);
        mFilePath = manager.getPreferences(SessionManager.SOURCE_PATH, "");

        if (mFilePath.equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Select Photo", Toast.LENGTH_SHORT).show();
        }
        if (mEditFirstName.getText().toString().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Enter First Name", Toast.LENGTH_SHORT).show();
        }
        if (mEditLastName.getText().toString().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Enter Last Name", Toast.LENGTH_SHORT).show();
        }
        if (mEditSalary.getText().toString().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Enter Phone", Toast.LENGTH_SHORT).show();
        }
        if (mEditAddress.getText().toString().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Enter Address", Toast.LENGTH_SHORT).show();
        }
        if (mEditWokringHours.getText().toString().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Enter Wokring Hours", Toast.LENGTH_SHORT).show();
        }
        if (!mEditCPass.getText().toString().equals(mEditPassword.getText().toString())) {
            status = false;
            Toast.makeText(mActivity, "Password Not Match", Toast.LENGTH_SHORT).show();
        }
        if (mEditPassword.getText().toString().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Enter Password", Toast.LENGTH_SHORT).show();
        }
        if (DesignationID.equalsIgnoreCase("") || DesignationID.equalsIgnoreCase("0")) {
            status = false;
            Toast.makeText(mActivity, "Please Select Designation", Toast.LENGTH_SHORT).show();
        }
        if (CityID.equalsIgnoreCase("") || CityID.equalsIgnoreCase("0")) {
            status = false;
            Toast.makeText(mActivity, "Please Select City", Toast.LENGTH_SHORT).show();
        }
//        if (selectedFile.getName().equalsIgnoreCase("")) {
//            status = false;
//            Toast.makeText(mActivity, "Please Select Photo", Toast.LENGTH_SHORT).show();
//        }

        return status;
    }

    private void AddEmployee() {
        try {
            String ConvertedDate = Common.convertDateToServer(mActivity, mEditJoiningDate.getText().toString());
            File file = new File(ProfilePicPath);
            final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

            MultipartBody.Part body = MultipartBody.Part.createFormData(WebFields.ADD_EMPLOYEE.REQUEST_IMAGE,
                    file.getName(), requestFile);

            File fileaadhar = new File(AadharCardPath);
            final RequestBody requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), fileaadhar);

            MultipartBody.Part body2 = MultipartBody.Part.createFormData(WebFields.ADD_EMPLOYEE.REQUEST_DOCUMENT_IMAGE,
                    fileaadhar.getName(), requestFile2);

            File fileOfferLetter = new File(OfferLetterPath);

            final RequestBody requestFileOffer = RequestBody.create(MediaType.parse("multipart/form-data"), fileOfferLetter);

            MultipartBody.Part body3 = MultipartBody.Part.createFormData(WebFields.ADD_EMPLOYEE.REQUEST_OFFER_IMAGE,
                    fileOfferLetter.getName(), requestFileOffer);

            RequestBody
                    method = RequestBody.create(MediaType.parse("text/plain"), WebFields.ADD_EMPLOYEE.MODE);

            RequestBody FirstName = RequestBody.create(MediaType.parse("text/plain"), mEditFirstName.getText().toString());
            RequestBody LastName = RequestBody.create(MediaType.parse("text/plain"), mEditLastName.getText().toString());
            RequestBody Email = RequestBody.create(MediaType.parse("text/plain"), "");
            RequestBody MobileNo = RequestBody.create(MediaType.parse("text/plain"), mEditMobile.getText().toString());
            RequestBody salary = RequestBody.create(MediaType.parse("text/plain"), mEditSalary.getText().toString());
            RequestBody Adress = RequestBody.create(MediaType.parse("text/plain"), mEditAddress.getText().toString());
            RequestBody Password = RequestBody.create(MediaType.parse("text/plain"), mEditPassword.getText().toString());
            RequestBody Date = RequestBody.create(MediaType.parse("text/plain"), ConvertedDate);
            RequestBody Hrs = RequestBody.create(MediaType.parse("text/plain"), mEditWokringHours.getText().toString());
            RequestBody ID = RequestBody.create(MediaType.parse("text/plain"), DesignationID);
            RequestBody BankName = RequestBody.create(MediaType.parse("text/plain"), mEditBankName.getText().toString().trim());
            RequestBody BranchName = RequestBody.create(MediaType.parse("text/plain"), mEditBranchName.getText().toString().trim());
            RequestBody AccoountNumber = RequestBody.create(MediaType.parse("text/plain"), mEditAccountNumber.getText().toString().trim());
            RequestBody IFSCCode = RequestBody.create(MediaType.parse("text/plain"), mEditIFSCCode.getText().toString().trim());
            RequestBody cityID = RequestBody.create(MediaType.parse("text/plain"), CityID);

            Call<AddEmployeeModel> callRepos = new RetrofitClient().createService(ApiInterface.class).AddEmployeeAPI(method,
                    FirstName, LastName, Email, Password, MobileNo, Adress, ID, salary, Date, Hrs, BankName, BranchName, AccoountNumber, IFSCCode, cityID, body, body2, body3);

            callRepos.enqueue(new Callback<AddEmployeeModel>() {
                @Override
                public void onResponse(@NonNull Call<AddEmployeeModel> call, @NonNull Response<AddEmployeeModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            finish();
                        } else {

                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddEmployeeModel> call, @NonNull Throwable t) {
                    Common.insertLog("Add UploadPics error " + t.getMessage());
//                    Common.hideDialog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getDesignation() {
        try {

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getUserType());

            Call<GetUserType> call = RetrofitClient.createService(ApiInterface.class).getUserType(body);

            call.enqueue(new Callback<GetUserType>() {
                @Override
                public void onResponse(Call<GetUserType> call, Response<GetUserType> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            designationList.addAll(response.body().getData());
                            setTrainingAdapter();

                        } else {
                            // Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<GetUserType> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTrainingAdapter() {
        try {
            DesignationString.add(0, "Select UserType");
            if (designationList.size() > 0) {
                for (GetUserType.Datum Services : designationList) {
                    DesignationString.add(Services.getUsertype());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, DesignationString) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == SelectedDesignationIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinner.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIds() {
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        imgProfileEmployeeImagel = findViewById(R.id.imgProfileEmployeeImage);
        addDocEmployee = findViewById(R.id.addDocEmployee);
        addOfferletterEmployee = findViewById(R.id.add_OfferLetter);
        tv_uniformType = findViewById(R.id.tv_uniformType);

        spinnerCity = findViewById(R.id.spinner_register_city);

        mEditFirstName = findViewById(R.id.edit_first_name);
        mEditLastName = findViewById(R.id.edit_last_name);
        mEditMobile = findViewById(R.id.edit_mobile);
        mEditSalary = findViewById(R.id.edit_salary);
        mEditPassword = findViewById(R.id.edit_password);
        mEditCPass = findViewById(R.id.edit_confirm_password);
        mEditJoiningDate = findViewById(R.id.edit_date_join);
        mEditWokringHours = findViewById(R.id.edit_working_hours);
        mEditAddress = findViewById(R.id.edit_address);
        mEditBankName = findViewById(R.id.edit_bank_name);
        mEditBranchName = findViewById(R.id.edit_branchname);
        mEditAccountNumber = findViewById(R.id.edit_accountnumber);
        mEditIFSCCode = findViewById(R.id.edit_ifsc_code);

        mSpinner = findViewById(R.id.Spinner_Designation);
        mSpinner.setOnItemSelectedListener(onItemSelectedListener);
        spinnerCity.setOnItemSelectedListener(onItemSelectedListener);
    }

    private void setData() {
        mTextHeader.setText("EMPLOYEE");
        mImageBack.setVisibility(View.VISIBLE);
    }


    private void setRegister() {

        mEditJoiningDate.setOnClickListener(v -> {
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(AddEmployeeActivity.this,
                    (view, year, monthOfYear, dayOfMonth) -> mEditJoiningDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
            datePickerDialog.show();
        });

        mImageBack.setOnClickListener(v -> {
            onBackPressed();
            finish();
        });

        imgProfileEmployeeImagel.setOnClickListener(v -> Dexter.withActivity(AddEmployeeActivity.this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();

                            IMAGE_STATUS = PROFILE_PIC;
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check());

        addDocEmployee.setOnClickListener(v -> Dexter.withActivity(AddEmployeeActivity.this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();

                            IMAGE_STATUS = AADHARCARD;
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check());

        addOfferletterEmployee.setOnClickListener(v -> Dexter.withActivity(AddEmployeeActivity.this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();

                            IMAGE_STATUS = OFFER_LETTER;
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check());
    }

    /***\
     *Open Camera and Upload
     */
    private void cameraIntent() {
        destination = AppUtil.currentTimeStampFile();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
        } else {
            java.io.File file = new File(Uri.fromFile(destination).getPath());
            Uri photoUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (intent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    /***
     * Get file for upload the CV, user can upload PDF,Doc and Docx file only
     */
    private void getFile() {
        String[] mimeTypes =
                {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", //
                        // .doc & .docx
                        "application/pdf",
                        "image/*"};

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";
            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }
            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }
        startActivityForResult(Intent.createChooser(intent, "ChooseFile"), REQUEST_CODE_DOC);
    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(AddEmployeeActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(AddEmployeeActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(AddEmployeeActivity.this);
        builder.setTitle("Grant Permission");
        builder.setMessage("This App Needs Permission To Use This Feature");
        builder.setPositiveButton("Open Settings", (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                try {
                    // You can update this bitmap to your server
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);

                    // loading profile image from local cache
                    loadProfile(uri.toString());
//                    mFilePath = uri.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void loadProfile(String url) {
        Log.d(TAG, "Image cache path: " + url);

        if (IMAGE_STATUS == PROFILE_PIC) {
            SessionManager manager = new SessionManager(mActivity);
            ProfilePicPath = manager.getPreferences(SessionManager.SOURCE_PATH, "");
            Glide.with(this).load(url)
                    .into(imgProfileEmployeeImagel);
        }
        if (IMAGE_STATUS == AADHARCARD) {
            SessionManager manager = new SessionManager(mActivity);
            AadharCardPath = manager.getPreferences(SessionManager.SOURCE_PATH, "");
            Glide.with(this).load(url)
                    .into(addDocEmployee);
        }
        if (IMAGE_STATUS == OFFER_LETTER) {
            SessionManager manager = new SessionManager(mActivity);
            OfferLetterPath = manager.getPreferences(SessionManager.SOURCE_PATH, "");
            Glide.with(this).load(url)
                    .into(addOfferletterEmployee);
        }
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[]
            grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CameraGalleryPermission.INTERNAL_EXTERNAL_PERMISSION:
                Map<String, Integer> perms = new HashMap<String, Integer>();
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                if (isDownload && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager
                        .PERMISSION_GRANTED) {
                    //downloadDocument(EditDocument.getDocumentUrl());
                } else {
                    if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        if (choosenTask.equals("Capture Image"))
                            cameraIntent();
                        else if (choosenTask.equals("Select From File"))
                            getFile();
                    } else {
                        Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    /**
     * This method should call the City API
     */
    private void callToCityAPI(int mStateId) {
        try {
            if (mArrCity.size() > 0)
                mArrCity.clear();
            if (mArrayCityList.size() > 0)
                mArrayCityList.clear();

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCityListJson(String.valueOf(mStateId)));

            Call<CityModel> call = RetrofitClient.createService(ApiInterface.class).getCityListAPI(body);
            call.enqueue(new Callback<CityModel>() {
                @Override
                public void onResponse(@NonNull Call<CityModel> call, @NonNull Response<CityModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                mArrayCityList.addAll(response.body().getData());
                                setCityAdapter();
                            }
                        } else {
                            setCityAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CityModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setCityAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setCityAdapter() {
        try {
            mArrCity.add(0, getString(R.string.spinner_select_city));

            if (mArrayCityList.size() > 0) {
                for (CityModel.Datum cityModel : mArrayCityList) {
                    mArrCity.add(cityModel.getCityName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrCity) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCityIndex) {
                        // Set spinner selected popup item's text color
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerCity.setAdapter(adapter);

            for (int i = 0; i < mArrCity.size(); i++) {
                if (mArrCity.get(i).equals("Ahmedabad")) {
                    spinnerCity.setSelection(i);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
