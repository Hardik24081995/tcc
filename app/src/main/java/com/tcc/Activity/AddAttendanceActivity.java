package com.tcc.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tcc.Adapter.AddAttendanceAdapter;
import com.tcc.Model.AddAttendanceModel;
import com.tcc.Model.GetTeamEmployeeModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAttendanceActivity extends AppCompatActivity {

    Button mSubmit;
    Activity mActivity;
    ArrayList<GetTeamEmployeeModel.Datum> datumArrayList = new ArrayList<>();
    SessionManager mSession;
    String SitesID, UserID, currentTime, formattedDate, QuotationID;
    RecyclerView ev_attendanceadd;
    JSONArray jsonArray = new JSONArray();
    private ImageView mImageBack, mImageAdd;
    private TextView mTextHeader, tvTodayDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_attendance);

        mActivity = AddAttendanceActivity.this;
        mSession = new SessionManager(mActivity);
        String UserData = mSession.getPreferences(mSession.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);
        SitesID = mSession.getPreferences(AppConstants.SITE_ID, "");
        QuotationID = mSession.getPreferences(AppConstants.QUOTATION_ID, "");

        tvTodayDate = findViewById(R.id.tvTodayDate);

        Date c = Calendar.getInstance().getTime();
        currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        formattedDate = df.format(c);

        tvTodayDate.setText("Date : " + formattedDate);

        getID();
        setData();
        getTeamEmployeeList();

    }

    private void getTeamEmployeeList() {
        try {
            Common.showLoadingDialog(mActivity, "Loading");
            String ConvertedReminderDate = Common.convertDateToServer(this, formattedDate);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getTeamEmployeeJson("-1", "1", SitesID, ConvertedReminderDate, QuotationID));

            Call<GetTeamEmployeeModel> call = RetrofitClient.createService(ApiInterface.class).getTeamEmployeeAPI(body);
            call.enqueue(new Callback<GetTeamEmployeeModel>() {
                @Override
                public void onResponse(@NonNull Call<GetTeamEmployeeModel> call, @NonNull Response<GetTeamEmployeeModel> response) {

                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            datumArrayList.addAll(response.body().getData());
                            setAdapter();
                        } else {
                            // Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetTeamEmployeeModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        AddAttendanceAdapter sitesAdapter = new AddAttendanceAdapter(mActivity, datumArrayList) {
            @Override
            protected void addToJsonArray(GetTeamEmployeeModel.Datum datum, String Attendance, String EmployeeID, String SitesID) {
                super.addToJsonArray(datum, Attendance, EmployeeID, SitesID);
                addJsonArray(datum, Attendance, EmployeeID, SitesID);
            }
        };
        ev_attendanceadd.setHasFixedSize(true);
        ev_attendanceadd.setLayoutManager(new LinearLayoutManager(mActivity));
        ev_attendanceadd.setAdapter(sitesAdapter);
    }

    private void addJsonArray(GetTeamEmployeeModel.Datum datum, String attendance, String employeeID, String SitesID) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("EmployeeID", employeeID);
            jsonObject.put("Attendance", attendance);
            jsonObject.put("SitesID", SitesID);
            jsonObject.put("QuotationID", QuotationID);
            jsonArray.put(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getID() {
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);

        ev_attendanceadd = findViewById(R.id.ev_attendanceadd);
        mSubmit = findViewById(R.id.submitAttendance);
    }

    private void setData() {
        mTextHeader.setText("ATTENDANCE");
        mImageBack.setVisibility(View.VISIBLE);

        mImageBack.setOnClickListener(v -> onBackPressed());


        mSubmit.setOnClickListener(v -> {
            doSubmit();
        });
    }

    private void doSubmit() {

        try {
            String ConvertedReminderDate = Common.convertDateToServer(this, formattedDate);
            Common.showLoadingDialog(this, "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.addEmployeeAttendanceJson(UserID, ConvertedReminderDate, jsonArray));

            Call<AddAttendanceModel> call = RetrofitClient.createService(ApiInterface.class).addEmployeeAttendanceAPI(body);
            call.enqueue(new Callback<AddAttendanceModel>() {
                @Override
                public void onResponse(@NonNull Call<AddAttendanceModel> call, @NonNull Response<AddAttendanceModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            finish();
                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddAttendanceModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
