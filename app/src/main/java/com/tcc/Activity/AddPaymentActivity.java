package com.tcc.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.tcc.Model.AddCustomerPaymentModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tcc.utils.Common.hideDialog;

public class AddPaymentActivity extends AppCompatActivity {

    RadioGroup radioGroupPaymentType, radioGroupAmounttype;
    RadioButton mRadioCash, mRadioCheque, mRadioOnline, mRadioInclude, mRadioExclude, mRadioOnly;
    LinearLayout llBookingAmt, llBookingGstAmt, llBankName, llIFSCCode, llAccountNo, ll_UTR_no, llMobile, llDescription, llBranch, llCheque, llPaymentDate;
    String PaymentMode = "Cash", AmountType = "0", TotalAmount, GstAmount, BankName, IFSCcode, UtrNo, Mobile, Description, BranchName, Cheque, DATE, AccountNo;
    SessionManager mSessionManager;
    String UserID, SiteID, QuotationID;
    float totalamount = 0, totalgst = 0, enteredTotal = 0, enteredGst = 0;
    private Activity mActivity;
    private ImageView mImageBack;
    private TextView mTextTitle, mTextName, mTextFromdate, mTexttoDate, mTextRemianingAmount, mTextTotalGSTAmount, mTextTotalAmount, mTextReminingGstAmount;
    private Button addSubmit;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private EditText mEditTotalAmount, mEditGstAmount, mEditBankName, mEditIFSCcode, mEditUtrNo, mEditMobile, mEditDescription, mEditBranName, mEditCheque, mEditDate, mEditAccountNo;
    private View.OnClickListener clickListener = v -> {
        switch (v.getId()) {
            case R.id.image_custom_toolbar_back_arrow:
                onBackPressed();
                break;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment);
        mActivity = AddPaymentActivity.this;

        mSessionManager = new SessionManager(AddPaymentActivity.this);
        String UserData = mSessionManager.getPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(AddPaymentActivity.this, WebFields.LOGIN.RESPONSE_USERID);
        SiteID = mSessionManager.getPreferences(AppConstants.SITE_ID, "");
        QuotationID = mSessionManager.getPreferences(AppConstants.QUOTATION_ID, "");

        getIds();
        setData();
        setRegister();

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        mEditDate.setText(dateFormat.format(date));
    }

    private void setData() {
        mImageBack.setVisibility(View.VISIBLE);
        mTextTitle.setText("Payment");

        if (mSessionManager.getPreferences(AppConstants.TOTAL_REMAINING_AMOUNT, "").equalsIgnoreCase("")) {
            mTextRemianingAmount.setText("Remaining Amount : " + mSessionManager.getPreferences(AppConstants.AMOUNT, ""));
        } else {
            mTextRemianingAmount.setText("Remaining Amount : " + mSessionManager.getPreferences(AppConstants.TOTAL_REMAINING_AMOUNT, ""));
        }

        if (mSessionManager.getPreferences(AppConstants.TOTAL_REMAINING_GST, "").equalsIgnoreCase("")) {
            mTextReminingGstAmount.setText("Remaining GST Amount : " + mSessionManager.getPreferences(AppConstants.GST_AMOUNT, ""));
        } else {
            mTextReminingGstAmount.setText("Remaining GST Amount : " + mSessionManager.getPreferences(AppConstants.TOTAL_REMAINING_GST, ""));
        }

        mTextName.setText("Name : " + mSessionManager.getPreferences("Name", ""));
        mTextFromdate.setText("From Date : " + mSessionManager.getPreferences(AppConstants.START_DATE, ""));
        mTexttoDate.setText("To Date : " + mSessionManager.getPreferences(AppConstants.END_DATE, ""));
        mTextTotalAmount.setText("Total Amount : " + mSessionManager.getPreferences(AppConstants.AMOUNT, ""));
        mTextTotalGSTAmount.setText("Total GST Amount : " + mSessionManager.getPreferences(AppConstants.GST_AMOUNT, ""));
    }

    private void getIds() {
        addSubmit = findViewById(R.id.button_add_payment_submit);

        mTextName = findViewById(R.id.txtCustomerName);
        mTextFromdate = findViewById(R.id.txtFromDate);
        mTexttoDate = findViewById(R.id.txtToDate);
        mTextTotalAmount = findViewById(R.id.txtTotalAmount);
        mTextRemianingAmount = findViewById(R.id.txtRemainingAmount);
        mTextTotalGSTAmount = findViewById(R.id.txtTotalGSTAmount);
        mTextReminingGstAmount = findViewById(R.id.txtRemainingGSTAmount);

        mEditBankName = findViewById(R.id.edit_add_payment_bank_name);
        mEditTotalAmount = findViewById(R.id.edit_add_payment_total_amount);
        mEditGstAmount = findViewById(R.id.edit_add_payment_gst_amount);
        mEditIFSCcode = findViewById(R.id.edit_add_ifsc);
        mEditUtrNo = findViewById(R.id.edit_add_utrno);
        mEditMobile = findViewById(R.id.edit_add_contact_no);
        mEditDescription = findViewById(R.id.edit_add_description);
        mEditBranName = findViewById(R.id.edit_add_payment_branch_name);
        mEditCheque = findViewById(R.id.edit_add_payment_cheque_no);
        mEditDate = findViewById(R.id.edit_add_payment_payment_date);
        mEditAccountNo = findViewById(R.id.edit_add_account_no);


        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        mTextTitle = findViewById(R.id.text_custom_toolbar_without_back_header);

        radioGroupPaymentType = findViewById(R.id.rg_payment_options);
        radioGroupAmounttype = findViewById(R.id.rg_Amount_Type);

        mRadioCash = findViewById(R.id.rbCash);
        mRadioCheque = findViewById(R.id.rbCheque);
        mRadioOnline = findViewById(R.id.rbOnline);
        mRadioInclude = findViewById(R.id.rbIncludingGST);
        mRadioExclude = findViewById(R.id.rbExcludingGST);
        mRadioOnly = findViewById(R.id.rbOnlyGST);

        llBookingAmt = findViewById(R.id.llBookingAmt);
        llBookingGstAmt = findViewById(R.id.llBookingGstAmt);
        llBankName = findViewById(R.id.llBankName);
        llIFSCCode = findViewById(R.id.llIFSCCode);
        llAccountNo = findViewById(R.id.llAccountNo);
        ll_UTR_no = findViewById(R.id.ll_UTR_no);
        llMobile = findViewById(R.id.llMobile);
        llDescription = findViewById(R.id.llDescription);
        llBranch = findViewById(R.id.llBranch);
        llCheque = findViewById(R.id.llCheque);
    }

    /**
     * Set Register Listener
     */
    private void setRegister() {
        mImageBack.setOnClickListener(clickListener);

        addSubmit.setOnClickListener(v -> {
            if (checkValidation()) {
                doAddPayment();
            }
        });

        mEditDate.setOnClickListener(v -> {
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    (view, year, monthOfYear, dayOfMonth) -> mEditDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
            datePickerDialog.show();

        });

        radioGroupPaymentType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton radioButton = findViewById(checkedId);

                if (radioButton.getId() == R.id.rbCash) {
                    ll_UTR_no.setVisibility(View.GONE);
                    llAccountNo.setVisibility(View.GONE);
                    llBankName.setVisibility(View.GONE);
                    llBranch.setVisibility(View.GONE);
                    llCheque.setVisibility(View.GONE);
                    llIFSCCode.setVisibility(View.GONE);

                    PaymentMode = "Cash";

                } else if (radioButton.getId() == R.id.rbCheque) {
                    ll_UTR_no.setVisibility(View.GONE);
                    llAccountNo.setVisibility(View.VISIBLE);
                    llBankName.setVisibility(View.VISIBLE);
                    llBranch.setVisibility(View.VISIBLE);
                    llCheque.setVisibility(View.VISIBLE);
                    llIFSCCode.setVisibility(View.GONE);

                    PaymentMode = "Cheque";
                } else if (radioButton.getId() == R.id.rbOnline) {
                    ll_UTR_no.setVisibility(View.VISIBLE);
                    llAccountNo.setVisibility(View.VISIBLE);
                    llBankName.setVisibility(View.VISIBLE);
                    llBranch.setVisibility(View.VISIBLE);
                    llCheque.setVisibility(View.GONE);
                    llIFSCCode.setVisibility(View.VISIBLE);

                    PaymentMode = "Online";
                }

            }
        });

        radioGroupAmounttype.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton radioButton = findViewById(checkedId);

                if (radioButton.getId() == R.id.rbIncludingGST) {
                    llBookingGstAmt.setVisibility(View.VISIBLE);
                    llBookingAmt.setVisibility(View.VISIBLE);

                    AmountType = "0";
                } else if (radioButton.getId() == R.id.rbExcludingGST) {
                    llBookingAmt.setVisibility(View.VISIBLE);
                    llBookingGstAmt.setVisibility(View.GONE);

                    AmountType = "1";
                } else if (radioButton.getId() == R.id.rbOnlyGST) {
                    llBookingGstAmt.setVisibility(View.VISIBLE);
                    llBookingAmt.setVisibility(View.GONE);

                    AmountType = "2";
                }
            }
        });
    }

    private void doAddPayment() {
        try {

            String ConvertedDate = Common.convertDateToServer(mActivity, mEditDate.getText().toString());
            Common.showLoadingDialog(this, "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.addCustomerPaymentJson(UserID, SiteID, AmountType, TotalAmount, GstAmount, ConvertedDate, PaymentMode, Cheque, IFSCcode,
                    AccountNo, BankName, BranchName, UtrNo, QuotationID));

            Call<AddCustomerPaymentModel> call = RetrofitClient.createService(ApiInterface.class).addCustomerPaymentAPI(body);
            call.enqueue(new Callback<AddCustomerPaymentModel>() {
                @Override
                public void onResponse(@NonNull Call<AddCustomerPaymentModel> call, @NonNull Response<AddCustomerPaymentModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            hideDialog();
                            mSessionManager.setPreferences(AppConstants.TOTAL_REMAINING_GST, "");
                            mSessionManager.setPreferences(AppConstants.TOTAL_REMAINING_AMOUNT, "");
                            finish();

                        } else {
                            hideDialog();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddCustomerPaymentModel> call, @NonNull Throwable t) {
                    hideDialog();
                    Common.insertLog("Failure:::> " + t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkValidation() {
        final boolean[] status = {true};

        TotalAmount = mEditTotalAmount.getText().toString();
        GstAmount = mEditGstAmount.getText().toString();
        Mobile = mEditMobile.getText().toString();
        Description = mEditDescription.getText().toString();
        UtrNo = mEditUtrNo.getText().toString();
        IFSCcode = mEditIFSCcode.getText().toString();
        AccountNo = mEditAccountNo.getText().toString();
        Cheque = mEditCheque.getText().toString();
        DATE = mEditDate.getText().toString();
        BankName = mEditBankName.getText().toString();
        BranchName = mEditBranName.getText().toString();


        if (!TotalAmount.equalsIgnoreCase("")) {
            enteredTotal = Float.parseFloat(TotalAmount);
        }
        if (!GstAmount.equalsIgnoreCase("")) {
            enteredGst = Float.parseFloat(GstAmount);
        }

        String currentStringTotalAmount = mTextRemianingAmount.getText().toString();
        String[] separated = currentStringTotalAmount.split(": ");
        totalamount = Float.parseFloat(separated[1]);

        String currentStringTotalGSTAmount = mTextReminingGstAmount.getText().toString();
        String[] separatedGST = currentStringTotalGSTAmount.split(": ");
        totalgst = Float.parseFloat(separatedGST[1]);


        if (mRadioCash.isChecked()) {
            if (mRadioInclude.isChecked()) {

                if (TotalAmount.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (enteredTotal > totalamount) {
                    status[0] = false;
                    Toast.makeText(mActivity, "Entered Amount Cannot Be Higher then Remaining Amount", Toast.LENGTH_SHORT).show();
                }
                if (GstAmount.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (enteredGst > totalgst) {
                    status[0] = false;
                    Toast.makeText(mActivity, "Entered Amount Cannot Be Higher then Remaining Amount", Toast.LENGTH_SHORT).show();
                }

            } else if (mRadioExclude.isChecked()) {

                if (enteredTotal > totalamount) {
                    status[0] = false;
                    Toast.makeText(mActivity, "Entered Amount Cannot Be Higher then Remaining Amount", Toast.LENGTH_SHORT).show();
                }
                if (TotalAmount.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }

            } else if (mRadioOnly.isChecked()) {

                if (enteredGst > totalgst) {
                    status[0] = false;
                    Toast.makeText(mActivity, "Entered Amount Cannot Be Higher then Remaining Amount", Toast.LENGTH_SHORT).show();
                }
                if (GstAmount.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }

            }
        } else if (mRadioCheque.isChecked()) {
            if (mRadioInclude.isChecked()) {

                if (enteredTotal > totalamount) {
                    status[0] = false;
                    Toast.makeText(mActivity, "Entered Amount Cannot Be Higher then Remaining Amount", Toast.LENGTH_SHORT).show();
                }
                if (enteredGst > totalgst) {
                    status[0] = false;
                    Toast.makeText(mActivity, "Entered Amount Cannot Be Higher then Remaining Amount", Toast.LENGTH_SHORT).show();
                }
                if (TotalAmount.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (GstAmount.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (BankName.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (AccountNo.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (BranchName.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (Cheque.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }

            } else if (mRadioExclude.isChecked()) {

                if (enteredTotal > totalamount) {
                    status[0] = false;
                    Toast.makeText(mActivity, "Entered Amount Cannot Be Higher then Remaining Amount", Toast.LENGTH_SHORT).show();
                }
                if (TotalAmount.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (BankName.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (AccountNo.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (BranchName.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (Cheque.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }

            } else if (mRadioOnly.isChecked()) {

                if (enteredGst > totalgst) {
                    status[0] = false;
                    Toast.makeText(mActivity, "Entered Amount Cannot Be Higher then Remaining Amount", Toast.LENGTH_SHORT).show();
                }
                if (GstAmount.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (BankName.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (AccountNo.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (BranchName.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (Cheque.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }

            }
        } else if (mRadioOnline.isChecked()) {
            if (mRadioInclude.isChecked()) {

                if (enteredTotal > totalamount) {
                    status[0] = false;
                    Toast.makeText(mActivity, "Entered Amount Cannot Be Higher then Remaining Amount", Toast.LENGTH_SHORT).show();
                }
                if (enteredGst > totalgst) {
                    status[0] = false;
                    Toast.makeText(mActivity, "Entered Amount Cannot Be Higher then Remaining Amount", Toast.LENGTH_SHORT).show();
                }
                if (TotalAmount.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (GstAmount.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (IFSCcode.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (UtrNo.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (BankName.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (BranchName.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }

            } else if (mRadioExclude.isChecked()) {

                if (enteredTotal > totalamount) {
                    status[0] = false;
                    Toast.makeText(mActivity, "Entered Amount Cannot Be Higher then Remaining Amount", Toast.LENGTH_SHORT).show();
                }
                if (TotalAmount.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (IFSCcode.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (UtrNo.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (BankName.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (BranchName.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }

            } else if (mRadioOnly.isChecked()) {

                if (enteredGst > totalgst) {
                    status[0] = false;
                    Toast.makeText(mActivity, "Entered Amount Cannot Be Higher then Remaining Amount", Toast.LENGTH_SHORT).show();
                }
                if (GstAmount.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (IFSCcode.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (UtrNo.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (BankName.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }
                if (BranchName.isEmpty()) {
                    status[0] = false;
                    Toast.makeText(mActivity, "please enter all required fields", Toast.LENGTH_SHORT).show();
                }

            }
        }

        return status[0];
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

