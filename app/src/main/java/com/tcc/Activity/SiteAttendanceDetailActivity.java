package com.tcc.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.SiteAttendanceDetailAdapter;
import com.tcc.Model.GetTeamEmployeeModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SiteAttendanceDetailActivity extends AppCompatActivity {

    Activity mActivity;
    RelativeLayout mRelativeNoDataFound;
    SiteAttendanceDetailAdapter mAdapter;
    RecyclerView rv_attendancesitedetails;
    String SitesID, UserID, currentTime, formattedDate, QuotationID;
    SessionManager mSession;
    ArrayList<GetTeamEmployeeModel.Datum> datumArrayList = new ArrayList<>();
    private ImageView mImageBack, mImageAdd;
    private TextView mTextHeader;
    private TextView tvTodayDate;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_attendance_detail);


        mActivity = SiteAttendanceDetailActivity.this;
        mSession = new SessionManager(mActivity);
        String UserData = mSession.getPreferences(mSession.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);
        SitesID = mSession.getPreferences(AppConstants.SITE_ID, "");
        currentTime = mSession.getPreferences(AppConstants.ATTENDANCE_DATE, "");
        QuotationID = mSession.getPreferences(AppConstants.QUOTATION_ID, "");

        tvTodayDate = findViewById(R.id.tvTodayDate);
        tvTodayDate.setText("Date : " + mSession.getPreferences(AppConstants.ATTENDANCE_DATE, ""));

        getID();
        setDAta();
        getAttendanceData();

        mImageBack.setOnClickListener(v -> onBackPressed());

    }

    private void getAttendanceData() {
        try {
            String ConvertedReminderDate = Common.convertDateToServerMinus(this, currentTime);
            Common.showLoadingDialog(this, "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getTeamEmployeeJson("-1", "1", SitesID, ConvertedReminderDate, QuotationID));

            Call<GetTeamEmployeeModel> call = RetrofitClient.createService(ApiInterface.class).getTeamEmployeeAPI(body);
            call.enqueue(new Callback<GetTeamEmployeeModel>() {
                @Override
                public void onResponse(@NonNull Call<GetTeamEmployeeModel> call, @NonNull Response<GetTeamEmployeeModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            datumArrayList.addAll(response.body().getData());
                            setAdapter();
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetTeamEmployeeModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    // Common.setCustomToast(mActivity, t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setAdapter() {
        SiteAttendanceDetailAdapter sitesAdapter = new SiteAttendanceDetailAdapter(SiteAttendanceDetailActivity.this, datumArrayList);
        rv_attendancesitedetails.setHasFixedSize(true);
        rv_attendancesitedetails.setLayoutManager(new LinearLayoutManager(SiteAttendanceDetailActivity.this));
        rv_attendancesitedetails.setAdapter(sitesAdapter);
    }

    private void setDAta() {
        mTextHeader.setText("ATTENDANCE DETAILS");
        mImageBack.setVisibility(View.VISIBLE);
    }

    private void getID() {
        rv_attendancesitedetails = findViewById(R.id.rv_attendance_list);
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
    }
}
