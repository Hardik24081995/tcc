package com.tcc.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.tcc.Model.AddQuotationModel;
import com.tcc.Model.GetCompanyListModel;
import com.tcc.Model.GetServicesModel;
import com.tcc.Model.GetUserType;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuotationActivity extends AppCompatActivity {

    ArrayList<GetServicesModel.Datum> mArrayServiceList = new ArrayList<>();
    ArrayList<GetCompanyListModel.Datum> mArrayCompanyList = new ArrayList<>();
    String ServiceID = "", isGST = "Yes", CompanyID = "";
    SessionManager sessionManager;
    String CustomerID, VisitorID, UserID, SitesID, UserTypeID;
    String GSTAVAIL = "", CompanyName, Config, sgst, cgst, online;
    ArrayList<GetUserType.Datum> datumArrayList = new ArrayList<>();
    float setAmount = 0;
    private Activity mActivity;
    private LinearLayout mLinearContainer;
    private ImageView mImageItemAdd, mImageBack;
    private TextView mTextHeader;
    private EditText edtEstimateDate, edtStartDate, edtCompanyName, edtQuotationName, edtTotalAmount, edtGSTAmount, edtCGST, edtSGST, edtGSTNo, edtDisplayAddress, edtEstimateNo;
    private TextInputLayout edtGSTwrapper, edtCGSTwrapper, edtSGSTwrapper;
    private int mYear, mMonth, mDay;
    private Button button_add_visitor_submit;
    private Spinner mSpinnerServices, mSpinnerCompany;
    private int mSelectedServiceIndex = 0, mSelectedCompanyIndex = 0;
    private ArrayList<String> mArrServices = new ArrayList<>();
    private ArrayList<String> mArrCompany = new ArrayList<>();
    private RadioGroup radioGroupGST, rg;
    private RadioButton buttonWith, buttonWithout, rbOnline, rbOffline;
    private boolean isFixCost = false;
    private ArrayList<String> mArrayUser = new ArrayList<>();
    private int mSelectedUserType = 0;
    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinner_addquotation_services:
                    mSelectedServiceIndex = adapterView.getSelectedItemPosition();
                    if (mSelectedServiceIndex != 0) {
                        if (mArrayServiceList.get(mSelectedServiceIndex - 1).getIsFixCost().equals("Yes")) {
                            mImageItemAdd.setVisibility(View.GONE);
                            isFixCost = true;

                            if (mArrayServiceList.get(mSelectedServiceIndex - 1).getService().equals("Deep cleaning") || mArrayServiceList.get(mSelectedServiceIndex - 1).getService().equals("Sanetize")) {
                                //edtTotalAmount.setEnabled(true);
                                edtTotalAmount.setFocusable(true);
                                edtTotalAmount.setFocusableInTouchMode(true);
                                edtTotalAmount.setClickable(true);
                            } else {
                                edtTotalAmount.setFocusable(false);
                                edtTotalAmount.setFocusableInTouchMode(false);
                                edtTotalAmount.setClickable(false);
                            }
                            edtTotalAmount.setText(mArrayServiceList.get(mSelectedServiceIndex - 1).getRate());


                            float toat_amount = 0;
                            float CGST = 0;
                            float SGST = 0;
                            float toalGST = 0;
                            if (isGST.equalsIgnoreCase("Yes")) {
                                toat_amount = Float.parseFloat(edtTotalAmount.getText().toString().trim());
                                SGST = Float.parseFloat(sgst);
                                CGST = Float.parseFloat(cgst);
                                toalGST = toat_amount * SGST / 100;

                                float gst = toalGST * 2;

                                edtGSTAmount.setText(String.valueOf(gst));
                                edtCGST.setText(String.valueOf(toalGST));
                                edtSGST.setText(String.valueOf(toalGST));
                            }
                        } else {
                            isFixCost = false;
                            mImageItemAdd.setVisibility(View.VISIBLE);
                            edtTotalAmount.setFocusable(false);
                            edtTotalAmount.setFocusableInTouchMode(false);
                            edtTotalAmount.setClickable(false);


                            edtTotalAmount.setText(mArrayServiceList.get(mSelectedServiceIndex - 1).getRate());


                            float toat_amount = 0;
                            float CGST = 0;
                            float SGST = 0;
                            float toalGST = 0;
                            if (isGST.equalsIgnoreCase("Yes")) {
                                toat_amount = Float.parseFloat(edtTotalAmount.getText().toString().trim());
                                SGST = Float.parseFloat(sgst);
                                CGST = Float.parseFloat(cgst);
                                toalGST = toat_amount * SGST / 100;

                                float gst = toalGST * 2;

                                edtGSTAmount.setText(String.valueOf(gst));
                                edtCGST.setText(String.valueOf(toalGST));
                                edtSGST.setText(String.valueOf(toalGST));
                            }
                            ServiceID = mArrayServiceList.get(mSelectedServiceIndex - 1).getServiceID();

                        }
                    }
                    break;

                case R.id.spinner_addquotation_company:
                    mSelectedCompanyIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedCompanyIndex != 0) {
                        CompanyID = mArrayCompanyList.get(mSelectedCompanyIndex - 1).getCompanyID();
                        CompanyName = mArrayCompanyList.get(mSelectedCompanyIndex - 1).getCompanyName();

                        for (int i = 0; i < mArrayCompanyList.size(); i++) {
                            if (CompanyID.equalsIgnoreCase(mArrayCompanyList.get(i).getCompanyID())) {
                                GSTAVAIL = mArrayCompanyList.get(i).getISGST();
                            }
                        }

                        if (GSTAVAIL.equalsIgnoreCase("Yes")) {

                            buttonWithout.setVisibility(View.VISIBLE);
                            edtCGST.setVisibility(View.VISIBLE);
                            edtSGST.setVisibility(View.VISIBLE);
                            edtGSTAmount.setVisibility(View.GONE);
                            edtGSTwrapper.setVisibility(View.VISIBLE);
                            edtCGSTwrapper.setVisibility(View.VISIBLE);
                            edtSGSTwrapper.setVisibility(View.VISIBLE);

                            edtGSTAmount.setText("");
                            edtCGST.setText("");
                            edtSGST.setText("");
                            isGST = "Yes";

                        }
                        if (GSTAVAIL.equalsIgnoreCase("No")) {

                            buttonWithout.setVisibility(View.GONE);
                            edtCGST.setVisibility(View.GONE);
                            edtSGST.setVisibility(View.GONE);
                            edtGSTAmount.setVisibility(View.GONE);
                            edtGSTwrapper.setVisibility(View.GONE);
                            edtCGSTwrapper.setVisibility(View.GONE);
                            edtSGSTwrapper.setVisibility(View.GONE);

                            edtGSTAmount.setText("0");
                            edtCGST.setText("0");
                            edtSGST.setText("0");

                            isGST = "No";
                        }
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotation);

        mActivity = QuotationActivity.this;
        sessionManager = new SessionManager(mActivity);
        String UserData = sessionManager.getPreferences(sessionManager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);
        SitesID = sessionManager.getPreferences(AppConstants.SITE_ID, "");
        String add = sessionManager.getPreferences("Address", "");
        Config = sessionManager.getPreferences(sessionManager.KEY_CONFIGURATION_DATA, "");
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(Config);
            sgst = jsonObject.getString("SGST");
            cgst = jsonObject.getString("CGST");
            online = jsonObject.getString("Online");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        if (sessionManager.getPreferences(AppConstants.TYPO_FRAGS_CHANGE, "").equalsIgnoreCase("MenuSites")) {
            CustomerID = "0";
            VisitorID = "0";
        }
        if (sessionManager.getPreferences(AppConstants.TYPO_FRAGS_CHANGE, "").equalsIgnoreCase("VisitorSites")) {
            CustomerID = "0";
            VisitorID = sessionManager.getPreferences("VisitorID", "");
        }
        if (sessionManager.getPreferences(AppConstants.TYPO_FRAGS_CHANGE, "").equalsIgnoreCase("CustomerSites")) {
            CustomerID = sessionManager.getPreferences("CustomerID", "");
            VisitorID = "0";
        }

        getIds();
        setData();
        setRegister();
        getServices();
        getUser();
        getCompany();


        edtEstimateDate.setOnClickListener(v -> selectDate());
        edtStartDate.setOnClickListener(v -> selectStartDate());
        button_add_visitor_submit = findViewById(R.id.button_addquotation);

        button_add_visitor_submit.setOnClickListener(v -> {
            if (checkValidation()) {
                addQuotation();
            }
        });
    }

    private void getCompany() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getCompanyJson());

            Call<GetCompanyListModel> call = RetrofitClient.createService(ApiInterface.class).getCompanyListAPI(body);

            call.enqueue(new Callback<GetCompanyListModel>() {
                @Override
                public void onResponse(Call<GetCompanyListModel> call, Response<GetCompanyListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            mArrayCompanyList.addAll(response.body().getData());
                            setCompanySpinnerAdapter();

                        } else {
                            // Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<GetCompanyListModel> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setCompanySpinnerAdapter() {
        try {
            mArrCompany.add(0, "Select Company");
            if (mArrayCompanyList.size() > 0) {
                for (GetCompanyListModel.Datum Services : mArrayCompanyList) {
                    mArrCompany.add(Services.getCompanyName());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, mArrCompany) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCompanyIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerCompany.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getUser() {
        try {

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getUserType());

            Call<GetUserType> call = RetrofitClient.createService(ApiInterface.class).getUserType(body);

            call.enqueue(new Callback<GetUserType>() {
                @Override
                public void onResponse(Call<GetUserType> call, Response<GetUserType> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            datumArrayList.addAll(response.body().getData());

                        } else {
                            //   Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<GetUserType> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @return - JSON Array
     */
    private JSONArray getArrayDiagnosis() {
        JSONArray jsonArray = new JSONArray();
        try {
            if (mLinearContainer.getChildCount() > 0) {
                for (int a = 0; a < mLinearContainer.getChildCount(); a++) {
                    View child_view = mLinearContainer.getChildAt(a);

                    Spinner spinnerUserType = child_view.findViewById(R.id.spinner_addquotation_usertype);
                    EditText edt_desc = child_view.findViewById(R.id.edit_quotation_item_description);
                    EditText edt_HSN = child_view.findViewById(R.id.edit_quotation_item_isn_sag);
                    EditText Qty = child_view.findViewById(R.id.edit_quotation_item_qty);
                    EditText Rate = child_view.findViewById(R.id.edit_quotation_item_rate);

                    String UserTypeID = "0";

                    int index_category = spinnerUserType.getSelectedItemPosition();
                    if (index_category > 0)
                        UserTypeID = datumArrayList.get(index_category - 1).getUsertypeID();


                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("UsertypeID", UserTypeID);
                    jsonObject.put("Description", edt_desc.getText().toString().trim());
                    jsonObject.put("HSN_SAC", edt_HSN.getText().toString().trim());
                    jsonObject.put("Qty", Qty.getText().toString().trim());
                    jsonObject.put("Rate", Rate.getText().toString().trim());

                    jsonArray.put(jsonObject);
                }
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
        return jsonArray;
    }

    private void addQuotation() {
        try {
            String ConvertedEstimateDate = Common.convertDateToServer(mActivity, edtEstimateDate.getText().toString());
            String ConvertedStartDate = Common.convertDateToServer(mActivity, edtStartDate.getText().toString());
            Common.showLoadingDialog(this, "Loading");
            JSONArray jsonArray = getArrayDiagnosis();
            Float Online = 0F;

            if (rbOnline.isChecked()) {
                Float SGST = Float.parseFloat(sgst);
                Float CGST = Float.parseFloat(cgst);
                Float toalGST = Float.parseFloat(online) * SGST / 100;

                Online = toalGST * 2;
            }
           /* try {
                rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if (R.id.rbOnline==checkedId){

                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }*/


            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.addQuotationJson(UserID, CustomerID, VisitorID, ServiceID, SitesID, CompanyName,
                    edtQuotationName.getText().toString().trim(), ConvertedEstimateDate, ConvertedStartDate, "0", isGST, edtTotalAmount.getText().toString().trim(),
                    edtGSTAmount.getText().toString().trim(), edtCGST.getText().toString().trim(), edtSGST.getText().toString().trim(), edtGSTNo.getText().toString().trim(), edtDisplayAddress.getText().toString().trim(),
                    Online.toString(), jsonArray));

            Call<AddQuotationModel> call = RetrofitClient.createService(ApiInterface.class).addQuotationAPI(body);
            call.enqueue(new Callback<AddQuotationModel>() {
                @Override
                public void onResponse(@NonNull Call<AddQuotationModel> call, @NonNull Response<AddQuotationModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            finish();
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddQuotationModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //   Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkValidation() {
        boolean status = true;

      /*  if (edtEstimateNo.getText().toString().trim().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Enter Estimate No.", Toast.LENGTH_SHORT).show();
        }*/
        if (CompanyID.equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Enter Company ID", Toast.LENGTH_SHORT).show();
        }
        if (edtQuotationName.getText().toString().trim().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Enter Quotation Name", Toast.LENGTH_SHORT).show();
        }
        if (edtDisplayAddress.getText().toString().trim().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Enter Display Address", Toast.LENGTH_SHORT).show();
        }
      /*  try {
            if (!GSTINValidator.validGSTIN(edtGSTNo.getText().toString().trim())  && !edtGSTNo.getText().toString().isEmpty()){
                status = false;
                Toast.makeText(mActivity, "Please Enter Valid GST No.", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        try {
            if (edtGSTNo.getText().toString().length() != 15) {
                status = false;
                Toast.makeText(mActivity, "Please Enter Valid GST No.", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (edtTotalAmount.getText().toString().trim().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Enter Total Amount", Toast.LENGTH_SHORT).show();
        }
        if (edtGSTAmount.getText().toString().trim().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Enter GST Amount", Toast.LENGTH_SHORT).show();
        }
        if (edtCGST.getText().toString().trim().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Enter CGST Amount", Toast.LENGTH_SHORT).show();
        }
        if (edtSGST.getText().toString().trim().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please SGST Amount", Toast.LENGTH_SHORT).show();
        }
        if (ServiceID.equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Enter Service ID", Toast.LENGTH_SHORT).show();
        }

        if (isFixCost) {
            status = true;
        } else {
            if (mLinearContainer.getChildCount() > 0) {

                for (int i = 0; i < mLinearContainer.getChildCount(); i++) {
                    View view = mLinearContainer.getChildAt(i);

                    Spinner spinner_category = view.findViewById(R.id.spinner_addquotation_usertype);
                    EditText mEditDescription = view.findViewById(R.id.edit_quotation_item_description);
                    EditText mEditHSN = view.findViewById(R.id.edit_quotation_item_isn_sag);
                    EditText mEditQty = view.findViewById(R.id.edit_quotation_item_qty);
                    EditText mEditRate = view.findViewById(R.id.edit_quotation_item_rate);

                    int index_category = spinner_category.getSelectedItemPosition();

                    if (index_category == 0) {
                        Toast.makeText(mActivity, "Please Select User Type", Toast.LENGTH_SHORT).show();
                        status = false;
                    }
                    if (mEditHSN.getText().toString().trim().equalsIgnoreCase("")) {
                        status = false;
                        Toast.makeText(mActivity, "Please Enter HSN/SAC ", Toast.LENGTH_SHORT).show();
                    }
//                if (mEditDescription.getText().toString().trim().equalsIgnoreCase("")) {
//                    status = false;
//                    Toast.makeText(mActivity, "Please Enter All Fields", Toast.LENGTH_SHORT).show();
//                }
                    if (mEditQty.getText().toString().trim().equalsIgnoreCase("")) {
                        status = false;
                        Toast.makeText(mActivity, "Please Enter Qty", Toast.LENGTH_SHORT).show();
                    }
                    if (mEditRate.getText().toString().trim().equalsIgnoreCase("")) {
                        status = false;
                        Toast.makeText(mActivity, "Please Enter Rate", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                status = false;
                Toast.makeText(mActivity, "Please Add Services", Toast.LENGTH_SHORT).show();
            }
        }


        return status;
    }

    private void getServices() {
        try {

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getVisitorServiceJson());
            Common.showLoadingDialog(this, "Loading");
            Call<GetServicesModel> call = RetrofitClient.createService(ApiInterface.class).getVisitorServiceAPI(body);

            call.enqueue(new Callback<GetServicesModel>() {
                @Override
                public void onResponse(Call<GetServicesModel> call, Response<GetServicesModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            mArrayServiceList.addAll(response.body().getData());
                            setServiceAdapter();

                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<GetServicesModel> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setServiceAdapter() {
        try {
            mArrServices.add(0, getString(R.string.spinner_select_services));
            if (mArrayServiceList.size() > 0) {
                for (GetServicesModel.Datum Services : mArrayServiceList) {
                    mArrServices.add(Services.getService() + " " + Services.getQty());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, mArrServices) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedServiceIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerServices.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void selectDate() {

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> edtEstimateDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
        datePickerDialog.show();

    }

    private void selectStartDate() {

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> edtStartDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
        datePickerDialog.show();

    }


    private void getIds() {
        mLinearContainer = findViewById(R.id.linear_quotation_items);
        mImageItemAdd = findViewById(R.id.image_quotation_add);

        edtEstimateDate = findViewById(R.id.edtEstimateDate);
        edtStartDate = findViewById(R.id.edit_addquoation_startdate);
        edtCompanyName = findViewById(R.id.edtCompanyName);
        edtQuotationName = findViewById(R.id.edit_addquoation_quotationname);
        edtEstimateNo = findViewById(R.id.edtEstimateNo);
        edtTotalAmount = findViewById(R.id.edit_addquoation_totalamount);
        edtGSTAmount = findViewById(R.id.edit_addquotation_gstamount);
        edtCGST = findViewById(R.id.edit_addquoation_cgst);
        edtSGST = findViewById(R.id.edit_addquoation_sgst);
        edtDisplayAddress = findViewById(R.id.edtDisplayAddress);
        edtGSTNo = findViewById(R.id.edtGSTNO);
        edtGSTwrapper = findViewById(R.id.wrapperTotalGST);
        edtCGSTwrapper = findViewById(R.id.wrapperTotalCGST);
        edtSGSTwrapper = findViewById(R.id.wrapperTotalSGST);

        rg = findViewById(R.id.rg);
        rbOnline = findViewById(R.id.rbOnline);
        rbOffline = findViewById(R.id.rbOffline);

        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);

        mSpinnerServices = findViewById(R.id.spinner_addquotation_services);
        mSpinnerServices.setOnItemSelectedListener(onItemSelectedListener);
        mSpinnerCompany = findViewById(R.id.spinner_addquotation_company);
        mSpinnerCompany.setOnItemSelectedListener(onItemSelectedListener);

        radioGroupGST = findViewById(R.id.gstRadio);

        buttonWith = findViewById(R.id.withGST);
        buttonWithout = findViewById(R.id
                .withoutGST);
        //edtTotalAmount.setEnabled(false);

        edtTotalAmount.setFocusable(false);
        edtTotalAmount.setFocusableInTouchMode(false);
        edtTotalAmount.setClickable(false);
        /*edtGSTNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if(GSTINValidator.validGSTIN(s.toString()))
                    {
                        Toast.makeText(mActivity, "GST  Valid", Toast.LENGTH_SHORT).show();

                    }else {
                        Toast.makeText(mActivity, "GST No Invalid", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });*/

    }

    private void setData() {
        mTextHeader.setText(mActivity.getResources().getString(R.string.header_add_quotation));
        mImageBack.setVisibility(View.VISIBLE);

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c);

        edtStartDate.setText(formattedDate);
        edtEstimateDate.setText(formattedDate);
    }


    /***
     *
     */
    private void setRegister() {
        mImageItemAdd.setOnClickListener(v -> {
            AddItems();
            callToAddCusomerService();
        });

        mImageBack.setOnClickListener(v -> finish());

        radioGroupGST.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton button = findViewById(checkedId);

                if (button.getId() == R.id.withGST) {
                    isGST = "Yes";
                    edtCGST.setVisibility(View.VISIBLE);
                    edtSGST.setVisibility(View.VISIBLE);
                    edtGSTAmount.setVisibility(View.VISIBLE);
                    edtGSTwrapper.setVisibility(View.VISIBLE);
                    edtCGSTwrapper.setVisibility(View.VISIBLE);
                    edtSGSTwrapper.setVisibility(View.VISIBLE);

                    edtGSTAmount.setText("");
                    edtCGST.setText("");
                    edtSGST.setText("");

                } else if (button.getId() == R.id.withoutGST) {
                    isGST = "No";
                    edtCGST.setVisibility(View.GONE);
                    edtSGST.setVisibility(View.GONE);
                    edtGSTAmount.setVisibility(View.GONE);
                    edtGSTwrapper.setVisibility(View.GONE);
                    edtCGSTwrapper.setVisibility(View.GONE);
                    edtSGSTwrapper.setVisibility(View.GONE);

                    edtGSTAmount.setText("0");
                    edtCGST.setText("0");
                    edtSGST.setText("0");
                }
            }
        });
    }

    private void callToAddCusomerService() {
        final View view = LayoutInflater.from(mActivity).inflate(R.layout.row_quotation_item, null,
                false);
        EditText mEditQty = view.findViewById(R.id.edit_quotation_item_qty);
        EditText mEditRate = view.findViewById(R.id.edit_quotation_item_rate);

    }

    /**
     * Add Items
     */
    private void AddItems() {

        final View view = LayoutInflater.from(mActivity).inflate(R.layout.row_quotation_item, null,
                false);

        Spinner mSpinnerUsertype = view.findViewById(R.id.spinner_addquotation_usertype);
        EditText mEditDescription = view.findViewById(R.id.edit_quotation_item_description);
        EditText mEditHSN = view.findViewById(R.id.edit_quotation_item_isn_sag);
        EditText mEditQty = view.findViewById(R.id.edit_quotation_item_qty);
        EditText mEditRate = view.findViewById(R.id.edit_quotation_item_rate);
        EditText mEditTotalAmount = view.findViewById(R.id.totalRadioamount);

        ImageView mImageRemove = view.findViewById(R.id.image_quotation_item_remove);

        mImageRemove.setOnClickListener(v -> mLinearContainer.removeView(view));

        setUserTypeAdapter(mActivity, datumArrayList, mSpinnerUsertype);

        mSpinnerUsertype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    SelectUserTypeID(datumArrayList.get(position - 1), mSpinnerUsertype);
                    mEditQty.setText("1");
                    mEditHSN.setText(datumArrayList.get(position - 1).getHSNNo());
                    mEditRate.setText(datumArrayList.get(position - 1).getRate());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mEditQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!mEditRate.getText().toString().equalsIgnoreCase("")) {
                    try {
                        float TotalQty;
                        float TotalRate;

                        TotalRate = Float.valueOf(mEditRate.getText().toString());
                        TotalQty = Float.valueOf(mEditQty.getText().toString());

                        float TotalAmount = TotalRate * TotalQty;

                        if (mEditQty.getText().toString().equalsIgnoreCase("")) {
                            mEditTotalAmount.setText("0");
                        }
                        mEditTotalAmount.setText(String.valueOf(TotalAmount));

                    } catch (NumberFormatException ie) {
                        ie.printStackTrace();
                    }
                }
                if (s.toString().isEmpty()) {
                    mEditTotalAmount.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mEditRate.getText().toString().equalsIgnoreCase("")) {
                    try {
                        float TotalQty;
                        float TotalRate;

                        TotalRate = Float.valueOf(mEditRate.getText().toString());
                        TotalQty = Float.valueOf(mEditQty.getText().toString());

                        float TotalAmount = TotalRate * TotalQty;

                        if (mEditQty.getText().toString().equalsIgnoreCase("")) {
                            mEditTotalAmount.setText("0");
                        }
                        mEditTotalAmount.setText(String.valueOf(TotalAmount));

                    } catch (NumberFormatException ie) {
                        ie.printStackTrace();
                    }
                }
                if (s.toString().isEmpty()) {
                    mEditTotalAmount.setText("0");
                }

            }
        });

        mEditRate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (!mEditQty.getText().toString().equalsIgnoreCase("")) {
                        float TotalQty;
                        float TotalRate;

                        TotalRate = Float.valueOf(mEditRate.getText().toString());
                        TotalQty = Float.valueOf(mEditQty.getText().toString());

                        float TotalAmount = TotalRate * TotalQty;

                        if (mEditRate.getText().toString().equalsIgnoreCase("")) {
                            mEditTotalAmount.setText("0");
                        }
                        mEditTotalAmount.setText(String.valueOf(TotalAmount));
                    }
                    if (s.toString().isEmpty()) {
                        mEditTotalAmount.setText("0");
                    }
                } catch (NumberFormatException ie) {
                    ie.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty()) {
                    mEditTotalAmount.setText("0");
                }
            }
        });

        mEditTotalAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updatUI();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mLinearContainer.addView(view);
    }

    private void updatUI() {

        float toat_amount = 0;
        float CGST = 0;
        float SGST = 0;
        float toalGST = 0;

        for (int a = 0; a < mLinearContainer.getChildCount(); a++) {
            View view = mLinearContainer.getChildAt(a);

            EditText mEditTotalAmount = view.findViewById(R.id.totalRadioamount);

            if (!mEditTotalAmount.getText().toString().equalsIgnoreCase("")) {
                float amount = Float.parseFloat(mEditTotalAmount.getText().toString().trim());
                toat_amount = toat_amount + amount;
            } else {
                toat_amount = toat_amount + 0;
            }
        }

        edtTotalAmount.setText(String.valueOf(toat_amount));

        if (isGST.equalsIgnoreCase("Yes")) {
            SGST = Float.parseFloat(sgst);
            CGST = Float.parseFloat(cgst);
            toalGST = toat_amount * SGST / 100;

            float gst = toalGST * 2;

            edtGSTAmount.setText(String.valueOf(gst));
            edtCGST.setText(String.valueOf(toalGST));
            edtSGST.setText(String.valueOf(toalGST));
        }
    }

    private void SelectUserTypeID(GetUserType.Datum datum, Spinner mSpinnerUsertype) {

        UserTypeID = String.valueOf(mSpinnerUsertype.getSelectedItemPosition());

    }

    private void setUserTypeAdapter(Activity mActivity, ArrayList<GetUserType.Datum> datumArrayList, Spinner mSpinnerUsertype) {
        try {

            if (mArrayUser.size() > 0) {
                mArrayUser.clear();
            }

            if (datumArrayList != null && datumArrayList.size() > 0) {

                mArrayUser.add("Select User-Type");

                for (int i = 0; i < datumArrayList.size(); i++) {
                    mArrayUser.add(datumArrayList.get(i).getUsertype());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>
                        (mActivity, android.R.layout.simple_selectable_list_item, mArrayUser);

                mSpinnerUsertype.setAdapter(adapter);
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }
}
