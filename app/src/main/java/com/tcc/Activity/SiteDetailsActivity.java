package com.tcc.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.tcc.R;
import com.tcc.fragment.SiteAttendanceFragment;
import com.tcc.fragment.SiteInfoFragment;
import com.tcc.fragment.SiteTeamDefinitionFragment;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

public class SiteDetailsActivity extends AppCompatActivity {

    SessionManager mSession;
    private Activity mActivity;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageView mImageBack, image_visitor_profile_pic, img_add;
    private TextView mSiteName, mCompanyName, mTextHeader;

    private int[] tabIcons = {
            R.drawable.user_white,
            R.drawable.team,
            R.drawable.ic_reminder
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_details);

        mActivity = SiteDetailsActivity.this;
        mSession = new SessionManager(mActivity);
        getData();
        mImageBack = findViewById(R.id.img_back);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.siteTab);
        image_visitor_profile_pic = findViewById(R.id.image_visitor_profile_pic);
        img_add = findViewById(R.id.img_add);
        mSiteName = findViewById(R.id.text_site_siteNAme);
        mCompanyName = findViewById(R.id.text_visitor_profile_phone);
        mTextHeader = findViewById(R.id.headerVisitor);
        mTextHeader.setText(mSession.getPreferences(AppConstants.SITE_NAME, ""));

        setRegister();

        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        image_visitor_profile_pic.setImageDrawable(Common.setLabeledImageView(mActivity, mSession.getPreferences(AppConstants.SITE_NAME, ""),
                ""));
        mCompanyName.setText(mSession.getPreferences(AppConstants.ADDRESS, ""));
        mSiteName.setText(mSession.getPreferences(AppConstants.SITE_NAME, ""));

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

    }

    private void getData() {

        mSession.getPreferences(AppConstants.SITE_NAME, "");
        mSession.getPreferences(AppConstants.EMPLOYEE_NAME, "");
        mSession.getPreferences(AppConstants.START_DATE, "");
        mSession.getPreferences(AppConstants.END_DATE, "");
        mSession.getPreferences(AppConstants.LOCATION, "");
        mSession.getPreferences(AppConstants.GST_NO, "");

    }

    private void setupTabIcons() {

        tabLayout.getTabAt(0).setIcon(R.drawable.user_white);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);

    }

    private void setupViewPager(ViewPager viewPager) {
        SiteDetailsActivity.ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new SiteInfoFragment(), "Information");
        adapter.addFragment(new SiteTeamDefinitionFragment(), "Team Definition");
        adapter.addFragment(new SiteAttendanceFragment(), "Attendance");
        viewPager.setAdapter(adapter);
    }

    private void setRegister() {

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if (viewPager.getCurrentItem() == 0) {
                    img_add.setVisibility(View.GONE);
                } else if (viewPager.getCurrentItem() == 1) {
                    img_add.setVisibility(View.VISIBLE);
                } else {
                    img_add.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        img_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callActivity();
            }
        });
    }

    private void callActivity() {
        try {
            // Is Property is canceled then don't allow to add

            if (viewPager.getCurrentItem() == 2) {
                /*if (mSharedPreferences.getAppProjectData().getPayment().getIs_insert() == 1) {

                } else {
                    Toast.makeText(PropertyDetailActivity.this, getString(R.string.txt_not_eligible), Toast.LENGTH_SHORT).show();
                }*/
                AddUniformActivity();
            }
            if (viewPager.getCurrentItem() == 1) {
                AddTeamDefinationActivity();
            }
//            else if (viewPager.getCurrentItem() == 2) {
//                /*if (mSharedPreferences.getAppProjectData().getPayment_Reminder().getIs_insert() == 1) {
//
//                } else {
//                    Toast.makeText(PropertyDetailActivity.this, getString(R.string.txt_not_eligible), Toast.LENGTH_SHORT).show();
//                }*/
//                RoomAllocationActivity();
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AddUniformActivity() {
        Intent intent = new Intent(mActivity, AddAttendanceActivity.class);
        startActivity(intent);
    }

    private void AddTeamDefinationActivity() {
        Intent intent = new Intent(mActivity, AddTeamDefinitionActivity.class);
        startActivity(intent);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
