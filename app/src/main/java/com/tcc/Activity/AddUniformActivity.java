package com.tcc.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.tcc.Model.AddUniformModel;
import com.tcc.Model.GetUniformModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tcc.utils.Common.hideDialog;

public class AddUniformActivity extends AppCompatActivity {

    SessionManager mSessionManager;
    Spinner spinnerUniform;
    ArrayList<GetUniformModel.Datum> mArrayUniformList;
    private ArrayList<String> mArrUniform;
    private Activity mActivity;
    private ImageView mImageBack, mImageAdd;
    private TextView mTextHeader;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private EditText selectDate;
    private Button button_submit;
    private String UserID, UniformID;
    private int mSelectedUniformIndex = 0;

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinnerUniform:
                    mSelectedUniformIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedUniformIndex != 0) {
                        UniformID = String.valueOf((Integer.parseInt(mArrayUniformList.get(mSelectedUniformIndex - 1).getUniformTypeID())));
                        String mCountryName = (mArrayUniformList.get(mSelectedUniformIndex - 1).getUniformtype());
                        Common.insertLog("mUniformId::> " + UniformID);
                        Common.insertLog("mUniformName::> " + mCountryName);
                    }
                    break;

            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_uniform);
        mActivity = AddUniformActivity.this;
        mArrayUniformList = new ArrayList<>();
        mArrUniform = new ArrayList<>();

        mSessionManager = new SessionManager(mActivity);
        String UserData = mSessionManager.getPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);

        getID();
        setRegister();
        getUniformSpinner();
        setData();

    }

    private void getUniformSpinner() {
        try {

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getUniformJson());
            Common.showLoadingDialog(this, "Loading");
            Call<GetUniformModel> call = RetrofitClient.createService(ApiInterface.class).getUniformAPI(body);

            call.enqueue(new Callback<GetUniformModel>() {
                @Override
                public void onResponse(Call<GetUniformModel> call, Response<GetUniformModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            mArrayUniformList.addAll(response.body().getData());
                            setUniformAdapter();

                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<GetUniformModel> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setUniformAdapter() {
        try {
            mArrUniform.add(0, "Select Uniform");
            if (mArrayUniformList.size() > 0) {
                for (GetUniformModel.Datum Services : mArrayUniformList) {
                    mArrUniform.add(Services.getUniformtype());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, mArrUniform) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedUniformIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerUniform.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getID() {
        button_submit = findViewById(R.id.submitAddUniform);
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        selectDate = findViewById(R.id.selectDate);
        spinnerUniform = findViewById(R.id.spinnerUniform);
    }

    private void setRegister() {

        spinnerUniform.setOnItemSelectedListener(onItemSelectedListener);

        button_submit.setOnClickListener(v -> {
            if (checkValidation()) {
                addUniform();
            }
        });

        selectDate.setOnClickListener(v -> {
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(AddUniformActivity.this,
                    (view, year, monthOfYear, dayOfMonth) -> selectDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            datePickerDialog.show();
        });

        mImageBack.setVisibility(View.VISIBLE);

        mImageBack.setOnClickListener(v -> onBackPressed());
    }

    private void addUniform() {
        try {
            String Date;
            Date = Common.convertDateToServer(mActivity, selectDate.getText().toString());
            String UserIDforAdd = mSessionManager.getPreferences(AppConstants.EMPLOYEE_ID_FOR_ADD, "");

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.addUniformJson(UserIDforAdd, UniformID, Date));

            Call<AddUniformModel> call = RetrofitClient.createService(ApiInterface.class).addUniformAPI(body);
            call.enqueue(new Callback<AddUniformModel>() {
                @Override
                public void onResponse(@NonNull Call<AddUniformModel> call, @NonNull Response<AddUniformModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            hideDialog();
                            finish();

                        } else {
                            hideDialog();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddUniformModel> call, @NonNull Throwable t) {
                    hideDialog();
                    Common.insertLog("Failure:::> " + t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkValidation() {
        boolean status = true;
        if (UniformID == null) {
            status = false;
            Toast.makeText(mActivity, "Please Select Uniform", Toast.LENGTH_SHORT).show();
        }
        return status;
    }

    private void setData() {

        mTextHeader.setText("UNIFORM");

        Date c = Calendar.getInstance().getTime();
        String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c);

        selectDate.setText(formattedDate);

    }

}
