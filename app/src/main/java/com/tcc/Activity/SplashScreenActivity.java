package com.tcc.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.tcc.R;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.WebFields;

public class SplashScreenActivity extends AppCompatActivity {

    SessionManager mSessionManager;
    String UserID, UserData;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        mSessionManager = new SessionManager(SplashScreenActivity.this);
        UserData = mSessionManager.getPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(SplashScreenActivity.this, WebFields.LOGIN.RESPONSE_USERID);

        mDeclaration();
    }

    private void mDeclaration() {
        handler = new Handler();
        handler.postDelayed(() -> {
            if (UserData.equalsIgnoreCase("")) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            } else {
                if (mSessionManager.getPreferences("IsFirst", true)) {
                    Intent intent = new Intent(getApplicationContext(), WelComeActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 3000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeMessages(0);

    }
}
