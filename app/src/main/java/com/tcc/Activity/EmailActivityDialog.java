package com.tcc.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;

import com.google.gson.Gson;
import com.tcc.Model.SENDEmailModel;
import com.tcc.R;
import com.tcc.utils.AppPermissions;
import com.tcc.utils.AppUtil;
import com.tcc.utils.CameraGalleryPermission;
import com.tcc.utils.Common;
import com.tcc.utils.FilePath;
import com.tcc.utils.ImageUtils.Compressor;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmailActivityDialog extends Activity {


    private final int REQUEST_CODE_DOC = 101;
    private final int REQUEST_CAMERA = 0;
    private final int REQUEST_GELLERY = 1;
    File destination;
    private TextView txtEmail_Dialog, txtFileName_EmailDialog;
    private EditText edtSubject_EmailDialog, edtMessage_EmailDialog;
    private Button btnSendEmail;
    private ImageView imgDocument_EmailDialog;
    private String strSubject, strMessage;
    private String id;
    private String actionUser, strEmailID;
    private ProgressDialog mProgressDialog;
    private String choosenTask = "";
    private RelativeLayout relative_dialog_email_reminder_main;
    private Bitmap mBitmapDocument;
    private File imageData;
    private String fileName, mimeType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_email_reminder);
        this.setFinishOnTouchOutside(true);


        getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        // getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        mDeclaration();
    }

    /***
     * Declare member variable
     */
    private void mDeclaration() {


        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        mProgressDialog.setIndeterminate(true);


        txtEmail_Dialog = findViewById(R.id.text_dialog_email_id);
        txtFileName_EmailDialog = findViewById(R.id.text_dialog_email_file_name);
        edtSubject_EmailDialog = findViewById(R.id.edit_dialog_email_subject);
        edtMessage_EmailDialog = findViewById(R.id.edit_dialog_email_description);
        btnSendEmail = findViewById(R.id.button_dialog_email_send);
        imgDocument_EmailDialog = findViewById(R.id.image_dialog_email_document);
        relative_dialog_email_reminder_main = findViewById(R.id.relative_dialog_email_reminder_main);

        if (getIntent() != null) {
            id = getIntent().getStringExtra("ID");
            strEmailID = getIntent().getStringExtra("Email");
            actionUser = getIntent().getStringExtra("Actionuser");
        }
        txtEmail_Dialog.setText("" + strEmailID);

        btnSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidationForEmail() && !TextUtils.isEmpty(strEmailID)) {
                    sendEmailApicall(strSubject, strMessage);
                } else {
                    if (TextUtils.isEmpty(strEmailID)) {
                        Toast.makeText(EmailActivityDialog.this,
                                "" + getString(R.string.error_email_not_found), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        imgDocument_EmailDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDocument();
            }
        });

        relative_dialog_email_reminder_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /***
     *  Send SMS or EMail to visitor for particular reminder
     * @param strSubject
     * @param strMessage
     */
    private void sendEmailApicall(String strSubject, String strMessage) {

        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.sendEMAILJson(strEmailID, strSubject, strMessage));

            Call<SENDEmailModel> call = RetrofitClient.createService(ApiInterface.class).sendEmailAPI(body);

            call.enqueue(new Callback<SENDEmailModel>() {
                @Override
                public void onResponse(Call<SENDEmailModel> call, Response<SENDEmailModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            finish();
                        } else {
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<SENDEmailModel> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /***
     * Add action in reminder in visitor
     * @param mJsonObj
     * @param MethodName
     */
    public void parseResponseForAddReminderAction(JSONObject mJsonObj, String MethodName) {
        try {
            hideDialog();
            finish();
            Toast.makeText(this, "" + mJsonObj.getString("Message"), Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkValidationForEmail() {

        boolean status = true;

        strSubject = edtSubject_EmailDialog.getText().toString().trim();
        strMessage = edtMessage_EmailDialog.getText().toString().trim();

        edtSubject_EmailDialog.setError(null);
        edtMessage_EmailDialog.setError(null);


        if (TextUtils.isEmpty(strSubject)) {
            edtSubject_EmailDialog.setError(getResources().getString(R.string.error_field_required));
            status = false;
        }
        if (TextUtils.isEmpty(strMessage)) {
            edtMessage_EmailDialog.setError(getResources().getString(R.string.error_field_required));
            status = false;
        }

        return status;
    }


    public void showDialog(boolean isCancelable) {
        mProgressDialog.setMessage("Please wait.");
        mProgressDialog.setCancelable(isCancelable);
        mProgressDialog.setCanceledOnTouchOutside(isCancelable);
        mProgressDialog.show();
    }

    public void hideDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }


    /***
     * Upload File
     * @param file
     * @param FileName
     * @param MimeType
     */
    /*private void uploadDocument(File file, String FileName, String MimeType) {
        if (file != null) {
            if (!mAppUtils.getConnectionState()) {
                mAppUtils.displayNoInternetSnackBar(findViewById(android.R.id.content));
            } else {
                showDailog(false);
                mApiCall.uploadDocument(mSharedPref.getLoginUser().getUserID(), file, "PropertyDocument",
                        "File", FileName, MimeType,AppConstansts.UserType,"",CustomerPropertyID,
                        new OnTaskCompleted(this, this),
                        mApiCall.UploadDocument);
            }
        } else
            AppUtil.displaySnackBarWithMessage(findViewById(android.R.id.content),
                    getString(R.string.Error_Msg_Try_Later));
    }*/


    /***
     * Select Document
     */
    private void selectDocument() {
        final CharSequence[] menus = {"Capture Image", "Select From File", "Cancel"};

        final AlertDialog.Builder builder = new AlertDialog.Builder(EmailActivityDialog.this);

        builder.setTitle("Select Options");
        builder.setItems(menus, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int pos) {

                boolean result = CameraGalleryPermission.checkPermission(EmailActivityDialog.this,
                        AppPermissions.ReadWriteExternalStorageRequiredPermission());

                if (menus[pos].equals("Capture Image")) {
                    choosenTask = "Capture Image";
                    if (result)
                        cameraIntent();
                } else if (menus[pos].equals("Select From File")) {
                    choosenTask = "Select From File";
                    if (result)
                        getFile();
                } else if (menus[pos].equals("Cancel")) {
                    choosenTask = "";
                    dialog.dismiss();
                }
            }
        });

        builder.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[]
            grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CameraGalleryPermission.INTERNAL_EXTERNAL_PERMISSION:
                Map<String, Integer> perms = new HashMap<String, Integer>();
                perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                if (perms.get(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    if (choosenTask.equals("Capture Image"))
                        cameraIntent();
                    else if (choosenTask.equals("Select From File"))
                        getFile();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    /***
     * Get file for upload the CV, user can upload PDF,Doc and Docx file only
     */
    private void getFile() {
        String[] mimeTypes =
                {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", //
                        // .doc & .docx
                        "application/pdf",
                        "image/*"};

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";
            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }
            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }
        startActivityForResult(Intent.createChooser(intent, "ChooseFile"), REQUEST_CODE_DOC);
    }

    /***
     *Open Camera and Upload
     */
    private void cameraIntent() {
        destination = AppUtil.currentTimeStampFile();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
        } else {
            File file = new File(Uri.fromFile(destination).getPath());
            Uri photoUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (intent.resolveActivity(getApplicationContext().getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    private void onCaptureImageResult(Intent data) {


        Bitmap bit = null;
        File file = null;
        Compressor compressor = new Compressor(this);
        try {
            bit = compressor.compressToBitmap(destination);
            file = compressor.compressToFile(destination);
            if (bit != null) {

                imageData = file;
                fileName = "Photo.jpg";
                mimeType = "image/jpg";
                txtFileName_EmailDialog.setText(fileName);
                mBitmapDocument = bit;

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            switch (requestCode) {
                case REQUEST_CODE_DOC:
                    try {
                        Uri filePath = data.getData();
                        String MimeType = FilePath.getMimeType(this, filePath);
                        String File = FilePath.getPath(this, filePath);
                        String FileName = File.substring(File.lastIndexOf("/") + 1);
                        String[] MimeTypeArray = FileName.split("\\.");
                        if (MimeTypeArray.length > 1) {

                            if (MimeType != null && (MimeType.contains("pdf") || MimeType.contains("doc") || MimeType.contains("docx") ||
                                    MimeType.contains("image"))) {
                                if (!MimeType.contains("image")) {
                                    String FilePath = data.getData().getPath();
                                    java.io.File selectedFile = new File(File);
                                    if (selectedFile.length() < 2000000) {
                                        fileName = FileName;
                                        txtFileName_EmailDialog.setText("" + fileName);
                                        mimeType = MimeTypeArray[1];
//                                        uploadDocument(selectedFile, FileName, MimeTypeArray[1]);
                                        imageData = selectedFile;
                                    } else {
                                        AppUtil.displaySnackBarWithMessage(findViewById(android.R.id.content),
                                                "Please select file of max size 2 MB");
                                    }
                                } else {
                                    // Image
                                    // imageData= new File(File);
                                    Compressor compressor = new Compressor(this);
                                    imageData = compressor.compressToFile(new File(File));

                                    fileName = FileName;
                                    mimeType = MimeTypeArray[1];
                                    txtFileName_EmailDialog.setText("" + fileName);

//                                    uploadDocument(selectedFile, FileName, MimeTypeArray[1]);
                                }

                            } else {
                                AppUtil.displaySnackBarWithMessage(findViewById(android.R.id.content),
                                        "Please select file format DOC/DOCX/PDF");
                            }
                        } else {
                            AppUtil.displaySnackBarWithMessage(findViewById(android.R.id.content),
                                    "Please check the file format");
                        }
                    } catch (Exception e) {
                        AppUtil.displaySnackBarWithMessage(findViewById(android.R.id.content),
                                "Not able to fetch file, please try later");
                    }
                    break;
                case REQUEST_CAMERA:
                    onCaptureImageResult(data);
                    break;
            }
        }
    }


}
