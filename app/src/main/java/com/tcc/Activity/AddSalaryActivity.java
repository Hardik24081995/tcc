package com.tcc.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.tcc.Model.AddRoomModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tcc.utils.Common.hideDialog;

public class AddSalaryActivity extends AppCompatActivity {

    SessionManager mSessionManager;
    private Activity mActivity;
    private ImageView mImageBack, mImageAdd;
    private TextView mTextHeader;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private EditText startDate, endDate, edtAmount, edtDesc;
    private Button btn_submitRoom;
    private String UserID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_salary);

        mActivity = AddSalaryActivity.this;

        mSessionManager = new SessionManager(mActivity);
        String UserData = mSessionManager.getPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);

        getID();
        setData();
        setRegister();
    }

    private void setRegister() {

        btn_submitRoom.setOnClickListener(v -> {
            if (checkValidation()) {
                doAddSalary();
            }
        });

        startDate.setOnClickListener(v -> {
            // Get Current Date
            final Calendar c1 = Calendar.getInstance();
            mYear = c1.get(Calendar.YEAR);
            mMonth = c1.get(Calendar.MONTH);
            mDay = c1.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(AddSalaryActivity.this,
                    (view, year, monthOfYear, dayOfMonth) -> startDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        });

        endDate.setOnClickListener(v -> {
            // Get Current Date
            final Calendar c12 = Calendar.getInstance();
            mYear = c12.get(Calendar.YEAR);
            mMonth = c12.get(Calendar.MONTH);
            mDay = c12.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(AddSalaryActivity.this,
                    (view, year, monthOfYear, dayOfMonth) -> endDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
            datePickerDialog.show();
        });

        mImageBack.setOnClickListener(v -> onBackPressed());

    }

    private boolean checkValidation() {
        boolean status = true;
       /* if (endDate.getText().toString().isEmpty()) {
            status = false;
            endDate.setError("");
        }*/
        if (startDate.getText().toString().isEmpty()) {
            status = false;
            startDate.setError("");
        }
        if (edtAmount.getText().toString().isEmpty()) {
            status = false;
            edtAmount.setError("");
        }
        if (edtDesc.getText().toString().isEmpty()) {
            status = false;
            edtDesc.setError("");
        }
        return status;
    }

    private void setData() {
        mTextHeader.setText("ADD PAYMENT");
        mImageBack.setVisibility(View.VISIBLE);

        Date c = Calendar.getInstance().getTime();
        String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c);

        startDate.setText(formattedDate);

    }

    private void getID() {


        mActivity = AddSalaryActivity.this;
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);

        startDate = findViewById(R.id.selectDate);
        endDate = findViewById(R.id.endDate);
        edtDesc = findViewById(R.id.edtDesc);
        edtAmount = findViewById(R.id.edtAmount);
        btn_submitRoom = findViewById(R.id.submit_room);
    }

    double roundTwoDecimals(double d) {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        return Double.valueOf(twoDForm.format(d));
    }

    private void doAddSalary() {
        try {

            //   String EndDate = Common.convertDateToServer(this, endDate.getText().toString());

            Double fAmount = roundTwoDecimals(Double.parseDouble(edtAmount.getText().toString()));

            // String Amount=  String.format("%.2f",  Double.parseDouble(edtAmount.getText().toString()));
            Log.d("amount", fAmount.toString());


            String StartDate = Common.convertDateToServer(this, startDate.getText().toString());
            Common.showLoadingDialog(this, "Loading");
            String UserIDforAdd = mSessionManager.getPreferences(AppConstants.EMPLOYEE_ID_FOR_ADD, "");


            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.addSalary(StartDate, fAmount.toString(), edtDesc.getText().toString(), UserIDforAdd));

            Call<AddRoomModel> call = RetrofitClient.createService(ApiInterface.class).addSalaryAPI(body);
            call.enqueue(new Callback<AddRoomModel>() {
                @Override
                public void onResponse(@NonNull Call<AddRoomModel> call, @NonNull Response<AddRoomModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            hideDialog();
                            finish();

                        } else {
                            hideDialog();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddRoomModel> call, @NonNull Throwable t) {
                    hideDialog();
                    Common.insertLog("Failure:::> " + t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
