package com.tcc.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.QuoationListAdapter;
import com.tcc.Model.ConvertSiteModel;
import com.tcc.Model.GetQuotationListModel;
import com.tcc.Model.GetUserListModel;
import com.tcc.Model.PenatlyReasonModel;
import com.tcc.Model.QuotationStatusModel;
import com.tcc.Model.VisitSiteModel;
import com.tcc.Model.VisitorToCustomerModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuotationListActivity extends AppCompatActivity {

    Activity mActivity;
    SessionManager mSessionManager;
    String UserID, Name, Address, Email, Mobile, CityID, StateID, SiteID, CustomerID, VisitorID;
    RecyclerView mRecyclerView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    RelativeLayout mRelativeNoDataFound;
    ArrayList<GetQuotationListModel.Datum> myArrayList = new ArrayList<>();
    ArrayList<PenatlyReasonModel.Datum> myArrayReason = new ArrayList<>();
    ArrayList<String> Reson = new ArrayList<>();
    String ResonID = "0", Status, TeamCount = "0";
    Dialog dialog;
    QuoationListAdapter mCustomerListAdapter;
    ArrayList<GetUserListModel.Datum> OperationManagerArray;
    ArrayList<GetUserListModel.Datum> FieldOperatorArray;
    ArrayList<GetUserListModel.Datum> QualityManagerArray;
    ArrayList<String> OperationManagerArr;
    ArrayList<String> FieldOperatorArr;
    ArrayList<String> QualityManagerArr;
    private TextView mTextHeader;
    private ImageView mImageBack, addTicket;
    private int mYear, mMonth, mDay;
    private boolean isBackFromB;
    private int selectedFieldOeratorIndex = 0, selectedOperationMangerIndex = 0, selectedQualityManagerIndex = 0;
    private String FieldOeratorID = "0", OperationMangerID = "0", QualityManagerID = "0";

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinnerOperationManager:
                    selectedOperationMangerIndex = adapterView.getSelectedItemPosition();

                    if (selectedOperationMangerIndex != 0) {
                        OperationMangerID = OperationManagerArray.get(selectedOperationMangerIndex - 1).getUserID();
                    }
                    break;

                case R.id.spinnerFieldOperator:
                    selectedFieldOeratorIndex = adapterView.getSelectedItemPosition();

                    if (selectedFieldOeratorIndex != 0) {
                        FieldOeratorID = FieldOperatorArray.get(selectedFieldOeratorIndex - 1).getUserID();
                    }
                    break;

                case R.id.spinnerQualityManager:
                    selectedQualityManagerIndex = adapterView.getSelectedItemPosition();

                    if (selectedQualityManagerIndex != 0) {
                        QualityManagerID = QualityManagerArray.get(selectedQualityManagerIndex - 1).getUserID();
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotation_list);

        mActivity = QuotationListActivity.this;
        OperationManagerArray = new ArrayList<>();
        FieldOperatorArray = new ArrayList<>();
        QualityManagerArray = new ArrayList<>();

        isBackFromB = false;

        mSessionManager = new SessionManager(mActivity);
        String UserData = mSessionManager.getPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);
        Name = mSessionManager.getPreferences("Name", "");
        Mobile = mSessionManager.getPreferences("Mobile", "");
        Address = mSessionManager.getPreferences("Address", "");
        Email = mSessionManager.getPreferences("Email", "");
        CityID = mSessionManager.getPreferences("CityID", "");
        StateID = mSessionManager.getPreferences("StateID", "");
        SiteID = mSessionManager.getPreferences(AppConstants.SITE_ID, "");
        CustomerID = mSessionManager.getPreferences("CustomerID", "");

        getID();
        setRegister();
        getQuotation();
        getReason();
        getOperationManager();
        getFieldOperator();
        getQualityManager();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            myArrayList.clear();
            getQuotation();
            mSwipeRefreshLayout.setRefreshing(false);
        });

    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        if (isBackFromB) {
            //Refresh your stuff here
            getQuotation();
        }
    }

    private void getOperationManager() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getUserTypeJson("OperationManager"));
            Call<GetUserListModel> call = RetrofitClient.createService(ApiInterface.class).getUserTypeAPI(body);
            call.enqueue(new Callback<GetUserListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetUserListModel> call, @NonNull Response<GetUserListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            OperationManagerArray.addAll(response.body().getData());
                            if (OperationManagerArray.isEmpty()) {
                            } else {
                            }

                        } else {
                            // Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetUserListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //   Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setOperationAdapter(Spinner spinnerOperationManager) {
        try {
            OperationManagerArr.add(0, "Select OperationManager");
            if (OperationManagerArray.size() > 0) {
                for (GetUserListModel.Datum Services : OperationManagerArray) {
                    OperationManagerArr.add(Services.getFirstName() + " " + Services.getLastName());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, OperationManagerArr) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == selectedOperationMangerIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerOperationManager.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getFieldOperator() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getUserTypeJson("FieldOperator"));
            Call<GetUserListModel> call = RetrofitClient.createService(ApiInterface.class).getUserTypeAPI(body);
            call.enqueue(new Callback<GetUserListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetUserListModel> call, @NonNull Response<GetUserListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            FieldOperatorArray.addAll(response.body().getData());
                            if (FieldOperatorArray.isEmpty()) {
                            } else {
                            }

                        } else {
                            //  Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetUserListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //   Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setFieldOperatorAdapter(Spinner spinnerFieldOperator) {
        try {
            FieldOperatorArr.add(0, "Select FieldOperator");
            if (FieldOperatorArray.size() > 0) {
                for (GetUserListModel.Datum Services : FieldOperatorArray) {
                    FieldOperatorArr.add(Services.getFirstName() + " " + Services.getLastName());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, FieldOperatorArr) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == selectedFieldOeratorIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerFieldOperator.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getQualityManager() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getUserTypeJson("QualityManager"));
            Call<GetUserListModel> call = RetrofitClient.createService(ApiInterface.class).getUserTypeAPI(body);
            call.enqueue(new Callback<GetUserListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetUserListModel> call, @NonNull Response<GetUserListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            QualityManagerArray.addAll(response.body().getData());
                            if (QualityManagerArray.isEmpty()) {
                            } else {
                            }

                        } else {
                            //  Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetUserListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setQualityManagerAdapter(Spinner spinnerQualityManager) {
        try {
            QualityManagerArr.add(0, "Select QualityManager");
            if (QualityManagerArray.size() > 0) {
                for (GetUserListModel.Datum Services : QualityManagerArray) {
                    QualityManagerArr.add(Services.getFirstName() + " " + Services.getLastName());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, QualityManagerArr) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == selectedQualityManagerIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerQualityManager.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getReason() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getPenaltyReasonJson());
            Call<PenatlyReasonModel> call = RetrofitClient.createService(ApiInterface.class).getPenaltyReasonAPI(body);
            call.enqueue(new Callback<PenatlyReasonModel>() {
                @Override
                public void onResponse(@NonNull Call<PenatlyReasonModel> call, @NonNull Response<PenatlyReasonModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayReason.addAll(response.body().getData());
                        } else {

                            //  Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PenatlyReasonModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getQuotation() {
        myArrayList.clear();
        try {
            Common.showLoadingDialog(this, "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getQuotationJson("-1", "1", SiteID, "-1", "-1"));
            Call<GetQuotationListModel> call = RetrofitClient.createService(ApiInterface.class).getQuotationAPI(body);
            call.enqueue(new Callback<GetQuotationListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetQuotationListModel> call, @NonNull Response<GetQuotationListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayList.addAll(response.body().getData());
                            if (myArrayList.isEmpty()) {
                                mRelativeNoDataFound.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                mRelativeNoDataFound.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                                setAdapter();
                            }
                        } else {
                            mRelativeNoDataFound.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                            //    Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetQuotationListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //   Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mCustomerListAdapter = new QuoationListAdapter(mActivity, myArrayList) {
            @Override
            protected void onRejectClick(GetQuotationListModel.Datum datum) {
                super.onRejectClick(datum);
                aleterforReject(datum);
            }

            @Override
            protected void onAcceptClick(GetQuotationListModel.Datum datum, String typeO) {
                super.onAcceptClick(datum, typeO);
                alterforAccept(datum, typeO);
            }

            @Override
            protected void onConvertClicked(GetQuotationListModel.Datum datum, String typeO) {
                super.onConvertClicked(datum, typeO);
                onConvertClick(datum, typeO);
            }

            @Override
            protected void changeStatus(String quotationID, String status) {
                super.changeStatus(quotationID, status);
                statusAPI(quotationID, status);
            }
        };
        mRecyclerView.setAdapter(mCustomerListAdapter);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void statusAPI(String quotationID, String status) {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.setVisitJson(UserID, quotationID, status));
            Call<VisitSiteModel> call = RetrofitClient.createService(ApiInterface.class).visitSiteAPI(body);
            call.enqueue(new Callback<VisitSiteModel>() {
                @Override
                public void onResponse(@NonNull Call<VisitSiteModel> call, @NonNull Response<VisitSiteModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            getQuotation();
                            mCustomerListAdapter.notifyDataSetChanged();
                        } else {
                            //   Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<VisitSiteModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    // Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onConvertClick(GetQuotationListModel.Datum datum, String typeO) {

        try {
            dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.alertaccept);
            Status = "Accept";
            OperationManagerArr = new ArrayList<>();
            FieldOperatorArr = new ArrayList<>();
            QualityManagerArr = new ArrayList<>();

            EditText mEditStartDate = dialog.findViewById(R.id.edtStartDateAccept);
            EditText mEditEndDate = dialog.findViewById(R.id.edtEndDateAccept);
            EditText mEditTeamCount = dialog.findViewById(R.id.edtTeamCount);
            TextView mTextOK = dialog.findViewById(R.id.accept);
            TextView mTextCancel = dialog.findViewById(R.id.dialogdismiss);

            Spinner spinnerFieldOperator = dialog.findViewById(R.id.spinnerFieldOperator);
            spinnerFieldOperator.setOnItemSelectedListener(onItemSelectedListener);
            Spinner spinnerOperationManager = dialog.findViewById(R.id.spinnerOperationManager);
            spinnerOperationManager.setOnItemSelectedListener(onItemSelectedListener);
            Spinner spinnerQualityManager = dialog.findViewById(R.id.spinnerQualityManager);
            spinnerQualityManager.setOnItemSelectedListener(onItemSelectedListener);

            TeamCount = mEditTeamCount.getText().toString().trim();

            setOperationAdapter(spinnerOperationManager);
            setFieldOperatorAdapter(spinnerFieldOperator);
            setQualityManagerAdapter(spinnerQualityManager);

            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);

            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            String formattedDate = df.format(c);

            mEditStartDate.setText(formattedDate);
            mEditEndDate.setText(formattedDate);

            mEditStartDate.setOnClickListener(v -> {
                // Get Current Date
                final Calendar c1 = Calendar.getInstance();
                mYear = c1.get(Calendar.YEAR);
                mMonth = c1.get(Calendar.MONTH);
                mDay = c1.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                        (view, year, monthOfYear, dayOfMonth) -> mEditStartDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
                datePickerDialog.show();
            });

//            String dtStart = mEditStartDate.getText().toString();
//            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
//            Date date = null;
//            try {
//                date = format.parse(dtStart);
//                System.out.println(date);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//
//            Date finalDate = date;
            mEditEndDate.setOnClickListener(v -> {
                // Get Current Date
                final Calendar c1 = Calendar.getInstance();
                mYear = c1.get(Calendar.YEAR);
                mMonth = c1.get(Calendar.MONTH);
                mDay = c1.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                        (view, year, monthOfYear, dayOfMonth) -> mEditEndDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            });

            mTextCancel.setOnClickListener(v -> dialog.dismiss());

            mTextOK.setOnClickListener(v -> {
                if (checkValidation()) {
                    onConvertToVisitor(datum, mEditStartDate.getText().toString().trim(), mEditEndDate.getText().toString().trim(), typeO, mEditTeamCount.getText().toString().trim());
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void onConvertToVisitor(GetQuotationListModel.Datum datum, String startDate, String endDate, String typeO, String tc) {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.convertToCustomerJson(UserID, datum.getVisitorID(), Name, Email, Mobile,
                    Address, CityID, StateID));

            Call<VisitorToCustomerModel> call = RetrofitClient.createService(ApiInterface.class).convertToCustomerAPI(body);
            call.enqueue(new Callback<VisitorToCustomerModel>() {
                @Override
                public void onResponse(@NonNull Call<VisitorToCustomerModel> call, @NonNull Response<VisitorToCustomerModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            String CustomerID = response.body().getData().get(0).getID();
                            convertToSite(CustomerID, datum, startDate, endDate, typeO, tc);
                        } else {
                            //   Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<VisitorToCustomerModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void convertToSite(String customerID, GetQuotationListModel.Datum datum, String startDate, String endDate, String typeO, String tc) {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.convertSiteJson(UserID, customerID, SiteID));
            Call<ConvertSiteModel> call = RetrofitClient.createService(ApiInterface.class).convertToSiteAPI(body);
            call.enqueue(new Callback<ConvertSiteModel>() {
                @Override
                public void onResponse(@NonNull Call<ConvertSiteModel> call, @NonNull Response<ConvertSiteModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            acceptQuotation(startDate, endDate, ResonID, datum.getQuotationID(), UserID, Status, "converted", customerID, tc);
                        } else {
                            //  Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ConvertSiteModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void alterforAccept(GetQuotationListModel.Datum datum, String typeO) {
        try {
            dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.alertaccept);
            Status = "Accept";
            OperationManagerArr = new ArrayList<>();
            FieldOperatorArr = new ArrayList<>();
            QualityManagerArr = new ArrayList<>();


            EditText mEditStartDate = dialog.findViewById(R.id.edtStartDateAccept);
            EditText mEditEndDate = dialog.findViewById(R.id.edtEndDateAccept);
            EditText mEditTeamCount = dialog.findViewById(R.id.edtTeamCount);
            TextView mTextOK = dialog.findViewById(R.id.accept);
            TextView mTextCancel = dialog.findViewById(R.id.dialogdismiss);
            Spinner spinnerFieldOperator = dialog.findViewById(R.id.spinnerFieldOperator);
            spinnerFieldOperator.setOnItemSelectedListener(onItemSelectedListener);
            Spinner spinnerOperationManager = dialog.findViewById(R.id.spinnerOperationManager);
            spinnerOperationManager.setOnItemSelectedListener(onItemSelectedListener);
            Spinner spinnerQualityManager = dialog.findViewById(R.id.spinnerQualityManager);
            spinnerQualityManager.setOnItemSelectedListener(onItemSelectedListener);

            TeamCount = mEditTeamCount.getText().toString().trim();

            setOperationAdapter(spinnerOperationManager);
            setFieldOperatorAdapter(spinnerFieldOperator);
            setQualityManagerAdapter(spinnerQualityManager);

            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);

            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            String formattedDate = df.format(c);

            mEditStartDate.setText(formattedDate);
            mEditEndDate.setText(formattedDate);

            mEditStartDate.setOnClickListener(v -> {
                // Get Current Date
                final Calendar c1 = Calendar.getInstance();
                mYear = c1.get(Calendar.YEAR);
                mMonth = c1.get(Calendar.MONTH);
                mDay = c1.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                        (view, year, monthOfYear, dayOfMonth) -> mEditStartDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
                datePickerDialog.show();
            });

            mEditEndDate.setOnClickListener(v -> {
                // Get Current Date
                final Calendar c1 = Calendar.getInstance();
                mYear = c1.get(Calendar.YEAR);
                mMonth = c1.get(Calendar.MONTH);
                mDay = c1.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                        (view, year, monthOfYear, dayOfMonth) -> mEditEndDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            });

            mTextCancel.setOnClickListener(v -> dialog.dismiss());

            mTextOK.setOnClickListener(v -> {
                if (checkValidation()) {
                    acceptQuotation(mEditStartDate.getText().toString(), mEditEndDate.getText().toString(), ResonID, datum.getQuotationID(), UserID, Status, typeO, CustomerID, mEditTeamCount.getText().toString().trim());
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkValidation() {
        boolean status = true;

        if (FieldOeratorID.equalsIgnoreCase("0")) {
            status = false;
            Toast.makeText(mActivity, "Select Field Operator", Toast.LENGTH_SHORT).show();
        }

        return status;
    }

    private void acceptQuotation(String toString, String toString1, String resonID, String quotationID, String userID, String status, String type, String customerID, String tc) {
        try {

            String ConvertedStartDate = Common.convertDateToServer(mActivity, toString);
            String ConvertedEndDate = Common.convertDateToServer(mActivity, toString1);
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.addStatusJson(userID, quotationID, status, resonID, ConvertedStartDate, ConvertedEndDate,
                    customerID, FieldOeratorID, QualityManagerID, OperationMangerID, tc));

            Call<QuotationStatusModel> call = RetrofitClient.createService(ApiInterface.class).addQuotationStatusAPI(body);
            call.enqueue(new Callback<QuotationStatusModel>() {
                @Override
                public void onResponse(@NonNull Call<QuotationStatusModel> call, @NonNull Response<QuotationStatusModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        dialog.dismiss();
                        if (response.isSuccessful() && mError == 200) {

                            if (type.equalsIgnoreCase("MenuSites")) {

                                Intent intent = new Intent(mActivity, SiteDetailsActivity.class);
                                startActivity(intent);

                            } else if (type.equalsIgnoreCase("CustomerSites")) {

                                Intent intent = new Intent(mActivity, PropertyDetailActivity.class);
                                startActivity(intent);

                            } else if (type.equalsIgnoreCase("VisitorSites")) {

                                Intent intent = new Intent(mActivity, SiteDetailsActivity.class);
                                startActivity(intent);

                            } else if (type.equalsIgnoreCase("")) {
                                Toast.makeText(QuotationListActivity.this, "Quotation Rejected Successfully", Toast.LENGTH_SHORT).show();
                            } else if (type.equalsIgnoreCase("converted")) {
                                finish();
                            }

                        } else {
                            //   Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<QuotationStatusModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void aleterforReject(GetQuotationListModel.Datum datum) {
        try {
            dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.alertreject);
            Status = "Reject";
            String StartDate = "";
            String EndDate = "";

            Spinner mSpinnerReson = dialog.findViewById(R.id.spinner_alert_reason);
            TextView mTextOK = dialog.findViewById(R.id.okreject);
            TextView mTextcancel = dialog.findViewById(R.id.dialogdismiss);

            setReasonAdapter(mSpinnerReson, myArrayReason);

            mSpinnerReson.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position > 0) {

                        int index_category = mSpinnerReson.getSelectedItemPosition();
                        if (index_category > 0)
                            ResonID = myArrayReason.get(index_category - 1).getReasonID();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            mTextcancel.setOnClickListener(v -> dialog.dismiss());

            mTextOK.setOnClickListener(v -> {
                if (ResonID.equalsIgnoreCase("0")) {
                    Toast.makeText(mActivity, "Please Select Reason", Toast.LENGTH_SHORT).show();
                } else {
                    if (CustomerID.equalsIgnoreCase("")) {
                        acceptQuotation(StartDate, EndDate, ResonID, datum.getQuotationID(), UserID, Status, "", "0", "0");
                    } else {
                        acceptQuotation(StartDate, EndDate, ResonID, datum.getQuotationID(), UserID, Status, "", CustomerID, "0");
                    }
                }

            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setReasonAdapter(Spinner mSpinnerReson, ArrayList<PenatlyReasonModel.Datum> myArrayReason) {
        try {

            if (Reson.size() > 0) {
                Reson.clear();
            }

            if (myArrayReason != null && myArrayReason.size() > 0) {

                Reson.add("Select Reason");

                for (int i = 0; i < myArrayReason.size(); i++) {
                    Reson.add(myArrayReason.get(i).getReason());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>
                        (mActivity, android.R.layout.simple_selectable_list_item, Reson);

                mSpinnerReson.setAdapter(adapter);
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }

    }

    private void setRegister() {
        addTicket.setOnClickListener(v -> {
            Intent intent = new Intent(QuotationListActivity.this, QuotationActivity.class);
            startActivity(intent);
        });

        mImageBack.setOnClickListener(v -> onBackPressed());
    }

    private void getID() {
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        addTicket = findViewById(R.id.image_custom_toolbar_add);

        mTextHeader.setText("Quotations");
        mImageBack.setVisibility(View.VISIBLE);

      /*  if (mSessionManager.getPreferences(AppConstants.TYPO_FRAGS_CHANGE, "").equalsIgnoreCase("MenuSites")) {
            addTicket.setVisibility(View.GONE);
        } else {
            addTicket.setVisibility(View.VISIBLE);
        }*/
        addTicket.setVisibility(View.VISIBLE);
        mRecyclerView = findViewById(R.id.rv_quotation);
        mRelativeNoDataFound = findViewById(R.id.relative_no_data_available);
        mSwipeRefreshLayout = findViewById(R.id.swipeToRefresh);
    }
}
