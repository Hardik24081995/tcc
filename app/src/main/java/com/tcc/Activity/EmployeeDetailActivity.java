package com.tcc.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.tcc.R;
import com.tcc.fragment.AttendanceFragment;
import com.tcc.fragment.EmployeeRoomFragment;
import com.tcc.fragment.EmployeeSalaryFragment;
import com.tcc.fragment.EmployeeTrainingFragment;
import com.tcc.fragment.EmployeeUniformFragment;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

public class EmployeeDetailActivity extends AppCompatActivity {

    private Activity mActivity;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageView mImageBack, mImageAdd;
    private TextView mTextHeader;

    private int[] tabIcons = {
            R.drawable.ic_process,
            R.drawable.ic_payment_profile,
            R.drawable.ic_documents,
            R.drawable.ic_reminder,
            R.drawable.ic_payment_profile
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_detail);

        mActivity = EmployeeDetailActivity.this;

        getIds();
        setRegister();

        mImageAdd.setVisibility(View.VISIBLE);

    }

    private void getIds() {
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        mImageAdd = findViewById(R.id.image_custom_toolbar_add);

        SessionManager sessionManager = new SessionManager(mActivity);
        String header = sessionManager.getPreferences("Name", "");
        mTextHeader.setText(header);
        mImageBack.setVisibility(View.VISIBLE);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }

    private void setupTabIcons() {

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
        tabLayout.getTabAt(4).setIcon(tabIcons[4]);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new EmployeeTrainingFragment(), "Training");
        adapter.addFragment(new EmployeeUniformFragment(), "Uniform");
        adapter.addFragment(new AttendanceFragment(), "Attendance");
        adapter.addFragment(new EmployeeRoomFragment(), "Room");
        adapter.addFragment(new EmployeeSalaryFragment(), "Payment");
        viewPager.setAdapter(adapter);
    }

    private void setRegister() {
        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mImageAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callActivity();
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {

                if (viewPager.getCurrentItem() == 2) {
                    mImageAdd.setVisibility(View.GONE);
                } else {
                    mImageAdd.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    /**
     * This method should be redirected to the Activity as per the view pager page scrolling
     */
    private void callActivity() {
        try {
            // Is Property is canceled then don't allow to add
            if (viewPager.getCurrentItem() == 1) {
                AddUniformActivity();
            } else if (viewPager.getCurrentItem() == 3) {
                RoomAllocationActivity();
            } else if (viewPager.getCurrentItem() == 0) {
                AddTrainingActivity();
            } else if (viewPager.getCurrentItem() == 4) {
                AddSalary();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AddUniformActivity() {
        Intent intent = new Intent(mActivity, AddUniformActivity.class);
        startActivity(intent);
    }

    private void RoomAllocationActivity() {
        Intent intent = new Intent(mActivity, RoomAllocationActivity.class);
        startActivity(intent);
    }

    private void AddTrainingActivity() {
        Intent intent = new Intent(mActivity, AddTrainingActivity.class);
        startActivity(intent);
    }

    private void AddSalary() {
        Intent intent = new Intent(mActivity, AddSalaryActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
