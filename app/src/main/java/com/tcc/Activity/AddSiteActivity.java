package com.tcc.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.tcc.Model.AddSiteModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddSiteActivity extends AppCompatActivity {

    SessionManager manager;
    private Activity mActivity;
    private ImageView mImageBack;
    private EditText mEditSiteName, mEditCompanyName, mEditSiteAddress;
    private Button mBtnSubmit;
    private String UserID, CustomerID, VisitorID;
    private View.OnClickListener clickListener = v -> {
        switch (v.getId()) {
            case R.id.image_custom_toolbar_back_arrow:
                onBackPressed();
                break;
            case R.id.button_add_site_submit:
                if (checkValidation()) {
                    doSubmitSites();
                }
                break;
        }
    };

    private boolean checkValidation() {
        boolean status = true;

        if (mEditSiteName.getText().toString().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Enter All Field", Toast.LENGTH_SHORT).show();
        }
        if (mEditCompanyName.getText().toString().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Enter All Field", Toast.LENGTH_SHORT).show();
        }
        if (mEditSiteAddress.getText().toString().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Please Site Address", Toast.LENGTH_SHORT).show();
        }

        return status;
    }

    private void doSubmitSites() {
        try {

            if (CustomerID == null) {
                CustomerID = "0";
            }
            if (VisitorID == null) {
                VisitorID = "0";
            }
            Common.showLoadingDialog(this, "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.addSites(UserID, CustomerID, mEditSiteName.getText().toString().trim(), mEditCompanyName.getText().toString().trim(), mEditSiteAddress.getText().toString().trim(), VisitorID));

            Call<AddSiteModel> call = RetrofitClient.createService(ApiInterface.class).doAddSite(body);
            call.enqueue(new Callback<AddSiteModel>() {
                @Override
                public void onResponse(@NonNull Call<AddSiteModel> call, @NonNull Response<AddSiteModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            finish();
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddSiteModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_site);

        mActivity = AddSiteActivity.this;

        Intent intent = getIntent();
        manager = new SessionManager(mActivity);
        String UserData = manager.getPreferences(manager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);
        VisitorID = intent.getStringExtra("VisitorID");
        CustomerID = intent.getStringExtra("CustomerID");

        getIds();
        setRegister();
    }


    @SuppressLint("SetTextI18n")
    private void getIds() {
        TextView mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);

        mBtnSubmit = findViewById(R.id.button_add_site_submit);

        mTextHeader.setText("SITE");
        mImageBack.setVisibility(View.VISIBLE);

        mEditSiteName = findViewById(R.id.edit_site_sitename);
        mEditCompanyName = findViewById(R.id.edit_site_companyname);
        mEditSiteAddress = findViewById(R.id.edit_site_companyaddress);

        mEditCompanyName.setText(manager.getPreferences("CompanyName", ""));
        //  mEditSiteAddress.setText(manager.getPreferences("Address", ""));

    }

    /**
     *
     */
    private void setRegister() {

        mImageBack.setOnClickListener(clickListener);
        mBtnSubmit.setOnClickListener(clickListener);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
