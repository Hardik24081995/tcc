package com.tcc.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.tcc.R;
import com.tcc.WelcomeFragment;
import com.tcc.utils.AppConstants;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class WelComeActivity extends AppCompatActivity {

    SessionManager manager;
    private Activity mActivity;
    private ViewPager mPager;
    private Button mButtonGetStarted;
    private LinearLayout mLinearDotIndicator;
    private ArrayList<Drawable> mArrDrawable;
    private ArrayList<Fragment> mArrFragment;
    private TabLayout mTabLayout;
    private TextView mTextSkip;
    private Timer timer;
    private int DELAY_MS = 3 * 1000, PERIOD_MS = 1000, NUM_PAGES = 7;
    private int currentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_wel_come);
        mActivity = WelComeActivity.this;
        mArrDrawable = new ArrayList<>();
        mArrFragment = new ArrayList<>();

        manager = new SessionManager(mActivity);
        manager.setPreferences(AppConstants.WELCOME_FLAG, "LoggedIn");
        manager.setPreferences("IsFirst", false);

        getIds();
        setData();
        setViewPager();
        setRegister();
        setTimer();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Exit");
        builder.setMessage("Would you like to exit from the App?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    private void setData() {
        mArrDrawable.add(mActivity.getResources().getDrawable(R.drawable.welcome1));
        mArrDrawable.add(mActivity.getResources().getDrawable(R.drawable.welcome2));
        mArrDrawable.add(mActivity.getResources().getDrawable(R.drawable.welcome3));
        mArrDrawable.add(mActivity.getResources().getDrawable(R.drawable.welcome4));
        mArrDrawable.add(mActivity.getResources().getDrawable(R.drawable.welcome5));
        mArrDrawable.add(mActivity.getResources().getDrawable(R.drawable.welcome6));


        if (mArrDrawable.size() > 0) {
            for (int a = 0; a < mArrDrawable.size(); a++) {
                Bundle bundle = new Bundle();
                bundle.putInt("position", a);
                Fragment fragment = new WelcomeFragment(a);
                fragment.setArguments(bundle);

                mArrFragment.add(fragment);
            }
        }
    }

    /**
     * This is Method Declare Widgets ID
     */
    private void getIds() {
        mButtonGetStarted = findViewById(R.id.button_wel_come_get_started);
        mPager = findViewById(R.id.view_pager_wel_come);
        mLinearDotIndicator = findViewById(R.id.linear_wel_come_dot_indicator);
        mTabLayout = findViewById(R.id.tab_layout_wel_come);

        mTextSkip = findViewById(R.id.text_wel_come_skip);
    }

    /**
     * Set View Pager
     */
    private void setViewPager() {
        ViewPagerAdapter mAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mAdapter);
        mTabLayout.setupWithViewPager(mPager);
    }

    /**
     * Set Register Listener
     */
    private void setRegister() {
        mButtonGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mTextSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                Intent intent = new Intent(mActivity, HomeActivity.class);
                startActivity(intent);
                finish();

            }
        });
    }

    public void setTimer() {
        /*After setting the adapter use the timer */
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES - 1) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS);

    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int i) {
            return mArrFragment.get(i);
        }

        @Override
        public int getCount() {
            return mArrFragment.size();
        }
    }

}
