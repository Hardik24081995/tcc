package com.tcc.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.TicketListAdapter;
import com.tcc.Model.TicketListingModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TicketListingActivity extends AppCompatActivity {

    Activity mActivity;
    SessionManager mSessionManager;
    String UserID;
    RecyclerView mRecyclerView;
    ArrayList<TicketListingModel.Datum> datumArrayList = new ArrayList<>();
    SwipeRefreshLayout mSwipeRefreshLayout;
    RelativeLayout mRelative_no_data;
    TicketListAdapter mCustomerListAdapter;
    private TextView mTextHeader;
    private ImageView mImageBack, addTicket;
    private boolean isBackFromB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_listing);

        isBackFromB = false;

        mActivity = TicketListingActivity.this;

        mSessionManager = new SessionManager(mActivity);
        String UserData = mSessionManager.getPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);

        getID();
        setRegister();
        getTicketListing();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getTicketListing();
            mSwipeRefreshLayout.setRefreshing(false);
        });

    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }


    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        if (isBackFromB) {
            //Refresh your stuff here
            getTicketListing();
        }

    }

    private void setRegister() {
        addTicket.setOnClickListener(v -> {
            Intent intent = new Intent(TicketListingActivity.this, TicketActivity.class);
            startActivity(intent);
        });

        mImageBack.setOnClickListener(v -> onBackPressed());
    }

    private void getTicketListing() {
        datumArrayList.clear();
        try {
            Common.showLoadingDialog(this, "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getTicket("-1", "1", UserID));

            Call<TicketListingModel> call = RetrofitClient.createService(ApiInterface.class).getTicketAPI(body);
            call.enqueue(new Callback<TicketListingModel>() {
                @Override
                public void onResponse(@NonNull Call<TicketListingModel> call, @NonNull Response<TicketListingModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            datumArrayList.addAll(response.body().getData());
                            if (datumArrayList.isEmpty()) {
                                mRelative_no_data.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                mRelative_no_data.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                                setEmployeeAdapter();
                            }

                        } else {
                            mRelative_no_data.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                            // Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<TicketListingModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setEmployeeAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mCustomerListAdapter = new TicketListAdapter(mActivity, datumArrayList);
        mRecyclerView.setAdapter(mCustomerListAdapter);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void getID() {
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        addTicket = findViewById(R.id.image_custom_toolbar_add);

        mTextHeader.setText("Tickets");
        mImageBack.setVisibility(View.VISIBLE);
        addTicket.setVisibility(View.VISIBLE);

        mRecyclerView = findViewById(R.id.rv_ticket_listing);
        mRelative_no_data = findViewById(R.id.relative_no_data_available);
        mSwipeRefreshLayout = findViewById(R.id.swipeToRefresh);
    }
}
