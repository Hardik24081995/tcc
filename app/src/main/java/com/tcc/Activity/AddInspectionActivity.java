package com.tcc.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;

import com.google.gson.Gson;
import com.tcc.Model.AddInspectionModel;
import com.tcc.Model.GetSiteModel;
import com.tcc.Model.GetUserListModel;
import com.tcc.Model.InspectionModel;
import com.tcc.Model.UploadInspectionImageModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.AppPermissions;
import com.tcc.utils.AppUtil;
import com.tcc.utils.CameraGalleryPermission;
import com.tcc.utils.Common;
import com.tcc.utils.ImageUtils.Compressor;
import com.tcc.utils.ImageUtils.ImageUtil;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddInspectionActivity extends AppCompatActivity {

    public static final int REQUEST_CAMERA = 101, REQUEST_GALLERY = 102;
    public String mFilePath = "", mImagePath;
    Spinner mSpinnerSites, spinnerFieldOperator, spinnerOperationManager, spinnerQualityManager;
    Activity mActivity;
    ArrayList<GetSiteModel.Datum> myArrayList;
    ArrayList<GetUserListModel.Datum> OperationManagerArray;
    ArrayList<GetUserListModel.Datum> FieldOperatorArray;
    ArrayList<GetUserListModel.Datum> QualityManagerArray;
    ArrayList<String> sitesArr;
    ArrayList<String> OperationManagerArr;
    ArrayList<String> FieldOperatorArr;
    ArrayList<String> QualityManagerArr;
    ArrayList<InspectionModel.Datum> mArrayInspection;
    SessionManager mSessionManager;
    RadioGroup ll;
    ArrayList<String> Questions;
    String SelectedOptions = "";
    int i = 0;
    String[] Options;
    JSONArray jsonArray;
    private ImageView mImageBack, mImageAdd;
    private TextView mTextHeader;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private EditText edt_insdate, mEditRemarks;
    private LinearLayout mLinearLayout;
    private CardView mCardView;
    private TextView mTextQuestions;
    private Button mButtonNext, mButtonSubmit;
    private int selectedSiteIndex = 0, selectedFieldOeratorIndex = 0, selectedOperationMangerIndex = 0, selectedQualityManagerIndex = 0;
    private String siteID = "0", FieldOeratorID = "0", OperationMangerID = "0", QualityManagerID = "0";
    private String chosenTask;
    private File destination;
    private Uri mImageUri;
    private File mPath;
    private Bitmap mBitmap;
    private ImageView mImageDoc;

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinner_sites:
                    selectedSiteIndex = adapterView.getSelectedItemPosition();

                    if (selectedSiteIndex != 0) {
                        siteID = myArrayList.get(selectedSiteIndex - 1).getSitesID();
                    }
                    break;

                case R.id.spinnerOperationManager:
                    selectedOperationMangerIndex = adapterView.getSelectedItemPosition();

                    if (selectedOperationMangerIndex != 0) {
                        OperationMangerID = OperationManagerArray.get(selectedOperationMangerIndex - 1).getUserID();
                    }
                    break;

                case R.id.spinnerFieldOperator:
                    selectedFieldOeratorIndex = adapterView.getSelectedItemPosition();

                    if (selectedFieldOeratorIndex != 0) {
                        FieldOeratorID = FieldOperatorArray.get(selectedFieldOeratorIndex - 1).getUserID();
                    }
                    break;

                case R.id.spinnerQualityManager:
                    selectedQualityManagerIndex = adapterView.getSelectedItemPosition();

                    if (selectedQualityManagerIndex != 0) {
                        QualityManagerID = QualityManagerArray.get(selectedQualityManagerIndex - 1).getUserID();
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_inspection);

        mActivity = AddInspectionActivity.this;
        mSessionManager = new SessionManager(mActivity);
        mArrayInspection = new ArrayList<>();
        Questions = new ArrayList<>();
        jsonArray = new JSONArray();
        myArrayList = new ArrayList<>();
        OperationManagerArray = new ArrayList<>();
        FieldOperatorArray = new ArrayList<>();
        QualityManagerArray = new ArrayList<>();
        sitesArr = new ArrayList<>();
        OperationManagerArr = new ArrayList<>();
        FieldOperatorArr = new ArrayList<>();
        QualityManagerArr = new ArrayList<>();

        mLinearLayout = findViewById(R.id.imageView);
        mCardView = findViewById(R.id.cardView);

        initView();
    }

    private void initView() {
        getID();
        setID();
        getSite();
        getOperationManager();
        getFieldOperator();
        getQualityManager();
        getInspection();
        setRegister();

        Date c = Calendar.getInstance().getTime();
        String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c);

        edt_insdate.setText(formattedDate);

        mTextHeader.setText("INSPECTION");
        mImageBack.setVisibility(View.VISIBLE);
    }

    private void getOperationManager() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getUserTypeJson("OperationManager"));
            Call<GetUserListModel> call = RetrofitClient.createService(ApiInterface.class).getUserTypeAPI(body);
            call.enqueue(new Callback<GetUserListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetUserListModel> call, @NonNull Response<GetUserListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            OperationManagerArray.addAll(response.body().getData());
                            if (OperationManagerArray.isEmpty()) {
                            } else {
                                setOperationAdapter();
                            }

                        } else {
                            // Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetUserListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    // Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setOperationAdapter() {
        try {
            OperationManagerArr.add(0, "Select OperationManager");
            if (OperationManagerArray.size() > 0) {
                for (GetUserListModel.Datum Services : OperationManagerArray) {
                    OperationManagerArr.add(Services.getFirstName() + " " + Services.getLastName());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, OperationManagerArr) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == selectedOperationMangerIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerOperationManager.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getFieldOperator() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getUserTypeJson("FieldOperator"));
            Call<GetUserListModel> call = RetrofitClient.createService(ApiInterface.class).getUserTypeAPI(body);
            call.enqueue(new Callback<GetUserListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetUserListModel> call, @NonNull Response<GetUserListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            FieldOperatorArray.addAll(response.body().getData());
                            if (FieldOperatorArray.isEmpty()) {
                            } else {
                                setFieldOperatorAdapter();
                            }

                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetUserListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //   Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setFieldOperatorAdapter() {
        try {
            FieldOperatorArr.add(0, "Select FieldOperator");
            if (FieldOperatorArray.size() > 0) {
                for (GetUserListModel.Datum Services : FieldOperatorArray) {
                    FieldOperatorArr.add(Services.getFirstName() + " " + Services.getLastName());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, FieldOperatorArr) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == selectedFieldOeratorIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerFieldOperator.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getQualityManager() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getUserTypeJson("QualityManager"));
            Call<GetUserListModel> call = RetrofitClient.createService(ApiInterface.class).getUserTypeAPI(body);
            call.enqueue(new Callback<GetUserListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetUserListModel> call, @NonNull Response<GetUserListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            QualityManagerArray.addAll(response.body().getData());
                            if (QualityManagerArray.isEmpty()) {
                            } else {
                                setQualityManagerAdapter();
                            }

                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetUserListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setQualityManagerAdapter() {
        try {
            QualityManagerArr.add(0, "Select QualityManager");
            if (QualityManagerArray.size() > 0) {
                for (GetUserListModel.Datum Services : QualityManagerArray) {
                    QualityManagerArr.add(Services.getFirstName() + " " + Services.getLastName());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, QualityManagerArr) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == selectedQualityManagerIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerQualityManager.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getSite() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getSiteListJson("-1", "1", "-1", "-1", "-1"
                    , "", ""));
            Call<GetSiteModel> call = RetrofitClient.createService(ApiInterface.class).getSiteAPI(body);
            call.enqueue(new Callback<GetSiteModel>() {
                @Override
                public void onResponse(@NonNull Call<GetSiteModel> call, @NonNull Response<GetSiteModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayList.addAll(response.body().getData());
                            if (myArrayList.isEmpty()) {
                            } else {
                                setSitesAdapter();
                            }

                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetSiteModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setSitesAdapter() {
        try {
            sitesArr.add(0, "Select Sites");
            if (myArrayList.size() > 0) {
                for (GetSiteModel.Datum Services : myArrayList) {
                    sitesArr.add(Services.getCompanyName());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, sitesArr) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == selectedSiteIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerSites.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getInspection() {
        try {
            Common.showLoadingDialog(mActivity, "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getInspectionJson());

            Call<InspectionModel> call = RetrofitClient.createService(ApiInterface.class).inspectionAPI(body);
            call.enqueue(new Callback<InspectionModel>() {
                @Override
                public void onResponse(@NonNull Call<InspectionModel> call, @NonNull Response<InspectionModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayInspection.addAll(response.body().getData());
                            getQuestions(mArrayInspection);
                        } else {
                            //   Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<InspectionModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getQuestions(ArrayList<InspectionModel.Datum> mArrayInspection) {

        if (mArrayInspection.size() > 0) {
            if (mArrayInspection.size() > i) {
                mTextQuestions.setText(mArrayInspection.get(i).getQuestion());
                Questions.clear();
                String tempOptions = mArrayInspection.get(i).getQuestionoption();
                Options = tempOptions.split(",");
                Questions = new ArrayList<String>(Arrays.asList(Options));

                for (int row = 0; row < 1; row++) {
                    ll = new RadioGroup(mActivity);
                    ll.setOrientation(LinearLayout.VERTICAL);
                    for (int opt = 1; opt <= Questions.size(); opt++) {

                        RadioButton rdbtn = new RadioButton(mActivity);
                        rdbtn.setId(View.generateViewId());
                        String radioButtonText = Questions.get(opt - 1);
                        rdbtn.setText(radioButtonText);
                        ll.addView(rdbtn);

                    }

                    ll.setOnCheckedChangeListener((group, checkedId) -> {

                        for (int pos = 0; pos < ll.getChildCount(); pos++) {
                            RadioButton btn = (RadioButton) ll.getChildAt(pos);
                            if (btn.getId() == checkedId) {
                                SelectedOptions = btn.getText().toString();
                                // do something with text
                                return;
                            }
                        }
                    });

                    ((ViewGroup) findViewById(R.id.rg_inspectionoptions)).addView(ll);

                }

            }
        } else {
            Toast.makeText(mActivity, "Question List Is Empty", Toast.LENGTH_SHORT).show();
        }

        mButtonNext.setOnClickListener(v -> {
            if (SelectedOptions.equalsIgnoreCase("")) {
                Toast.makeText(mActivity, "Select One Option", Toast.LENGTH_SHORT).show();

            } else {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("QuestionID", mArrayInspection.get(i).getQuestionID());
                    jsonObject.put("Answer", SelectedOptions);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                jsonArray.put(jsonObject);

                i += 1;
                int size = mArrayInspection.size();

                if (size == i) {
                    mButtonNext.setVisibility(View.GONE);
                    mButtonSubmit.setVisibility(View.VISIBLE);
                    mLinearLayout.setVisibility(View.VISIBLE);
                    mCardView.setVisibility(View.VISIBLE);
                } else {
                    if (mArrayInspection.size() > i) {

                        if (ll.getChildCount() > 0) {
                            ll.removeAllViews();
                        }

                        mTextQuestions.setText(mArrayInspection.get(i).getQuestion());
                        Questions.clear();
                        String tempOptions = mArrayInspection.get(i).getQuestionoption();
                        Options = tempOptions.split(",");
                        Questions = new ArrayList<String>(Arrays.asList(Options));

                        for (int row = 0; row < 1; row++) {
                            ll = new RadioGroup(mActivity);
                            ll.setOrientation(LinearLayout.VERTICAL);
                            for (int opt = 1; opt <= Questions.size(); opt++) {

                                RadioButton rdbtn = new RadioButton(mActivity);
                                rdbtn.setId(View.generateViewId());
                                String radioButtonText = Questions.get(opt - 1);
                                rdbtn.setText(radioButtonText);
                                ll.addView(rdbtn);

                            }

                            ll.setOnCheckedChangeListener((group, checkedId) -> {

                                for (int pos = 0; pos < ll.getChildCount(); pos++) {
                                    RadioButton btn = (RadioButton) ll.getChildAt(pos);
                                    if (btn.getId() == checkedId) {
                                        SelectedOptions = btn.getText().toString();
                                        // do something with text
                                        return;
                                    }
                                }
                            });

                            ((ViewGroup) findViewById(R.id.rg_inspectionoptions)).addView(ll);

                        }

                    }
                }
            }

        });

        mButtonSubmit.setOnClickListener(v -> {
            if (checkValidation()) {
                addInspections();
            }
        });
    }

    private boolean checkValidation() {
        boolean status = true;

        if (siteID.equalsIgnoreCase("0")) {
            status = false;
            Toast.makeText(mActivity, "Select Site", Toast.LENGTH_SHORT).show();
        }
        if (FieldOeratorID.equalsIgnoreCase("0")) {
            status = false;
            Toast.makeText(mActivity, "Select Field Operator", Toast.LENGTH_SHORT).show();
        }
       /* if (QualityManagerID.equalsIgnoreCase("0")) {
            status = false;
            Toast.makeText(mActivity, "Select Quality Manager", Toast.LENGTH_SHORT).show();
        }*/
       /* if (OperationMangerID.equalsIgnoreCase("0")) {
            status = false;
            Toast.makeText(mActivity, "Select Operation Manager", Toast.LENGTH_SHORT).show();
        }*/
     /*   if (mImagePath == null) {
            status = false;
            Toast.makeText(mActivity, "Please Upload Image To Continue", Toast.LENGTH_SHORT).show();
        }*/

        return status;
    }

    private void addInspections() {
        try {
            Common.showLoadingDialog(mActivity, "Loading");
            SessionManager mSession = new SessionManager(mActivity);
            String UserData = mSession.getPreferences(mSession.KEY_LOGIN_USER_DATA, "");
            String UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.addInspectionJson(UserID, siteID, FieldOeratorID, QualityManagerID, OperationMangerID,
                    mEditRemarks.getText().toString().trim(), jsonArray));

            Call<AddInspectionModel> call = RetrofitClient.createService(ApiInterface.class).inspectionOptionsAPI(body);
            call.enqueue(new Callback<AddInspectionModel>() {
                @Override
                public void onResponse(@NonNull Call<AddInspectionModel> call, @NonNull Response<AddInspectionModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (mImagePath == null) {
                                finish();
                            } else {
                                UploadImage(response.body().getData().get(0).getID());
                            }

                            //  Toast.makeText(mActivity, "Inspection Done Successfully", Toast.LENGTH_SHORT).show();
                        } else {
                            Common.hideDialog();
                        }
                    } catch (JSONException e) {
                        Common.hideDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddInspectionModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.hideDialog();
                    // Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void UploadImage(String id) {
        try {
            Common.showLoadingDialog(mActivity, "Loading");
            File file = new File(mImagePath);
            final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

            MultipartBody.Part body = MultipartBody.Part.createFormData(WebFields.ADD_INSPECTION_IMAGE.REQUEST_IMAGE_DATE,
                    file.getName(), requestFile);
            RequestBody method = RequestBody.create(MediaType.parse("text/plain"), WebFields.ADD_INSPECTION_IMAGE.MODE);

            RequestBody ID = RequestBody.create(MediaType.parse("text/plain"), id);

            Call<UploadInspectionImageModel> callRepos = new RetrofitClient().createService(ApiInterface.class).uploadInspectionImageAPI(method,
                    ID, body);

            callRepos.enqueue(new Callback<UploadInspectionImageModel>() {
                @Override
                public void onResponse(@NonNull Call<UploadInspectionImageModel> call, @NonNull Response<UploadInspectionImageModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            // Common.setCustomToast(mActivity, mMessage);
                            finish();

                        } else {
                            // Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<UploadInspectionImageModel> call, @NonNull Throwable t) {
                    Common.insertLog("Add UploadPics error " + t.getMessage());
//                    Common.hideDialog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getID() {

        mButtonNext = findViewById(R.id.nextQuestion);
        mButtonSubmit = findViewById(R.id.submitQuestion);
        mTextQuestions = findViewById(R.id.Questions);
        ll = findViewById(R.id.rg_inspectionoptions);
        mEditRemarks = findViewById(R.id.addRemarks);
        mImageDoc = findViewById(R.id.addImage);

    }

    private void setID() {
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        edt_insdate = findViewById(R.id.edt_insdate);
        mSpinnerSites = findViewById(R.id.spinner_sites);
        mSpinnerSites.setOnItemSelectedListener(onItemSelectedListener);
        spinnerFieldOperator = findViewById(R.id.spinnerFieldOperator);
        spinnerFieldOperator.setOnItemSelectedListener(onItemSelectedListener);
        spinnerOperationManager = findViewById(R.id.spinnerOperationManager);
        spinnerOperationManager.setOnItemSelectedListener(onItemSelectedListener);
        spinnerQualityManager = findViewById(R.id.spinnerQualityManager);
        spinnerQualityManager.setOnItemSelectedListener(onItemSelectedListener);
    }

    private void setRegister() {

        mImageBack.setOnClickListener(v -> onBackPressed());

        mImageDoc.setOnClickListener(v -> openImageUpload());

        edt_insdate.setOnClickListener(v -> {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(AddInspectionActivity.this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            edt_insdate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();

        });
    }

    /**
     * Open popup for camera and gallery
     */
    private void openImageUpload() {

        final CharSequence[] menus = {"Capture Image", "Gallery",
                "Cancel"};

        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        builder.setTitle("Select Options");
        builder.setItems(menus, (dialog, pos) -> {

            boolean result = CameraGalleryPermission.checkPermission(mActivity,
                    AppPermissions.ReadWriteExternalStorageRequiredPermission());

            if (pos == 0) {
                chosenTask = "Capture Image";
                if (result) {
                    openCameraIntent();
                }
            } else if (pos == 1) {
                chosenTask = "Gallery";
                if (result) {
                    openGalleryIntent();
                }
            } else if (pos == 2) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /**
     * Open the camera intent
     */
    private void openCameraIntent() {
        destination = AppUtil.currentTimeStampFile();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
        } else {
            File file = new File(Uri.fromFile(destination).getPath());
            Uri photoUri = FileProvider.getUriForFile(mActivity, mActivity.getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }

        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        if (intent.resolveActivity(mActivity.getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    /**
     * Open the gallery intent
     */
    private void openGalleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Show only images, no videos or anything else
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_GALLERY);
    }

    /**
     * Request camera permission dynamically
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case CameraGalleryPermission.INTERNAL_EXTERNAL_PERMISSION:
                Map<String, Integer> perms = new HashMap<>();
                perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                if (perms.get(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    if (chosenTask.equals("Capture Image")) {
                        openCameraIntent();
                    } else if (chosenTask.equals("Gallery")) {
                        openGalleryIntent();
                    }
                } else {
                    Toast.makeText(mActivity, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode,resultCode,data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                    CropImage.activity(Uri.fromFile(destination)).start(mActivity);
                    break;
                case REQUEST_GALLERY:
                    mImageUri = data.getData();

                    CropImage.activity(mImageUri).start(mActivity);

                   /* CropImage.activity(mImageUri).setMinCropResultSize(1800, 1800)
                            .setMaxCropResultSize(1800, 1800).start(mActivity);*/

                    mFilePath = Common.getRealPathFromURI(mImageUri, mActivity);
                    if (mFilePath == null)
                        mFilePath = mImageUri.getPath(); // from File Manager

                    if (mFilePath != null) {
                        File mFile = new File(mFilePath);
                        mBitmap = Common.decodeFile(mFile, AppConstants.PIC_WIDTH,
                                AppConstants.PIC_HEIGHT);
                        mPath = mFile;
                    }
                    break;
                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    mImageUri = result.getUri();
                    onSelectFromGalleryResult(mImageUri);
                    break;
            }
        }
    }

    /**
     * Sets the cropped image from the gallery or camera intent
     */
    private void onSelectFromGalleryResult(Uri dataUri) {
        Bitmap bm = null;
        String imageName = null;
        if (dataUri != null) {
            try {
                //  bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), dataUri);
                File file = new File(dataUri.getPath());
                imageName = file.getName();
                mImagePath = dataUri.getPath();
                Compressor compressor = new Compressor(mActivity);
                if (ImageUtil.convertURItoFile(mActivity, dataUri) != null)
                    bm = compressor.compressToBitmap(ImageUtil.convertURItoFile(mActivity, dataUri));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        if (bm != null) {
            bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        }

        mImageDoc.setImageBitmap(bm);
    }

}
