package com.tcc.Activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.tcc.Model.AddVisitorReminderModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tcc.utils.Common.hideDialog;

public class AddReminderActivity extends AppCompatActivity {

    EditText edt_pastdate, edt_pasttime, edt_reminderdate, edt_remindertime, edt_msg;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private TextView mTextName, mTextEmail, mTextHeader;
    private ImageView mImageBack;
    private String VisitorID, currentTime, formattedDate, UserID;
    private Button Submit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_reminder);

        Date c = Calendar.getInstance().getTime();
        currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        formattedDate = df.format(c);

        getID();
        setRegister();
        getData();
        setData();

    }

    private void setRegister() {

        mImageBack.setOnClickListener(v -> finish());


        edt_pastdate.setOnClickListener(v -> {
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(AddReminderActivity.this,
                    (view, year, monthOfYear, dayOfMonth) -> edt_pastdate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
            datePickerDialog.show();
        });

        edt_pasttime.setOnClickListener(v -> {
            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(AddReminderActivity.this,
                    (view, hourOfDay, minute) -> edt_pasttime.setText(hourOfDay + ":" + minute), mHour, mMinute, false);
            timePickerDialog.show();

        });

        edt_reminderdate.setOnClickListener(v -> {
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(AddReminderActivity.this,
                    (view, year, monthOfYear, dayOfMonth) -> edt_reminderdate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
            datePickerDialog.show();
        });

        edt_remindertime.setOnClickListener(v -> {
            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(AddReminderActivity.this,
                    (view, hourOfDay, minute) -> edt_remindertime.setText(hourOfDay + ":" + minute), mHour, mMinute, false);
            timePickerDialog.show();
        });


        Submit.setOnClickListener(v -> {

            if (CheckValidation()) {
                doAddReminder();
            }
        });
    }

    private boolean CheckValidation() {
        boolean status = true;


        if (edt_remindertime.getText().toString().trim().isEmpty()) {
            status = false;
            edt_remindertime.setError("EnterTime");
        }
        if (edt_reminderdate.getText().toString().trim().isEmpty()) {
            status = false;
            edt_reminderdate.setError("EnterDate");
        }
        if (edt_msg.getText().toString().trim().isEmpty()) {
            status = false;
            edt_msg.setError("Enter Message");
        }
        return status;
    }

    private void doAddReminder() {
        try {
            Common.showLoadingDialog(this, "Loading");
            String ConvertedReminderDate = Common.convertDateToServer(this, edt_reminderdate.getText().toString());
            String ConvertedPastDate = Common.convertDateToServer(this, edt_pastdate.getText().toString());


            String ReminderDateTime = ConvertedReminderDate + " " + edt_remindertime.getText().toString().trim() + ":00";
            String PastDateTime = ConvertedPastDate + " " + edt_pasttime.getText().toString().trim() + ":00";

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.addVisitorReminderJson(UserID, VisitorID, edt_msg.getText().toString(), ReminderDateTime, PastDateTime));

            Call<AddVisitorReminderModel> call = RetrofitClient.createService(ApiInterface.class).addVisitorReminderAPI(body);
            call.enqueue(new Callback<AddVisitorReminderModel>() {
                @Override
                public void onResponse(@NonNull Call<AddVisitorReminderModel> call, @NonNull Response<AddVisitorReminderModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            hideDialog();
                            // Common.setCustomToast(AddReminderActivity.this, mMessage);
                            Intent intent = new Intent(AddReminderActivity.this, VisitorDetailActivity.class);
                            startActivity(intent);
                            finish();

                        } else {
                            hideDialog();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddVisitorReminderModel> call, @NonNull Throwable t) {
                    hideDialog();
                    Common.insertLog("Failure:::> " + t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getID() {


        edt_pastdate = findViewById(R.id.edt_pastdate);
        edt_pasttime = findViewById(R.id.edt_pasttime);
        edt_reminderdate = findViewById(R.id.edt_reminderdate);
        edt_remindertime = findViewById(R.id.edt_remindertime);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mTextName = findViewById(R.id.text_visitor_profile_name);
        edt_msg = findViewById(R.id.edt_disc);
        Submit = findViewById(R.id.button_addreminder);


    }

    private void getData() {
        SessionManager manager = new SessionManager(this);
        VisitorID = manager.getPreferences("VisitorID", "");
        String UserData = manager.getPreferences(manager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(AddReminderActivity.this, WebFields.LOGIN.RESPONSE_USERID);
    }

    private void setData() {

        mTextHeader.setText("Reminder");
        mImageBack.setVisibility(View.VISIBLE);
        edt_pastdate.setText(formattedDate);
        edt_pasttime.setText(currentTime);

    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (isTaskRoot()) {
            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        }
        finish();
    }

}
