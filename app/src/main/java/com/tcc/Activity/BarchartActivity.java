package com.tcc.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.google.gson.Gson;
import com.tcc.Model.ReportPaymentModal;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BarchartActivity extends AppCompatActivity {

    ArrayList<String> cleintList = new ArrayList();
    ArrayList<BarEntry> amountList = new ArrayList();
    BarChart barChart;
    BarData barData;
    BarDataSet barDataSet;
    ArrayList barEntries;
    RelativeLayout relativeLayout;
    ArrayList<ReportPaymentModal.DataItem> arrayListResponse;
    private TextView mTextHeader;
    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);

        arrayListResponse = new ArrayList<>();

        getID();
        getResponse();


    }

    private void getID() {
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        relativeLayout = findViewById(R.id.relative_no_data_available);
        mTextHeader.setText("Payment Report");
        mImageBack.setVisibility(View.VISIBLE);

        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        barChart = findViewById(R.id.BarChart);
      /*  getEntries();
        barDataSet = new BarDataSet(barEntries, "");
        barData = new BarData(barDataSet);
        barChart.setData(barData);
        barDataSet.setColors(ColorTemplate.JOYFUL_COLORS);
        barDataSet.setValueTextColor(Color.BLACK);
        barDataSet.setValueTextSize(18f);*/
    }


    private void getResponse() {
        try {

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getReportPaymentJson("-1", "1", getIntent().getStringExtra("sDate"), getIntent().getStringExtra("lDate")));
            Common.showLoadingDialog(this, "Loading");
            Call<ReportPaymentModal> call = RetrofitClient.createService(ApiInterface.class).getReportPaymentAPI(body);
            call.enqueue(new Callback<ReportPaymentModal>() {
                @Override
                public void onResponse(@NonNull Call<ReportPaymentModal> call, @NonNull Response<ReportPaymentModal> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            if (response.body() != null) {
                                arrayListResponse.addAll(response.body().getData());
                                getResponseData();
                            } else {
                                relativeLayout.setVisibility(View.VISIBLE);

                            }

                        } else {
                            relativeLayout.setVisibility(View.VISIBLE);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ReportPaymentModal> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(getApplicationContext(), t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void configureChartAppearance() {
        barChart.getDescription().setEnabled(false);
        barChart.setDrawValueAboveBar(false);

       /* XAxis xAxis = barChart.getXAxis();
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return cleintList.get((int)value);
            }
        });*/
        XAxis xAxis = barChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(cleintList));//setting String values in Xaxis

        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);

        YAxis axisLeft = barChart.getAxisLeft();
        axisLeft.setGranularity(10f);
        axisLeft.setAxisMinimum(0);

        YAxis axisRight = barChart.getAxisRight();
        axisRight.setGranularity(10f);
        axisRight.setAxisMinimum(0);
    }

    private void getResponseData() {
        try {


            for (int i = 0; i < arrayListResponse.size(); i++) {

                float x = i;
                float y = Float.parseFloat(arrayListResponse.get(i).getPaymentAmount());

                amountList.add(new BarEntry(x, y));
                //   cleintList.add(new BarEntry(Float.parseFloat(arrayListResponse.get(i).getCompanyName()), i));
                cleintList.add(arrayListResponse.get(i).getCompanyName());
            }

            new Handler().postDelayed(() -> {
                BarData data = createChartData();
                configureChartAppearance();
                prepareChartData(data);
            }, 1000);



          /*  BarDataSet bardataset = new BarDataSet(amountList, "Amount");
            barChart.animateY(5000);
            BarData data = new BarData(bardataset);
            bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
            barChart.setData(data);*/

/*
            BarDataSet Bardataset = new BarDataSet(amountList, "Amount");

            BarData BARDATA = new BarData(Bardataset);

            Bardataset.setColors(ColorTemplate.COLORFUL_COLORS);

            barChart.setData(BARDATA);

            barChart.animateY(3000);*/

        } catch (Exception e) {

        }

    }

    private BarData createChartData() {


        BarDataSet set1 = new BarDataSet(amountList, "Amount");

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);

        return data;
    }

    private void prepareChartData(BarData data) {
        data.setValueTextSize(12f);
        barChart.setData(data);
        barChart.invalidate();
    }

    private void getEntries() {
        barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(2f, 0));
        barEntries.add(new BarEntry(4f, 1));
        barEntries.add(new BarEntry(6f, 1));
        barEntries.add(new BarEntry(8f, 3));
        barEntries.add(new BarEntry(7f, 4));
        barEntries.add(new BarEntry(3f, 3));
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (isTaskRoot()) {
            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        }
        finish();
    }
}

