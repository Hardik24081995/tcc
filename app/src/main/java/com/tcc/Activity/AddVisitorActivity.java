package com.tcc.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.tcc.Model.CityModel;
import com.tcc.Model.GetServicesModel;
import com.tcc.Model.StateModel;
import com.tcc.Model.VisitorModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.GSTINValidator;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tcc.utils.Common.hideDialog;

public class AddVisitorActivity extends AppCompatActivity {
    SessionManager mSessionManager;
    String ServiceName, StateName, CityID;
    ArrayList<GetServicesModel.Datum> mArrayServiceList;
    ArrayList<StateModel.Datum> mArrayStateList;
    ArrayList<CityModel.Datum> mArrayCityList;
    RadioGroup ll;
    ArrayList<String> LeadTypes;
    private Activity mActivity;
    private ImageView mImageBack;
    private Button mButtonSubmit;
    private TextView mTextHeader;
    private Spinner spinnerState, spinnerCity, spinner_Service;
    private EditText mEditPropesedDate, mEditFullName, mEditCompanyName, mEditPhone, mEditEmail, mEditWorkingDays,
            mEditWorkingHours, mEditNoOfMen, mEditLoc, mEditGstNo;
    private String strFullName, strCompanyName, strEmail, strPhone, strWorkingDays, strWorkingHrs, strNoOfMen,
            strLoc, strGstNo, strProposedDate, UserID, mSelectedLead = "Hot", Config;
    private int mSelectedServiceIndex = 0, mSelectedStateIndex = 0, mSelectedCityIndex = 0, mServiceID, mStateId, mCityId;
    private ArrayList<String> mArrServices, mArrState, mArrCity;
    private int mYear, mMonth, mDay, mHour, mMinute;

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinner_Service:
                    mSelectedServiceIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedServiceIndex != 0) {
                        ServiceName = String.valueOf((Integer.parseInt(mArrayServiceList.get(mSelectedServiceIndex - 1).getServiceID())));
                        String mCountryName = (mArrayServiceList.get(mSelectedServiceIndex - 1).getService());
                        Common.insertLog("mServiceId::> " + ServiceName);
                        Common.insertLog("mServiceName::> " + mCountryName);
                    }
                    break;

                case R.id.spinner_register_state:
                    mSelectedStateIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedStateIndex != 0) {
                        StateName = String.valueOf((Integer.parseInt(mArrayStateList.get(mSelectedStateIndex - 1).getStateID())));
                        String mStateName = (mArrayStateList.get(mSelectedStateIndex - 1).getStateName());
                        Common.insertLog("mStateId::> " + StateName);
                        Common.insertLog("mStateName::> " + mStateName);
                        callToCityAPI(Integer.parseInt(StateName));
                    }
                    break;

                case R.id.spinner_register_city:
                    mSelectedCityIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedCityIndex != 0) {
                        CityID = String.valueOf((Integer.parseInt(mArrayCityList.get(mSelectedCityIndex - 1).getCityID())));
                        String mCityName = (mArrayCityList.get(mSelectedCityIndex - 1).getCityName());
                        Common.insertLog("mCityId::> " + CityID);
                        Common.insertLog("mCityName::> " + mCityName);
                    }
                    break;

            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);

        mActivity = AddVisitorActivity.this;
        mArrayServiceList = new ArrayList<>();
        mArrayStateList = new ArrayList<>();
        mArrayCityList = new ArrayList<>();
        mArrServices = new ArrayList<>();
        mArrState = new ArrayList<>();
        mArrCity = new ArrayList<>();
        LeadTypes = new ArrayList<>();

        mSessionManager = new SessionManager(mActivity);
        String UserData = mSessionManager.getPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
        Config = mSessionManager.getPreferences(mSessionManager.KEY_CONFIGURATION_DATA, "");
        UserID = GetJsonData.getLoginData(AddVisitorActivity.this, WebFields.LOGIN.RESPONSE_USERID);
        //LeadType = GetJsonData.getConfigData(AddVisitorActivity.this, WebFields.Config.LEAD_TYPE);

        getIds();
        getServices();
        getState();
        setData();
        setRegiseter();
        getLeadStatus();

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c);
        mEditPropesedDate.setText(formattedDate);

    }

    private void getLeadStatus() {
        try {
            JSONObject jsonObject = new JSONObject(Config);
            String leadType = jsonObject.getString("LeadType");
            String[] arr_leadType = leadType.split(",");
            LeadTypes.add(arr_leadType[0]);
            LeadTypes.add(arr_leadType[1]);
            LeadTypes.add(arr_leadType[2]);
            LeadTypes.add(arr_leadType[3]);
            LeadTypes.add(arr_leadType[3]);

            for (int row = 0; row < 1; row++) {
                ll = new RadioGroup(this);
                ll.setOrientation(LinearLayout.VERTICAL);
                for (int i = 1; i < LeadTypes.size(); i++) {

                    RadioButton rdbtn = new RadioButton(this);
                    rdbtn.setId(View.generateViewId());
                    String radioButtonText = LeadTypes.get(i - 1);
                    rdbtn.setText(radioButtonText);
                    if (radioButtonText.equalsIgnoreCase("Hot")) {
                        rdbtn.setChecked(true);
                    }
                    ll.addView(rdbtn);

                }

                ll.setOnCheckedChangeListener((group, checkedId) -> {

                    for (int i = 0; i < ll.getChildCount(); i++) {
                        RadioButton btn = (RadioButton) ll.getChildAt(i);
                        if (btn.getId() == checkedId) {
                            mSelectedLead = btn.getText().toString();
                            // do something with text
                            return;
                        }
                    }
                });

                ((ViewGroup) findViewById(R.id.rg_leadType)).addView(ll);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void selectDate() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> mEditPropesedDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void getIds() {

        mEditCompanyName = findViewById(R.id.edit_add_vistor_company_name);
        mEditFullName = findViewById(R.id.edit_add_visitor_full_name);
//        mEditFullName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        mEditPhone = findViewById(R.id.edit_add_visitor_mobile);
        mEditEmail = findViewById(R.id.edit_add_visitor_email);
        mEditWorkingDays = findViewById(R.id.edit_add_visitor_working_days);
        mEditWorkingHours = findViewById(R.id.edit_add_visitor_working_hour);
        mEditNoOfMen = findViewById(R.id.edit_add_visitor_no_power_req);
        mEditGstNo = findViewById(R.id.edit_add_visitor_GSTNO);
        mEditLoc = findViewById(R.id.edit_add_visitor_locations);

        spinner_Service = findViewById(R.id.spinner_Service);
        spinnerState = findViewById(R.id.spinner_register_state);
        spinnerCity = findViewById(R.id.spinner_register_city);

        spinner_Service.setOnItemSelectedListener(onItemSelectedListener);
        spinnerState.setOnItemSelectedListener(onItemSelectedListener);
        spinnerCity.setOnItemSelectedListener(onItemSelectedListener);

        ll = findViewById(R.id.rg_leadType);

        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mButtonSubmit = findViewById(R.id.button_add_visitor_submit);
        mEditPropesedDate = findViewById(R.id.edit_add_visitor_propesed_date);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);

    }

    private void setData() {
        mTextHeader.setText(mActivity.getResources().getString(R.string.header_add_visitor));
        mImageBack.setVisibility(View.VISIBLE);
    }

    private void setRegiseter() {

        mButtonSubmit.setOnClickListener(v -> {
            if (checkValidation()) {
                DoAddVisitor();
            }
        });
        mImageBack.setOnClickListener(v -> finish());
        mEditPropesedDate.setOnClickListener(v -> selectDate());


    }

    private boolean checkValidation() {
        boolean status = true;

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        strFullName = mEditFullName.getText().toString();
        strCompanyName = mEditCompanyName.getText().toString();
        strPhone = mEditPhone.getText().toString();
        strEmail = mEditEmail.getText().toString();
        strProposedDate = Common.convertDateToServer(mActivity, mEditPropesedDate.getText().toString());
        strWorkingDays = mEditWorkingDays.getText().toString();
        strWorkingHrs = mEditWorkingHours.getText().toString();
        strNoOfMen = mEditNoOfMen.getText().toString();
        strLoc = mEditLoc.getText().toString();
        strGstNo = mEditGstNo.getText().toString();

        if (strFullName.equalsIgnoreCase("")) {
            status = false;
            mEditFullName.setError("Enter Name");
        }
        if (!strEmail.equalsIgnoreCase("")) {
            if (!strEmail.matches(emailPattern)) {
                status = false;
                mEditEmail.setError("Enter Valid Email");
            }
        }
        if (strCompanyName.equalsIgnoreCase("")) {
            status = false;
            mEditCompanyName.setError("Enter Company Name");
        }
        if (strPhone.equalsIgnoreCase("")) {
            status = false;
            mEditPhone.setError("Enter Phone");
        }
        if (strWorkingDays.equalsIgnoreCase("")) {
            status = false;
            mEditWorkingDays.setError("Enter Working Days");
        }
        if (strWorkingHrs.equalsIgnoreCase("")) {
            status = false;
            mEditWorkingHours.setError("Enter Working Hours");
        }
        if (strNoOfMen.equalsIgnoreCase("")) {
            status = false;
            mEditNoOfMen.setError("This field is required");
        }
        if (strLoc.equalsIgnoreCase("")) {
            status = false;
            mEditLoc.setError("Enter Location");
        }
        if (strGstNo.equalsIgnoreCase("")) {
            status = false;
            mEditGstNo.setError("Enter GST No");
        }
        try {
            if (!GSTINValidator.validGSTIN(strGstNo)) {
                status = false;
                mEditGstNo.setError("Enter GST Valid Number");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (ServiceName == null) {
            status = false;
            Toast.makeText(mActivity, "Please Select Service", Toast.LENGTH_SHORT).show();
        }
        if (CityID == null) {
            status = false;
            Toast.makeText(mActivity, "Please Select City", Toast.LENGTH_SHORT).show();
        }

        return status;
    }

    private void getServices() {
        try {

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getVisitorServiceJson());
            Common.showLoadingDialog(this, "Loading");
            Call<GetServicesModel> call = RetrofitClient.createService(ApiInterface.class).getVisitorServiceAPI(body);

            call.enqueue(new Callback<GetServicesModel>() {
                @Override
                public void onResponse(Call<GetServicesModel> call, Response<GetServicesModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            mArrayServiceList.addAll(response.body().getData());
                            setServiceAdapter();

                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<GetServicesModel> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setServiceAdapter() {
        try {
            mArrServices.add(0, getString(R.string.spinner_select_services));
            if (mArrayServiceList.size() > 0) {
                for (GetServicesModel.Datum Services : mArrayServiceList) {
                    mArrServices.add(Services.getService() + " " + Services.getQty());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, mArrServices) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedServiceIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_Service.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getState() {
        try {
            if (mArrState.size() > 0)
                mArrState.clear();
            if (mArrayStateList.size() > 0)
                mArrayStateList.clear();

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setStateListJson());

            Call<StateModel> call = RetrofitClient.createService(ApiInterface.class).getStateListAPI(body);
            call.enqueue(new Callback<StateModel>() {
                @Override
                public void onResponse(@NonNull Call<StateModel> call, @NonNull Response<StateModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                mArrayStateList.addAll(response.body().getData());
                                setStateAdapter();
                            }
                        } else {
                            setStateAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<StateModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setStateAdapter();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setStateAdapter() {
        try {
            spinnerState.setAdapter(null);
            mArrState.add(getString(R.string.spinner_select_state));

            if (mArrayStateList.size() > 0) {
                for (StateModel.Datum state : mArrayStateList) {
                    mArrState.add(state.getStateName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrState) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedStateIndex) {
                        // Set spinner selected popup item's text color
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerState.setAdapter(adapter);

            // Default Selected
            for (int c = 0; c < mArrState.size(); c++) {
                if (mArrState.get(c).equalsIgnoreCase("Gujarat")) {
                    spinnerState.setSelection(c);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the City API
     */
    private void callToCityAPI(int mStateId) {
        try {
            if (mArrCity.size() > 0)
                mArrCity.clear();
            if (mArrayCityList.size() > 0)
                mArrayCityList.clear();

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCityListJson(String.valueOf(mStateId)));

            Call<CityModel> call = RetrofitClient.createService(ApiInterface.class).getCityListAPI(body);
            call.enqueue(new Callback<CityModel>() {
                @Override
                public void onResponse(@NonNull Call<CityModel> call, @NonNull Response<CityModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                mArrayCityList.addAll(response.body().getData());
                                setCityAdapter();
                            }
                        } else {
                            setCityAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CityModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setCityAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setCityAdapter() {
        try {
            mArrCity.add(0, getString(R.string.spinner_select_city));

            if (mArrayCityList.size() > 0) {
                for (CityModel.Datum cityModel : mArrayCityList) {
                    mArrCity.add(cityModel.getCityName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrCity) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCityIndex) {
                        // Set spinner selected popup item's text color
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerCity.setAdapter(adapter);


            for (int i = 0; i < mArrCity.size(); i++) {
                if (mArrCity.get(i).equals("Ahmedabad")) {
                    spinnerCity.setSelection(i);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void DoAddVisitor() {
        try {

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.addVisitorJson(UserID, strFullName, strCompanyName, strEmail, strPhone, strLoc, ServiceName, strProposedDate, strWorkingDays, strWorkingHrs, strNoOfMen, mSelectedLead, CityID, StateName));

            Call<VisitorModel> call = RetrofitClient.createService(ApiInterface.class).addVisitorAPI(body);
            call.enqueue(new Callback<VisitorModel>() {
                @Override
                public void onResponse(@NonNull Call<VisitorModel> call, @NonNull Response<VisitorModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            hideDialog();
                            finish();

                        } else {
                            hideDialog();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<VisitorModel> call, @NonNull Throwable t) {
                    hideDialog();
                    Common.insertLog("Failure:::> " + t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
