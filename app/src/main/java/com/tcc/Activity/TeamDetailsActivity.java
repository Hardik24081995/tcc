package com.tcc.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.TeamDeatailAdapter;
import com.tcc.Model.AddTeamModel;
import com.tcc.Model.GetQuotationListModel;
import com.tcc.Model.GetSiteModel;
import com.tcc.Model.GetTeamEmployeeModel;
import com.tcc.Model.RemoveEmployeeModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeamDetailsActivity extends AppCompatActivity {

    Activity mActivity;
    SessionManager mSessionManager;
    RelativeLayout mRelativeNoDataFound;
    TeamDeatailAdapter mAdapter;
    ArrayList<GetTeamEmployeeModel.Datum> mArrayTeam;
    String formattedDate, SitesID, QuotationID, UserID, CurrentSiteName;
    Spinner mSpinnerSites, mSpinnerQuotation;
    EditText mEditStartDate, mEditEndDate;
    ArrayList<GetSiteModel.Datum> myArrayList;
    ArrayList<String> sitesArr;
    ArrayList<GetQuotationListModel.Datum> mArrayQuotes;
    ArrayList<String> quoteArr;
    private TextView mTextHeader, mTextFieldOperator, mTextOperationManager, mTextQualityManager;
    private ImageView mImageBack, mImageAdd;
    private RecyclerView mRecyclerView;
    private boolean isBackFromB;
    private int selectedSiteIndex = 0, selectedQuotationIndex;
    private String siteID = "0", QuotesID = "0";
    private SwipeRefreshLayout mSwipeRefreshLayout;
    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinner_sites:
                    selectedSiteIndex = adapterView.getSelectedItemPosition();

                    if (selectedSiteIndex != 0) {
                        siteID = myArrayList.get(selectedSiteIndex - 1).getSitesID();
                        getQuotation(myArrayList.get(selectedSiteIndex).getSitesID());
//                        getQuotation(siteID);
                    }
                    break;

                case R.id.spinner_quotation:
                    selectedQuotationIndex = adapterView.getSelectedItemPosition();

                    if (selectedQuotationIndex != 0) {
                        QuotesID = mArrayQuotes.get(selectedQuotationIndex - 1).getQuotationID();
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_details);

        isBackFromB = false;

        mActivity = TeamDetailsActivity.this;
        mSessionManager = new SessionManager(mActivity);
        SitesID = mSessionManager.getPreferences(AppConstants.SITE_ID, "");
        QuotationID = mSessionManager.getPreferences(AppConstants.QUOTATION_ID, "");
        UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);
        CurrentSiteName = mSessionManager.getPreferences(AppConstants.SITE_NAME, "");
        mArrayQuotes = new ArrayList<>();
        quoteArr = new ArrayList<>();
        mArrayTeam = new ArrayList<>();
        myArrayList = new ArrayList<>();
        sitesArr = new ArrayList<>();

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        formattedDate = df.format(c);

        initView();
    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }


    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        if (isBackFromB) {
            //Refresh your stuff here
            getTeam();
        }

    }

    private void initView() {
        getID();
        setRegister();
        getTeam();
        getSite();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getTeam();
            mSwipeRefreshLayout.setRefreshing(false);
        });
    }

    private void getSite() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getSiteListJson("-1", "1", "-1", "-1", "-1"
                    , "", ""));
            Call<GetSiteModel> call = RetrofitClient.createService(ApiInterface.class).getSiteAPI(body);
            call.enqueue(new Callback<GetSiteModel>() {
                @Override
                public void onResponse(@NonNull Call<GetSiteModel> call, @NonNull Response<GetSiteModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayList.addAll(response.body().getData());
                            if (myArrayList.isEmpty()) {
                            } else {
                            }

                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetSiteModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getQuotation(String SiteID) {
        mArrayQuotes.clear();
        quoteArr.clear();
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getQuotationJson("-1", "1", SiteID, "-1", "-1"));
            Call<GetQuotationListModel> call = RetrofitClient.createService(ApiInterface.class).getQuotationAPI(body);
            call.enqueue(new Callback<GetQuotationListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetQuotationListModel> call, @NonNull Response<GetQuotationListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayQuotes.addAll(response.body().getData());
                            if (mArrayQuotes.isEmpty()) {

                            } else {
                                setQuotationAdapter();
                            }
                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetQuotationListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setQuotationAdapter() {
        try {
            quoteArr.add(0, "Select Quotation");
            if (mArrayQuotes.size() > 0) {
                for (GetQuotationListModel.Datum Services : mArrayQuotes) {
                    quoteArr.add(Services.getQuotationName() + " " + Services.getEstimateNo());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, quoteArr) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == selectedQuotationIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerQuotation.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSitesAdapter(Spinner mSpinnerSites) {
        try {
            sitesArr.add(0, "Select Sites");
            if (myArrayList.size() > 0) {
                for (GetSiteModel.Datum Services : myArrayList) {
                    if (Services.getSitesID().equalsIgnoreCase(SitesID)) {

                    } else {
                        sitesArr.add(Services.getCompanyName());
                    }
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, sitesArr) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == selectedSiteIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerSites.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getTeam() {
        mArrayTeam.clear();
        try {
            Common.showLoadingDialog(mActivity, "Loading");
            String ConvertedReminderDate = Common.convertDateToServer(this, formattedDate);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getTeamEmployeeJson("-1", "1", SitesID, ConvertedReminderDate, QuotationID));

            Call<GetTeamEmployeeModel> call = RetrofitClient.createService(ApiInterface.class).getTeamEmployeeAPI(body);
            call.enqueue(new Callback<GetTeamEmployeeModel>() {
                @Override
                public void onResponse(@NonNull Call<GetTeamEmployeeModel> call, @NonNull Response<GetTeamEmployeeModel> response) {

                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayTeam.addAll(response.body().getData());
                            mRecyclerView.setVisibility(View.VISIBLE);
                            mRelativeNoDataFound.setVisibility(View.GONE);
                            setAdapter();
                        } else {
                            mRecyclerView.setVisibility(View.GONE);
                            mRelativeNoDataFound.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetTeamEmployeeModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        mAdapter = new TeamDeatailAdapter(mActivity, mArrayTeam) {
            @Override
            protected void deleteEmpployee(int adapterPosition, GetTeamEmployeeModel.Datum datum) {
                super.deleteEmpployee(adapterPosition, datum);
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        mActivity);
                builder.setMessage("Would You Like To Remove Employee From Team?");
                builder.setNegativeButton("NO",
                        (dialog, which) -> dialog.dismiss());
                builder.setPositiveButton("YES",
                        (dialog, which) -> deleteTeamMember(adapterPosition, datum));
                builder.show();
            }

            @Override
            protected void shufflEmployee(GetTeamEmployeeModel.Datum datum) {
                super.shufflEmployee(datum);
                shuffleTeamMember(datum);
            }
        };
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void shuffleTeamMember(GetTeamEmployeeModel.Datum datum) {
        Dialog dialog;
        try {
            dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.alert_employee_shuffle);

            mEditStartDate = dialog.findViewById(R.id.edtStartDateAccept);
            mEditEndDate = dialog.findViewById(R.id.edtEndDateAccept);
            TextView mTextEmployeeName = dialog.findViewById(R.id.shuffleEmployeeName);
            TextView mTextStartDate = dialog.findViewById(R.id.shuffleStartDate);
            TextView mTextEndDate = dialog.findViewById(R.id.shuffleEndDate);
            TextView mTextCurrentSite = dialog.findViewById(R.id.shuffleCurrentSite);
            TextView mTextOK = dialog.findViewById(R.id.accept);
            TextView mTextCancel = dialog.findViewById(R.id.dialogdismiss);
            mSpinnerSites = dialog.findViewById(R.id.spinner_sites);
            mSpinnerSites.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerQuotation = dialog.findViewById(R.id.spinner_quotation);
            mSpinnerQuotation.setOnItemSelectedListener(onItemSelectedListener);
            setSitesAdapter(mSpinnerSites);

            mEditStartDate.setText(formattedDate);
            mEditEndDate.setText(formattedDate);

            mTextEmployeeName.setText(datum.getEmployeeName());
            mTextCurrentSite.setText(CurrentSiteName);
            mTextStartDate.setText(datum.getStartDate());
            mTextEndDate.setText(datum.getEndDate());

            mEditStartDate.setOnClickListener(v -> {
                // Get Current Date
                final Calendar c1 = Calendar.getInstance();
                int mYear = c1.get(Calendar.YEAR);
                int mMonth = c1.get(Calendar.MONTH);
                int mDay = c1.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                        (view, year, monthOfYear, dayOfMonth) -> mEditStartDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
                datePickerDialog.show();
            });

            mEditEndDate.setOnClickListener(v -> {
                // Get Current Date
                final Calendar c1 = Calendar.getInstance();
                int mYear = c1.get(Calendar.YEAR);
                int mMonth = c1.get(Calendar.MONTH);
                int mDay = c1.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                        (view, year, monthOfYear, dayOfMonth) -> mEditEndDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            });

            mTextCancel.setOnClickListener(v -> dialog.dismiss());

            mTextOK.setOnClickListener(v -> {
                if (checkValidation()) {
                    shuffleEmployee(datum, dialog);
                    dialog.dismiss();
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void shuffleEmployee(GetTeamEmployeeModel.Datum datum, Dialog dialog) {
        try {
            JSONArray jsonArray = getArrayDiagnosis(dialog, datum);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.addTeamDefinitonJson(UserID, jsonArray));
            Common.showLoadingDialog(this, "Loading");
            Call<AddTeamModel> call = RetrofitClient.createService(ApiInterface.class).addTeamDefinitionAPI(body);
            call.enqueue(new Callback<AddTeamModel>() {
                @Override
                public void onResponse(@NonNull Call<AddTeamModel> call, @NonNull Response<AddTeamModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            getTeam();
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddTeamModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param dialog
     * @param datum
     * @return - JSON Array
     */
    private JSONArray getArrayDiagnosis(Dialog dialog, GetTeamEmployeeModel.Datum datum) {
        JSONArray jsonArray = new JSONArray();
        try {

            String ConvertedStartDate = Common.convertDateToServer(mActivity, mEditStartDate.getText().toString());
            String ConvertedEndDate = Common.convertDateToServer(mActivity, mEditEndDate.getText().toString());

            String ToSiteID = "0";

            int index_category = mSpinnerSites.getSelectedItemPosition();
            if (index_category > 0)
                ToSiteID = myArrayList.get(index_category).getSitesID();

            String ToQuotationID = "0";

            int index_quotes = mSpinnerQuotation.getSelectedItemPosition();
            if (index_quotes > 0)
                ToQuotationID = mArrayQuotes.get(index_quotes - 1).getQuotationID();


            JSONObject jsonObject = new JSONObject();
            jsonObject.put("EmployeeID", datum.getUserID());
            jsonObject.put("SitesID", ToSiteID);
            jsonObject.put("StartDate", ConvertedStartDate);
            jsonObject.put("EndDate", ConvertedEndDate);
            jsonObject.put("FromSitesID", SitesID);
            jsonObject.put("ToSitesID", ToSiteID);
            jsonObject.put("Type", "Shuffle");
            jsonObject.put("QuotationID", ToQuotationID);
            jsonObject.put("FromQuotationID", QuotationID);
            jsonObject.put("ToQuotationID", ToQuotationID);

            jsonArray.put(jsonObject);


        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
        return jsonArray;
    }

    private boolean checkValidation() {
        boolean status = true;

        if (siteID.equalsIgnoreCase("0")) {
            status = false;
            Toast.makeText(mActivity, "Please Select Site", Toast.LENGTH_SHORT).show();
        }

        if (QuotesID.equalsIgnoreCase("0")) {
            status = false;
            Toast.makeText(mActivity, "Please Select Quotation", Toast.LENGTH_SHORT).show();
        }

        return status;
    }

    private void deleteTeamMember(int adapterPosition, GetTeamEmployeeModel.Datum datum) {
        try {
            Common.showLoadingDialog(mActivity, "Loading");
            String ConvertedReminderDate = Common.convertDateToServer(this, formattedDate);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.removeTeamEmployeeJson(UserID, datum.getTeamDefinitionID()));

            Call<RemoveEmployeeModel> call = RetrofitClient.createService(ApiInterface.class).deleteEmployeeFromListAPI(body);
            call.enqueue(new Callback<RemoveEmployeeModel>() {
                @Override
                public void onResponse(@NonNull Call<RemoveEmployeeModel> call, @NonNull Response<RemoveEmployeeModel> response) {

                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayTeam.remove(adapterPosition);
                            mAdapter.notifyDataSetChanged();
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<RemoveEmployeeModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setRegister() {
        mTextHeader.setText("TEAM DETAILS");

        mImageBack.setVisibility(View.VISIBLE);
        mImageBack.setOnClickListener(v -> onBackPressed());

        mImageAdd.setVisibility(View.VISIBLE);
        mImageAdd.setOnClickListener(v -> {
            Intent intent = new Intent(mActivity, AddNewTeamMemberActivity.class);
            startActivity(intent);
        });
    }

    private void getID() {
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        mImageAdd = findViewById(R.id.image_custom_toolbar_add);
        mSwipeRefreshLayout = findViewById(R.id.swipeToRefresh);
        mRelativeNoDataFound = findViewById(R.id.relative_no_data_available);

        mTextFieldOperator = findViewById(R.id.FieldOperator);
        mTextOperationManager = findViewById(R.id.OperationManager);
        mTextQualityManager = findViewById(R.id.QualityManager);
        mRecyclerView = findViewById(R.id.RecyclerView);

        mTextFieldOperator.setText(mSessionManager.getPreferences(AppConstants.FIELD_OPERATOR, ""));
        mTextQualityManager.setText(mSessionManager.getPreferences(AppConstants.QUALITY_MANAGER, ""));
        mTextOperationManager.setText(mSessionManager.getPreferences(AppConstants.OPERATION_MANAGER, ""));
    }
}
