package com.tcc.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.tcc.R;

import java.util.ArrayList;

public class MCQInspectionActivity extends AppCompatActivity {

    ArrayList<String> arrayList = new ArrayList<>();
    int counter = 0;
    private ImageView mImageBack, mImageAdd;
    private TextView mTextHeader, que;
    private Button btnsubmit1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcqinspection);

        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        btnsubmit1 = findViewById(R.id.btnsubmit1);
        que = findViewById(R.id.que);

        mTextHeader.setText("INSPECTION");
        mImageBack.setVisibility(View.VISIBLE);

        arrayList.add("Hows Was Company's Work?");
        arrayList.add("How Was Company's Environment");
        arrayList.add("How Was People's Intensity To Work With You?");
        arrayList.add("How Was The Location?");

        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnsubmit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (counter < arrayList.size()) {
                    que.setText(arrayList.get(counter));
                    counter++;
                } else {
                    btnsubmit1.setText("Submit");
                }

            }
        });
    }
}
