package com.tcc.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.tcc.Model.AddTeamModel;
import com.tcc.Model.GetAvailableEmployee;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNewTeamMemberActivity extends AppCompatActivity {

    Activity mActivity;
    SessionManager mSessionManager;
    String formattedDate;
    int EmployeeID = 0;
    String EmployeeName;
    ArrayList<GetAvailableEmployee.Datum> mArrayEmployee = new ArrayList<>();
    ArrayList<String> mArray = new ArrayList<>();
    String RadioType = "New", UserID, SiteID, QuotationID;
    private TextView mTextHeader;
    private ImageView mImageBack, img_plus;
    private LinearLayout mLinearContainer;
    private int selectedSpinnerService = 0;
    private Button mButtonAddTeam;
    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinnerEmployee:
                    selectedSpinnerService = adapterView.getSelectedItemPosition();

                    if (selectedSpinnerService != 0) {
                        EmployeeID = Integer.parseInt(mArrayEmployee.get(selectedSpinnerService - 1).getUserID());
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_team_member);

        mActivity = AddNewTeamMemberActivity.this;
        mSessionManager = new SessionManager(mActivity);
        UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);
        QuotationID = mSessionManager.getPreferences(AppConstants.QUOTATION_ID, "");
        SiteID = mSessionManager.getPreferences(AppConstants.SITE_ID, "");

        Date c = Calendar.getInstance().getTime();
        String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        formattedDate = df.format(c);

        initView();
    }

    private void getEmployee(Spinner mSpinnerEmployee, String StartDate, String EndDate) {

        String convertedStartDate = Common.convertDateToServer(mActivity, StartDate);
        String convertedEndDate = Common.convertDateToServer(mActivity, EndDate);

        mArray.clear();
        mArrayEmployee.clear();
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getAvailableEmployeeJson("-1", "1", convertedStartDate, convertedEndDate));
            Common.showLoadingDialog(this, "Loading");
            Call<GetAvailableEmployee> call = RetrofitClient.createService(ApiInterface.class).getAvailEmployeeFromListAPI(body);
            call.enqueue(new Callback<GetAvailableEmployee>() {
                @Override
                public void onResponse(@NonNull Call<GetAvailableEmployee> call, @NonNull Response<GetAvailableEmployee> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayEmployee.addAll(response.body().getData());
                            setEmployeeAdapter(mSpinnerEmployee);
                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetAvailableEmployee> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setEmployeeAdapter(Spinner spinnerEmployee) {
        mArray.clear();
        try {
            mArray.add(0, "Select Employee");
            if (mArrayEmployee.size() > 0) {
                for (GetAvailableEmployee.Datum Services : mArrayEmployee) {
                    mArray.add(Services.getFirstName() + " " + Services.getLastName());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, mArray) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == selectedSpinnerService) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerEmployee.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        getID();
        setRegister();
    }

    private void setRegister() {
        mTextHeader.setText("TEAM DETAILS");

        mImageBack.setVisibility(View.VISIBLE);
        mImageBack.setOnClickListener(v -> onBackPressed());

        img_plus.setOnClickListener(v -> AddItems());

        mButtonAddTeam.setOnClickListener(v -> {
            if (checkValidation()) {
                addEmployee();
            }
        });
    }

    private void addEmployee() {
        try {

            JSONArray jsonArray = getArrayDiagnosis();

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.addTeamDefinitonJson(UserID, jsonArray));
            Common.showLoadingDialog(this, "Loading");
            Call<AddTeamModel> call = RetrofitClient.createService(ApiInterface.class).addTeamDefinitionAPI(body);
            call.enqueue(new Callback<AddTeamModel>() {
                @Override
                public void onResponse(@NonNull Call<AddTeamModel> call, @NonNull Response<AddTeamModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            finish();
                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddTeamModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    // Common.setCustomToast(mActivity, t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkValidation() {
        boolean status = true;

        if (mLinearContainer.getChildCount() > 0) {

            for (int i = 0; i < mLinearContainer.getChildCount(); i++) {
                View view = mLinearContainer.getChildAt(i);
                Spinner spinnerEmployee = view.findViewById(R.id.spinnerEmployee);

                int index_category = spinnerEmployee.getSelectedItemPosition();

                if (index_category == 0) {
                    Toast.makeText(mActivity, "Please Select Employee", Toast.LENGTH_SHORT).show();
                    status = false;
                }

            }
        } else {
            status = false;
            Toast.makeText(mActivity, "Click on Plus Button to Add Employee", Toast.LENGTH_LONG).show();
        }

        return status;
    }

    /**
     * @return - JSON Array
     */
    private JSONArray getArrayDiagnosis() {
        JSONArray jsonArray = new JSONArray();
        try {
            if (mLinearContainer.getChildCount() > 0) {
                for (int a = 0; a < mLinearContainer.getChildCount(); a++) {
                    View view = mLinearContainer.getChildAt(a);

                    RadioGroup mRadioGroupType = view.findViewById(R.id.rg_teamDefinition);
                    ;
                    RadioButton mRadioNew = view.findViewById(R.id.rb_new);
                    RadioButton mRadioRokdi = view.findViewById(R.id.rb_rokdi);
                    Spinner spinnerEmployee = view.findViewById(R.id.spinnerEmployee);
                    EditText edt_startDate = view.findViewById(R.id.edt_startDate);
                    EditText edt_endDate = view.findViewById(R.id.edt_endDate);

                    mRadioGroupType.setOnCheckedChangeListener((group, checkedId) -> {
                        switch (checkedId) {
                            case R.id.rb_new:
                                // do operations specific to this selection
                                RadioType = "New";
                                break;
                            case R.id.rb_rokdi:
                                // do operations specific to this selection
                                RadioType = "Rokdi";
                                break;
                        }
                    });

                    if (mRadioNew.isChecked()) {
                        RadioType = "New";
                    } else if (mRadioRokdi.isChecked()) {
                        RadioType = "Rokdi";
                    }


                    String ConvertedStartDate = Common.convertDateToServer(mActivity, edt_startDate.getText().toString());
                    String ConvertedEndDate = Common.convertDateToServer(mActivity, edt_endDate.getText().toString());
                    ;

                    String EmployeeID = "0";

                    int index_category = spinnerEmployee.getSelectedItemPosition();
                    if (index_category > 0)
                        EmployeeID = mArrayEmployee.get(index_category - 1).getUserID();


                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("EmployeeID", EmployeeID);
                    jsonObject.put("SitesID", SiteID);
                    jsonObject.put("StartDate", ConvertedStartDate);
                    jsonObject.put("EndDate", ConvertedEndDate);
                    jsonObject.put("FromSitesID", "0");
                    jsonObject.put("ToSitesID", "0");
                    jsonObject.put("Type", RadioType);
                    jsonObject.put("QuotationID", QuotationID);
                    jsonObject.put("FromQuotationID", "0");
                    jsonObject.put("ToQuotationID", "0");

                    jsonArray.put(jsonObject);
                }
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
        return jsonArray;
    }


    private void getID() {
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        img_plus = findViewById(R.id.img_plus);
        mLinearContainer = findViewById(R.id.linear_quotation_items);
        mButtonAddTeam = findViewById(R.id.submit_team);
    }

    private void AddItems() {

        final View view = LayoutInflater.from(AddNewTeamMemberActivity.this).inflate(R.layout.layout_containerdefinition, null,
                false);

        RadioGroup mRadioGroupType = view.findViewById(R.id.rg_teamDefinition);
        RadioButton mRadioNew = view.findViewById(R.id.rb_new);
        RadioButton mRadioRokdi = view.findViewById(R.id.rb_rokdi);
        EditText edt_startDate = view.findViewById(R.id.edt_startDate);
        EditText edt_endDate = view.findViewById(R.id.edt_endDate);

        Spinner spinnerEmployee = view.findViewById(R.id.spinnerEmployee);
        spinnerEmployee.setOnItemSelectedListener(onItemSelectedListener);
        ImageView img_remove_view = view.findViewById(R.id.img_remove_view);

        edt_startDate.setText(formattedDate);
        edt_endDate.setText(formattedDate);

        getEmployee(spinnerEmployee, formattedDate, formattedDate);

        edt_startDate.setOnClickListener(v -> {
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(AddNewTeamMemberActivity.this,
                    (view1, year, monthOfYear, dayOfMonth) -> edt_startDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
            datePickerDialog.show();
        });

        edt_endDate.setOnClickListener(v -> {
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(AddNewTeamMemberActivity.this,
                    (view12, year, monthOfYear, dayOfMonth) -> edt_endDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            datePickerDialog.show();
        });

        edt_startDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                getEmployee(spinnerEmployee, edt_startDate.getText().toString(), edt_endDate.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edt_endDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                getEmployee(spinnerEmployee, edt_startDate.getText().toString(), edt_endDate.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mRadioGroupType.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.rb_new:
                    // do operations specific to this selection
                    RadioType = "New";
                    break;
                case R.id.rb_rokdi:
                    // do operations specific to this selection
                    RadioType = "Rokdi";
                    break;
            }
        });

        img_remove_view.setOnClickListener(v -> mLinearContainer.removeView(view));
        mLinearContainer.addView(view);
    }
}
