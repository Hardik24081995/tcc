package com.tcc.Activity;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tcc.Model.EditDocumentModel;
import com.tcc.Model.UploadDocModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.AppPermissions;
import com.tcc.utils.AppUtil;
import com.tcc.utils.CameraGalleryPermission;
import com.tcc.utils.Common;
import com.tcc.utils.ImageUtils.Compressor;
import com.tcc.utils.ImageUtils.ImageUtil;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadDocumentActivity extends AppCompatActivity
        implements View.OnClickListener {

    public static final int REQUEST_CAMERA = 101, REQUEST_GALLERY = 102;
    public String mFilePath = "", mImagePath = null;
    SessionManager mSessionManager;
    String UserID, SiteID, QuotationID, EnumDoCType, DocumentID;
    String imageURL = "", titleDoc = "";
    private Activity mActivity;
    private EditText mEdtTitle;
    private ImageView imgAttachmentType, mBtnDelete, mDocumentEdit, mImageBack, mImageDoc, mImageRemove;
    private Button mButtonUpload;
    private LinearLayout mLinearUploadImage;
    private TextView mTextTitle;
    private String chosenTask;
    private File destination;
    private Uri mImageUri;
    private File mPath;
    private Bitmap mBitmap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attachment_doc_activity);

        mActivity = UploadDocumentActivity.this;
        mSessionManager = new SessionManager(UploadDocumentActivity.this);

        Intent intent = getIntent();
        imageURL = intent.getStringExtra("ImagePath");
        titleDoc = intent.getStringExtra("TitleDoc");

        mGetIds();
        setRegister();

        String UserData = mSessionManager.getPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(UploadDocumentActivity.this, WebFields.LOGIN.RESPONSE_USERID);
        SiteID = mSessionManager.getPreferences(AppConstants.SITE_ID, "");
        QuotationID = mSessionManager.getPreferences(AppConstants.QUOTATION_ID, "");
        EnumDoCType = mSessionManager.getPreferences("ENUMDOCTYPE", "");
        DocumentID = mSessionManager.getPreferences("DocID", "");

    }

    private void mGetIds() {
        mTextTitle = findViewById(R.id.text_custom_toolbar_without_back_header);

        mEdtTitle = (EditText) findViewById(R.id.edit_upload_document_title);
        imgAttachmentType = findViewById(R.id.image_upload_document_attachment_type);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        mDocumentEdit = findViewById(R.id.image_upload_document_edit);
        mBtnDelete = findViewById(R.id.image_upload_document_delete);
        mButtonUpload = findViewById(R.id.button_upload_document);

        mImageDoc = findViewById(R.id.image);
        mLinearUploadImage = findViewById(R.id.linear_upload_document_select_file);
        mImageRemove = findViewById(R.id.remove);

        mImageRemove.setOnClickListener(v -> {
            mImageDoc.setImageDrawable(null);
        });

        mTextTitle.setText(getResources().getString(R.string.title_upload_doc));
        mImageBack.setVisibility(View.VISIBLE);

        if (imageURL == null) {

        } else {
            Glide.with(mActivity)
                    .load(imageURL)
                    .into(mImageDoc);
        }
        if (titleDoc == null) {

        } else {
            mEdtTitle.setText(titleDoc);
        }
    }


    /**
     * Set Register Listener
     */
    private void setRegister() {
        mBtnDelete.setOnClickListener(this);
        mLinearUploadImage.setOnClickListener(this);
        mDocumentEdit.setOnClickListener(this);
        mImageBack.setOnClickListener(this);
        mButtonUpload.setOnClickListener(this);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        boolean result;
        switch (v.getId()) {
            case R.id.button_upload_document:


                if (checkValidation()) {
                    if (EnumDoCType.equalsIgnoreCase("ADD")) {
                        doUploadDoc();
                    } else {
                        editDoc();
                    }
                }

                /*if (MimeType != null && FileName != null && selectedFile != null) {
                    uploadDocument(selectedFile, FileName, MimeType);
                } else {
                    Toast.makeText(this, "Please select file", Toast.LENGTH_SHORT).show();
                }*/

                break;
            case R.id.linear_upload_document_select_file:
                openImageUpload();
                break;
            case R.id.image_upload_document_edit:
                openImageUpload();
                break;
            case R.id.image_upload_document_download:
                openImageUpload();
                break;
            case R.id.image_upload_document_delete:
                break;

            case R.id.image_custom_toolbar_back_arrow:
                onBackPressed();
                break;
        }
    }

    private void editDoc() {
        try {
            Common.showLoadingDialog(this, "Loading");

            File file = new File(mImagePath);
            final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

            MultipartBody.Part body = MultipartBody.Part.createFormData(WebFields.EDIT_DOCUMENT.REQUEST_FILENAME,
                    file.getName(), requestFile);
            RequestBody method = RequestBody.create(MediaType.parse("text/plain"), WebFields.EDIT_DOCUMENT.MODE);

            RequestBody Title = RequestBody.create(MediaType.parse("text/plain"), mEdtTitle.getText().toString());
            RequestBody userID = RequestBody.create(MediaType.parse("text/plain"), UserID);
            RequestBody documentID = RequestBody.create(MediaType.parse("text/plain"), DocumentID);

            Call<EditDocumentModel> callRepos = new RetrofitClient().createService(ApiInterface.class).editDocumentAPI(method, userID,
                    Title, documentID, body);

            callRepos.enqueue(new Callback<EditDocumentModel>() {
                @Override
                public void onResponse(@NonNull Call<EditDocumentModel> call, @NonNull Response<EditDocumentModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            finish();
                        } else {
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<EditDocumentModel> call, @NonNull Throwable t) {
                    Common.insertLog("Add UploadPics error " + t.getMessage());
//                    Common.hideDialog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void doUploadDoc() {
        try {
            Common.showLoadingDialog(this, "Loading");

            File file = new File(mImagePath);
            final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

            MultipartBody.Part body = MultipartBody.Part.createFormData(WebFields.UPLOAD_DOCUMENT.REQUEST_FILENAME,
                    file.getName(), requestFile);
            RequestBody method = RequestBody.create(MediaType.parse("text/plain"), WebFields.UPLOAD_DOCUMENT.MODE);

            RequestBody Title = RequestBody.create(MediaType.parse("text/plain"), mEdtTitle.getText().toString());
            RequestBody userID = RequestBody.create(MediaType.parse("text/plain"), UserID);
            RequestBody siteID = RequestBody.create(MediaType.parse("text/plain"), SiteID);
            RequestBody Quotationid = RequestBody.create(MediaType.parse("text/plain"), QuotationID);

            Call<UploadDocModel> callRepos = new RetrofitClient().createService(ApiInterface.class).UploadDocumentAPI(method, userID,
                    Title, siteID, Quotationid, body);

            callRepos.enqueue(new Callback<UploadDocModel>() {
                @Override
                public void onResponse(@NonNull Call<UploadDocModel> call, @NonNull Response<UploadDocModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            finish();
                        } else {
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<UploadDocModel> call, @NonNull Throwable t) {
                    Common.insertLog("Add UploadPics error " + t.getMessage());
//                    Common.hideDialog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkValidation() {
        boolean status = true;

        if (mImagePath == null) {
            status = false;
            Toast.makeText(mActivity, "Enter All Fields", Toast.LENGTH_SHORT).show();
        }
        if (mEdtTitle.getText().toString().isEmpty()) {
            status = false;
            Toast.makeText(mActivity, "Enter All Fields", Toast.LENGTH_SHORT).show();
        }

        return status;
    }

    /**
     * Open popup for camera and gallery
     */
    private void openImageUpload() {

        final CharSequence[] menus = {"Capture Image", "Gallery",
                "Cancel"};

        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        builder.setTitle("Select Options");
        builder.setItems(menus, (dialog, pos) -> {

            boolean result = CameraGalleryPermission.checkPermission(mActivity,
                    AppPermissions.ReadWriteExternalStorageRequiredPermission());

            if (pos == 0) {
                chosenTask = "Capture Image";
                if (result) {
                    openCameraIntent();
                }
            } else if (pos == 1) {
                chosenTask = "Gallery";
                if (result) {
                    openGalleryIntent();
                }
            } else if (pos == 2) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /**
     * Open the camera intent
     */
    private void openCameraIntent() {
        destination = AppUtil.currentTimeStampFile();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
        } else {
            File file = new File(Uri.fromFile(destination).getPath());
            Uri photoUri = FileProvider.getUriForFile(mActivity, mActivity.getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }

        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        if (intent.resolveActivity(mActivity.getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    /**
     * Open the gallery intent
     */
    private void openGalleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Show only images, no videos or anything else
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_GALLERY);
    }

    /**
     * Request camera permission dynamically
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case CameraGalleryPermission.INTERNAL_EXTERNAL_PERMISSION:
                Map<String, Integer> perms = new HashMap<>();
                perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                if (perms.get(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    if (chosenTask.equals("Capture Image")) {
                        openCameraIntent();
                    } else if (chosenTask.equals("Gallery")) {
                        openGalleryIntent();
                    }
                } else {
                    Toast.makeText(mActivity, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode,resultCode,data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                    CropImage.activity(Uri.fromFile(destination)).start(mActivity);
                    break;
                case REQUEST_GALLERY:
                    mImageUri = data.getData();

                    CropImage.activity(mImageUri).start(mActivity);

                    mFilePath = Common.getRealPathFromURI(mImageUri, mActivity);
                    if (mFilePath == null)
                        mFilePath = mImageUri.getPath(); // from File Manager

                    if (mFilePath != null) {
                        File mFile = new File(mFilePath);
                        mBitmap = Common.decodeFile(mFile, AppConstants.PIC_WIDTH,
                                AppConstants.PIC_HEIGHT);
                        mPath = mFile;
                    }
                    break;
                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    mImageUri = result.getUri();
                    onSelectFromGalleryResult(mImageUri);
                    break;
            }
        }
    }

    /**
     * Sets the cropped image from the gallery or camera intent
     */
    private void onSelectFromGalleryResult(Uri dataUri) {
        Bitmap bm = null;
        String imageName = null;
        if (dataUri != null) {
            try {
                //  bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), dataUri);
                File file = new File(dataUri.getPath());
                imageName = file.getName();
                mImagePath = dataUri.getPath();
                Compressor compressor = new Compressor(mActivity);
                if (ImageUtil.convertURItoFile(mActivity, dataUri) != null)
                    bm = compressor.compressToBitmap(ImageUtil.convertURItoFile(mActivity, dataUri));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        if (bm != null) {
            bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        }

        mImageDoc.setImageBitmap(bm);
    }
}
