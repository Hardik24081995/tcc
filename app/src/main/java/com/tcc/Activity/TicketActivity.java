package com.tcc.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.tcc.Model.AddTicketModel;
import com.tcc.Model.CityModel;
import com.tcc.Model.GetSubjectModal;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TicketActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public static final int REQUEST_IMAGE = 100;
    private static final String TAG = TicketActivity.class.getSimpleName();
    SessionManager mSessionManager;
    String UserID;
    ArrayList<CityModel.Datum> mArrayCityList = new ArrayList<>();
    ArrayList<GetSubjectModal.DataItem> mArraySubjectList = new ArrayList<>();
    ArrayList<String> mArrCity = new ArrayList<>();
    ArrayList<String> mArrSubject = new ArrayList<>();
    String[] Ticket = {"Low", "Medium", "High"};
    private Activity mActivity;
    private TextView mTextHeader;
    private ImageView mImageBack, addTicket;
    private Spinner spinnerTicket;
    private EditText mEditTitle, mEditDescription;
    private String Priority;
    private TextView PhotoName;
    private int SelectedSubjectIndex = 0, mSelectedCityIndex = 0, mCityId;
    private Button submitTicket;
    private Spinner mspinnerSubject, spinnerCity;
    private String mFilePath, CityID = "", SubjectID = "";


    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.Spinner_subject:
                    SelectedSubjectIndex = adapterView.getSelectedItemPosition();

                    if (SelectedSubjectIndex != 0) {
                        SubjectID = String.valueOf((Integer.parseInt(mArraySubjectList.get(SelectedSubjectIndex - 1).getSubjectID())));
                        String Name = (mArraySubjectList.get(SelectedSubjectIndex - 1).getSubjectID());
                        Common.insertLog("mStateId::> " + SubjectID);
                        Common.insertLog("mStateName::> " + Name);
                    }
                    break;
                case R.id.spinner_register_city:
                    mSelectedCityIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedCityIndex != 0) {
                        CityID = String.valueOf((Integer.parseInt(mArrayCityList.get(mSelectedCityIndex - 1).getCityID())));
                        String mCityName = (mArrayCityList.get(mSelectedCityIndex - 1).getCityName());
                        Common.insertLog("mCityId::> " + CityID);
                        Common.insertLog("mCityName::> " + mCityName);
                    }
                    break;


            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket);

        mActivity = TicketActivity.this;
        mSessionManager = new SessionManager(mActivity);
        String UserData = mSessionManager.getPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);
        mFilePath = mSessionManager.getPreferences(mSessionManager.SOURCE_PATH, "");
        ImagePickerActivity.clearCache(this);

        spinnerCity = findViewById(R.id.spinner_register_city);
        mspinnerSubject = findViewById(R.id.Spinner_subject);

        mspinnerSubject.setOnItemSelectedListener(onItemSelectedListener);
        spinnerCity.setOnItemSelectedListener(onItemSelectedListener);

        getIds();
        setData();
        setRegister();
        setSpinner();
        callToSubjectAPI();
        callToCityAPI(-1);

    }

    private void getIds() {

        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);

        mEditTitle = findViewById(R.id.edt_ticket_title);
        mEditDescription = findViewById(R.id.edt_ticket_description);
        PhotoName = findViewById(R.id.PhotoName);

        spinnerTicket = findViewById(R.id.spinnerPriority);
        addTicket = findViewById(R.id.addticket);
        submitTicket = findViewById(R.id.submitTicket);

    }

    private void setData() {
        mTextHeader.setText("Ticket");
        mImageBack.setVisibility(View.VISIBLE);

        submitTicket.setOnClickListener(v -> {
            if (CheckValidation()) {
                adTicket();
            }
        });

        addTicket.setOnClickListener(v -> Dexter.withActivity(TicketActivity.this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check());

    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(TicketActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(TicketActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new AlertDialog.Builder(TicketActivity.this);
        builder.setTitle("Grant Permission");
        builder.setMessage("This App Needs Permission To Use This Feature");
        builder.setPositiveButton("Open Settings", (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                try {
                    // You can update this bitmap to your server
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);

                    // loading profile image from local cache
                    loadProfile(uri.toString());
//                    mFilePath = uri.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void loadProfile(String url) {
        Log.d(TAG, "Image cache path: " + url);

        Glide.with(this).load(url)
                .into(addTicket);
    }


    private void adTicket() {
        try {

            mFilePath = mSessionManager.getPreferences(mSessionManager.SOURCE_PATH, "");
            Common.showLoadingDialog(this, "Loading");

            MultipartBody.Part body = null;
            if (mFilePath.equals("")) {

            } else {
                File file = new File(mFilePath);

                final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                body = MultipartBody.Part.createFormData(WebFields.ADD_TICKET.REQUEST_IMAGE,
                        file.getName(), requestFile);

            }


            RequestBody method = RequestBody.create(MediaType.parse("text/plain"), WebFields.ADD_TICKET.MODE);

            RequestBody userID = RequestBody.create(MediaType.parse("text/plain"), UserID);
            RequestBody Title = RequestBody.create(MediaType.parse("text/plain"), mEditTitle.getText().toString());
            RequestBody Description = RequestBody.create(MediaType.parse("text/plain"), mEditDescription.getText().toString());
            RequestBody priority = RequestBody.create(MediaType.parse("text/plain"), Priority);
            RequestBody subjectID = RequestBody.create(MediaType.parse("text/plain"), SubjectID);
            RequestBody cityID = RequestBody.create(MediaType.parse("text/plain"), CityID);

            Call<AddTicketModel> callRepos = new RetrofitClient().createService(ApiInterface.class).AddTicketAPI(method,
                    Title, Description, priority, userID, subjectID, cityID, body);

            callRepos.enqueue(new Callback<AddTicketModel>() {
                @Override
                public void onResponse(@NonNull Call<AddTicketModel> call, @NonNull Response<AddTicketModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            finish();

                        } else {

                        }
                    } catch (Exception e) {

                        Common.insertLog(e.getMessage());

                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddTicketModel> call, @NonNull Throwable t) {
                    Common.insertLog("Add UploadPics error " + t.getMessage());
//                    Common.hideDialog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private boolean CheckValidation() {
        boolean status = true;

        mFilePath = mSessionManager.getPreferences(mSessionManager.SOURCE_PATH, "");

        if (mEditTitle.getText().toString().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Enter All Fields", Toast.LENGTH_SHORT).show();
        }
        if (mEditDescription.getText().toString().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Enter All Fields", Toast.LENGTH_SHORT).show();
        }
        if (Priority.equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Enter All Fields", Toast.LENGTH_SHORT).show();
        }

        if (SubjectID.equalsIgnoreCase("") || SubjectID.equalsIgnoreCase("0")) {
            status = false;
            Toast.makeText(mActivity, "Please Select Subject", Toast.LENGTH_SHORT).show();
        }
        if (CityID.equalsIgnoreCase("") || CityID.equalsIgnoreCase("0")) {
            status = false;
            Toast.makeText(mActivity, "Please Select City", Toast.LENGTH_SHORT).show();
        }
        return status;
    }

    private void setRegister() {
        mImageBack.setOnClickListener(v -> onBackPressed());
    }

    private void setSpinner() {
        spinnerTicket.setOnItemSelectedListener(this);

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter arrayAdapter = new ArrayAdapter(TicketActivity.this, android.R.layout.simple_spinner_dropdown_item, Ticket);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spinnerTicket.setAdapter(arrayAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Priority = spinnerTicket.getSelectedItem().toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    /**
     * This method should call the City API
     */
    private void callToCityAPI(int mStateId) {
        try {
            if (mArrCity.size() > 0)
                mArrCity.clear();
            if (mArrayCityList.size() > 0)
                mArrayCityList.clear();

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCityListJson(String.valueOf(mStateId)));

            Call<CityModel> call = RetrofitClient.createService(ApiInterface.class).getCityListAPI(body);
            call.enqueue(new Callback<CityModel>() {
                @Override
                public void onResponse(@NonNull Call<CityModel> call, @NonNull Response<CityModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                mArrayCityList.addAll(response.body().getData());
                                setCityAdapter();
                            }
                        } else {
                            setCityAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CityModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setCityAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setCityAdapter() {
        try {
            mArrCity.add(0, getString(R.string.spinner_select_city));

            if (mArrayCityList.size() > 0) {
                for (CityModel.Datum cityModel : mArrayCityList) {
                    mArrCity.add(cityModel.getCityName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrCity) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCityIndex) {
                        // Set spinner selected popup item's text color
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerCity.setAdapter(adapter);
            for (int i = 0; i < mArrCity.size(); i++) {
                if (mArrCity.get(i).equals("Ahmedabad")) {
                    spinnerCity.setSelection(i);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * This method should call the City API
     */
    private void callToSubjectAPI() {
        try {
            if (mArrSubject.size() > 0)
                mArrSubject.clear();
            if (mArraySubjectList.size() > 0)
                mArraySubjectList.clear();

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setSubjectListJson());

            Call<GetSubjectModal> call = RetrofitClient.createService(ApiInterface.class).getSubjectListAPI(body);
            call.enqueue(new Callback<GetSubjectModal>() {
                @Override
                public void onResponse(@NonNull Call<GetSubjectModal> call, @NonNull Response<GetSubjectModal> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                mArraySubjectList.addAll(response.body().getData());
                                setSubjectAdapter();
                            }
                        } else {
                            setSubjectAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetSubjectModal> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setSubjectAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setSubjectAdapter() {
        try {
            mArrSubject.add(0, getString(R.string.spinner_select_subject));

            if (mArraySubjectList.size() > 0) {
                for (GetSubjectModal.DataItem modal : mArraySubjectList) {
                    mArrSubject.add(modal.getSubject());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrSubject) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCityIndex) {
                        // Set spinner selected popup item's text color
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mspinnerSubject.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
