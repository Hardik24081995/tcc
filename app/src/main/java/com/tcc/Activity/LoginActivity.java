package com.tcc.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.installations.FirebaseInstallations;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.tcc.Model.ConfigModel;
import com.tcc.Model.GetModuleIDList;
import com.tcc.Model.LoginModel;
import com.tcc.Model.RoleModal;
import com.tcc.Model.RoleModalResponse;
import com.tcc.R;
import com.tcc.utils.AppUtil;
import com.tcc.utils.Common;
import com.tcc.utils.DeviceUtils;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    AppUtil mAppUtils;
    SessionManager mSessionManager;
    BaseActivity baseActivity;
    private Activity mActivity;
    private Button loginButton;
    private TextView forgotPassword;
    private String strMobileNumber, strPassword, strDeviceName, strDeviceUID, strOSVersion, strDeviceTokenID = "", strDeviceType, strDeviceOS,
            UserType, mMessage;
    private EditText mEditMobile, mEditPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        mActivity = LoginActivity.this;
        mAppUtils = new AppUtil(this);
        mSessionManager = new SessionManager(this);

        getID();
        getFCM();
        setRegister();
        getConfig();
        getModuleList();
    }

    private void getFCM() {
        String token = mSessionManager.getPreferences("token", "");
        if (!token.equals("") && token != null) {
            strDeviceTokenID = token;
        } else {
            FirebaseInstallations.getInstance().getToken(true);
            Task<String> tk = FirebaseMessaging.getInstance().getToken();

            tk.addOnSuccessListener(new OnSuccessListener<String>() {
                @Override
                public void onSuccess(String s) {
                    strDeviceTokenID = s;
                    mSessionManager.setPreferences("token", strDeviceTokenID);
                }
            });

            tk.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(mActivity, "Token Not Found Click Again", Toast.LENGTH_SHORT).show();
                }
            });

        }
    }

    private void getID() {

        mEditMobile = findViewById(R.id.login_username);
        mEditPassword = findViewById(R.id.login_password);
        loginButton = findViewById(R.id.submitLogin);
        forgotPassword = findViewById(R.id.forgot);

    }

    private void setRegister() {

        loginButton.setOnClickListener(v -> {

            if (!mAppUtils.getConnectionState()) {
                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                Toast.makeText(mActivity, "No Internet", Toast.LENGTH_SHORT).show();
            } else {

                strMobileNumber = mEditMobile.getText().toString();
                strPassword = mEditPassword.getText().toString();
                strDeviceName = DeviceUtils.getDeviceName();
                strDeviceUID = "AS2123ASD";
                strOSVersion = DeviceUtils.getDeviceOSNumber();


                Log.d("FCM", strDeviceTokenID);
                strDeviceType = "Android";
                strDeviceOS = DeviceUtils.getDeviceOS();
                UserType = "Android";

                if (CheckValidation()) {
                    DoLogin();
                }


            }
        });

        forgotPassword.setOnClickListener(v -> {

            Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
            startActivity(intent);

        });
    }

    private boolean CheckValidation() {
        boolean status = true;

        if (mEditMobile.getText().toString().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(LoginActivity.this, "Enter Phone", Toast.LENGTH_SHORT).show();
        } else if (mEditPassword.getText().toString().equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(LoginActivity.this, "Enter Password", Toast.LENGTH_SHORT).show();
        } /*else if (strDeviceTokenID.equals("")) {
            status = false;
            getFCM();
            Toast.makeText(LoginActivity.this, "FCM Tokan Not Found try again!", Toast.LENGTH_SHORT).show();
        }*/
        Log.d("token", strDeviceTokenID);
        return status;
    }

    private void DoLogin() {
        try {

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.setLoginJson(strMobileNumber, strPassword, strDeviceName, strDeviceUID, strOSVersion, strDeviceTokenID, strDeviceType, strDeviceOS, UserType));
            Common.showLoadingDialog(this, "Loading");
            Call<LoginModel> call = RetrofitClient.createService(ApiInterface.class).loginAPI(body);
            call.enqueue(new Callback<LoginModel>() {
                @Override
                public void onResponse(@NonNull Call<LoginModel> call, @NonNull Response<LoginModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            hideDialog();

                            saveDataToSharedPreferences(jsonObject);
                            getFCM();

                        } else {
                            hideDialog();
                            Toast.makeText(mActivity, mMessage, Toast.LENGTH_SHORT).show();
                            // Common.setCustomToast(LoginActivity.this, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<LoginModel> call, @NonNull Throwable t) {
                    hideDialog();
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(LoginActivity.this, "error");

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveDataToSharedPreferences(JSONObject jsonObject) {
        try {
            if (jsonObject.has(WebFields.DATA)) {
                String mUserData = jsonObject.getString(WebFields.DATA);
                mSessionManager.setPreferences(mSessionManager.KEY_LOGIN_USER_DATA, mUserData);
                String RoleID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_ROLE_ID);

                getRole(RoleID);

                getFCM();


            } else {
                mSessionManager.setPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getConfig() {
        try {

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.setConfigJson());
            Common.showLoadingDialog(this, "Loading");
            Call<ConfigModel> call = RetrofitClient.createService(ApiInterface.class).getConfigAPI(body);

            call.enqueue(new Callback<ConfigModel>() {
                @Override
                public void onResponse(Call<ConfigModel> call, Response<ConfigModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            saveDataToSharedPreferencesforConfig(jsonObject);

                        } else {
                            Common.setCustomToast(LoginActivity.this, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ConfigModel> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getRole(String roleID) {
        try {

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getRole(roleID));
            Common.showLoadingDialog(this, "Loading");
            Call<RoleModalResponse> call = RetrofitClient.createService(ApiInterface.class).getRole(body);

            call.enqueue(new Callback<RoleModalResponse>() {
                @Override
                public void onResponse(Call<RoleModalResponse> call, Response<RoleModalResponse> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            saveDataToSharedPreferencesforRole(response.body());

                        } else {
                            Common.setCustomToast(LoginActivity.this, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<RoleModalResponse> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void saveDataToSharedPreferencesforConfig(JSONObject jsonObject) {

        try {
            if (jsonObject.has(WebFields.DATA)) {
                String mUserData = jsonObject.getString(WebFields.DATA);
                mSessionManager.setPreferences(mSessionManager.KEY_CONFIGURATION_DATA, mUserData);
            } else {
                mSessionManager.setPreferences(mSessionManager.KEY_CONFIGURATION_DATA, "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void saveDataToSharedPreferencesforRole(RoleModalResponse roleList) {


        Gson gson = new Gson();
        String json = gson.toJson(roleList);
        mSessionManager.setPreferences(mSessionManager.KEY_ROLE_DATA, json);

        try {

            JSONObject jsonData = new JSONObject();
            JSONObject jsonObject = new JSONObject();
            RoleModal.Data data = null;
            for (int i = 0; i < roleList.getData().size(); i++) {
                JSONObject jsonEmp = new JSONObject();
                jsonEmp.put("is_view", roleList.getData().get(i).getIsView());
                jsonEmp.put("is_insert", roleList.getData().get(i).getIsInsert());
                jsonEmp.put("is_edit", roleList.getData().get(i).getIsEdit());
                jsonObject.put(roleList.getData().get(i).getModuleName(), jsonEmp);
                jsonData.put("data", jsonObject);


            }

            Log.d("json", jsonData.toString());

            mSessionManager.setPreferences(mSessionManager.KEY_ROLE, jsonData.toString());


        } catch (Exception e) {

        }

        if (mSessionManager.getPreferences("IsFirst", true)) {
            Intent intent = new Intent(mActivity, WelComeActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(mActivity, HomeActivity.class);
            startActivity(intent);
            finish();
        }


    }

    private void getModuleList() {
        try {
            //  Common.showLoadingDialog(this, "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getModuleList());

            Call<GetModuleIDList> call = RetrofitClient.createService(ApiInterface.class).getModuleList(body);
            call.enqueue(new Callback<GetModuleIDList>() {
                @Override
                public void onResponse(@NonNull Call<GetModuleIDList> call, @NonNull Response<GetModuleIDList> response) {
                    //   Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            saveDataToSharedPreferencesforModuleID(response.body());
                        } else {
                            // Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetModuleIDList> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void saveDataToSharedPreferencesforModuleID(GetModuleIDList dataItem) {

        try {
            Gson gson = new Gson();
            String json = gson.toJson(dataItem);
            mSessionManager.setPreferences(mSessionManager.KEY_MODULE_DATA, json);


        } catch (Exception e) {

        }

    }
}
