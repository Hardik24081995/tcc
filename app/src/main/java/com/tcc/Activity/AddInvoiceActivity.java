package com.tcc.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.tcc.Model.AddSiteModel;
import com.tcc.Model.GetCustomerListModel;
import com.tcc.Model.GetMetrealModal;
import com.tcc.Model.GetQuotationListModel;
import com.tcc.Model.GetSiteModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddInvoiceActivity extends AppCompatActivity {


    ArrayList<GetCustomerListModel.Datum> mArrayCustomer = new ArrayList<>();
    ArrayList<GetMetrealModal.DataItem> mArrayMetreal = new ArrayList<>();
    ArrayList<GetSiteModel.Datum> myArraySite = new ArrayList<>();
    ArrayList<GetQuotationListModel.Datum> myArrayQuotation = new ArrayList<>();
    ArrayList<String> CustomerName = new ArrayList<>();
    ArrayList<String> SiteName = new ArrayList<>();
    ArrayList<String> QuotationName = new ArrayList<>();
    ArrayList<String> Meterial = new ArrayList<>();
    int SelectedCustomerIndex = 0;
    int SelectedSiteIndex = 0;
    int SelectedQuotationIndex = 0;
    int SelectedMerirealIndex = 0;
    String CustomerID = "";
    String SiteID = "";
    String QuotationID = "";
    String MetirealID = "";
    SessionManager mSessionManager;
    String UserID, EmployeeID, DateTimeID;
    String formattedDate;
    EditText edt_startDate;
    EditText edt_endDate;
    EditText edt_quantity;
    EditText edt_rate, edt_uom;
    private Activity mActivity;
    private ImageView mImageBack;
    private TextView mTextHeader;
    private Spinner spinnerCustomer, spinnerSites, spinnerQuation, spinner_metireal;
    private Button addTraining;

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinner_customer:
                    SelectedCustomerIndex = adapterView.getSelectedItemPosition();

                    if (SelectedCustomerIndex != 0) {
                        CustomerID = String.valueOf((Integer.parseInt(mArrayCustomer.get(SelectedCustomerIndex - 1).getCustomerID())));
                        String mTrainingName = (mArrayCustomer.get(SelectedCustomerIndex - 1).getName());
                        Common.insertLog("mStateId::> " + CustomerID);
                        Common.insertLog("mStateName::> " + mTrainingName);
                        getSite(CustomerID);
                    }
                    break;
                case R.id.spinner_sites:
                    SelectedSiteIndex = adapterView.getSelectedItemPosition();

                    if (SelectedSiteIndex != 0) {
                        SiteID = String.valueOf((Integer.parseInt(myArraySite.get(SelectedSiteIndex - 1).getSitesID())));
                        String mTrainingName = (myArraySite.get(SelectedSiteIndex - 1).getCompanyName());
                        Common.insertLog("mStateId::> " + SiteID);
                        Common.insertLog("mStateName::> " + mTrainingName);
                        getQuotation(SiteID);
                    }
                    break;
                case R.id.spinner_quotation:
                    SelectedQuotationIndex = adapterView.getSelectedItemPosition();

                    if (SelectedQuotationIndex != 0) {
                        QuotationID = String.valueOf((Integer.parseInt(myArrayQuotation.get(SelectedQuotationIndex - 1).getQuotationID())));
                        String mTrainingName = (myArrayQuotation.get(SelectedQuotationIndex - 1).getQuotationName());
                        Common.insertLog("mStateId::> " + QuotationID);
                        Common.insertLog("mStateName::> " + mTrainingName);
                    }
                    break;
                case R.id.spinner_metireal:
                    SelectedMerirealIndex = adapterView.getSelectedItemPosition();

                    if (SelectedMerirealIndex != 0) {
                        MetirealID = String.valueOf((Integer.parseInt(mArrayMetreal.get(SelectedMerirealIndex - 1).getMaterialID())));
                        String mTrainingName = (mArrayMetreal.get(SelectedMerirealIndex - 1).getMaterial());
                        Common.insertLog("mStateId::> " + MetirealID);
                        Common.insertLog("mStateName::> " + mTrainingName);
                    }
                    break;

            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_invoice);

        mActivity = AddInvoiceActivity.this;

        mSessionManager = new SessionManager(mActivity);
        String UserData = mSessionManager.getPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);
        EmployeeID = mSessionManager.getPreferences(AppConstants.EMPLOYEE_ID_FOR_ADD, "");
        DateTimeID = mSessionManager.getPreferences("TimeID", "");

        getID();
        setData();
        getCustomerList();
        getMetirealList();
        setRegister();

    }


    private void getMetirealList() {
        mArrayMetreal.clear();
        try {
            //   Common.showLoadingDialog(this, "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getMetirealList());

            Call<GetMetrealModal> call = RetrofitClient.createService(ApiInterface.class).getMeterial(body);
            call.enqueue(new Callback<GetMetrealModal>() {
                @Override
                public void onResponse(@NonNull Call<GetMetrealModal> call, @NonNull Response<GetMetrealModal> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayMetreal.addAll(response.body().getData());
                            setMetirealAdapter();
                        } else {
                            // Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetMetrealModal> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getCustomerList() {
        mArrayCustomer.clear();
        try {
            Common.showLoadingDialog(this, "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getCustomerListJson("-1", "1", "", "", "-1", "-1"));

            Call<GetCustomerListModel> call = RetrofitClient.createService(ApiInterface.class).getCustomerAPI(body);
            call.enqueue(new Callback<GetCustomerListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetCustomerListModel> call, @NonNull Response<GetCustomerListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayCustomer.addAll(response.body().getData());
                            setCustomerAdapter();
                        } else {
                            // Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetCustomerListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setCustomerAdapter() {
        try {
            CustomerName.add(0, "Select Customer");
            if (mArrayCustomer.size() > 0) {
                for (GetCustomerListModel.Datum Services : mArrayCustomer) {
                    CustomerName.add(Services.getName());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, CustomerName) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == SelectedCustomerIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerCustomer.setAdapter(adapter);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setMetirealAdapter() {
        try {
            Meterial.add(0, "Select Material");
            if (mArrayMetreal.size() > 0) {
                for (GetMetrealModal.DataItem Services : mArrayMetreal) {
                    Meterial.add(Services.getMaterial());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, Meterial) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == SelectedMerirealIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_metireal.setAdapter(adapter);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getSite(String customerID) {
        myArraySite.clear();
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getSiteListJson("-1", "1", "-1", customerID, "-1"
                    , "", ""));
            Call<GetSiteModel> call = RetrofitClient.createService(ApiInterface.class).getSiteAPI(body);
            call.enqueue(new Callback<GetSiteModel>() {
                @Override
                public void onResponse(@NonNull Call<GetSiteModel> call, @NonNull Response<GetSiteModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArraySite.addAll(response.body().getData());
                            setSiteAdapter();
                        } else {
                            myArraySite.clear();
                            setSiteAdapter();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetSiteModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    // Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setSiteAdapter() {
        SiteName.clear();
        try {
            SiteName.add(0, "Select Site");
            if (myArraySite.size() > 0) {
                for (GetSiteModel.Datum Services : myArraySite) {
                    SiteName.add(Services.getCompanyName());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, SiteName) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == SelectedSiteIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerSites.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getQuotation(String SiteID) {
        myArrayQuotation.clear();
        try {
            Common.showLoadingDialog(this, "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getQuotationJson("-1", "1", SiteID, "-1", "-1"));
            Call<GetQuotationListModel> call = RetrofitClient.createService(ApiInterface.class).getQuotationAPI(body);
            call.enqueue(new Callback<GetQuotationListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetQuotationListModel> call, @NonNull Response<GetQuotationListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayQuotation.addAll(response.body().getData());
                            setQuotationAdapter();

                        } else {
                            myArrayQuotation.clear();
                            setQuotationAdapter();
                            // Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetQuotationListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    // Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setQuotationAdapter() {
        QuotationName.clear();
        try {
            QuotationName.add(0, "Select Quotation");
            if (myArrayQuotation.size() > 0) {
                for (GetQuotationListModel.Datum Services : myArrayQuotation) {
                    QuotationName.add(Services.getQuotationName());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, QuotationName) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == SelectedQuotationIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerQuation.setAdapter(adapter);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setRegister() {
        mImageBack.setOnClickListener(v -> onBackPressed());

        addTraining.setOnClickListener(v -> {
            if (checkValidation()) {
                doAddInvoice();

            }
        });
    }

    private void doAddInvoice() {
        try {
            JSONArray jsonArray = getJsonArray();
            String convertedStartDate = Common.convertDateToServer(mActivity, edt_startDate.getText().toString());
            String convertedEndDate = Common.convertDateToServer(mActivity, edt_endDate.getText().toString());

            Common.showLoadingDialog(this, "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.addInvoice(UserID, CustomerID, SiteID, QuotationID, convertedStartDate, convertedEndDate, jsonArray));

            Call<AddSiteModel> call = RetrofitClient.createService(ApiInterface.class).doAddSite(body);
            call.enqueue(new Callback<AddSiteModel>() {
                @Override
                public void onResponse(@NonNull Call<AddSiteModel> call, @NonNull Response<AddSiteModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            finish();
                        } else {
                            // Common.setCustomToast(mActivity, mMessage);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddSiteModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    // Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkValidation() {
        boolean status = true;

        if (SelectedCustomerIndex == 0) {
            status = false;
            Toast.makeText(mActivity, "Please Select Customer", Toast.LENGTH_SHORT).show();
        }
        if (SelectedSiteIndex == 0) {
            status = false;
            Toast.makeText(mActivity, "Please Select Site", Toast.LENGTH_SHORT).show();
        }
        if (SelectedQuotationIndex == 0) {
            status = false;
            Toast.makeText(mActivity, "Please Select Quotation", Toast.LENGTH_SHORT).show();
        }
        if (SelectedMerirealIndex == 0) {
            status = false;
            Toast.makeText(mActivity, "Please Select Material", Toast.LENGTH_SHORT).show();
        }
        if (edt_rate.getText().toString().equals("") || edt_rate.getText().toString().isEmpty()) {
            status = false;
            Toast.makeText(mActivity, "Please Enter Rate", Toast.LENGTH_SHORT).show();
        }
        if (edt_quantity.getText().toString().equals("") || edt_quantity.getText().toString().isEmpty()) {
            status = false;
            Toast.makeText(mActivity, "Please Enter Quanitity", Toast.LENGTH_SHORT).show();
        }
        if (edt_uom.getText().toString().equals("") || edt_uom.getText().toString().isEmpty()) {
            status = false;
            Toast.makeText(mActivity, "Please Enter UOM", Toast.LENGTH_SHORT).show();
        }


        return status;
    }

    private void setData() {
        mTextHeader.setText("Add Invoice");
        mImageBack.setVisibility(View.VISIBLE);
    }


    /**
     * @return - JSON Array
     */
    private JSONArray getJsonArray() {
        JSONArray jsonArray = new JSONArray();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("MaterialID", MetirealID);
            jsonObject.put("UOM", edt_uom.getText().toString());
            jsonObject.put("Rate", edt_rate.getText().toString());
            jsonObject.put("Qty", edt_quantity.getText().toString());

            jsonArray.put(jsonObject);

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
        return jsonArray;
    }


    private void getID() {
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        edt_startDate = findViewById(R.id.edt_startDate);
        edt_endDate = findViewById(R.id.edt_endDate);
        edt_quantity = findViewById(R.id.edt_quantity);
        edt_rate = findViewById(R.id.edt_rate);
        edt_uom = findViewById(R.id.edt_uom);

        addTraining = findViewById(R.id.button_submit_training);

        spinnerCustomer = findViewById(R.id.spinner_customer);
        spinnerSites = findViewById(R.id.spinner_sites);
        spinnerQuation = findViewById(R.id.spinner_quotation);
        spinner_metireal = findViewById(R.id.spinner_metireal);
        spinnerCustomer.setOnItemSelectedListener(onItemSelectedListener);
        spinnerSites.setOnItemSelectedListener(onItemSelectedListener);
        spinnerQuation.setOnItemSelectedListener(onItemSelectedListener);
        spinner_metireal.setOnItemSelectedListener(onItemSelectedListener);

        Date c = Calendar.getInstance().getTime();
        String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        formattedDate = df.format(c);

        edt_startDate.setText(formattedDate);
        edt_endDate.setText(formattedDate);


        edt_startDate.setOnClickListener(v -> {
            // Get Current Date
            final Calendar c2 = Calendar.getInstance();
            int mYear = c2.get(Calendar.YEAR);
            int mMonth = c2.get(Calendar.MONTH);
            int mDay = c2.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(AddInvoiceActivity.this,
                    (view1, year, monthOfYear, dayOfMonth) -> edt_startDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        });

        edt_endDate.setOnClickListener(v -> {
            // Get Current Date
            final Calendar c1 = Calendar.getInstance();
            int mYear = c1.get(Calendar.YEAR);
            int mMonth = c1.get(Calendar.MONTH);
            int mDay = c1.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(AddInvoiceActivity.this,
                    (view12, year, monthOfYear, dayOfMonth) -> edt_endDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            datePickerDialog.show();
        });

    }
}
