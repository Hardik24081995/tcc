package com.tcc.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.ReportUniformAdapter;
import com.tcc.Model.ReportUniformModal;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportUniformActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ArrayList<ReportUniformModal.DataItem> arrayListResponse;
    String ReminderID;
    RelativeLayout relativeLayout;
    private TextView mTextHeader;
    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_response);

        arrayListResponse = new ArrayList<>();
        SessionManager manager = new SessionManager(getApplicationContext());
        ReminderID = manager.getPreferences("ReminderID", "");

        getID();
        setRegister();
        getResponse();

    }

    private void getID() {
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        recyclerView = findViewById(R.id.recycler_view_response);
        relativeLayout = findViewById(R.id.relative_no_data_available);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorBlack);
    }

    @SuppressLint("SetTextI18n")
    private void setRegister() {
        mTextHeader.setText("Uniform");
        mImageBack.setVisibility(View.VISIBLE);

        mImageBack.setOnClickListener(v -> finish());

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            arrayListResponse.clear();
            getResponse();
            mSwipeRefreshLayout.setRefreshing(false);
        });

    }

    private void getResponse() {
        try {

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getReportUniformJson("-1", "1", getIntent().getStringExtra("sDate"), getIntent().getStringExtra("lDate")));
            Common.showLoadingDialog(this, "Loading");
            Call<ReportUniformModal> call = RetrofitClient.createService(ApiInterface.class).getReportUniformAPI(body);
            call.enqueue(new Callback<ReportUniformModal>() {
                @Override
                public void onResponse(@NonNull Call<ReportUniformModal> call, @NonNull Response<ReportUniformModal> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            if (response.body() != null) {
                                arrayListResponse.addAll(response.body().getData());
                                setAdapter();
                            } else {
                                relativeLayout.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                            }

                        } else {
                            relativeLayout.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ReportUniformModal> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(getApplicationContext(), t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setAdapter() {

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        ReportUniformAdapter visitorResponseAdapter = new ReportUniformAdapter(this, arrayListResponse);
        recyclerView.setAdapter(visitorResponseAdapter);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (isTaskRoot()) {
            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        }
        finish();
    }
}

