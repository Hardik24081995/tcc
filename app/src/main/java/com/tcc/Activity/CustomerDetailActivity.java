package com.tcc.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.tcc.R;
import com.tcc.fragment.CustomerInfoProfileFragment;
import com.tcc.fragment.CustomerProcessFragment;
import com.tcc.fragment.CustomerSiteFragment;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

public class CustomerDetailActivity extends AppCompatActivity {

    String Name, Mobile, Address, Email, CompanyName;
    String CustomerID;
    private Activity mActivity;
    private TextView mTextName, mTextMobile, mTextHeader;
    private ImageView mImageProfile;
    private ImageView mImageBack;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private ImageView img_add;
    /**
     * Page Change listener
     */
    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (mViewPager.getCurrentItem() == 0) {
                img_add.setVisibility(View.GONE);
            } else if (mViewPager.getCurrentItem() == 2) {
                img_add.setVisibility(View.GONE);
            } else {
                img_add.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_detail);
        mActivity = CustomerDetailActivity.this;

        getIds();
        setViewPager();
        getData();
        setData();
        setRegListeners();

    }

    /**
     * get IDs
     */
    private void getIds() {
        mTextName = findViewById(R.id.text_customer_profile_name);
        mTextHeader = findViewById(R.id.headerCustomer);

        mTextMobile = findViewById(R.id.text_customer_profile_phone);
        mImageProfile = findViewById(R.id.image_customer_profile_pic);
        img_add = findViewById(R.id.img_add);
        img_add.setVisibility(View.GONE);
        mTabLayout = findViewById(R.id.tab_layout_customer_profile);
        mViewPager = findViewById(R.id.view_pager_customer_profile);
        mImageBack = findViewById(R.id.img_back);

    }

    private void getData() {
        SessionManager manager = new SessionManager(getApplicationContext());
        Name = manager.getPreferences("Name", "");
        Mobile = manager.getPreferences("Mobile", "");
        Address = manager.getPreferences("Address", "");
        Email = manager.getPreferences("Email", "");
        CompanyName = manager.getPreferences("CompanyName", "");
        Intent intent = getIntent();
        CustomerID = intent.getStringExtra("CustomerID");
    }

    /**
     * Sets up the data
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            mImageProfile.setImageDrawable(Common.setLabeledImageView(mActivity, Name,
                    ""));
            mTextHeader.setText(CompanyName);
            mTextName.setText(Name);
            mTextMobile.setText(Mobile);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: On click listener
            // mImageAdd.setOnClickListener(clickListener);

            // ToDo: View pager page change listener
            mViewPager.addOnPageChangeListener(pageChangeListener);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mImageBack.setOnClickListener(v -> onBackPressed());

        img_add.setOnClickListener(v -> {
            Intent intent = new Intent(CustomerDetailActivity.this, AddSiteActivity.class);
            intent.putExtra("CustomerID", CustomerID);
            startActivity(intent);
        });
    }

    /**
     * Sets up the view pager
     */
    private void setViewPager() {
        try {
            ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
            adapter.addFragment(new CustomerInfoProfileFragment(), getString(R.string.txt_information));
            adapter.addFragment(new CustomerSiteFragment(), "Sites");
            adapter.addFragment(new CustomerProcessFragment(), "Process");

            mViewPager.setOffscreenPageLimit(2);
            mViewPager.setAdapter(adapter);
            mTabLayout.setupWithViewPager(mViewPager);

            mTabLayout.getTabAt(0).setIcon(R.drawable.user_white);
            mTabLayout.getTabAt(1).setIcon(R.drawable.building_white);
            mTabLayout.getTabAt(2).setIcon(R.drawable.ic_process);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * View pager adapter to bind the tab data
     */
    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        List<Fragment> mFragmentList = new ArrayList<>();
        List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
