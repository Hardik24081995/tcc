package com.tcc.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.tcc.Model.AddTrainingModel;
import com.tcc.Model.EmployeeTrainingSpinnerModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddTrainingActivity extends AppCompatActivity {

    ArrayList<EmployeeTrainingSpinnerModel.Datum> mArrayListTraining = new ArrayList<>();
    ArrayList<String> traingName = new ArrayList<>();
    int SelectedTrainingIndex = 0;
    String TrainingID = "";
    SessionManager mSessionManager;
    String UserID, EmployeeID, DateTimeID;
    private Activity mActivity;
    private ImageView mImageBack;
    private TextView mTextHeader;
    private Spinner spinnerTraining;
    private Button addTraining;

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinner_traininglist:
                    SelectedTrainingIndex = adapterView.getSelectedItemPosition();

                    if (SelectedTrainingIndex != 0) {
                        TrainingID = String.valueOf((Integer.parseInt(mArrayListTraining.get(SelectedTrainingIndex - 1).getTrainingDateTimeID())));
                        String mTrainingName = (mArrayListTraining.get(SelectedTrainingIndex - 1).getTraining());
                        Common.insertLog("mStateId::> " + TrainingID);
                        Common.insertLog("mStateName::> " + mTrainingName);
                    }
                    break;


            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_training);

        mActivity = AddTrainingActivity.this;

        mSessionManager = new SessionManager(mActivity);
        String UserData = mSessionManager.getPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);
        EmployeeID = mSessionManager.getPreferences(AppConstants.EMPLOYEE_ID_FOR_ADD, "");
        DateTimeID = mSessionManager.getPreferences("TimeID", "");

        getID();
        setData();
        getTraining();
        setRegister();

    }

    private void getTraining() {
        try {

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getTrainingSpinnerJson());
            Common.showLoadingDialog(this, "Loading");
            Call<EmployeeTrainingSpinnerModel> call = RetrofitClient.createService(ApiInterface.class).getEmployeeTrainingListAPI(body);
            call.enqueue(new Callback<EmployeeTrainingSpinnerModel>() {
                @Override
                public void onResponse(@NonNull Call<EmployeeTrainingSpinnerModel> call, @NonNull Response<EmployeeTrainingSpinnerModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayListTraining.addAll(response.body().getData());
                            setTrainingAdapter();
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<EmployeeTrainingSpinnerModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //   Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTrainingAdapter() {
        try {
            traingName.add(0, "Select Training");
            if (mArrayListTraining.size() > 0) {
                for (EmployeeTrainingSpinnerModel.Datum Services : mArrayListTraining) {
                    traingName.add(Services.getTraining() + " " + Services.getTrainingDate() + " " + Services.getTrainingTime());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, traingName) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == SelectedTrainingIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerTraining.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setRegister() {
        mImageBack.setOnClickListener(v -> onBackPressed());

        addTraining.setOnClickListener(v -> {
            if (checkValidation()) {
                doAddTraining();
            }
        });
    }

    private void doAddTraining() {

        try {
            String UserIDforAdd = mSessionManager.getPreferences(AppConstants.EMPLOYEE_ID_FOR_ADD, "");
            Common.showLoadingDialog(this, "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.addEmployeeTrainingJson(UserIDforAdd, TrainingID, EmployeeID));

            Call<AddTrainingModel> call = RetrofitClient.createService(ApiInterface.class).addEmployeeTrainingAPI(body);
            call.enqueue(new Callback<AddTrainingModel>() {
                @Override
                public void onResponse(@NonNull Call<AddTrainingModel> call, @NonNull Response<AddTrainingModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            finish();
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddTrainingModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //   Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private boolean checkValidation() {
        boolean status = true;

        if (TrainingID == null) {
            status = false;
            Toast.makeText(mActivity, "Please Select Training", Toast.LENGTH_SHORT).show();
        }
        return status;
    }

    private void setData() {
        mTextHeader.setText("TRAINING");
        mImageBack.setVisibility(View.VISIBLE);
    }

    private void getID() {
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);

        addTraining = findViewById(R.id.button_submit_training);

        spinnerTraining = findViewById(R.id.spinner_traininglist);
        spinnerTraining.setOnItemSelectedListener(onItemSelectedListener);

    }
}
