package com.tcc.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.PenaltyListAdapter;
import com.tcc.Model.PenaltyListModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PenaltyListingActivity extends AppCompatActivity {

    Activity mActivity;
    PenaltyListAdapter mCustomerListAdapter;
    ArrayList<PenaltyListModel.Datum> datumArrayList = new ArrayList<>();
    private TextView mTextHeader;
    private ImageView mImageBack, mImageAdd;
    private RecyclerView mRecyclerView;
    private RelativeLayout mRelativeNoDataFound;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean isBackFromB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_penalty_listing);

        mActivity = PenaltyListingActivity.this;
        isBackFromB = false;

        getID();
        setData();
        getPenalty();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getPenalty();
            mSwipeRefreshLayout.setRefreshing(false);
        });


    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        //Refresh your stuff here
        if (isBackFromB) {
            //Refresh your stuff here
            getPenalty();
        }
    }

    private void getPenalty() {
        try {
            datumArrayList.clear();
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getPenatyJson("-1", "1"));
            Common.showLoadingDialog(this, "Loading");
            Call<PenaltyListModel> call = RetrofitClient.createService(ApiInterface.class).getPenaltyAPI(body);
            call.enqueue(new Callback<PenaltyListModel>() {
                @Override
                public void onResponse(@NonNull Call<PenaltyListModel> call, @NonNull Response<PenaltyListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            //  Common.setCustomToast(mActivity, mMessage);
                            datumArrayList.addAll(response.body().getData());

                            if (datumArrayList.isEmpty()) {
                                mRelativeNoDataFound.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                mRelativeNoDataFound.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                                setAdapter();
                            }
                        } else {
                            // Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PenaltyListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mCustomerListAdapter = new PenaltyListAdapter(mActivity, datumArrayList);
        mRecyclerView.setAdapter(mCustomerListAdapter);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void setData() {
        mTextHeader.setText("PENALTY");

        mImageBack.setVisibility(View.VISIBLE);
        mImageBack.setOnClickListener(v -> onBackPressed());

        mImageAdd.setVisibility(View.VISIBLE);
        mImageAdd.setOnClickListener(v -> {
            Intent intent = new Intent(PenaltyListingActivity.this, PenaltyActivity.class);
            startActivity(intent);
        });
    }

    private void getID() {
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        mImageAdd = findViewById(R.id.image_custom_toolbar_add);

        mRecyclerView = findViewById(R.id.rv_penaltyListing);
        mRelativeNoDataFound = findViewById(R.id.relative_no_data_available);
        mSwipeRefreshLayout = findViewById(R.id.swipeToRefresh);
    }
}
