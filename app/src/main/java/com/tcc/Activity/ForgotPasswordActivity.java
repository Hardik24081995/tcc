package com.tcc.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.tcc.Model.ForgotPasswordModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private ImageView imgback;
    private EditText mEditEmail;
    private Button mButtonSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        getId();
        setRegister();

//        imgback.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
    }

    private void getId() {
//        imgback = findViewById(R.id.img_back);
        mEditEmail = findViewById(R.id.forgot_email);
        mButtonSubmit = findViewById(R.id.button_forgot_password_submit);
    }

    private void setRegister() {
        mButtonSubmit.setOnClickListener(v -> {
            if (mEditEmail.getText().toString().isEmpty()) {
                mEditEmail.setError("Enter Phone");
//
//                if (!mEditEmail.getText().toString().matches(emailPattern)) {
//                    mEditEmail.setError("Enter Valid Email");
//                }
            } else {
                ForgotPasswordApi();
            }
        });
    }

    private void ForgotPasswordApi() {
        try {
            Common.showLoadingDialog(this, "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.setForgotPasswordJson(mEditEmail.getText().toString().trim()));

            Call<ForgotPasswordModel> call = RetrofitClient.createService(ApiInterface.class).getPasswordAPI(body);

            call.enqueue(new Callback<ForgotPasswordModel>() {
                @Override
                public void onResponse(Call<ForgotPasswordModel> call, Response<ForgotPasswordModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                            startActivity(intent);

                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ForgotPasswordModel> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
