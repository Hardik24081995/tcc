package com.tcc.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.tcc.R;
import com.tcc.fragment.AboutUsFragment;
import com.tcc.fragment.ChangePasswordFragment;
import com.tcc.fragment.NotificationFragment;
import com.tcc.fragment.PrivacyPolicyFragment;
import com.tcc.fragment.TermsFragment;
import com.tcc.utils.AppConstants;

public class AboutUsActivity extends AppCompatActivity {

    private Activity mActivity;
    private String Title;
    private ImageView mImageBack;
    private TextView mTextTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        mActivity = AboutUsActivity.this;

        getBundle();
        getIds();
        setData();
        setRegister();
    }


    /**
     * Set Bundle
     */
    private void getBundle() {
        if (getIntent() != null) {
            Title = getIntent().getStringExtra(AppConstants.TITLE);
        }
    }

    private void getIds() {
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        mTextTitle = findViewById(R.id.text_custom_toolbar_without_back_header);
        mTextTitle.setTextColor(mActivity.getResources().getColor(R.color.color_header_text));
        mImageBack.setVisibility(View.VISIBLE);
    }

    private void setData() {
        mTextTitle.setText(Title);

        if (Title.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_change_password))) {
            changeFragment(new ChangePasswordFragment(),
                    mActivity.getResources().getString(R.string.text_change_password));

        } else if (Title.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_notification))) {
            changeFragment(new NotificationFragment(),
                    mActivity.getResources().getString(R.string.text_notification));

        } else if (Title.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_term_policy))) {
            changeFragment(new TermsFragment(), mActivity.getResources().getString(R.string.text_term_policy));

        } else if (Title.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_privancy_polices))) {
            changeFragment(new PrivacyPolicyFragment(), mActivity.getResources().getString(R.string.text_privancy_polices));

        } else if (Title.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_about))) {
            changeFragment(new AboutUsFragment(), mActivity.getResources().getString(R.string.text_about));
        }
    }

    private void changeFragment(Fragment fragment, String title) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_about_us_container, fragment);
        fragmentTransaction.commit();
    }

    /**
     * Set Register
     */
    private void setRegister() {
        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
