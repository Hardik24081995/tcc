package com.tcc.Activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.tcc.R;
import com.tcc.fragment.VisitorInfoFragment;
import com.tcc.fragment.VisitorReminderFragment;
import com.tcc.fragment.VisitorSitesFragment;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

public class VisitorDetailActivity extends AppCompatActivity {

    String visitorid;
    private ImageView mImageBack, mImageProfile;
    private Activity mActivity;
    private TextView mTextName, mTextPhone, mTextHeader;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageView mImageAdd, visitortocustomer;
    private String Name, CompanyName, Mobile, Services, ProposedDate, WorkingDays, WorkingHours, NoOfMen, Address, Email, Lead;

    private int[] tabIcons = {
            R.drawable.user_white,
            R.drawable.ic_reminder,
            R.drawable.building_white
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_detail);
        mActivity = VisitorDetailActivity.this;

        getID();
        getData();
        setData();
        setRegister();

        mImageAdd.setVisibility(View.GONE);

        Intent intent = getIntent();
        visitorid = intent.getStringExtra("VisitorID");

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

    }

    private void getID() {
        mImageBack = findViewById(R.id.img_back);
        mTextHeader = findViewById(R.id.headerVisitor);
        mTextName = findViewById(R.id.text_visitor_profile_name);
        mTextPhone = findViewById(R.id.text_visitor_profile_phone);
        mImageProfile = findViewById(R.id.image_visitor_profile_pic);
        mImageAdd = findViewById(R.id.img_add);
        visitortocustomer = findViewById(R.id.visitortocustomer);
        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.visitorTab);
    }

    private void getData() {
        SessionManager manager = new SessionManager(this);
        Name = manager.getPreferences("Name", "");
        CompanyName = manager.getPreferences("CompanyName", "");
        Mobile = manager.getPreferences("Mobile", "");
        Services = manager.getPreferences("Service", "");
        ProposedDate = manager.getPreferences("ProposedDate", "");
        WorkingDays = manager.getPreferences("WorkingDays", "");
        WorkingHours = manager.getPreferences("WorkingHours", "");
        NoOfMen = manager.getPreferences("NoOfMen", "");
        Address = manager.getPreferences("Address", "");
        Email = manager.getPreferences("Email", "");
        Lead = manager.getPreferences("LeadType", "");

        mImageProfile.setImageDrawable(Common.setLabeledImageView(mActivity, manager.getPreferences("Name", ""),
                ""));

    }

    private void setData() {
        mTextHeader.setText(CompanyName);
        mTextPhone.setText(Mobile);
        mTextName.setText(Name);
    }

    private void setRegister() {
        visitortocustomer.setOnClickListener(v -> {
            Intent intent = new Intent(VisitorDetailActivity.this, QuotationActivity.class);
            startActivity(intent);
        });

        mImageAdd.setOnClickListener(v -> {
            callActivity();
        });

        mTextPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + Mobile));
                startActivity(intent);
                finish();
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if (viewPager.getCurrentItem() == 0) {
                    mImageAdd.setVisibility(View.GONE);
                } else {
                    mImageAdd.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        mImageBack.setOnClickListener(v -> onBackPressed());
    }

    private void callActivity() {
        try {
            // Is Property is canceled then don't allow to add

            if (viewPager.getCurrentItem() == 1) {
                /*if (mSharedPreferences.getAppProjectData().getPayment().getIs_insert() == 1) {

                } else {
                    Toast.makeText(PropertyDetailActivity.this, getString(R.string.txt_not_eligible), Toast.LENGTH_SHORT).show();
                }*/
                CallToAddReminder();
            } else if (viewPager.getCurrentItem() == 2) {
                /*
                 if (mSharedPreferences.getAppProjectData().getDocument().getIs_insert() == 1) {
                    callUploadDocumentActivity();
                } else {
                    Toast.makeText(PropertyDetailActivity.this, getString(R.string.txt_not_eligible), Toast.LENGTH_SHORT).show();
                }*/
                callToAddVisitorSites();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callToAddVisitorSites() {
        Intent intent = new Intent(mActivity, AddSiteActivity.class);
        intent.putExtra("VisitorID", visitorid);
        startActivity(intent);
    }

    private void CallToAddReminder() {
        Intent intent = new Intent(mActivity, AddReminderActivity.class);
        startActivity(intent);
    }

    private void setupTabIcons() {

        tabLayout.getTabAt(0).setIcon(R.drawable.user_white);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);

    }

    private void setupViewPager(ViewPager viewPager) {
        VisitorDetailActivity.ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new VisitorInfoFragment(), "Information");
        adapter.addFragment(new VisitorReminderFragment(), "Reminder");
        adapter.addFragment(new VisitorSitesFragment(), "Sites");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
