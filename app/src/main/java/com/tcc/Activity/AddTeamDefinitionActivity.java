package com.tcc.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.tcc.Model.GetEmployeListModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddTeamDefinitionActivity extends AppCompatActivity {

    Activity mActivity;
    TextInputLayout fromSiteWrapper, toSiteWrapper;
    EditText edt_startDate, edt_endDate, edtFromSite, edtToSite;
    Spinner spinnerEmployee;
    RadioGroup rg_teamdefinition;
    RadioButton rb_new, rb_suffle, rb_rokdi;
    String formattedDate;
    //    String EmployeeID = "", EmployeeName = "";
    int EmployeeID = 0;
    String EmployeeName;
    ArrayList<GetEmployeListModel.Datum> mArrayEmployee = new ArrayList<>();
    ArrayList<String> mArray = new ArrayList<>();
    private ImageView mImageBack, img_plus, img_remove_view;
    private TextView mTextHeader;
    private Button AddView, mButtonSubmit;
    private TextView tvtoSite, tvFormSite, tvEmployee, tvStart, tvEnd;
    private ImageView img_delete;
    private LinearLayout mLinearAddContainer;
    private LinearLayout mLinearContainer;
    private int mYear, mMonth, mDay;
    private int selectedSpinnerService = 0;
    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinnerEmployee:
                    selectedSpinnerService = adapterView.getSelectedItemPosition();

                    if (selectedSpinnerService != 0) {
                        EmployeeID = Integer.parseInt(mArrayEmployee.get(selectedSpinnerService - 1).getUserID());
                        EmployeeName = String.valueOf((Integer.parseInt(mArrayEmployee.get(selectedSpinnerService - 1).getFirstName())));
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_team_definition);

        mActivity = AddTeamDefinitionActivity.this;

        getID();
        setData();
        getEmployee();
        setRegister();
    }

    private void getEmployee() {
        try {

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getEmployeeListJson("-1", "1", "", "", "-1"));
            Common.showLoadingDialog(this, "Loading");
            Call<GetEmployeListModel> call = RetrofitClient.createService(ApiInterface.class).getEmployeAPI(body);
            call.enqueue(new Callback<GetEmployeListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetEmployeListModel> call, @NonNull Response<GetEmployeListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayEmployee.addAll(response.body().getData());


                            if (mArrayEmployee.size() > 0) {
                                for (int a = 0; a < mArrayEmployee.size(); a++) {
                                    mArray.add(mArrayEmployee.get(a).getFirstName());
                                }
                            }
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetEmployeListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    // Common.setCustomToast(mActivity, t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSpinnerAdapter() {

        ArrayAdapter projectAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, mArray);
        projectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEmployee.setAdapter(projectAdapter);
    }

    private void setData() {
        Date c = Calendar.getInstance().getTime();
        String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        formattedDate = df.format(c);
    }

    private void setRegister() {
        img_plus.setOnClickListener(v -> AddItems());

        mTextHeader.setText("TEAM DEFINITION");
        mImageBack.setVisibility(View.VISIBLE);

        mImageBack.setOnClickListener(v -> onBackPressed());

        rg_teamdefinition.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton radioButton = AddTeamDefinitionActivity.this.findViewById(checkedId);

                if (radioButton.getId() == R.id.rb_suffle) {
                    toSiteWrapper.setVisibility(View.VISIBLE);
                    fromSiteWrapper.setVisibility(View.VISIBLE);
                } else {
                    fromSiteWrapper.setVisibility(View.GONE);
                    toSiteWrapper.setVisibility(View.GONE);
                }
            }
        });

        mButtonSubmit.setOnClickListener(v -> {
            if (checkValidation()) {
                doSubmitTeam();
            }
        });

    }

    private void getID() {
        mLinearContainer = findViewById(R.id.linear_quotation_items);
        mLinearAddContainer = findViewById(R.id.linear_add_items);

        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);

        rg_teamdefinition = findViewById(R.id.rg_teamDefinition);
        rb_new = findViewById(R.id.rb_new);
        rb_suffle = findViewById(R.id.rb_suffle);
        rb_rokdi = findViewById(R.id.rb_rokdi);

        img_plus = findViewById(R.id.img_plus);

        mButtonSubmit = findViewById(R.id.submit_team);
    }

    private void AddItems() {

        final View view = LayoutInflater.from(AddTeamDefinitionActivity.this).inflate(R.layout.layout_containerdefinition, null,
                false);

        edt_startDate = view.findViewById(R.id.edt_startDate);
        edt_endDate = view.findViewById(R.id.edt_endDate);
        edtFromSite = view.findViewById(R.id.edtFromSite);
        edtToSite = view.findViewById(R.id.edtToSite);

        AddView = view.findViewById(R.id.AddView);
        AddView.setOnClickListener(v -> AddLayout());

        fromSiteWrapper = view.findViewById(R.id.fromSiteWrapper);
        toSiteWrapper = view.findViewById(R.id.toSiteWrapper);
        spinnerEmployee = view.findViewById(R.id.spinnerEmployee);
        spinnerEmployee.setOnItemSelectedListener(onItemSelectedListener);
        img_remove_view = view.findViewById(R.id.img_remove_view);

        edt_startDate.setText(formattedDate);

        edt_startDate.setOnClickListener(v -> {
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(AddTeamDefinitionActivity.this,
                    (view1, year, monthOfYear, dayOfMonth) -> edt_startDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
            datePickerDialog.show();
        });

        edt_endDate.setOnClickListener(v -> {
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(AddTeamDefinitionActivity.this,
                    (view12, year, monthOfYear, dayOfMonth) -> edt_endDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
            datePickerDialog.show();
        });

        img_remove_view.setOnClickListener(v -> mLinearContainer.removeView(view));
        setSpinnerAdapter();
        mLinearContainer.addView(view);
    }

    private void AddLayout() {

        final View view = LayoutInflater.from(AddTeamDefinitionActivity.this).inflate(R.layout.layout_addview, null,
                false);

        img_delete = view.findViewById(R.id.img_delete);
        tvStart = view.findViewById(R.id.tvStart);
        tvEnd = view.findViewById(R.id.tvEnd);
        tvEmployee = view.findViewById(R.id.tvEmployee);
        tvtoSite = view.findViewById(R.id.tvToSite);
        tvFormSite = view.findViewById(R.id.tvFromSite);

//        tvStart.setText("Start Date: " + edt_startDate.getText().toString());
//        tvEnd.setText("End Date: " + edt_endDate.getText().toString());
//        tvEmployee.setText("Employee Name: " + EmployeeName);
//        tvtoSite.setText("To Site: " + edtToSite.getText().toString());
//        tvFormSite.setText("From Site: " + edtFromSite.getText().toString());

        img_delete.setOnClickListener(v -> mLinearAddContainer.removeView(view));

        mLinearAddContainer.addView(view);

    }

    private boolean checkValidation() {
        boolean status = false;

        return status;
    }

    private void doSubmitTeam() {
    }
}
