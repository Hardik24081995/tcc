package com.tcc.Activity;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;
import com.tcc.Model.GetModuleIDList;
import com.tcc.Model.RoleModal;
import com.tcc.R;
import com.tcc.fragment.CustomerFragment;
import com.tcc.fragment.EmployeFragment;
import com.tcc.fragment.HomeFragment;
import com.tcc.fragment.InspectionFragment;
import com.tcc.fragment.InvoiceFragment;
import com.tcc.fragment.MenuAttendanceFragment;
import com.tcc.fragment.PenaltyListFragment;
import com.tcc.fragment.ProfileTabFragment;
import com.tcc.fragment.ReportsFragment;
import com.tcc.fragment.SettingFragment;
import com.tcc.fragment.SiteFragment;
import com.tcc.fragment.VisitorFragment;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.WebFields;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    CircleImageView image_nav_header_profile;
    FloatingActionButton floating_button_nav_header_logout;
    SessionManager mSessionManager;
    String ProPic;
    String UserData;
    GetModuleIDList moduleIDList;
    RoleModal roleModal;
    private Activity mActivity;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mActionToggle;
    private NavigationView navigationView;
    private Toolbar mtoolbar;
    private TextView mTextHeader, text_nav_header_user_name;
    private boolean isEditMenuHide = true;
    private ImageView mImageAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mActivity = HomeActivity.this;
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_content_frame, new HomeFragment()).commit();

        mSessionManager = new SessionManager(mActivity);
        UserData = mSessionManager.getPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
        ProPic = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_PHOTOURL);
        roleModal = mSessionManager.getRole();

        getIds();
        setNavigation();
        setRegister();
        hideItem();


    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Exit");
        builder.setMessage("Would you like to exit from the App?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    /**
     * This method declare Widgets ID
     */
    private void getIds() {
        mtoolbar = (Toolbar) findViewById(R.id.toolbar_nav_drawer);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mActionToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mtoolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        mTextHeader = findViewById(R.id.text_nav_header);

        mImageAdd = findViewById(R.id.image_app_bar_tool_add);


        // ToDo: Sets the tool bar header properties
        mTextHeader.setText(getResources().getString(R.string.nav_menu_home));
        mTextHeader.setSelected(true);

        // ToDo: Sets the action bar
        setSupportActionBar(mtoolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        View hView = navigationView.getHeaderView(0);

        floating_button_nav_header_logout = hView.findViewById(R.id.floating_button_nav_header_logout);
        FloatingActionButton floating_button_nav_header_setting = hView.findViewById(R.id.floating_button_nav_header_setting);
        image_nav_header_profile = hView.findViewById(R.id.image_nav_header_profile);
        text_nav_header_user_name = hView.findViewById(R.id.text_nav_header_user_name);

        String name = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_FIRST_NAME);
        String lname = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_LAST_NAME);
        text_nav_header_user_name.setText(name);
        if (ProPic != null && !ProPic.equals("")) {
            Picasso.with(mActivity)
                    .load(ProPic)
                    .placeholder(R.drawable.demo_user_pic)
                    .into(image_nav_header_profile);
        } else {
            //  image_nav_header_profile.setImageDrawable(Common.setLabeledImageView(this, name, lname));
        }
        floating_button_nav_header_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
                changeFragment(new SettingFragment(), mActivity.getResources().getString(R.string.nav_menu_settings));
                mImageAdd.setVisibility(View.GONE);
            }
        });


        image_nav_header_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
                changeFragment(new ProfileTabFragment(),
                        mActivity.getResources().getString(R.string.nav_menu_profile));
                mImageAdd.setVisibility(View.GONE);
            }
        });


        text_nav_header_user_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
                changeFragment(new ProfileTabFragment(),
                        mActivity.getResources().getString(R.string.nav_menu_profile));
                mImageAdd.setVisibility(View.GONE);
            }
        });

        floating_button_nav_header_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        HomeActivity.this);
//              builder.setTitle("Sample Alert");
                builder.setMessage("Would you like to logout from the App?");
                builder.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {

                            }
                        });
                builder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                SessionManager sessionManager = new SessionManager(HomeActivity.this);
                                sessionManager.clearAllPreferences();
                                sessionManager.clearAllPreferences(sessionManager.KEY_LOGIN_USER_DATA);
                                sessionManager.clearAllPreferences(sessionManager.KEY_CONFIGURATION_DATA);

                                sessionManager.setPreferences("IsFirst", false);

                                Intent intent = new Intent();
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                mActivity.finishAffinity();
                                startActivity(new Intent(mActivity, LoginActivity.class));
                            }
                        });
                builder.show();
            }
        });


    }


    private void setNavigation() {
        mActionToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mtoolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
                hideKeyboard();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
                //hideKeyboard();
            }
        };

        mDrawerLayout.setDrawerListener(mActionToggle);
        mActionToggle.syncState();
    }

    /**
     * Set Register
     */
    private void setRegister() {
        mImageAdd.setOnClickListener(v -> {
            String header = mTextHeader.getText().toString();
            if (header.equals(mActivity.getResources().getString(R.string.nav_menu_visitor))) {

               /* for (int i = 0; i < roleList.getData().size(); i++) {
                    if (roleList.getData().get(i).getModuleName().equals("Employee")) {
                        if (roleList.getData().get(i).getIsInsert().equals("1")) {
                            Intent intent = new Intent(mActivity, AddEmployeeActivity.class);
                            startActivity(intent);
                        }else {
                            Common.setCustomToast(mActivity, "You dont have access");
                        }
                    }
                }*/

                Intent intent = new Intent(mActivity, AddVisitorActivity.class);
                startActivity(intent);
            } else if (header.equals(mActivity.getResources().getString(R.string.nav_menu_employee))) {

                if (roleModal.getData().getEmployee().getIsInsert().equals("1")) {
                    Intent intent = new Intent(mActivity, AddEmployeeActivity.class);
                    startActivity(intent);
                } else {
                    Common.setCustomToast(mActivity, getString(R.string.not_access));
                }


            } else if (header.equals(mActivity.getResources().getString(R.string.nav_menu_sites))) {
                mImageAdd.setVisibility(View.GONE);
            } else if (header.equals(mActivity.getResources().getString(R.string.nav_menu_survey))) {
                Intent intent = new Intent(mActivity, AddInspectionActivity.class);
                startActivity(intent);
            } else if (header.equals(mActivity.getResources().getString(R.string.nav_menu_penalty))) {
                Intent intent = new Intent(mActivity, PenaltyActivity.class);
                startActivity(intent);
            } else if (header.equals(mActivity.getResources().getString(R.string.nav_menu_invoice))) {
                Intent intent = new Intent(mActivity, AddInvoiceActivity.class);
                startActivity(intent);
            }

        });
    }

    /**
     * This method is used to hide the keyboard.
     */
    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        isEditMenuHide = true;

        switch (menuItem.getItemId()) {
            case R.id.nav_customer:
                drawer.closeDrawer(GravityCompat.START);
                changeFragment(new CustomerFragment(),
                        mActivity.getResources().getString(R.string.nav_menu_customer));
                mImageAdd.setVisibility(View.GONE);
                break;
            case R.id.nav_home:
                drawer.closeDrawer(GravityCompat.START);
                changeFragment(new HomeFragment(),
                        mActivity.getResources().getString(R.string.nav_menu_home));
                mImageAdd.setVisibility(View.GONE);
                break;
            case R.id.nav_profile:
                drawer.closeDrawer(GravityCompat.START);
                changeFragment(new ProfileTabFragment(),
                        mActivity.getResources().getString(R.string.nav_menu_profile));
                mImageAdd.setVisibility(View.GONE);
                break;
            case R.id.nav_visitor:
                drawer.closeDrawer(GravityCompat.START);
                changeFragment(new VisitorFragment(),
                        mActivity.getResources().getString(R.string.nav_menu_visitor));
                mImageAdd.setVisibility(View.VISIBLE);
                break;
            case R.id.nav_survey:
                drawer.closeDrawer(GravityCompat.START);
                changeFragment(new InspectionFragment(), mActivity.getResources().getString(R.string.nav_menu_survey));
                mImageAdd.setVisibility(View.VISIBLE);
                break;
            case R.id.nav_booking:
                drawer.closeDrawer(GravityCompat.START);
                changeFragment(new SettingFragment(), mActivity.getResources().getString(R.string.nav_menu_settings));
                mImageAdd.setVisibility(View.GONE);
                break;
            case R.id.nav_Employee:
                drawer.closeDrawer(GravityCompat.START);
                changeFragment(new EmployeFragment(), mActivity.getResources().getString(R.string.nav_menu_employee));
                mImageAdd.setVisibility(View.VISIBLE);
                break;
            case R.id.nav_attendance:
                drawer.closeDrawer(GravityCompat.START);
                changeFragment(new MenuAttendanceFragment(), mActivity.getResources().getString(R.string.nav_menu_attendance));
                mImageAdd.setVisibility(View.GONE);
                break;
            case R.id.nav_penalty:
                drawer.closeDrawer(GravityCompat.START);
                changeFragment(new PenaltyListFragment(), mActivity.getResources().getString(R.string.nav_menu_penalty));
                mImageAdd.setVisibility(View.VISIBLE);
                break;
            case R.id.nav_sites:
                drawer.closeDrawer(GravityCompat.START);
                changeFragment(new SiteFragment(),
                        mActivity.getResources().getString(R.string.nav_menu_sites));
                mImageAdd.setVisibility(View.GONE);
                break;
            case R.id.nav_invoice:
                drawer.closeDrawer(GravityCompat.START);
                changeFragment(new InvoiceFragment(),
                        mActivity.getResources().getString(R.string.nav_menu_invoice));
                mImageAdd.setVisibility(View.VISIBLE);
                break;
            case R.id.nav_reports:
                drawer.closeDrawer(GravityCompat.START);
                changeFragment(new ReportsFragment(),
                        mActivity.getResources().getString(R.string.nav_menu_reports));
                mImageAdd.setVisibility(View.GONE);
                break;
        }
        return true;
    }

    private void hideItem() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();

        if (roleModal.getData().getEmployee().getIsView().equals("0")) {
            nav_Menu.findItem(R.id.nav_Employee).setVisible(false);
        }
        if (roleModal.getData().getCustomer().getIsView().equals("0")) {
            nav_Menu.findItem(R.id.nav_customer).setVisible(false);
        }
        if (roleModal.getData().getSites().getIsView().equals("0")) {
            nav_Menu.findItem(R.id.nav_sites).setVisible(false);
        }
        if (roleModal.getData().getInspection().getIsView().equals("0")) {
            nav_Menu.findItem(R.id.nav_survey).setVisible(false);
        }
        if (roleModal.getData().getInvoice().getIsView().equals("0")) {
            nav_Menu.findItem(R.id.nav_invoice).setVisible(false);
        }
        if (roleModal.getData().getAttendance().getIsView().equals("0")) {
            nav_Menu.findItem(R.id.nav_attendance).setVisible(false);
        }
        if (roleModal.getData().getPenlty().getIsView().equals("0")) {
            nav_Menu.findItem(R.id.nav_penalty).setVisible(false);
        }


    }

    public void changeFragment(Fragment fragment, String title) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_content_frame, fragment);
        // fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();



        /*String backStateName =  fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate (backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null){ //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.fragment_content_frame, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }*/

        mTextHeader.setText(title);
    }
}
