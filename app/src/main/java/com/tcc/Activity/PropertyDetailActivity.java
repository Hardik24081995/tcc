package com.tcc.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.tcc.R;
import com.tcc.fragment.CustomerDocumentFragment;
import com.tcc.fragment.CustomerPaymentFragment;
import com.tcc.fragment.CustomerReminderFragment;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

public class PropertyDetailActivity extends AppCompatActivity {

    SessionManager mSessionManager;
    private Activity mActivity;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageView mImageBack, mImageAdd;
    private TextView mTextHeader;
    private int[] tabIcons = {
            R.drawable.ic_payment_profile,
            R.drawable.ic_documents,
            R.drawable.ic_reminder
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_detail);

        mActivity = PropertyDetailActivity.this;
        mSessionManager = new SessionManager(mActivity);

        getIds();
        setRegister();

    }


    private void getIds() {
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        mImageAdd = findViewById(R.id.image_custom_toolbar_add);

        mTextHeader.setText(mSessionManager.getPreferences("CompanyName", ""));
        mImageBack.setVisibility(View.VISIBLE);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }

    private void setupTabIcons() {

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new CustomerPaymentFragment(), "Payment");
        adapter.addFragment(new CustomerDocumentFragment(), "Document");
        adapter.addFragment(new CustomerReminderFragment(), "Reminder");
        viewPager.setAdapter(adapter);
    }

    private void setRegister() {
        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mImageAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callActivity();
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                mImageAdd.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    /**
     * This method should be redirected to the Activity as per the view pager page scrolling
     */
    private void callActivity() {
        try {
            // Is Property is canceled then don't allow to add

            if (viewPager.getCurrentItem() == 0) {
                /*if (mSharedPreferences.getAppProjectData().getPayment().getIs_insert() == 1) {

                } else {
                    Toast.makeText(PropertyDetailActivity.this, getString(R.string.txt_not_eligible), Toast.LENGTH_SHORT).show();
                }*/
                callAddPaymentActivity();
            } else if (viewPager.getCurrentItem() == 1) {
                /*
                 if (mSharedPreferences.getAppProjectData().getDocument().getIs_insert() == 1) {
                    callUploadDocumentActivity();
                } else {
                    Toast.makeText(PropertyDetailActivity.this, getString(R.string.txt_not_eligible), Toast.LENGTH_SHORT).show();
                }*/
                callAddDocumentActivity();
            } else if (viewPager.getCurrentItem() == 2) {
                /*if (mSharedPreferences.getAppProjectData().getPayment_Reminder().getIs_insert() == 1) {

                } else {
                    Toast.makeText(PropertyDetailActivity.this, getString(R.string.txt_not_eligible), Toast.LENGTH_SHORT).show();
                }*/
                callAddCustomerReminder();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callAddCustomerReminder() {
        Intent intent = new Intent(mActivity, AddCustomerReminderActivity.class);
        startActivity(intent);
    }

    private void callAddPaymentActivity() {
        Intent intent = new Intent(mActivity, AddPaymentActivity.class);
        startActivity(intent);
    }

    public void callAddDocumentActivity() {
        Intent intent = new Intent(mActivity, UploadDocumentActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
