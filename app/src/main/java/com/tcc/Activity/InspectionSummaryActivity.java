package com.tcc.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.tcc.Adapter.InspectionSummaryAdapter;
import com.tcc.Model.GetInspectionListModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InspectionSummaryActivity extends AppCompatActivity {

    Activity mActivity;
    SessionManager mSession;
    String UserID, InspectionID;
    RecyclerView mRecyclerView;
    ArrayList<GetInspectionListModel.Datum> mArrayInspection;
    ArrayList<GetInspectionListModel.Datum.Item> mArrayItem;
    private ImageView mImageBack, mImageAdd, mImageInspection;
    private TextView mTextHeader, tvTodayDate;
    private TextView mTextSiteName, mTextEmployeeName, mTextInspectionDate, mTextEmployeeType, mTextRemarks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspection_summary);

        mActivity = InspectionSummaryActivity.this;
        mSession = new SessionManager(mActivity);
        String UserData = mSession.getPreferences(mSession.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);
        InspectionID = mSession.getPreferences("id", "");

        tvTodayDate = findViewById(R.id.inspectiionDate);
        tvTodayDate.setText(mSession.getPreferences("inspectionDate", ""));

        mArrayInspection = new ArrayList<>();
        mArrayItem = new ArrayList<>();

        initView();
        getInspection();
    }

    private void getInspection() {
        try {
            mArrayItem.clear();
            Common.showLoadingDialog(mActivity, "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getInspectionJson("-1", "1", UserID));

            Call<GetInspectionListModel> call = RetrofitClient.createService(ApiInterface.class).getInspectionAPI(body);
            call.enqueue(new Callback<GetInspectionListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetInspectionListModel> call, @NonNull Response<GetInspectionListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayItem.addAll(response.body().getData().get(Integer.parseInt(InspectionID)).getItem());
                            if (mArrayItem.size() > 0) {
                                setAdapter();
                            }
                        }
                    } catch (JSONException e) {
                        Common.hideDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetInspectionListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.hideDialog();
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);

        InspectionSummaryAdapter mAdapter = new InspectionSummaryAdapter(mActivity, mArrayItem);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void initView() {
        getID();
    }

    @SuppressLint("SetTextI18n")
    private void getID() {
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        mImageBack.setVisibility(View.VISIBLE);
        mImageInspection = findViewById(R.id.image);

        mTextHeader.setText("Inspection Details");
        mImageBack.setOnClickListener(v -> onBackPressed());

        mRecyclerView = findViewById(R.id.recyclerview_inspection);

        mTextSiteName = findViewById(R.id.inspection_name);
        mTextEmployeeName = findViewById(R.id.employeename);
        mTextEmployeeType = findViewById(R.id.typeEmployee);
        mTextRemarks = findViewById(R.id.remarks);

        mTextSiteName.setText(mSession.getPreferences("siteName", ""));
        mTextEmployeeName.setText(mSession.getPreferences("employeeName", ""));
        mTextEmployeeType.setText(mSession.getPreferences("employeeType", ""));
        mTextRemarks.setText(mSession.getPreferences("remarks", ""));

        Glide.with(mActivity)
                .load("http://societyfy.in/TheCleaingCompany/assets/uploads/inspection/" + mSession.getPreferences("image", ""))
                .apply(new RequestOptions()
                        .placeholder(R.drawable.demo_user_pic)
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                .into(mImageInspection);

        mImageInspection.setOnClickListener(v -> {
            try {
                Dialog dialog = new Dialog(mActivity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.dialog_imagepopup);

                // Text View
                ImageView mTextMobileNo = dialog.findViewById(R.id.image_popupImage);

                Glide.with(mActivity)
                        .load("http://societyfy.in/TheCleaingCompany/assets/uploads/inspection/" + mSession.getPreferences("image", ""))
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.demo_user_pic)
                                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                        .into(mTextMobileNo);

                dialog.show();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
