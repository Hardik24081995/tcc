package com.tcc.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.tcc.Adapter.PenaltyAdapter;
import com.tcc.Model.AddPenaltyModel;
import com.tcc.Model.GetEmployeListModel;
import com.tcc.Model.GetSiteModel;
import com.tcc.Model.PenaltyModel;
import com.tcc.Model.PenatlyReasonModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PenaltyActivity extends AppCompatActivity implements PenaltyAdapter.customSpinnerListener {

    ArrayList<GetSiteModel.Datum> myArrayList = new ArrayList<>();
    ArrayList<String> sitesArr = new ArrayList<>();
    ArrayList<String> mArrEmployee = new ArrayList<>();
    ArrayList<GetEmployeListModel.Datum> myArrayEmployee = new ArrayList<>();
    ArrayList<PenaltyModel> arrayMultiReason = new ArrayList<>();
    ArrayList<PenatlyReasonModel.Datum> myArrayReason = new ArrayList<>();
    SessionManager mSession;
    private Activity mActivity;
    private TextView mTextHeader;
    private ImageView mImageBack, mImageAdd;
    private Spinner mSpinnerSites, mSpinnerReason;
    private int selectedSiteIndex = 0;
    private String siteID = "1", EmployeeID = "", Reason = "", currentTime, formattedDate, UserID;
    private LinearLayout mLinearEmployee, mLinearReason;
    private Button submitPenalty;
    private RadioGroup radioGroup;
    private RadioButton radioSites, RadioEmployee;
    private EditText edt_penalty;
    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinner_sites:
                    selectedSiteIndex = adapterView.getSelectedItemPosition();

                    if (selectedSiteIndex != 0) {
                        siteID = myArrayList.get(selectedSiteIndex - 1).getSitesID();
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_penalty);

        mActivity = PenaltyActivity.this;

        mSession = new SessionManager(mActivity);
        String UserData = mSession.getPreferences(mSession.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);

        Date c = Calendar.getInstance().getTime();
        currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        formattedDate = df.format(c);

        getIds();
        setData();
        setRegister();
        getSites();
        getEmployee();
        getReasons();

    }

    private void getReasons() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getPenaltyReasonJson());
            Common.showLoadingDialog(this, "Loading");
            Call<PenatlyReasonModel> call = RetrofitClient.createService(ApiInterface.class).getPenaltyReasonAPI(body);
            call.enqueue(new Callback<PenatlyReasonModel>() {
                @Override
                public void onResponse(@NonNull Call<PenatlyReasonModel> call, @NonNull Response<PenatlyReasonModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayReason.addAll(response.body().getData());
                            setReasonAdapter();
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PenatlyReasonModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setReasonAdapter() {
        try {
            if (arrayMultiReason.size() > 0) {
                arrayMultiReason.clear();
            }

            PenaltyModel options = new PenaltyModel();
            options.setStatus("0");
            options.setOpetionSuggested("Select Reason");
            options.setOpetionSuggestedID("0");
            options.setChecked(false);
            arrayMultiReason.add(options);


            if (myArrayReason.size() > 0) {
                for (int a = 0; a < myArrayReason.size(); a++) {
                    PenaltyModel option = new PenaltyModel();
                    option.setStatus(myArrayReason.get(a).getStatus());
                    option.setOpetionSuggested(myArrayReason.get(a).getReason());
                    option.setOpetionSuggestedID(myArrayReason.get(a).getReasonID());
                    option.setChecked(false);
                    arrayMultiReason.add(option);
                }
            }
            PenaltyAdapter mAdapterPackageSuggesteed = new PenaltyAdapter
                    (mActivity, arrayMultiReason, this);

            mSpinnerReason.setAdapter(mAdapterPackageSuggesteed);
        } catch (Exception ie) {
            ie.printStackTrace();
        }
    }

    private void getEmployee() {
        try {

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getEmployeeListJson("-1", "1", "", "", "-1"));
            Call<GetEmployeListModel> call = RetrofitClient.createService(ApiInterface.class).getEmployeAPI(body);
            call.enqueue(new Callback<GetEmployeListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetEmployeListModel> call, @NonNull Response<GetEmployeListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayEmployee.addAll(response.body().getData());
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetEmployeListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onItemUpdate() {

        if (mLinearReason.getChildCount() > 0) {
            mLinearReason.removeAllViews();
        }
        if (arrayMultiReason.size() > 0) {

            for (int a = 0; a < arrayMultiReason.size(); a++) {
                if (arrayMultiReason.get(a).getChecked() == true) {
                    addItemsReason(arrayMultiReason.get(a));
                }
            }
        }
    }

    /**
     * Add Item Linear
     *
     * @param couselingPackageSuggestionModel -Package Suggested
     */
    private void addItemsReason(PenaltyModel couselingPackageSuggestionModel) {
        @SuppressLint("InflateParams")
        View view_container = LayoutInflater.from(mActivity).inflate(R.layout.
                        row_add_preliminary_examination_compliance_item,
                null, false);

        TextView mTextName = view_container.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
        ImageView mImageDelete = view_container.findViewById(R.id.image_row_add_preliminary_examination_compliance_delete);
        mImageDelete.setColorFilter(Common.setThemeColor(mActivity));

        mTextName.setText(couselingPackageSuggestionModel.getOpetionSuggested());

        mImageDelete.setOnClickListener(v -> {
            mLinearReason.removeView(view_container);
            couselingPackageSuggestionModel.setChecked(false);
        });

        mLinearReason.addView(view_container);
    }

    private void addEmployeeLayout() {
        LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view_dosage = layoutInflater.inflate(R.layout.layout_employee, null, false);

        EditText mEditAmount = view_dosage.findViewById(R.id.edit_penalty_amount);
        Spinner mSpinnerEmployee = view_dosage.findViewById(R.id.spinner_penalty_employee);
        ImageView mImageRemove = view_dosage.findViewById(R.id.image_quotation_item_remove);

        setEmployeeAdapter(myArrayEmployee, mSpinnerEmployee);

        mSpinnerEmployee.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {

                    SelectUserTypeID(myArrayEmployee.get(position - 1), mSpinnerEmployee);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mImageRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLinearEmployee.removeView(view_dosage);
            }
        });

        mLinearEmployee.addView(view_dosage);
    }

    private void setEmployeeAdapter(ArrayList<GetEmployeListModel.Datum> myArrayEmployee, Spinner mSpinnerEmployee) {
        try {

            if (mArrEmployee.size() > 0) {
                mArrEmployee.clear();
            }

            if (myArrayEmployee != null && myArrayEmployee.size() > 0) {

                mArrEmployee.add("Select Employee");

                for (int i = 0; i < myArrayEmployee.size(); i++) {
                    mArrEmployee.add(myArrayEmployee.get(i).getFirstName() + " " + myArrayEmployee.get(i).getLastName());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>
                        (mActivity, android.R.layout.simple_selectable_list_item, mArrEmployee);

                mSpinnerEmployee.setAdapter(adapter);
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    private void SelectUserTypeID(GetEmployeListModel.Datum datum, Spinner mSpinnerEmployee) {

        EmployeeID = String.valueOf(mSpinnerEmployee.getSelectedItemPosition());

    }

    private void getReasonSelected() {
        Reason = AppConstants.STR_EMPTY_STRING;
        if (mLinearReason.getChildCount() > 0) {
            for (int a = 0; a < mLinearReason.getChildCount(); a++) {
                View child_view = mLinearReason.getChildAt(a);
                TextView mTextName = child_view.findViewById
                        (R.id.text_row_add_preliminary_examination_compliance_compliance);

                String item = mTextName.getText().toString();
                if (Reason.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                    Reason = item;
                } else {
                    Reason = Reason + "," + item;
                }

            }
        }
    }

    private void getSites() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getSiteListJson("-1", "1", "-1", "-1", "-1",
                    "", ""));

            Call<GetSiteModel> call = RetrofitClient.createService(ApiInterface.class).getSiteAPI(body);
            call.enqueue(new Callback<GetSiteModel>() {
                @Override
                public void onResponse(@NonNull Call<GetSiteModel> call, @NonNull Response<GetSiteModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayList.addAll(response.body().getData());
                            setSitesAdapter();
                        } else {
//                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetSiteModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSitesAdapter() {
        try {
            sitesArr.add(0, "Select Sites");
            if (myArrayList.size() > 0) {
                for (GetSiteModel.Datum Services : myArrayList) {
                    sitesArr.add(Services.getCompanyName());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, sitesArr) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == selectedSiteIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerSites.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIds() {
        mTextHeader = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);
        mImageAdd = findViewById(R.id.image_penalty_addemployee);

        edt_penalty = findViewById(R.id.edt_penalty);

        radioGroup = findViewById(R.id.radio_group_penalty);
        RadioEmployee = findViewById(R.id.Employee_Radio);
        radioSites = findViewById(R.id.Site_Radio);

        //spinner
        mSpinnerSites = findViewById(R.id.spinner_sites);
        mSpinnerReason = findViewById(R.id.spinner_reason);

        mSpinnerSites.setOnItemSelectedListener(onItemSelectedListener);

        //Linear Layout
        mLinearEmployee = findViewById(R.id.linear_employee_layout);
        mLinearReason = findViewById(R.id.linear_check_up_reason_layout);

        submitPenalty = findViewById(R.id.submitPenalty);
    }

    private void setData() {
        mTextHeader.setText("PENALTY");
        mImageBack.setVisibility(View.VISIBLE);
    }


    private void setRegister() {
        mImageBack.setOnClickListener(v -> onBackPressed());


        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {

            RadioButton radioButton = findViewById(checkedId);

            if (radioButton.getId() == R.id.Site_Radio) {
                mSpinnerSites.setVisibility(View.VISIBLE);
            } else if (radioButton.getId() == R.id.Employee_Radio) {
                mSpinnerSites.setVisibility(View.GONE);
            }
        });

        mImageAdd.setOnClickListener(v -> {
            addEmployeeLayout();
        });

        submitPenalty.setOnClickListener(v -> {
            getReasonSelected();

            if (checkValidation()) {
                doAddPenalty();
            }
        });
    }

    /**
     * @return - JSON Array
     */
    private JSONArray getArrayEmployee() {
        JSONArray jsonArray = new JSONArray();
        try {
            if (mLinearEmployee.getChildCount() > 0) {
                for (int a = 0; a < mLinearEmployee.getChildCount(); a++) {
                    View child_view = mLinearEmployee.getChildAt(a);

                    EditText mEditAmount = child_view.findViewById(R.id.edit_penalty_amount);
                    Spinner mSpinnerEmployee = child_view.findViewById(R.id.spinner_penalty_employee);

                    String EmployeeID = "0";

                    int index_category = mSpinnerEmployee.getSelectedItemPosition();
                    if (index_category > 0)
                        EmployeeID = myArrayEmployee.get(index_category - 1).getUserID();

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("EmployeeID", EmployeeID);
                    jsonObject.put("Penalty", mEditAmount.getText().toString().trim());

                    jsonArray.put(jsonObject);
                }
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
        return jsonArray;
    }

    private void doAddPenalty() {
        try {

            JSONArray jsonArray = getArrayEmployee();
            Common.showLoadingDialog(this, "Loading");
            String ConvertedReminderDate = Common.convertDateToServer(this, formattedDate);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.AddPenaltyJson(UserID, ConvertedReminderDate, Reason, siteID, jsonArray));

            Call<AddPenaltyModel> call = RetrofitClient.createService(ApiInterface.class).AddPenaltyAPI(body);
            call.enqueue(new Callback<AddPenaltyModel>() {
                @Override
                public void onResponse(@NonNull Call<AddPenaltyModel> call, @NonNull Response<AddPenaltyModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            finish();
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddPenaltyModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private boolean checkValidation() {
        boolean status = true;

        if (radioSites.isChecked()) {
            if (siteID.equalsIgnoreCase("")) {
                status = false;
                Toast.makeText(mActivity, "Please Select Sites", Toast.LENGTH_SHORT).show();
            }
            if (Reason.equalsIgnoreCase("")) {
                status = false;
                Toast.makeText(mActivity, "Please Select Reason", Toast.LENGTH_SHORT).show();
            }

            if (mLinearEmployee.getChildCount() > 0) {

                for (int i = 0; i < mLinearEmployee.getChildCount(); i++) {
                    View view = mLinearEmployee.getChildAt(i);

                    EditText mEditAmount = view.findViewById(R.id.edit_penalty_amount);
                    Spinner mSpinnerEmployee = view.findViewById(R.id.spinner_penalty_employee);


                    int index_category = mSpinnerEmployee.getSelectedItemPosition();

                    if (index_category == 0) {
                        Toast.makeText(mActivity, "Please Select Employee", Toast.LENGTH_SHORT).show();
                        status = false;
                    }
                    if (mEditAmount.getText().toString().trim().equalsIgnoreCase("")) {
                        status = false;
                        Toast.makeText(mActivity, "Please Enter Amount", Toast.LENGTH_SHORT).show();
                    }

                }
            } else {
                status = false;
                Toast.makeText(mActivity, "Please Add Employee", Toast.LENGTH_SHORT).show();
            }


        } else if (RadioEmployee.isChecked()) {
            if (Reason.equalsIgnoreCase("")) {
                status = false;
                Toast.makeText(mActivity, "Please Select Reason", Toast.LENGTH_SHORT).show();
            }

            if (mLinearEmployee.getChildCount() > 0) {

                for (int i = 0; i < mLinearEmployee.getChildCount(); i++) {
                    View view = mLinearEmployee.getChildAt(i);

                    EditText mEditAmount = view.findViewById(R.id.edit_penalty_amount);
                    Spinner mSpinnerEmployee = view.findViewById(R.id.spinner_penalty_employee);


                    int index_category = mSpinnerEmployee.getSelectedItemPosition();

                    if (index_category == 0) {
                        Toast.makeText(mActivity, "Please Select Employee", Toast.LENGTH_SHORT).show();
                        status = false;
                    }
                    if (mEditAmount.getText().toString().trim().equalsIgnoreCase("")) {
                        status = false;
                        Toast.makeText(mActivity, "Please Enter Amount", Toast.LENGTH_SHORT).show();
                    }

                }
            } else {
                status = false;
                Toast.makeText(mActivity, "Please Add Employee", Toast.LENGTH_SHORT).show();
            }


            siteID = "0";
        }


        return status;
    }

}
