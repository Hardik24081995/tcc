package com.tcc.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.textfield.TextInputLayout;
import com.tcc.R;

import java.util.ArrayList;

public class CustomerSearchActivity extends AppCompatActivity {

    RadioGroup radio_group;
    ProgressBar progressBar;
    private Activity mActivity;
    private ImageView mImageBack;
    private TextView mTitle;
    private LinearLayout linearSortScreen, llSalaryScreen,
            llLocationScreen, llRoleScreen, llProjectScreen;
    private RadioGroup radioGroupTab;
    private RadioButton rbSalary, rbLocation,
            rbSort, rbDesignation, rbProjects;
    private EditText edtSearch;
    private TextInputLayout txtInputLay;
    private Button btnSubmit;
    private RadioButton rb1Name, rb2Salary;
    private RecyclerView mRecyclerView;


    private ProgressDialog progressDialog;
    private SwipeRefreshLayout mSwipeRefreshView;
    private int projectID;
    private Spinner mSpinnerProject;
    private ArrayList<String> mProjectArrayList;
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_custom_toolbar_back_arrow:
                    onBackPressed();
                    break;
                case R.id.btnSubmit_SearchActivity:
                    onBackPressed();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_search);

        mActivity = CustomerSearchActivity.this;
        mProjectArrayList = new ArrayList<>();

        getIds();
        setDefaultProject();
        setRegilistener();

    }

    /**
     * getIds
     */
    private void getIds() {

        mTitle = findViewById(R.id.text_custom_toolbar_without_back_header);
        mImageBack = findViewById(R.id.image_custom_toolbar_back_arrow);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycleviewRecyclerViewScreen);
        progressBar = (ProgressBar) findViewById(R.id.progressBarRecyclerViewScreen);
        mSwipeRefreshView = (SwipeRefreshLayout) findViewById(R.id.swipeContainerRecyclerViewScreen);
        mSwipeRefreshView.setEnabled(false);
        mRecyclerView.setHasFixedSize(true);

        edtSearch = findViewById(R.id.edtSearch_SearchActivity);
        txtInputLay = findViewById(R.id.txtInputLay_SearchActivity);
        rb1Name = findViewById(R.id.rb1_FilterActivity);
        rb2Salary = findViewById(R.id.rb2_FilterActivity);

        radioGroupTab = findViewById(R.id.radioGroupTab_FilterActivity);
        rbSort = findViewById(R.id.rbSort_FilterActivity);
        rbSalary = findViewById(R.id.rbSalary_FilterActivity);
        rbLocation = findViewById(R.id.rbLocation_FilterActivity);
        rbDesignation = findViewById(R.id.rbDesignation_FilterActivity);
        rbProjects = findViewById(R.id.rbProjects_FilterActivity);
        radio_group = findViewById(R.id.radio_group);
        btnSubmit = findViewById(R.id.btnSubmit_SearchActivity);


        linearSortScreen = findViewById(R.id.llSortScreen_FilterActivity);
        llSalaryScreen = findViewById(R.id.llSalaryScreen_FilterActivity);
        llLocationScreen = findViewById(R.id.llLocationScreen_FilterActivity);
        llRoleScreen = findViewById(R.id.llRoleScreen_FilterActivity);
        llProjectScreen = findViewById(R.id.llProjectScreen_FilterActivity);
        mSpinnerProject = findViewById(R.id.spinnerProject_HomeScreen);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mImageBack.setVisibility(View.VISIBLE);
        mTitle.setText(getResources().getString(R.string.text_search));
        mTitle.setAllCaps(true);
    }

    /**
     * set On Listener Method
     */
    private void setRegilistener() {
        btnSubmit.setOnClickListener(clickListener);
        mImageBack.setOnClickListener(clickListener);

        radioGroupTab.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                radioGroupTabActionHandler();
            }
        });
    }

    private void radioGroupTabActionHandler() {

        rbSort.setTextColor(ContextCompat.getColor(mActivity, R.color.txt_color_light));
        rbSalary.setTextColor(ContextCompat.getColor(mActivity, R.color.txt_color_light));
        rbLocation.setTextColor(ContextCompat.getColor(mActivity, R.color.txt_color_light));
        rbDesignation.setTextColor(ContextCompat.getColor(mActivity, R.color.txt_color_light));
        rbProjects.setTextColor(ContextCompat.getColor(mActivity, R.color.txt_color_light));

        linearSortScreen.setVisibility(View.GONE);
        llSalaryScreen.setVisibility(View.GONE);
        llLocationScreen.setVisibility(View.GONE);
        llRoleScreen.setVisibility(View.GONE);
        llProjectScreen.setVisibility(View.GONE);
        txtInputLay.setVisibility(View.VISIBLE);

        if (radioGroupTab.getCheckedRadioButtonId() == R.id.rbSort_FilterActivity) {

            rbSort.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
            linearSortScreen.setVisibility(View.GONE);


        } else if (radioGroupTab.getCheckedRadioButtonId() == R.id.rbSalary_FilterActivity) {

            rbSalary.setTextColor(ContextCompat.getColor(mActivity, R.color.colorWhite));
            llSalaryScreen.setVisibility(View.GONE);

        } else if (radioGroupTab.getCheckedRadioButtonId() == R.id.rbLocation_FilterActivity) {

            rbLocation.setTextColor(ContextCompat.getColor(mActivity, R.color.colorWhite));
            llLocationScreen.setVisibility(View.GONE);

        } else if (radioGroupTab.getCheckedRadioButtonId() == R.id.rbDesignation_FilterActivity) {

            rbDesignation.setTextColor(ContextCompat.getColor(mActivity, R.color.colorWhite));
            llRoleScreen.setVisibility(View.GONE);
        } else if (radioGroupTab.getCheckedRadioButtonId() == R.id.rbProjects_FilterActivity) {

            rbProjects.setTextColor(ContextCompat.getColor(mActivity, R.color.colorWhite));
            txtInputLay.setVisibility(View.GONE);
            llProjectScreen.setVisibility(View.VISIBLE);
//            if (mWingsPropertyArrayList == null || mWingsPropertyArrayList.size() == 0)
        }
    }


    /***
     * Set Default Project on the fist position
     */
    private void setDefaultProject() {
        mProjectArrayList.add("Please service");
        mProjectArrayList.add("Office Cleaner");
        mProjectArrayList.add("Sofa Washer");

        ArrayAdapter projectAdapter = new ArrayAdapter(this
                , android.R.layout.simple_spinner_dropdown_item, mProjectArrayList);
        projectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerProject.setAdapter(projectAdapter);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
