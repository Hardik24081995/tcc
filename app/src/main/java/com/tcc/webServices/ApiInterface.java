package com.tcc.webServices;


import com.tcc.Model.AddAttendanceModel;
import com.tcc.Model.AddCheckinModel;
import com.tcc.Model.AddCustomerPaymentModel;
import com.tcc.Model.AddEmployeeModel;
import com.tcc.Model.AddInspectionModel;
import com.tcc.Model.AddPenaltyModel;
import com.tcc.Model.AddQuotationModel;
import com.tcc.Model.AddRoomModel;
import com.tcc.Model.AddSiteModel;
import com.tcc.Model.AddTeamModel;
import com.tcc.Model.AddTicketModel;
import com.tcc.Model.AddTrainingModel;
import com.tcc.Model.AddUniformModel;
import com.tcc.Model.AddVisitorReminderModel;
import com.tcc.Model.AddVisitorResponseModel;
import com.tcc.Model.ChangePasswordModel;
import com.tcc.Model.CheckInOutListingModel;
import com.tcc.Model.CityModel;
import com.tcc.Model.CollectionModal;
import com.tcc.Model.ConfigModel;
import com.tcc.Model.ConvertSiteModel;
import com.tcc.Model.CustomerPaymentModel;
import com.tcc.Model.DeativateEmployeeModel;
import com.tcc.Model.EditDocumentModel;
import com.tcc.Model.EmployeeTrainingSpinnerModel;
import com.tcc.Model.ForgotPasswordModel;
import com.tcc.Model.GetAttendanceModel;
import com.tcc.Model.GetAvailableEmployee;
import com.tcc.Model.GetCompanyListModel;
import com.tcc.Model.GetCustomerDocumentModel;
import com.tcc.Model.GetCustomerListModel;
import com.tcc.Model.GetCustomerProcessModel;
import com.tcc.Model.GetCustomerReminderModel;
import com.tcc.Model.GetDashboardModal;
import com.tcc.Model.GetDisignationListModel;
import com.tcc.Model.GetEmployeListModel;
import com.tcc.Model.GetEmployeeAttendance;
import com.tcc.Model.GetEmployeeTrainingModel;
import com.tcc.Model.GetEmployeeUniformModel;
import com.tcc.Model.GetInspectionListModel;
import com.tcc.Model.GetInvoiceModel;
import com.tcc.Model.GetMetrealModal;
import com.tcc.Model.GetModuleIDList;
import com.tcc.Model.GetQuotationListModel;
import com.tcc.Model.GetRoomAlocModel;
import com.tcc.Model.GetSalaryModal;
import com.tcc.Model.GetServicesModel;
import com.tcc.Model.GetSiteModel;
import com.tcc.Model.GetSubjectModal;
import com.tcc.Model.GetTeamDefinitionModel;
import com.tcc.Model.GetTeamEmployeeModel;
import com.tcc.Model.GetUniformModel;
import com.tcc.Model.GetUserListModel;
import com.tcc.Model.GetUserType;
import com.tcc.Model.GetVisitorModel;
import com.tcc.Model.GetVisitorReminderModel;
import com.tcc.Model.GetVisitorResponseModel;
import com.tcc.Model.InspectionModel;
import com.tcc.Model.LoginModel;
import com.tcc.Model.NotificationModal;
import com.tcc.Model.PenaltyListModel;
import com.tcc.Model.PenatlyReasonModel;
import com.tcc.Model.ProfileModel;
import com.tcc.Model.QuotationStatusModel;
import com.tcc.Model.RemoveDocModel;
import com.tcc.Model.RemoveEmployeeModel;
import com.tcc.Model.ReportPaymentModal;
import com.tcc.Model.ReportTraningModal;
import com.tcc.Model.ReportUniformModal;
import com.tcc.Model.RoleModalResponse;
import com.tcc.Model.SENDEmailModel;
import com.tcc.Model.SMSModel;
import com.tcc.Model.StateModel;
import com.tcc.Model.TicketListingModel;
import com.tcc.Model.UploadDocModel;
import com.tcc.Model.UploadInspectionImageModel;
import com.tcc.Model.VisitSiteModel;
import com.tcc.Model.VisitorModel;
import com.tcc.Model.VisitorToCustomerModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    // Login API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<LoginModel> loginAPI(@Body RequestBody body);

    // Get Config API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ConfigModel> getConfigAPI(@Body RequestBody body);

    // Get Config API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<RoleModalResponse> getRole(@Body RequestBody body);

    // Change Password API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ChangePasswordModel> changePasswordAPI(@Body RequestBody body);

    // ForgotPassword API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ForgotPasswordModel> getPasswordAPI(@Body RequestBody body);

    // Get Visitor Service API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetServicesModel> getVisitorServiceAPI(@Body RequestBody body);


    // Get Dashboard Service API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetDashboardModal> getDashboardData(@Body RequestBody body);


    // Get State API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<StateModel> getStateListAPI(@Body RequestBody body);

    // Get City API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<CityModel> getCityListAPI(@Body RequestBody body);

    // Get City API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetSubjectModal> getSubjectListAPI(@Body RequestBody body);

    // Add Visitor API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<VisitorModel> addVisitorAPI(@Body RequestBody body);

    // Get Visitor API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetVisitorModel> getVisitorAPI(@Body RequestBody body);

    // Get Notification API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<NotificationModal> getNotification(@Body RequestBody body);

    // Add Visitor Reminder API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddVisitorReminderModel> addVisitorReminderAPI(@Body RequestBody body);

    // Add Customer Reminder API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddVisitorReminderModel> addCustomerReminderAPI(@Body RequestBody body);

    // Get Visitor Reminder API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetVisitorReminderModel> getVisitorReminderAPI(@Body RequestBody body);

    // Get Visitor Reminder API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<CollectionModal> getCollection(@Body RequestBody body);

    // Get Customer Reminder API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetCustomerReminderModel> getCustomerReminderAPI(@Body RequestBody body);

    // Add Visitor Response API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddVisitorResponseModel> addVisitorResponseAPI(@Body RequestBody body);

    // Get Visitor Response API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetVisitorResponseModel> getVisitorResponseAPI(@Body RequestBody body);

    // Get Visitor Response API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ReportTraningModal> getReportTraningAPI(@Body RequestBody body);

    // Get Visitor Response API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ReportUniformModal> getReportUniformAPI(@Body RequestBody body);


    // Get Visitor Response API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ReportPaymentModal> getReportPaymentAPI(@Body RequestBody body);

    // Get Customer API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetCustomerListModel> getCustomerAPI(@Body RequestBody body);

    // Get Customer API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetMetrealModal> getMeterial(@Body RequestBody body);

    // Get Customer API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetModuleIDList> getModuleList(@Body RequestBody body);

    // Get Employee API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetEmployeListModel> getEmployeAPI(@Body RequestBody body);

    // Get Employee Uniform List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetEmployeeUniformModel> getEmployeeUniformListAPI(@Body RequestBody body);

    // Get Employee Uniform API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetUniformModel> getUniformAPI(@Body RequestBody body);

    // Get Employee Uniform List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddUniformModel> addUniformAPI(@Body RequestBody body);

    // Add Employee Room API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddRoomModel> addRoomAPI(@Body RequestBody body);

    // Add Employee Room API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddRoomModel> addSalaryAPI(@Body RequestBody body);

    // Get Employee Room API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetRoomAlocModel> getRoomAPI(@Body RequestBody body);

    // Get Employee Room API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetSalaryModal> getPaymentAPI(@Body RequestBody body);

    // Get Site API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetSiteModel> getSiteAPI(@Body RequestBody body);


    // Get Invoice API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetInvoiceModel> getInvoceAPI(@Body RequestBody body);

    // Get CustomerPayment API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<CustomerPaymentModel> getCustomerPaymentAPI(@Body RequestBody body);

    // Add CustomerPayment API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddCustomerPaymentModel> addCustomerPaymentAPI(@Body RequestBody body);

    // Get Customer Process API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetCustomerProcessModel> getCustomerProcessAPI(@Body RequestBody body);

    // Upload Document Image API Interface
    @Multipart
    @POST(WebFields.API_SUB_URL)
    Call<UploadDocModel> UploadDocumentAPI(@Part(WebFields.UPLOAD_DOCUMENT.REQUEST_METHOD) RequestBody method,
                                           @Part(WebFields.UPLOAD_DOCUMENT.REQUEST_ID) RequestBody ID,
                                           @Part(WebFields.UPLOAD_DOCUMENT.REQUEST_TITLE) RequestBody title,
                                           @Part(WebFields.UPLOAD_DOCUMENT.REQUEST_SITEID) RequestBody date,
                                           @Part(WebFields.UPLOAD_DOCUMENT.REQUEST_QUOTATIONID) RequestBody QuotationID,
                                           @Part MultipartBody.Part file);

    // Get Customer Document API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetCustomerDocumentModel> getCustomerDocumentAPI(@Body RequestBody body);

    // Get Employee Training API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetEmployeeTrainingModel> getEmployeeTrainingAPI(@Body RequestBody body);

    // Get Employee Training LIST API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<EmployeeTrainingSpinnerModel> getEmployeeTrainingListAPI(@Body RequestBody body);

    // Add Employee Training API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddTrainingModel> addEmployeeTrainingAPI(@Body RequestBody body);

    // Get Designation API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetDisignationListModel> agetDesignationAPI(@Body RequestBody body);

    // Save Profile
    @Multipart
    @POST(WebFields.API_SUB_URL)
    Call<ProfileModel> EditProfileAPI(@Part(WebFields.REGISTER_USER.REQUEST_METHOD) RequestBody method,
                                      @Part(WebFields.REGISTER_USER.REQUEST_FIRSTNAME) RequestBody FirstName,
                                      @Part(WebFields.REGISTER_USER.REQUEST_LASTNAME) RequestBody LastName,
                                      @Part(WebFields.REGISTER_USER.REQUEST_MOBILENO) RequestBody MobileNo,
                                      @Part(WebFields.REGISTER_USER.REQUEST_USERID) RequestBody UserID,
                                      @Part(WebFields.REGISTER_USER.REQUEST_ADDRESS) RequestBody Address,
                                      @Part(WebFields.REGISTER_USER.REQUEST_DESIGNATION_ID) RequestBody DesignationID,
                                      @Part(WebFields.REGISTER_USER.REQUEST_SALARY) RequestBody Salary,
                                      @Part(WebFields.REGISTER_USER.REQUEST_USER_TYPE_ID) RequestBody UserTypeID,
                                      @Part(WebFields.REGISTER_USER.REQUEST_IMAGE_NAME) RequestBody imagename,
                                      @Part MultipartBody.Part file);

    // ADD Ticket
    @Multipart
    @POST(WebFields.API_SUB_URL)
    Call<AddTicketModel> AddTicketAPI(@Part(WebFields.ADD_TICKET.REQUEST_METHOD) RequestBody method,
                                      @Part(WebFields.ADD_TICKET.REQUEST_TITLE) RequestBody FirstName,
                                      @Part(WebFields.ADD_TICKET.REQUEST_DESC) RequestBody LastName,
                                      @Part(WebFields.ADD_TICKET.REQUEST_PRIORITY) RequestBody MobileNo,
                                      @Part(WebFields.ADD_TICKET.REQUEST_USERID) RequestBody UserID,
                                      @Part(WebFields.ADD_TICKET.REQUEST_SUBJECT_ID) RequestBody SubjectID,
                                      @Part(WebFields.ADD_TICKET.REQUEST_CITY_ID) RequestBody CityID,
                                      @Part MultipartBody.Part file);

    // Save Employee Profile
    @Multipart
    @POST(WebFields.API_SUB_URL)
    Call<AddEmployeeModel> AddEmployeeAPI(@Part(WebFields.ADD_EMPLOYEE.REQUEST_METHOD) RequestBody method,
                                          @Part(WebFields.ADD_EMPLOYEE.REQUEST_FIRSTNAME) RequestBody FirstName,
                                          @Part(WebFields.ADD_EMPLOYEE.REQUEST_LASTNAME) RequestBody LastName,
                                          @Part(WebFields.ADD_EMPLOYEE.REQUEST_EMAIL) RequestBody Email,
                                          @Part(WebFields.ADD_EMPLOYEE.REQUEST_PASSWORD) RequestBody Password,
                                          @Part(WebFields.ADD_EMPLOYEE.REQUEST_MOBILENO) RequestBody MobileNo,
                                          @Part(WebFields.ADD_EMPLOYEE.REQUEST_ADDRESS) RequestBody Address,
                                          @Part(WebFields.ADD_EMPLOYEE.REQUEST_USERTYPE_ID) RequestBody UserTypeID,
                                          @Part(WebFields.ADD_EMPLOYEE.REQUEST_SALARY) RequestBody Salary,
                                          @Part(WebFields.ADD_EMPLOYEE.REQUEST_JOINING_DATE) RequestBody JoiningDate,
                                          @Part(WebFields.ADD_EMPLOYEE.REQUEST_WORKING_HOURS) RequestBody WorkingHours,
                                          @Part(WebFields.ADD_EMPLOYEE.REQUEST_BANK_NAME) RequestBody BankName,
                                          @Part(WebFields.ADD_EMPLOYEE.REQUEST_BRANCH_NAME) RequestBody BranchName,
                                          @Part(WebFields.ADD_EMPLOYEE.REQUEST_ACCOUNT_NO) RequestBody AccountNo,
                                          @Part(WebFields.ADD_EMPLOYEE.REQUEST_IFSC_CODE) RequestBody IFSCCode,
                                          @Part(WebFields.ADD_EMPLOYEE.REQUEST_CITY_ID) RequestBody CityID,
                                          @Part MultipartBody.Part file,
                                          @Part MultipartBody.Part document,
                                          @Part MultipartBody.Part offerletter);

    // Get Penalty Reason API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<PenatlyReasonModel> getPenaltyReasonAPI(@Body RequestBody body);

    // Get Designation API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetTeamDefinitionModel> getTeamDefinitionAPI(@Body RequestBody body);

    // Get Ticket API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<TicketListingModel> getTicketAPI(@Body RequestBody body);

    // Get Team Employee API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetTeamEmployeeModel> getTeamEmployeeAPI(@Body RequestBody body);

    // Get Attendance API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetAttendanceModel> getAttendanceAPI(@Body RequestBody body);

    // Get Employee Attendancre API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetEmployeeAttendance> getEmployeeAttendanceAPI(@Body RequestBody body);

    // Add Employee Attendancre API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddAttendanceModel> addEmployeeAttendanceAPI(@Body RequestBody body);

    // Add Penalty API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddPenaltyModel> AddPenaltyAPI(@Body RequestBody body);

    // Get Penalty API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<PenaltyListModel> getPenaltyAPI(@Body RequestBody body);

    // Add Team Definition API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddTeamModel> addTeamDefinitionAPI(@Body RequestBody body);

    // Get Quotation API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetQuotationListModel> getQuotationAPI(@Body RequestBody body);

    // Add Sites API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddSiteModel> doAddSite(@Body RequestBody body);

    // Get UserType API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetUserType> getUserType(@Body RequestBody body);

    // Add Quotation API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddQuotationModel> addQuotationAPI(@Body RequestBody body);

    // Add Quotation Status API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<QuotationStatusModel> addQuotationStatusAPI(@Body RequestBody body);

    // Convert To Site API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ConvertSiteModel> convertToSiteAPI(@Body RequestBody body);

    // Convert To Customer API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<VisitorToCustomerModel> convertToCustomerAPI(@Body RequestBody body);

    // Is Visit API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<VisitSiteModel> visitSiteAPI(@Body RequestBody body);

    // CheckIn Listing API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<CheckInOutListingModel> checkInAPI(@Body RequestBody body);

    // Inspection API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<InspectionModel> inspectionAPI(@Body RequestBody body);

    // add CheckIn API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddCheckinModel> addCheckInTimeAPI(@Body RequestBody body);

    // add Checkout API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddCheckinModel> addCheckOutTimeAPI(@Body RequestBody body);

    // add OptionsInspections API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddInspectionModel> inspectionOptionsAPI(@Body RequestBody body);

    // Get Inspections API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetInspectionListModel> getInspectionAPI(@Body RequestBody body);

    // Get Employee Type List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetUserListModel> getUserTypeAPI(@Body RequestBody body);

    // Delete Employee Type List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<RemoveEmployeeModel> deleteEmployeeFromListAPI(@Body RequestBody body);

    // Get Available Employee Type List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetAvailableEmployee> getAvailEmployeeFromListAPI(@Body RequestBody body);

    @Multipart
    @POST(WebFields.API_SUB_URL)
    Call<UploadInspectionImageModel> uploadInspectionImageAPI(@Part(WebFields.ADD_INSPECTION_IMAGE.REQUEST_METHOD) RequestBody method,
                                                              @Part(WebFields.ADD_INSPECTION_IMAGE.REQUEST_INSPECTION_ID) RequestBody InspectionID,
                                                              @Part MultipartBody.Part file);

    // Get Available Employee Type List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetCompanyListModel> getCompanyListAPI(@Body RequestBody body);

    // Delete Doc From List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<RemoveDocModel> deleteDocumentAPI(@Body RequestBody body);

    // Upload Document Image API Interface
    @Multipart
    @POST(WebFields.API_SUB_URL)
    Call<EditDocumentModel> editDocumentAPI(@Part(WebFields.EDIT_DOCUMENT.REQUEST_METHOD) RequestBody method,
                                            @Part(WebFields.EDIT_DOCUMENT.REQUEST_ID) RequestBody ID,
                                            @Part(WebFields.EDIT_DOCUMENT.REQUEST_TITLE) RequestBody title,
                                            @Part(WebFields.EDIT_DOCUMENT.REQUEST_DOCUMENT_ID) RequestBody date,
                                            @Part MultipartBody.Part file);

    // Send SMS API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<SMSModel> sendSMSAPI(@Body RequestBody body);

    // Send Email API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<SENDEmailModel> sendEmailAPI(@Body RequestBody body);

    // Employee Deactivate API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<DeativateEmployeeModel> deactivateEmployeeAPI(@Body RequestBody body);

}