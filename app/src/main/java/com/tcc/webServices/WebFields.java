package com.tcc.webServices;

public class WebFields {

    public static final String API_PRIVACY_POLICY = "http://societyfy.in/TheCleaingCompany/api/service/getPage?PageName=PrivacyPolicy";
    public static final String API_ABOUT_US = "http://societyfy.in/TheCleaingCompany/api/service/getPage?PageName=AboutUS";
    public static final String API_TERM_CONDITION = "http://societyfy.in/TheCleaingCompany/api/service/getPage?PageName=TermandCondition";
    public static final String API_SUB_URL = "api/service";
    // Common Params
    public final static String METHOD = "method";
    public final static String BODY = "body";
    public final static String MESSAGE = "message";
    public final static String ERROR = "error";
    public final static String ROW_COUNT = "rowCount";
    public final static String DATA = "data";
    // Base URL
    public static String API_BASE_URL = "http://societyfy.in/TheCleaingCompany/";
    public static final String IMAGE_USER_FOLDER = API_BASE_URL + "assets/uploads/user/";
    //    public static String API_BASE_URL = "http://192.168.1.7/TCC_Web/";
    public static String DOCUMENT_IMAGE_URL = API_BASE_URL + "assets/uploads/document/";
    public static String BANNER_IMAGE_URL = API_BASE_URL + "assets/uploads/banner/";

    // Configuration API Params
    public final static class CONFIGURATION {
        public final static String MODE = "getConfig";

        public final static String REQUEST_DATABASE_NAME = "DatabaseName";

        public final static String RESPONSE_TIME_SLOT_DURATION = "TimeSlotDuration";
        public final static String RESPONSE_PAGE_SIZE = "PageSize";
        public final static String RESPONSE_APPOINTMENT_CONFIGURATION = "AppointmentConfiguration";
    }

    // Login API Params
    public final static class LOGIN {
        public final static String MODE = "checkLogin";

        public final static String REQUEST_EMAIL_ID = "EmailID";
        public final static String REQUEST_PASSWORD = "Password";
        public final static String DEVICE_NAME = "DeviceName";
        public final static String REQUEST_DEVICE_UID = "DeviceUID";
        public final static String OS_VERSION = "OSVersion";
        public final static String DEVICE_TOKEN_ID = "DeviceTokenID";
        public final static String REQUEST_DEVICE_TYPE = "DeviceType";
        public final static String DEVICE_OS = "DeviceOS";
        public final static String USER_TYPE = "UserType";

        public final static String RESPONSE_USERID = "UserID";
        public final static String RESPONSE_ROLE_ID = "RoleID";
        public final static String RESPONSE_EMAIL_ID = "EmailID";
        public final static String RESPONSE_MOBILE_NO = "MobileNo";
        public final static String RESPONSE_PASSWORD = "Password";
        public final static String RESPONSE_USER_TYPE = "UserType";
        public final static String RESPONSE_FIRST_NAME = "FirstName";
        public final static String RESPONSE_LAST_NAME = "LastName";
        public final static String RESPONSE_ADDRESS = "Address";
        public final static String RESPONSE_CITY = "CityName";
        public final static String RESPONSE_ADHAARCARD = "AadharCardNo";
        public final static String RESPONSE_PHOTOURL = "PhotoURL";
        public final static String RESPONSE_CITYID = "CityID";
        public final static String RESPONSE_COUNTRYID = "CountryID";
        public final static String RESPONSE_STATEID = "StateID";
        public final static String RESPONSE_SALARY = "Salary";
        public final static String RESPONSE_ADHAARCARDIMAGE = "AadharCardImage";
        public final static String RESPONSE_MODULE_ID = "ModuleID";

    }

    // Change Password API Params
    public final static class CHANGE_PASSWORD {
        public final static String MODE = "changePassword";

        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_OLD_PASSWORD = "OldPassword";
        public final static String REQUEST_PASSWORD = "Password";
    }

    // Forgot Password API Params
    public final static class FORGOT_PASSWORD {
        public final static String MODE = "forgotPassword";

        public final static String REQUEST_EMAIL_ID = "EmailID";

        public static final String RESPONSE_USERID = "UserID";
        public static final String RESPONSE_ROLEID = "RoleID";
        public static final String RESPONSE_EMAILID = "EmailID";
        public static final String RESPONSE_MOBILENO = "MobileNo";
        public static final String RESPONSE_PASSWORD = "Password";
        public static final String RESPONSE_USERTYPE = "UserType";
        public static final String RESPONSE_FIRSTNAME = "FirstName";
        public static final String RESPONSE_LASTNAME = "LastName";
        public static final String RESPONSE_PHOTO_URL = "PhotoURL";
    }

    // Get Services API Params
    public final static class VISITOR_SURVICES {
        public final static String MODE = "getService";


    }

    // Get Services API Params
    public final static class DASHBOARD_COUNT {
        public final static String MODE = "getDashboard";

        public final static String USERID = "UserID";
        public final static String FILTERTYPE = "FilterType";

    }

    // Get Services API Params
    public final static class ROLE {
        public final static String MODE = "getRole";

        public final static String ROLEID = "RoleID";


    }

    // Get Services API Params
    public final static class NOTIFICATION {
        public final static String MODE = "getNotification";

        public final static String USERID = "UserID";
        public final static String PAGE_SIZE = "PageSize";
        public final static String CURRENT_PAGE = "CurrentPage";
    }

    // Get Visitor API Params
    public final static class GET_VISITOR {
        public final static String MODE = "getVisitor";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_NAME = "Name";
        public static final String REQUEST_EMAIL = "EmailID";
        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_SERVICE_ID = "ServiceID";
        public static final String REQUEST_LEAD_TYPE = "LeadType";
        public static final String REQUEST_COMPANY_NAME = "CompanyName";
        public final static String REQUEST_MOBILENO = "MobileNo";
        public final static String REQUEST_CITY_ID = "CityID";


    }

    // Add Reminder API Params
    public final static class ADD_REMINDER {
        public final static String MODE = "addVisitorReminder";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_VISITOR_ID = "VisitorID";
        public static final String REQUEST_MESSAGE = "Message";
        public static final String REQUEST_REMINDER_DATE = "ReminderDate";
        public static final String REQUEST_PAST_DATE = "PastDate";


    }

    // Add CustomerReminder API Params
    public final static class ADD_REMINDER_CUSTOMER {
        public final static String MODE = "addCustomerPaymentReminder";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_CUSTOMER_ID = "SitesID";
        public static final String REQUEST_MESSAGE = "Message";
        public static final String REQUEST_REMINDER_DATE = "ReminderDate";
        public static final String REQUEST_PAST_DATE = "PastDate";
        public static final String REQUEST_AMOUNT = "Amount";
        public static final String REQUEST_QUOTATION_ID = "QuotationID";


    }

    // Get Reminder API Params
    public final static class GET_REMINDER_LIST {
        public final static String MODE = "getVisitorReminder";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_VISITOR_ID = "VisitorID";

    }

    // Get Reminder API Params
    public final static class GET_FOLLOWUP {
        public final static String MODE = "getDashboardFollowUp";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_USER_ID = "UserID";
        public static final String REQUEST_FILTER_TYPE = "FilterType";

    }

    // Get Reminder API Params
    public final static class GET_COLLECTION {
        public final static String MODE = "getServiceWiseCollection";

        public static final String REQUEST_FILTER_TYPE = "FilterType";

    }

    // Get Customer Reminder API Params
    public final static class GET_REMINDER_LIST_CUSTOMER {
        public final static String MODE = "getCustomerPaymentReminder";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_SITE_ID = "SitesID";
        public static final String REQUEST_QUOTATION_ID = "QuotationID";

    }

    // Add Response API Params
    public final static class ADD_RESPONSE {
        public final static String MODE = "addResponse";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_RESPONSE_ID = "ReminderID";
        public static final String REQUEST_RESPONSE = "Response";


    }

    // Get Response API Params
    public final static class GET_RESPONSE {
        public final static String MODE = "getResponse";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_REMINDER_ID = "ReminderID";

    }

    // Get Response API Params
    public final static class GET_REPORT {
        public final static String MODE = "getReportEmployeeTraining";
        public final static String MODE2 = "getReportEmployeeUniform";
        public final static String MODE3 = "getReportCustomerPayment";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_FORM_DATE = "FromDate";
        public static final String REQUEST_END_DATE = "EndDate";

    }

    // Get Customer API Params
    public final static class GET_CUSTOMER {
        public final static String MODE = "getCustomer";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_NAME = "Name";
        public static final String REQUEST_EMAIL = "EmailID";
        public static final String REQUEST_USERID = "UserID";
        public final static String REQUEST_CITY_ID = "CityID";

    }

    // Get Customer API Params
    public final static class GET_METIREAL {
        public final static String MODE = "getMaterial";

    }

    // Get Customer API Params
    public final static class GET_MODULE {
        public final static String MODE = "getModule";

    }

    // Get Employee API Params
    public final static class GET_EMPLOYEE {
        public final static String MODE = "getEmployee";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_NAME = "Name";
        public static final String REQUEST_EMAIL = "EmailID";
        public final static String REQUEST_CITY_ID = "CityID";

    }

    // Get Employee Uniform List API Params
    public final static class Get_EMPLOYEE_UNIFORM_LIST {
        public final static String MODE = "getEmployeeUniform";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_USER_ID = "UserID";

    }

    // Get Uniform API Params
    public final static class GETUNIFORM {
        public final static String MODE = "getUniformType";

    }

    // Add Uniform API Params
    public final static class ADDUNIFORM {
        public final static String MODE = "addEmployeeUniform";

        public static final String REQUEST_USER_ID = "UserID";
        public static final String REQUEST_USER_TYPE_ID = "UniformTypeID";
        public static final String REQUEST_DATE = "UniformDate";

    }

    // Add Room API Params
    public final static class ADD_ROOM {
        public final static String MODE = "addEmployeeRoom";

        public static final String REQUEST_ROOM_NO = "RoomNo";
        public static final String REQUEST_ROOM_ADDRESS = "RoomAddress";
        public static final String REQUEST_START_DATE = "StartDate";
        public static final String REQUEST_END_DATE = "EndDate";
        public static final String REQUEST_USERID = "UserID";
    }

    // Add Salary API Params
    public final static class ADD_SALARY {
        public final static String MODE = "addEmployeePayment";

        public static final String REQUEST_AMOUNT = "Amount";
        public static final String REQUEST_DESC = "Description";
        public static final String REQUEST_DATE = "PaymentDate";
        public static final String REQUEST_USERID = "UserID";
    }

    // Get Room API Params
    public final static class GET_ROOM {
        public final static String MODE = "getEmployeeRoom";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_USERID = "UserID";

    }

    // Get Room API Params
    public final static class GET_PAYMENT {
        public final static String MODE = "getEmployeePayment";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_USERID = "UserID";

    }

    // Get Site API Params
    public final static class GET_SITE {
        public final static String MODE = "getSites";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_CUSTOMER_ID = "CustomerID";
        public static final String REQUEST_VISITORID = "VisitorID";
        public static final String REQUEST_SITE_NAME = "SitesName";
        public static final String REQUEST_COMPANY_NAME = "CompanyName";


    }

    // Get Site API Params
    public final static class GET_INVOICE {
        public final static String MODE = "getInvoice";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_QUOTATION_ID = "QuotationID";

    }

    // Get CustomerPayment API Params
    public final static class GET_CUSTOMER_PAYMENT {
        public final static String MODE = "getCustomerPayment";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_SITES_ID = "SitesID";
        public static final String REQUEST_QUOTATION_ID = "QuotationID";

    }

    // Add CustomerPayment API Params
    public final static class ADD_CUSTOMER_PAYMENT {
        public final static String MODE = "addCustomerPayment";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_SITES_ID = "SitesID";
        public static final String REQUEST_AMOUNT_TYPE = "AmountType";
        public static final String REQUEST_PAYMENT_AMOUNT = "PaymentAmount";
        public static final String REQUEST_GST_AMOUNT = "GSTAmount";
        public static final String REQUEST_PAYMENTDATE = "PaymentDate";
        public static final String REQUEST_PAYMENT_MODE = "PaymentMode";
        public static final String REQUEST_CHEQUE_NO = "ChequeNo";
        public static final String REQUEST_IFSC_CODE = "IFCCode";
        public static final String REQUEST_ACCOUNT_NO = "AccountNo";
        public static final String REQUEST_BANK_NAME = "BankName";
        public static final String REQUEST_BRANCH_NAME = "BranchName";
        public static final String REQUEST_UTR = "UTR";
        public static final String REQUEST_QUOTATION_ID = "QuotationID";

    }

    // Get Customer Reminder API Params
    public final static class GET_CUSTOMER_PROCESS {
        public final static String MODE = "getCustomerProcess";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_CUSTOMER_ID = "CustomerID";

    }

    // Upload Document API Params
    public final static class UPLOAD_DOCUMENT {
        public final static String MODE = "addCustomerSitesDocument";

        public static final String REQUEST_ID = "UserID";
        public static final String REQUEST_TITLE = "Title";
        public static final String REQUEST_FILENAME = "ImageData";
        public static final String REQUEST_SITEID = "SitesID";
        public static final String REQUEST_METHOD = "method";
        public static final String REQUEST_QUOTATIONID = "QuotationID";
    }

    // Get Customer Document API Params
    public final static class GET_CUSTOMER_DOCUMENT {
        public final static String MODE = "getCustomerSitesDocument";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_SITESID = "SitesID";
        public static final String REQUEST_QUOTATIONID = "QuotationID";
    }

    // Get Customer Document API Params
    public final static class GET_EMPLOYYE_TRAINGING {
        public final static String MODE = "getEmployeeTraining";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_EMPLOYEEID = "EmployeeID";

    }

    // Get Customer Document API Params
    public final static class GET_EMPLOYYE_TRAINGING_COMBOBOX {
        public final static String MODE = "getTrainingDateTime";

    }

    // Get Customer Document API Params
    public final static class ADD_TRAINING {
        public final static String MODE = "addEmployeeTraining";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_TRAINING_DATETIME_ID = "TrainingDateTimeID";
        public static final String REQUEST_EMPLOYEEID = "EmployeeID";

    }

    // Get Customer Document API Params
    public final static class GET_DESIGNATION {
        public final static String MODE = "getDesignation";

    }

    // Profile User API Params
    public final static class REGISTER_USER {
        public final static String MODE = "editProfile";

        public final static String REQUEST_USERID = "UserID";
        public final static String REQUEST_FIRSTNAME = "FirstName";
        public final static String REQUEST_LASTNAME = "LastName";
        public final static String REQUEST_MOBILENO = "MobileNo";
        public final static String REQUEST_ADDRESS = "Address";
        public final static String REQUEST_DESIGNATION_ID = "DesignationID";
        public final static String REQUEST_USER_TYPE_ID = "UsertypeID";
        public final static String REQUEST_SALARY = "Salary";
        public final static String REQUEST_IMAGE = "ImageData";
        public final static String REQUEST_METHOD = "method";
        public final static String REQUEST_IMAGE_NAME = "ImageName";
    }

    // Add Ticket API Params
    public final static class ADD_TICKET {
        public final static String MODE = "addTicket";

        public final static String REQUEST_USERID = "UserID";
        public final static String REQUEST_TITLE = "Title";
        public final static String REQUEST_DESC = "Description";
        public final static String REQUEST_PRIORITY = "Priority";
        public final static String REQUEST_IMAGE = "ImageData";
        public final static String REQUEST_METHOD = "method";
        public final static String REQUEST_CITY_ID = "CityID";
        public final static String REQUEST_SUBJECT_ID = "SubjectID";
    }

    // Add Ticket API Params
    public final static class ADD_EMPLOYEE {
        public final static String MODE = "addEmployee";

        public final static String REQUEST_FIRSTNAME = "FirstName";
        public final static String REQUEST_LASTNAME = "LastName";
        public final static String REQUEST_EMAIL = "EmailID";
        public final static String REQUEST_PASSWORD = "Password";
        public final static String REQUEST_MOBILENO = "MobileNo";
        public final static String REQUEST_ADDRESS = "Address";
        public final static String REQUEST_USERTYPE_ID = "UsertypeID";
        public final static String REQUEST_SALARY = "Salary";
        public final static String REQUEST_JOINING_DATE = "JoiningDate";
        public final static String REQUEST_WORKING_HOURS = "WorkingHours";
        public final static String REQUEST_IMAGE = "ImageData";
        public final static String REQUEST_DOCUMENT_IMAGE = "DocumentImageData";
        public final static String REQUEST_OFFER_IMAGE = "OfferletterData";
        public final static String REQUEST_BANK_NAME = "BankName";
        public final static String REQUEST_BRANCH_NAME = "BranchName";
        public final static String REQUEST_ACCOUNT_NO = "AccountNo";
        public final static String REQUEST_IFSC_CODE = "IFSCCode";
        public final static String REQUEST_CITY_ID = "CityID";
        public final static String REQUEST_METHOD = "method";

    }

    // Get Penalty Reason API Params
    public final static class GET_PENALTY_REASON {
        public final static String MODE = "getReason";
    }

    // Get Team Definition API Params
    public final static class GET_TEAM_DEFINITION {
        public final static String MODE = "getTeamDefinition";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_SITESID = "SitesID";
    }

    // Get Room API Params
    public final static class GET_TICKET_LIST {
        public final static String MODE = "getTicket";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_USERID = "UserID";

    }

    // Get Room API Params
    public final static class GET_TEAM_EMPLOYEE {
        public final static String MODE = "getTeamEmployee";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_SITESID = "SitesID";
        public static final String REQUEST_ATTENDANCE_DATE = "AttendanceDate";
        public static final String REQUEST_QUOTATION_ID = "QuotationID";
    }

    // Get Attendance API Params
    public final static class GET_ATTENDANCE {
        public final static String MODE = "getAttendance";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_SITESID = "SitesID";
        public static final String REQUEST_QUOTATION_ID = "QuotationID";
    }

    // Get Room API Params
    public final static class GET_EMPLOYEE_ATTENDANCE {
        public final static String MODE = "getEmployeeAttendance";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_USERID = "UserID";

    }

    // Get Room API Params
    public final static class ADD_EMPLOYEE_ATTENDANCE {
        public final static String MODE = "addEmployeeAttendance";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_DATE = "AttendanceDate";
        public static final String ITEM = "Item";
    }

    // Add Penalty API Params
    public final static class ADD_PENALTY {
        public final static String MODE = "addPenalty";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_DATE = "PenaltyDate";
        public static final String PENALTY = "Penalty";
        public static final String REQUEST_REASON = "Reason";
        public static final String REQUEST_SITESID = "SitesID";
        public static final String REQUEST_EMPLOYEE_ID = "EmployeeID";
        public static final String REQUEST_ITEM = "Item";
    }

    // Get Penalty API Params
    public final static class GET_PENALTY {
        public final static String MODE = "getPenalty";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
    }

    // Add TeamDefinition API Params
    public final static class ADD_TEAM_DEFINITION {
        public final static String MODE = "addTeamDefinition";

        public static final String REQUEST_USERID = "UserID";
        public static final String ITEM = "Item";
    }

    // Get Quotation API Params
    public final static class GET_QUOTATION {
        public final static String MODE = "getQuotation";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_VISITOR_ID = "VisitorID";
        public static final String REQUEST_CUSTOMER_ID = "CustomerID";
        public static final String REQUEST_SITESID = "SitesID";
    }

    // Add Sites API Params
    public final static class ADD_SITES {
        public final static String MODE = "addSites";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_CUSTOMER_ID = "CustomerID";
        public static final String REQUEST_SITE_NAME = "SitesName";
        public static final String REQUEST_COMPANY_NAME = "CompanyName";
        public static final String REQUEST_COMPANY_ADDRESS = "CompanyAddress";
        public static final String REQUEST_VISITOR_ID = "VisitorID";
    }

    // Add Invoice API Params
    public final static class ADD_INVOICE {
        public final static String MODE = "generateInvoice";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_CUSTOMER_ID = "CustomerID";
        public static final String REQUEST_SITE_ID = "SitesID";
        public static final String REQUEST_QUOTATION_ID = "QuotationID";
        public static final String REQUEST_START_DATE = "StartDate";
        public static final String REQUEST_END_DATE = "EndDate";
        public static final String REQUEST_ITEM = "Item";
    }

    // Get Usertype API Params
    public final static class GET_USER_TYPE {
        public final static String MODE = "getUsertype";

    }

    // Add Quotation API Params
    public final static class ADD_QUOTATION {
        public final static String MODE = "addQuotation";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_CUSTOMER_ID = "CustomerID";
        public static final String REQUEST_COMPANY_NAME = "CompanyName";
        public static final String REQUEST_COMPANY_ADDRESS = "Address";
        public static final String REQUEST_VISITOR_ID = "VisitorID";
        public static final String REQUEST_SITE_ID = "SitesID";
        public static final String REQUEST_SERVICE_ID = "ServiceID";
        public static final String REQUEST_QUOTATION_NAME = "QuotationName";
        public static final String REQUEST_ESTIMATE_DATE = "EstimateDate";
        public static final String REQUEST_START_DATE = "StartDate";
        public static final String REQUEST_ESTIMATE_NO = "EstimateNo";
        public static final String REQUEST_ISGST = "IsGST";
        public static final String REQUEST_TOTAL_AMOUNT = "TotalAmount";
        public static final String REQUEST_GST_AMOUNT = "GSTAmount";
        public static final String REQUEST_CGST = "CGST";
        public static final String REQUEST_SGST = "SGST";
        public static final String REQUEST_GST_NO = "GSTNo";
        public static final String REQUEST_ONLINE = "Online";
        public static final String REQUEST_ITEM = "Item";

    }

    // Add Status Quotation API Params
    public final static class ADD_STATUS {
        public final static String MODE = "convertToQuotation";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_QUOTATION_ID = "QuotationID";
        public static final String REQUEST_QUOTATION_STATUS = "QuotationStatus";
        public static final String REQUEST_REASON_ID = "ReasonID";
        public static final String REQUEST_START_DATE = "StartDate";
        public static final String REQUEST_END_DATE = "EndDate";
        public static final String REQUEST_CUSTOMER_ID = "CustomerID";
        public static final String REQUEST_FO_ID = "FieldOperatorID";
        public static final String REQUEST_QM_ID = "QualityManagerID";
        public static final String REQUEST_OM_ID = "OperationManagerID";
        public static final String REQUEST_TOTAL_EMPLOYEE = "TotalEmployee";
    }

    // Convert To Sites API Params
    public final static class CONVERT_SITES {
        public final static String MODE = "convertToSites";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_CUSTOMER_ID = "CustomerID";
        public static final String REQUEST_SITE_ID = "SitesID";
    }

    // Convert To Customer API Params
    public final static class CONVERT_TO_CUSTOMER {
        public final static String MODE = "convertToCustomer";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_VISITOR_ID = "VisitorID";
        public static final String REQUEST_NAME = "Name";
        public static final String REQUEST_EMAILID = "EmailID";
        public static final String REQUEST_MOBILE_NO = "MobileNo";
        public static final String REQUEST_ADDRESS = "Address";
        public static final String REQUEST_CITY_ID = "CityID";
        public static final String REQUEST_STATE_ID = "StateID";
    }

    // Get Team Definition API Params
    public final static class SITE_VISIT {
        public final static String MODE = "editIsVisit";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_QUOTATION_ID = "QuotationID";
        public static final String REQUEST_VISIT = "IsVisit";
    }

    // Get ChecinOut API Params
    public final static class GET_CHECKINOUT {
        public final static String MODE = "getCheckinout";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_USERID = "UserID";

    }

    // Get Inspection API Params
    public final static class GET_INSPECTION {
        public final static String MODE = "getQuestion";

    }

    // ADD Checkin API Params
    public final static class ADD_CHECKINTIME {
        public final static String MODE = "addCheckIN";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_CHECKIN_TIME = "Checkintime";
        public static final String REQUEST_LATITUDE = "Inlatitude";
        public static final String REQUEST_LONGITUDE = "Inlongitude";
        public static final String REQUEST_ADDRESS = "InAddress";

    }

    // ADD CheckOut API Params
    public final static class ADD_CHECKOUTTIME {
        public final static String MODE = "addCheckOUT";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_CHECKOUT_TIME = "Checkouttime";
        public static final String REQUEST_LATITUDE = "Outlatitude";
        public static final String REQUEST_LONGITUDE = "Outlongitude";
        public static final String REQUEST_CHECKINOUT_ID = "CheckincheckoutID";
        public static final String REQUEST_ADDRESS = "OutAddress";

    }

    // Add Inspection API Params
    public final static class ADD_INSPECTION {
        public final static String MODE = "addInspection";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_SITE_ID = "SitesID";
        public static final String REQUEST_FO_ID = "FieldOperatorID";
        public static final String REQUEST_QM_ID = "QualityManagerID";
        public static final String REQUEST_OM_ID = "OperationManagerID";
        public static final String REQUEST_REMARKS = "Remarks";
        public static final String REQUEST_ITEM = "Item";

    }

    // Get Inspection API Params
    public final static class GET_INSPECTION_LIST {
        public final static String MODE = "getInspection";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_USERID = "UserID";

    }

    // Get User Type API Params
    public final static class GET_USERTYPE {
        public final static String MODE = "getUserByUserType";

        public static final String REQUEST_USER_TYPE = "UserType";

    }

    // Remove Team Employee API Params
    public final static class REMOVE_TEAM_EMPLOYEE {
        public final static String MODE = "removeTeamEmployee";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_TEAM_DEFINITION_ID = "TeamDefinitionID";
    }

    // Get Employee Available API Params
    public final static class GET_AVAIL_EMPOLYEE {
        public final static String MODE = "getavilableEmployee";

        public static final String REQUEST_PAGESIZE = "PageSize";
        public static final String REQUEST_CURRENT_PAGE = "CurrentPage";
        public static final String REQUEST_START_DATE = "StartDate";
        public static final String REQUEST_END_DATE = "EndDate";
    }

    // Add Image Inspection API Params
    public final static class ADD_INSPECTION_IMAGE {
        public final static String MODE = "addInspectionImage";

        public final static String REQUEST_INSPECTION_ID = "InspectionID";
        public final static String REQUEST_IMAGE_DATE = "ImageData";
        public final static String REQUEST_METHOD = "method";

    }

    // Get Company API Params
    public final static class GET_COMPANY {
        public final static String MODE = "getCompany";

    }

    // Remove Document API Params
    public final static class REMOVE_DOCUMENT {
        public final static String MODE = "removeCustomerSitesDocument";

        public static final String REQUEST_USERID = "UserID";
        public static final String REQUEST_DOCUMENT_ID = "CustomerSitesDocumentID";
    }

    // Edit Document API Params
    public final static class EDIT_DOCUMENT {
        public final static String MODE = "editCustomerSitesDocument";

        public static final String REQUEST_ID = "UserID";
        public static final String REQUEST_TITLE = "Title";
        public static final String REQUEST_FILENAME = "ImageData";
        public static final String REQUEST_DOCUMENT_ID = "CustomerSitesDocumentID";
        public static final String REQUEST_METHOD = "method";
    }

    // SMS API Params
    public final static class SMS_SEND {
        public final static String MODE = "sendReminderSMS";

        public static final String REQUEST_PHONE = "MobileNo";
        public static final String REQUEST_MESSAGE = "Message";

    }

    // EMAIL API Params
    public final static class SMS_EMAIL {
        public final static String MODE = "sendReminderMail";

        public static final String REQUEST_EMAILID = "EmailID";
        public static final String REQUEST_SUBJECT = "Subject";
        public static final String REQUEST_MESSAGE = "Message";
    }

    // Deactivate Employee API Params
    public final static class EMPLOYEE_DEACTIVATE {
        public final static String MODE = "deactiveEmployee";

        public static final String REQUEST_EMPLOYEE_ID = "EmployeeUserID";
        public static final String REQUEST_USER_ID = "UserID";
    }

    public class Config {
        public final static String MODE = "getConfig";

        public static final String LEAD_TYPE = "LeadType";
    }

    public class STATE {
        public final static String MODE = "getStates";
    }

    public class CITY {
        public final static String MODE = "getCities";

        public final static String REQUEST_STATE_ID = "StateID";
    }

    public class SUBJECT {
        public final static String MODE = "getSubject";

    }

    public class ADD_VISITOR {
        public final static String MODE = "addVisitor";

        public final static String REQUEST_USERID = "UserID";
        public final static String REQUEST_NAME = "Name";
        public final static String REQUEST_COMPANY_NAME = "CompanyName";
        public final static String REQUEST_EMAIL_ID = "EmailID";
        public final static String REQUEST_MOBILE = "MobileNo";
        public final static String REQUEST_LOCATION = "Location";
        public final static String REQUEST_SERVICEID = "ServiceID";
        public final static String REQUEST_PROPOSEDDATE = "ProposedDate";
        public final static String REQUEST_WORKING_DAYS = "WorkingDays";
        public final static String REQUEST_WORKING_HOURS = "WorkingHours";
        public final static String REQUEST_NO_OF_MEN = "NoOfPowerRequire";
        public final static String REQUEST_LEADTYPE = "LeadType";
        public final static String REQUEST_CITY_ID = "CityID";
        public final static String REQUEST_STATE_ID = "StateID";
    }
}