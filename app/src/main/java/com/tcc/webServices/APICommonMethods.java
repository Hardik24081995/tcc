package com.tcc.webServices;


import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;


public class APICommonMethods {

    /**
     * Sets up the media type for api call
     *
     * @return - Return Media Type
     */
    public static MediaType getMediaType() {
        return MediaType.parse("application/octet-stream");
    }

    public static String setConfigurationJson(String hospital_databse) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();

            result = setParentJsonData(WebFields.CONFIGURATION.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String setLoginJson(String mEmailId, String mPassword, String DeviceName, String DeviceUID, String OSVersion, String DeviceTokenID, String DeviceType, String DeviceOS, String UserType) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.LOGIN.REQUEST_EMAIL_ID, mEmailId);
            jsonBody.put(WebFields.LOGIN.REQUEST_PASSWORD, mPassword);
            jsonBody.put(WebFields.LOGIN.DEVICE_NAME, DeviceName);
            jsonBody.put(WebFields.LOGIN.REQUEST_DEVICE_UID, DeviceUID);
            jsonBody.put(WebFields.LOGIN.OS_VERSION, OSVersion);
            jsonBody.put(WebFields.LOGIN.DEVICE_TOKEN_ID, DeviceTokenID);
            jsonBody.put(WebFields.LOGIN.REQUEST_DEVICE_TYPE, DeviceType);
            jsonBody.put(WebFields.LOGIN.DEVICE_OS, DeviceOS);
            jsonBody.put(WebFields.LOGIN.RESPONSE_USER_TYPE, UserType);

            result = setParentJsonData(WebFields.LOGIN.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String setChangePassword(String UserID, String OldPassword, String Password) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.CHANGE_PASSWORD.REQUEST_USER_ID, UserID);
            jsonBody.put(WebFields.CHANGE_PASSWORD.REQUEST_OLD_PASSWORD, OldPassword);
            jsonBody.put(WebFields.CHANGE_PASSWORD.REQUEST_PASSWORD, Password);


            result = setParentJsonData(WebFields.CHANGE_PASSWORD.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Forgot Password API
     *
     * @param mEmailId - Email Id
     * @return - Return Result
     */
    public static String setForgotPasswordJson(String mEmailId) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.FORGOT_PASSWORD.REQUEST_EMAIL_ID, mEmailId);

            result = setParentJsonData(WebFields.FORGOT_PASSWORD.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String addVisitorJson(String UserID, String Name, String Companyname, String EmailID, String Mobile, String Location, String ServiceID,
                                        String ProposedDate, String WorkingDays, String WorkingHours, String NumberOfMen, String LeadType, String CityID, String StateID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_VISITOR.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.ADD_VISITOR.REQUEST_NAME, Name);
            jsonBody.put(WebFields.ADD_VISITOR.REQUEST_COMPANY_NAME, Companyname);
            jsonBody.put(WebFields.ADD_VISITOR.REQUEST_EMAIL_ID, EmailID);
            jsonBody.put(WebFields.ADD_VISITOR.REQUEST_MOBILE, Mobile);
            jsonBody.put(WebFields.ADD_VISITOR.REQUEST_LOCATION, Location);
            jsonBody.put(WebFields.ADD_VISITOR.REQUEST_SERVICEID, ServiceID);
            jsonBody.put(WebFields.ADD_VISITOR.REQUEST_PROPOSEDDATE, ProposedDate);
            jsonBody.put(WebFields.ADD_VISITOR.REQUEST_WORKING_DAYS, WorkingDays);
            jsonBody.put(WebFields.ADD_VISITOR.REQUEST_WORKING_HOURS, WorkingHours);
            jsonBody.put(WebFields.ADD_VISITOR.REQUEST_NO_OF_MEN, NumberOfMen);
            jsonBody.put(WebFields.ADD_VISITOR.REQUEST_LEADTYPE, LeadType);
            jsonBody.put(WebFields.ADD_VISITOR.REQUEST_CITY_ID, CityID);
            jsonBody.put(WebFields.ADD_VISITOR.REQUEST_STATE_ID, StateID);

            result = setParentJsonData(WebFields.ADD_VISITOR.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getVisitorListJson(String PageSize, String CurrentPage, String Name, String EmailID, String UserID, String ServiceID, String LeadType, String CompanyName, String Mobile, String cityID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_VISITOR.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_VISITOR.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_VISITOR.REQUEST_NAME, Name);
            jsonBody.put(WebFields.GET_VISITOR.REQUEST_EMAIL, EmailID);
            jsonBody.put(WebFields.GET_VISITOR.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.GET_VISITOR.REQUEST_SERVICE_ID, ServiceID);
            jsonBody.put(WebFields.GET_VISITOR.REQUEST_LEAD_TYPE, LeadType);
            jsonBody.put(WebFields.GET_VISITOR.REQUEST_COMPANY_NAME, CompanyName);
            jsonBody.put(WebFields.GET_VISITOR.REQUEST_MOBILENO, Mobile);
            jsonBody.put(WebFields.GET_VISITOR.REQUEST_CITY_ID, cityID);
            result = setParentJsonData(WebFields.GET_VISITOR.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String setConfigJson() {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();

            result = setParentJsonData(WebFields.Config.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getVisitorServiceJson() {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();

            result = setParentJsonData(WebFields.VISITOR_SURVICES.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getDashboardCount(String userid, String filterType) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.DASHBOARD_COUNT.USERID, userid);
            jsonBody.put(WebFields.DASHBOARD_COUNT.FILTERTYPE, filterType);

            result = setParentJsonData(WebFields.DASHBOARD_COUNT.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getRole(String roleID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ROLE.ROLEID, roleID);


            result = setParentJsonData(WebFields.ROLE.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    public static String getNotification(String userid, String pageSize, String page) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.NOTIFICATION.USERID, userid);
            jsonBody.put(WebFields.NOTIFICATION.PAGE_SIZE, pageSize);
            jsonBody.put(WebFields.NOTIFICATION.CURRENT_PAGE, page);

            result = setParentJsonData(WebFields.NOTIFICATION.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    public static String setStateListJson() {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            result = setParentJsonData(WebFields.STATE.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String setCityListJson(String mStateId) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.CITY.REQUEST_STATE_ID, mStateId);

            result = setParentJsonData(WebFields.CITY.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String setSubjectListJson() {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();

            result = setParentJsonData(WebFields.SUBJECT.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String addVisitorReminderJson(String UserID, String VisitorID, String Message, String ReminderDate, String PastDate) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_REMINDER.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.ADD_REMINDER.REQUEST_VISITOR_ID, VisitorID);
            jsonBody.put(WebFields.ADD_REMINDER.REQUEST_MESSAGE, Message);
            jsonBody.put(WebFields.ADD_REMINDER.REQUEST_REMINDER_DATE, ReminderDate);
            jsonBody.put(WebFields.ADD_REMINDER.REQUEST_PAST_DATE, PastDate);


            result = setParentJsonData(WebFields.ADD_REMINDER.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String addCustomerReminderJson(String UserID, String CustomerID, String Message, String ReminderDate, String PastDate, String Amount, String QuotationID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_REMINDER_CUSTOMER.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.ADD_REMINDER_CUSTOMER.REQUEST_CUSTOMER_ID, CustomerID);
            jsonBody.put(WebFields.ADD_REMINDER_CUSTOMER.REQUEST_MESSAGE, Message);
            jsonBody.put(WebFields.ADD_REMINDER_CUSTOMER.REQUEST_REMINDER_DATE, ReminderDate);
            jsonBody.put(WebFields.ADD_REMINDER_CUSTOMER.REQUEST_PAST_DATE, PastDate);
            jsonBody.put(WebFields.ADD_REMINDER_CUSTOMER.REQUEST_AMOUNT, Amount);
            jsonBody.put(WebFields.ADD_REMINDER_CUSTOMER.REQUEST_QUOTATION_ID, QuotationID);


            result = setParentJsonData(WebFields.ADD_REMINDER_CUSTOMER.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getVisitorReminderListJson(String PageSize, String CurrentPage, String VisitorID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_REMINDER_LIST.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_REMINDER_LIST.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_REMINDER_LIST.REQUEST_VISITOR_ID, VisitorID);

            result = setParentJsonData(WebFields.GET_REMINDER_LIST.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getFollowUp(String PageSize, String CurrentPage, String userID, String filterType) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_FOLLOWUP.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_FOLLOWUP.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_FOLLOWUP.REQUEST_USER_ID, userID);
            jsonBody.put(WebFields.GET_FOLLOWUP.REQUEST_FILTER_TYPE, filterType);

            result = setParentJsonData(WebFields.GET_FOLLOWUP.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getCOllection(String filterType) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();

            jsonBody.put(WebFields.GET_COLLECTION.REQUEST_FILTER_TYPE, filterType);

            result = setParentJsonData(WebFields.GET_COLLECTION.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String addVisitorResponseJson(String UserID, String ReminderID, String Response) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_RESPONSE.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.ADD_RESPONSE.REQUEST_RESPONSE_ID, ReminderID);
            jsonBody.put(WebFields.ADD_RESPONSE.REQUEST_RESPONSE, Response);

            result = setParentJsonData(WebFields.ADD_RESPONSE.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getCustomerReminderListJson(String PageSize, String CurrentPage, String SitesID, String QuotationID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_REMINDER_LIST_CUSTOMER.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_REMINDER_LIST_CUSTOMER.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_REMINDER_LIST_CUSTOMER.REQUEST_SITE_ID, SitesID);
            jsonBody.put(WebFields.GET_REMINDER_LIST_CUSTOMER.REQUEST_QUOTATION_ID, QuotationID);

            result = setParentJsonData(WebFields.GET_REMINDER_LIST_CUSTOMER.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getVisitorResponseJson(String PageSize, String CurrentPage, String ReminderID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_RESPONSE.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_RESPONSE.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_RESPONSE.REQUEST_REMINDER_ID, ReminderID);

            result = setParentJsonData(WebFields.GET_RESPONSE.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getReportTraningJson(String PageSize, String CurrentPage, String formDate, String endDate) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_REPORT.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_REPORT.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_REPORT.REQUEST_FORM_DATE, formDate);
            jsonBody.put(WebFields.GET_REPORT.REQUEST_END_DATE, endDate);

            result = setParentJsonData(WebFields.GET_REPORT.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getReportUniformJson(String PageSize, String CurrentPage, String formDate, String endDate) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_REPORT.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_REPORT.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_REPORT.REQUEST_FORM_DATE, formDate);
            jsonBody.put(WebFields.GET_REPORT.REQUEST_END_DATE, endDate);

            result = setParentJsonData(WebFields.GET_REPORT.MODE2, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getReportPaymentJson(String PageSize, String CurrentPage, String formDate, String endDate) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_REPORT.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_REPORT.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_REPORT.REQUEST_FORM_DATE, formDate);
            jsonBody.put(WebFields.GET_REPORT.REQUEST_END_DATE, endDate);

            result = setParentJsonData(WebFields.GET_REPORT.MODE3, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static String getCustomerListJson(String PageSize, String CurrentPage, String Name, String EmailID, String UserID, String cityID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_CUSTOMER.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_CUSTOMER.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_CUSTOMER.REQUEST_NAME, Name);
            jsonBody.put(WebFields.GET_CUSTOMER.REQUEST_EMAIL, EmailID);
            jsonBody.put(WebFields.GET_CUSTOMER.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.GET_CUSTOMER.REQUEST_CITY_ID, cityID);

            result = setParentJsonData(WebFields.GET_CUSTOMER.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getMetirealList() {
        String result = AppConstants.STR_EMPTY_STRING;
        JSONObject jsonBody = new JSONObject();
        result = setParentJsonData(WebFields.GET_METIREAL.MODE, jsonBody);

        return result;
    }

    public static String getModuleList() {
        String result = AppConstants.STR_EMPTY_STRING;
        JSONObject jsonBody = new JSONObject();
        result = setParentJsonData(WebFields.GET_MODULE.MODE, jsonBody);

        return result;
    }

    public static String getEmployeeListJson(String PageSize, String CurrentPage, String Name, String EmailID, String cityID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_EMPLOYEE.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_EMPLOYEE.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_EMPLOYEE.REQUEST_NAME, Name);
            jsonBody.put(WebFields.GET_EMPLOYEE.REQUEST_EMAIL, EmailID);
            jsonBody.put(WebFields.GET_EMPLOYEE.REQUEST_CITY_ID, cityID);

            result = setParentJsonData(WebFields.GET_EMPLOYEE.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getEmployeeUniformListJson(String PageSize, String CurrentPage, String UserID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.Get_EMPLOYEE_UNIFORM_LIST.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.Get_EMPLOYEE_UNIFORM_LIST.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.Get_EMPLOYEE_UNIFORM_LIST.REQUEST_USER_ID, UserID);

            result = setParentJsonData(WebFields.Get_EMPLOYEE_UNIFORM_LIST.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getUniformJson() {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            result = setParentJsonData(WebFields.GETUNIFORM.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String addUniformJson(String UserID, String UniformTypeID, String UniformDate) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADDUNIFORM.REQUEST_USER_ID, UserID);
            jsonBody.put(WebFields.ADDUNIFORM.REQUEST_USER_TYPE_ID, UniformTypeID);
            jsonBody.put(WebFields.ADDUNIFORM.REQUEST_DATE, UniformDate);

            result = setParentJsonData(WebFields.ADDUNIFORM.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String addRoomAllocationJson(String DateS, String DateE, String Address, String number, String UserID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_ROOM.REQUEST_START_DATE, DateS);
            jsonBody.put(WebFields.ADD_ROOM.REQUEST_END_DATE, DateE);
            jsonBody.put(WebFields.ADD_ROOM.REQUEST_ROOM_ADDRESS, Address);
            jsonBody.put(WebFields.ADD_ROOM.REQUEST_ROOM_NO, number);
            jsonBody.put(WebFields.ADD_ROOM.REQUEST_USERID, UserID);


            result = setParentJsonData(WebFields.ADD_ROOM.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String addSalary(String DateS, String Amount, String description, String UserID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_SALARY.REQUEST_DATE, DateS);
            jsonBody.put(WebFields.ADD_SALARY.REQUEST_AMOUNT, Amount);
            jsonBody.put(WebFields.ADD_SALARY.REQUEST_DESC, description);
            jsonBody.put(WebFields.ADD_SALARY.REQUEST_USERID, UserID);


            result = setParentJsonData(WebFields.ADD_SALARY.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getRoomAllocJson(String PageSize, String CurrentPage, String UserID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_ROOM.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_ROOM.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_ROOM.REQUEST_USERID, UserID);

            result = setParentJsonData(WebFields.GET_ROOM.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getPaymentList(String PageSize, String CurrentPage, String UserID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_PAYMENT.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_PAYMENT.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_PAYMENT.REQUEST_USERID, UserID);

            result = setParentJsonData(WebFields.GET_PAYMENT.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static String getSiteListJson(String PageSize, String CurrentPage, String UserID, String CustomerID, String VisitorID, String SitesName, String CompanyName) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_SITE.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_SITE.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_SITE.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.GET_SITE.REQUEST_CUSTOMER_ID, CustomerID);
            jsonBody.put(WebFields.GET_SITE.REQUEST_VISITORID, VisitorID);
            jsonBody.put(WebFields.GET_SITE.REQUEST_SITE_NAME, SitesName);
            jsonBody.put(WebFields.GET_SITE.REQUEST_COMPANY_NAME, CompanyName);

            result = setParentJsonData(WebFields.GET_SITE.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static String getInvoiceListJson(String PageSize, String CurrentPage, String QuotationID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_INVOICE.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_INVOICE.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_INVOICE.REQUEST_QUOTATION_ID, QuotationID);

            result = setParentJsonData(WebFields.GET_INVOICE.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static String getCustomerPaymentJson(String PageSize, String CurrentPage, String SitesID, String QuotationID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_CUSTOMER_PAYMENT.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_CUSTOMER_PAYMENT.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_CUSTOMER_PAYMENT.REQUEST_SITES_ID, SitesID);
            jsonBody.put(WebFields.GET_CUSTOMER_PAYMENT.REQUEST_QUOTATION_ID, QuotationID);

            result = setParentJsonData(WebFields.GET_CUSTOMER_PAYMENT.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String addCustomerPaymentJson(String UserID, String SitesID, String Amounttype, String PaymentAmount, String GSTAmount, String PaymentDate, String PaymentMode, String ChequeNo
            , String IFCCode, String AccountNo, String BankName, String BranchName, String UTR, String QuotationID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_CUSTOMER_PAYMENT.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.ADD_CUSTOMER_PAYMENT.REQUEST_SITES_ID, SitesID);
            jsonBody.put(WebFields.ADD_CUSTOMER_PAYMENT.REQUEST_AMOUNT_TYPE, Amounttype);
            jsonBody.put(WebFields.ADD_CUSTOMER_PAYMENT.REQUEST_PAYMENT_AMOUNT, PaymentAmount);
            jsonBody.put(WebFields.ADD_CUSTOMER_PAYMENT.REQUEST_GST_AMOUNT, GSTAmount);
            jsonBody.put(WebFields.ADD_CUSTOMER_PAYMENT.REQUEST_PAYMENTDATE, PaymentDate);
            jsonBody.put(WebFields.ADD_CUSTOMER_PAYMENT.REQUEST_PAYMENT_MODE, PaymentMode);
            jsonBody.put(WebFields.ADD_CUSTOMER_PAYMENT.REQUEST_CHEQUE_NO, ChequeNo);
            jsonBody.put(WebFields.ADD_CUSTOMER_PAYMENT.REQUEST_IFSC_CODE, IFCCode);
            jsonBody.put(WebFields.ADD_CUSTOMER_PAYMENT.REQUEST_ACCOUNT_NO, AccountNo);
            jsonBody.put(WebFields.ADD_CUSTOMER_PAYMENT.REQUEST_BANK_NAME, BankName);
            jsonBody.put(WebFields.ADD_CUSTOMER_PAYMENT.REQUEST_BRANCH_NAME, BranchName);
            jsonBody.put(WebFields.ADD_CUSTOMER_PAYMENT.REQUEST_UTR, UTR);
            jsonBody.put(WebFields.ADD_CUSTOMER_PAYMENT.REQUEST_QUOTATION_ID, QuotationID);

            result = setParentJsonData(WebFields.ADD_CUSTOMER_PAYMENT.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getCustomerProcessJson(String PageSize, String CurrentPage, String CustomerID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_CUSTOMER_PROCESS.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_CUSTOMER_PROCESS.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_CUSTOMER_PROCESS.REQUEST_CUSTOMER_ID, CustomerID);

            result = setParentJsonData(WebFields.GET_CUSTOMER_PROCESS.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getCustomerDocumentJson(String PageSize, String CurrentPage, String SitesID, String QuotationID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_CUSTOMER_DOCUMENT.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_CUSTOMER_DOCUMENT.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_CUSTOMER_DOCUMENT.REQUEST_SITESID, SitesID);
            jsonBody.put(WebFields.GET_CUSTOMER_DOCUMENT.REQUEST_QUOTATIONID, QuotationID);

            result = setParentJsonData(WebFields.GET_CUSTOMER_DOCUMENT.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getEmployeeTrainingJson(String PageSize, String CurrentPage, String EmployeeID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_EMPLOYYE_TRAINGING.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_EMPLOYYE_TRAINGING.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_EMPLOYYE_TRAINGING.REQUEST_EMPLOYEEID, EmployeeID);

            result = setParentJsonData(WebFields.GET_EMPLOYYE_TRAINGING.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static String getTrainingSpinnerJson() {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            result = setParentJsonData(WebFields.GET_EMPLOYYE_TRAINGING_COMBOBOX.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String addEmployeeTrainingJson(String UserID, String TrainingDateTimeID, String EmployeeID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_TRAINING.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.ADD_TRAINING.REQUEST_TRAINING_DATETIME_ID, TrainingDateTimeID);
            jsonBody.put(WebFields.ADD_TRAINING.REQUEST_EMPLOYEEID, EmployeeID);

            result = setParentJsonData(WebFields.ADD_TRAINING.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getDesignationJson() {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            result = setParentJsonData(WebFields.GET_DESIGNATION.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getPenaltyReasonJson() {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            result = setParentJsonData(WebFields.GET_PENALTY_REASON.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getTicket(String PageSize, String CurrentPage, String UserID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_TICKET_LIST.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_TICKET_LIST.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_TICKET_LIST.REQUEST_USERID, UserID);

            result = setParentJsonData(WebFields.GET_TICKET_LIST.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getTeamDefinitioJson(String PageSize, String CurrentPage, String SitesID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_TEAM_DEFINITION.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_TEAM_DEFINITION.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_TEAM_DEFINITION.REQUEST_SITESID, SitesID);

            result = setParentJsonData(WebFields.GET_TEAM_DEFINITION.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getTeamEmployeeJson(String PageSize, String CurrentPage, String SitesID, String AttendanceDate, String QuotationID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_TEAM_EMPLOYEE.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_TEAM_EMPLOYEE.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_TEAM_EMPLOYEE.REQUEST_SITESID, SitesID);
            jsonBody.put(WebFields.GET_TEAM_EMPLOYEE.REQUEST_ATTENDANCE_DATE, AttendanceDate);
            jsonBody.put(WebFields.GET_TEAM_EMPLOYEE.REQUEST_QUOTATION_ID, QuotationID);

            result = setParentJsonData(WebFields.GET_TEAM_EMPLOYEE.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getAtendance(String PageSize, String CurrentPage, String SitesID, String QuotationID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_ATTENDANCE.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_ATTENDANCE.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_ATTENDANCE.REQUEST_SITESID, SitesID);
            jsonBody.put(WebFields.GET_ATTENDANCE.REQUEST_QUOTATION_ID, QuotationID);

            result = setParentJsonData(WebFields.GET_ATTENDANCE.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getEmployeeAttendanceJson(String PageSize, String CurrentPage, String UserID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_EMPLOYEE_ATTENDANCE.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_EMPLOYEE_ATTENDANCE.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_EMPLOYEE_ATTENDANCE.REQUEST_USERID, UserID);

            result = setParentJsonData(WebFields.GET_EMPLOYEE_ATTENDANCE.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String addEmployeeAttendanceJson(String UserID, String Date, JSONArray Item) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_EMPLOYEE_ATTENDANCE.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.ADD_EMPLOYEE_ATTENDANCE.REQUEST_DATE, Date);
            jsonBody.put(WebFields.ADD_EMPLOYEE_ATTENDANCE.ITEM, Item);

            result = setParentJsonData(WebFields.ADD_EMPLOYEE_ATTENDANCE.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String AddPenaltyJson(String UserID, String Date, String Reason, String SiteID, JSONArray item) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_PENALTY.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.ADD_PENALTY.REQUEST_DATE, Date);
            jsonBody.put(WebFields.ADD_PENALTY.REQUEST_REASON, Reason);
            jsonBody.put(WebFields.ADD_PENALTY.REQUEST_SITESID, SiteID);
            jsonBody.put(WebFields.ADD_PENALTY.REQUEST_ITEM, item);

            result = setParentJsonData(WebFields.ADD_PENALTY.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getPenatyJson(String PageSize, String CurrentPage) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_PENALTY.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_PENALTY.REQUEST_CURRENT_PAGE, CurrentPage);

            result = setParentJsonData(WebFields.GET_PENALTY.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String addTeamDefinitonJson(String UserID, JSONArray Item) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_TEAM_DEFINITION.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.ADD_TEAM_DEFINITION.ITEM, Item);

            result = setParentJsonData(WebFields.ADD_TEAM_DEFINITION.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getQuotationJson(String PageSize, String CurrentPage, String SitesID, String CustomerID, String VisitorID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_QUOTATION.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_QUOTATION.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_QUOTATION.REQUEST_SITESID, SitesID);
            jsonBody.put(WebFields.GET_QUOTATION.REQUEST_CUSTOMER_ID, CustomerID);
            jsonBody.put(WebFields.GET_QUOTATION.REQUEST_VISITOR_ID, VisitorID);

            result = setParentJsonData(WebFields.GET_QUOTATION.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String addSites(String UserID, String CustomerID, String SitesName, String CompanyName, String CompanyAddress, String VisitorID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_SITES.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.ADD_SITES.REQUEST_CUSTOMER_ID, CustomerID);
            jsonBody.put(WebFields.ADD_SITES.REQUEST_SITE_NAME, SitesName);
            jsonBody.put(WebFields.ADD_SITES.REQUEST_COMPANY_NAME, CompanyName);
            jsonBody.put(WebFields.ADD_SITES.REQUEST_COMPANY_ADDRESS, CompanyAddress);
            jsonBody.put(WebFields.ADD_SITES.REQUEST_VISITOR_ID, VisitorID);

            result = setParentJsonData(WebFields.ADD_SITES.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static String addInvoice(String UserID, String CustomerID, String SiteID, String QuotationID, String StartDate, String EndDate, JSONArray item) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_INVOICE.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.ADD_INVOICE.REQUEST_CUSTOMER_ID, CustomerID);
            jsonBody.put(WebFields.ADD_INVOICE.REQUEST_SITE_ID, SiteID);
            jsonBody.put(WebFields.ADD_INVOICE.REQUEST_QUOTATION_ID, QuotationID);
            jsonBody.put(WebFields.ADD_INVOICE.REQUEST_START_DATE, StartDate);
            jsonBody.put(WebFields.ADD_INVOICE.REQUEST_END_DATE, EndDate);
            jsonBody.put(WebFields.ADD_INVOICE.REQUEST_ITEM, item);

            result = setParentJsonData(WebFields.ADD_INVOICE.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getUserType() {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            result = setParentJsonData(WebFields.GET_USER_TYPE.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String addQuotationJson(String UserID, String CustomerID, String VisitorID, String ServiceID, String SitesID, String CompanyName, String QuotationName, String EstimateDate,
                                          String StartDate, String EstimateNo, String IsGST, String TotalAmount, String GSTAmount, String CGST, String SGST, String GSTNo,
                                          String Address, String online, JSONArray Item) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_CUSTOMER_ID, CustomerID);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_VISITOR_ID, VisitorID);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_SERVICE_ID, ServiceID);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_SITE_ID, SitesID);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_COMPANY_NAME, CompanyName);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_QUOTATION_NAME, QuotationName);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_ESTIMATE_DATE, EstimateDate);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_START_DATE, StartDate);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_ESTIMATE_NO, EstimateNo);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_ISGST, IsGST);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_TOTAL_AMOUNT, TotalAmount);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_GST_AMOUNT, GSTAmount);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_CGST, CGST);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_SGST, SGST);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_GST_NO, GSTNo);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_COMPANY_ADDRESS, Address);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_ONLINE, online);
            jsonBody.put(WebFields.ADD_QUOTATION.REQUEST_ITEM, Item);

            result = setParentJsonData(WebFields.ADD_QUOTATION.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String addStatusJson(String UserID, String QuotationID, String QuotationStatus, String ReasonID, String StartDate, String EndDate, String CustomerID
            , String FieldOperatorID, String QualityManagerID, String OperationManagerID, String TotalEmployee) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_STATUS.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.ADD_STATUS.REQUEST_QUOTATION_ID, QuotationID);
            jsonBody.put(WebFields.ADD_STATUS.REQUEST_QUOTATION_STATUS, QuotationStatus);
            jsonBody.put(WebFields.ADD_STATUS.REQUEST_REASON_ID, ReasonID);
            jsonBody.put(WebFields.ADD_STATUS.REQUEST_START_DATE, StartDate);
            jsonBody.put(WebFields.ADD_STATUS.REQUEST_END_DATE, EndDate);
            jsonBody.put(WebFields.ADD_STATUS.REQUEST_CUSTOMER_ID, CustomerID);
            jsonBody.put(WebFields.ADD_STATUS.REQUEST_FO_ID, FieldOperatorID);
            jsonBody.put(WebFields.ADD_STATUS.REQUEST_QM_ID, QualityManagerID);
            jsonBody.put(WebFields.ADD_STATUS.REQUEST_OM_ID, OperationManagerID);
            jsonBody.put(WebFields.ADD_STATUS.REQUEST_TOTAL_EMPLOYEE, TotalEmployee);

            result = setParentJsonData(WebFields.ADD_STATUS.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String convertSiteJson(String UserID, String CustomerID, String SitesID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.CONVERT_SITES.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.CONVERT_SITES.REQUEST_CUSTOMER_ID, CustomerID);
            jsonBody.put(WebFields.CONVERT_SITES.REQUEST_SITE_ID, SitesID);

            result = setParentJsonData(WebFields.CONVERT_SITES.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String convertToCustomerJson(String UserID, String VisitorID, String Name, String EmailID, String MobileNo,
                                               String Address, String CityID, String StateID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.CONVERT_TO_CUSTOMER.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.CONVERT_TO_CUSTOMER.REQUEST_VISITOR_ID, VisitorID);
            jsonBody.put(WebFields.CONVERT_TO_CUSTOMER.REQUEST_NAME, Name);
            jsonBody.put(WebFields.CONVERT_TO_CUSTOMER.REQUEST_EMAILID, EmailID);
            jsonBody.put(WebFields.CONVERT_TO_CUSTOMER.REQUEST_MOBILE_NO, MobileNo);
            jsonBody.put(WebFields.CONVERT_TO_CUSTOMER.REQUEST_ADDRESS, Address);
            jsonBody.put(WebFields.CONVERT_TO_CUSTOMER.REQUEST_CITY_ID, CityID);
            jsonBody.put(WebFields.CONVERT_TO_CUSTOMER.REQUEST_STATE_ID, StateID);

            result = setParentJsonData(WebFields.CONVERT_TO_CUSTOMER.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String setVisitJson(String UserID, String QuotationID, String IsVisit) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.SITE_VISIT.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.SITE_VISIT.REQUEST_QUOTATION_ID, QuotationID);
            jsonBody.put(WebFields.SITE_VISIT.REQUEST_VISIT, IsVisit);


            result = setParentJsonData(WebFields.SITE_VISIT.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getCheckInOutJson(String PageSize, String CurrentPage, String UserID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_CHECKINOUT.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_CHECKINOUT.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_CHECKINOUT.REQUEST_USERID, UserID);

            result = setParentJsonData(WebFields.GET_CHECKINOUT.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getInspectionJson() {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            result = setParentJsonData(WebFields.GET_INSPECTION.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String addCheckInJson(String UserID, String Checkintime, String Inlatitude, String Inlongitude, String InAddress) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_CHECKINTIME.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.ADD_CHECKINTIME.REQUEST_CHECKIN_TIME, Checkintime);
            jsonBody.put(WebFields.ADD_CHECKINTIME.REQUEST_LATITUDE, Inlatitude);
            jsonBody.put(WebFields.ADD_CHECKINTIME.REQUEST_LONGITUDE, Inlongitude);
            jsonBody.put(WebFields.ADD_CHECKINTIME.REQUEST_ADDRESS, InAddress);

            result = setParentJsonData(WebFields.ADD_CHECKINTIME.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String addCheckOutJson(String UserID, String Checkouttime, String Outlatitude, String Outlongitude, String CheckincheckoutID, String OutAddress) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_CHECKOUTTIME.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.ADD_CHECKOUTTIME.REQUEST_CHECKOUT_TIME, Checkouttime);
            jsonBody.put(WebFields.ADD_CHECKOUTTIME.REQUEST_LATITUDE, Outlatitude);
            jsonBody.put(WebFields.ADD_CHECKOUTTIME.REQUEST_LONGITUDE, Outlongitude);
            jsonBody.put(WebFields.ADD_CHECKOUTTIME.REQUEST_CHECKINOUT_ID, CheckincheckoutID);
            jsonBody.put(WebFields.ADD_CHECKOUTTIME.REQUEST_ADDRESS, OutAddress);

            result = setParentJsonData(WebFields.ADD_CHECKOUTTIME.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String addInspectionJson(String UserID, String SitesID, String FieldOperatorID, String QualityManagerID, String OperationManagerID, String Remarks, JSONArray Item) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_INSPECTION.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.ADD_INSPECTION.REQUEST_SITE_ID, SitesID);
            jsonBody.put(WebFields.ADD_INSPECTION.REQUEST_FO_ID, FieldOperatorID);
            jsonBody.put(WebFields.ADD_INSPECTION.REQUEST_QM_ID, QualityManagerID);
            jsonBody.put(WebFields.ADD_INSPECTION.REQUEST_OM_ID, OperationManagerID);
            jsonBody.put(WebFields.ADD_INSPECTION.REQUEST_REMARKS, Remarks);
            jsonBody.put(WebFields.ADD_INSPECTION.REQUEST_ITEM, Item);

            result = setParentJsonData(WebFields.ADD_INSPECTION.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getInspectionJson(String PageSize, String CurrentPage, String UserID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_INSPECTION_LIST.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_INSPECTION_LIST.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_INSPECTION_LIST.REQUEST_USERID, UserID);

            result = setParentJsonData(WebFields.GET_INSPECTION_LIST.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getUserTypeJson(String UserType) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_USERTYPE.REQUEST_USER_TYPE, UserType);

            result = setParentJsonData(WebFields.GET_USERTYPE.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String removeTeamEmployeeJson(String UserID, String QuotationID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.REMOVE_TEAM_EMPLOYEE.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.REMOVE_TEAM_EMPLOYEE.REQUEST_TEAM_DEFINITION_ID, QuotationID);

            result = setParentJsonData(WebFields.REMOVE_TEAM_EMPLOYEE.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getAvailableEmployeeJson(String PageSize, String CurrentPage, String StartDate, String EndDate) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_AVAIL_EMPOLYEE.REQUEST_PAGESIZE, PageSize);
            jsonBody.put(WebFields.GET_AVAIL_EMPOLYEE.REQUEST_CURRENT_PAGE, CurrentPage);
            jsonBody.put(WebFields.GET_AVAIL_EMPOLYEE.REQUEST_START_DATE, StartDate);
            jsonBody.put(WebFields.GET_AVAIL_EMPOLYEE.REQUEST_END_DATE, EndDate);

            result = setParentJsonData(WebFields.GET_AVAIL_EMPOLYEE.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getCompanyJson() {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            result = setParentJsonData(WebFields.GET_COMPANY.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String removeDocumentJson(String UserID, String CustomerSitesDocumentID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.REMOVE_DOCUMENT.REQUEST_USERID, UserID);
            jsonBody.put(WebFields.REMOVE_DOCUMENT.REQUEST_DOCUMENT_ID, CustomerSitesDocumentID);

            result = setParentJsonData(WebFields.REMOVE_DOCUMENT.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String sendSMSJson(String PageSize, String CurrentPage) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.SMS_SEND.REQUEST_PHONE, PageSize);
            jsonBody.put(WebFields.SMS_SEND.REQUEST_MESSAGE, CurrentPage);

            result = setParentJsonData(WebFields.SMS_SEND.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String sendEMAILJson(String EmailID, String Subject, String Message) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.SMS_EMAIL.REQUEST_EMAILID, EmailID);
            jsonBody.put(WebFields.SMS_EMAIL.REQUEST_SUBJECT, Subject);
            jsonBody.put(WebFields.SMS_EMAIL.REQUEST_MESSAGE, Message);

            result = setParentJsonData(WebFields.SMS_EMAIL.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String DeactivateEmployeeJson(String EmployeeID, String UserID) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.EMPLOYEE_DEACTIVATE.REQUEST_EMPLOYEE_ID, EmployeeID);
            jsonBody.put(WebFields.EMPLOYEE_DEACTIVATE.REQUEST_USER_ID, UserID);

            result = setParentJsonData(WebFields.EMPLOYEE_DEACTIVATE.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the parent json for all the APIs and integrated in each of the child method.
     *
     * @param methodName - Method Name
     * @param jsonBody   - Json Body
     * @return - Json Object
     */
    private static String setParentJsonData(String methodName, JSONObject jsonBody) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(WebFields.METHOD, methodName);
            jsonObject.put(WebFields.BODY, jsonBody);

            Common.insertLog("Request::::> " + jsonObject.toString());

            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }
}