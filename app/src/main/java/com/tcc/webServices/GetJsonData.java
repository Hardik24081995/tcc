package com.tcc.webServices;


import android.content.Context;

import com.tcc.utils.AppConstants;
import com.tcc.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

public class GetJsonData {

    /**
     * This method should get the permissions from the json response.
     *
     * @param context - Context
     * @param value   - Value
     * @return - Return mField
     */
    public static String getLoginData(Context context, String value) {
        String mField = AppConstants.STR_EMPTY_STRING;
        try {
            SessionManager mSessionManager = new SessionManager(context);
            String mData = mSessionManager.getPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
            JSONObject jObject = new JSONObject(mData);
            mField = jObject.getString(value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mField;
    }

    /**
     * This method should get the permissions from the json response.
     *
     * @param context - Context
     * @param value   - Value
     * @return - Return mField
     */
    public static String getConfigData(Context context, String value) {
        String mField = AppConstants.STR_EMPTY_STRING;
        try {
            SessionManager mSessionManager = new SessionManager(context);
            String mData = mSessionManager.getPreferences(mSessionManager.KEY_CONFIGURATION_DATA, "");
            JSONObject jObject = new JSONObject(mData);
            mField = jObject.getString(value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mField;
    }

    /**
     * This method should get the permissions from the json response.
     *
     * @param context - Context
     * @param value   - Value
     * @return - Return mField
     */
    public static String getroleData(Context context, String value) {
        String mField = AppConstants.STR_EMPTY_STRING;
        try {
            SessionManager mSessionManager = new SessionManager(context);
            String mData = mSessionManager.getPreferences(mSessionManager.KEY_ROLE_DATA, "");
            JSONObject jObject = new JSONObject(mData);
            mField = jObject.getString(value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mField;
    }


}
