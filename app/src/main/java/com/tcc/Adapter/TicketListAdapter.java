package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.TicketListingModel;
import com.tcc.R;

import java.util.ArrayList;

public class TicketListAdapter extends RecyclerView.Adapter<TicketListAdapter.ViewHolder> {

    Context mContext;
    ArrayList<TicketListingModel.Datum> datumArrayList;

    public TicketListAdapter(Context mContext, ArrayList<TicketListingModel.Datum> datumArrayList) {
        this.mContext = mContext;
        this.datumArrayList = datumArrayList;
    }

    @NonNull
    @Override
    public TicketListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View mView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_ticketlisting, parent, false);

        return new TicketListAdapter.ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull TicketListAdapter.ViewHolder holder, int i) {

        holder.mTextTitle.setText(String.format("Ticket Title : %s", datumArrayList.get(i).getTitle()));
        holder.mTextName.setText(String.format("Employee Name : %s", datumArrayList.get(i).getEmployeeName()));
        holder.mTextDescription.setText(String.format("Ticket Description : %s", datumArrayList.get(i).getDescription()));
        holder.mTextPriority.setText(String.format("Ticket Priority : %s", datumArrayList.get(i).getPriority()));

    }

    @Override
    public int getItemCount() {
        return datumArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextName, mTextTitle, mTextDescription, mTextPriority;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextName = itemView.findViewById(R.id.tv_ticket_EmployeeName);
            mTextDescription = itemView.findViewById(R.id.tv_ticket_Description);
            mTextPriority = itemView.findViewById(R.id.tv_ticket_Priority);
            mTextTitle = itemView.findViewById(R.id.tv_ticket_Title);
        }
    }
}
