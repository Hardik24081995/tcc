package com.tcc.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.tcc.Activity.UploadDocumentActivity;
import com.tcc.Model.GetCustomerDocumentModel;
import com.tcc.R;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.WebFields;

import java.util.ArrayList;

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.MyViewHolder> {

    Activity mContext;
    ArrayList<GetCustomerDocumentModel.Datum> mArrayList;


    public DocumentAdapter(Activity mContext, ArrayList<GetCustomerDocumentModel.Datum> mArrayList) {
        this.mContext = mContext;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public DocumentAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_document, parent, false);

        return new DocumentAdapter.MyViewHolder(mView);

    }

    @Override
    public void onBindViewHolder(@NonNull DocumentAdapter.MyViewHolder holder, int i) {

        SessionManager mSession = new SessionManager(mContext);


        holder.myImageViewText.setText(mArrayList.get(i).getTitle());
        Glide.with(mContext)
                .load(WebFields.DOCUMENT_IMAGE_URL + mArrayList.get(i).getDocument())
                .into(holder.myImageView);

        holder.imgEdit.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, UploadDocumentActivity.class);
            intent.putExtra("ImagePath", WebFields.DOCUMENT_IMAGE_URL + mArrayList.get(i).getDocument());
            intent.putExtra("TitleDoc", mArrayList.get(i).getTitle());
            mSession.setPreferences("ENUMDOCTYPE", "EDIT");
            mSession.setPreferences("DocID", mArrayList.get(i).getCustomerSitesDocumentID());
            mContext.startActivity(intent);
        });

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage("Would You Like To Delete Document?");
                builder.setNegativeButton("NO",
                        (dialog, which) -> dialog.dismiss());
                builder.setPositiveButton("YES",
                        (dialog, which) -> onImageDelete(holder.getAdapterPosition(), mArrayList.get(i)));
                builder.show();

            }
        });


    }

    protected void onImageDelete(int adapterPosition, GetCustomerDocumentModel.Datum datum) {
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgEdit, myImageView, imgDelete;
        private TextView myImageViewText;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            myImageView = itemView.findViewById(R.id.myImageView);
            imgEdit = itemView.findViewById(R.id.imgEdit);

            myImageViewText = itemView.findViewById(R.id.myImageViewText);
            imgDelete = itemView.findViewById(R.id.imgDelete);

        }
    }
}
