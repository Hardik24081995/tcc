package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.GetInspectionListModel;
import com.tcc.R;

import java.util.ArrayList;

public class InspectionSummaryAdapter extends RecyclerView.Adapter<InspectionSummaryAdapter.ViewHolder> {

    Context mContext;
    ArrayList<GetInspectionListModel.Datum.Item> itemArrayList;

    public InspectionSummaryAdapter(Context mContext, ArrayList<GetInspectionListModel.Datum.Item> itemArrayList) {
        this.mContext = mContext;
        this.itemArrayList = itemArrayList;
    }

    @NonNull
    @Override
    public InspectionSummaryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View mView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_iinspectionsummary, viewGroup, false);

        return new InspectionSummaryAdapter.ViewHolder(mView);

    }

    @Override
    public void onBindViewHolder(@NonNull InspectionSummaryAdapter.ViewHolder holder, int i) {

        holder.mTextQuestions.setText(itemArrayList.get(i).getQuestion());
        holder.mTextAnswer.setText(itemArrayList.get(i).getAnswer());

    }

    @Override
    public int getItemCount() {
        return itemArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTextQuestions, mTextAnswer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextAnswer = itemView.findViewById(R.id.mTextAnswer);
            mTextQuestions = itemView.findViewById(R.id.mTextQuestions);
        }
    }
}
