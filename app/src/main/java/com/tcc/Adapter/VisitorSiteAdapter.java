package com.tcc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Activity.QuotationListActivity;
import com.tcc.Model.GetSiteModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;

public class VisitorSiteAdapter extends RecyclerView.Adapter<VisitorSiteAdapter.ViewHolder> {

    Context mContext;
    ArrayList<GetSiteModel.Datum> mArraySites;

    public VisitorSiteAdapter(Context mContext, ArrayList<GetSiteModel.Datum> mArraySites) {
        this.mContext = mContext;
        this.mArraySites = mArraySites;
    }

    @NonNull
    @Override
    public VisitorSiteAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View mView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout_visitorsite, viewGroup, false);

        return new VisitorSiteAdapter.ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull VisitorSiteAdapter.ViewHolder myViewHolder, int i) {


        myViewHolder.mTextName.setText(mArraySites.get(i).getCompanyName());
        myViewHolder.mTextAddress.setText(mArraySites.get(i).getCompanyAddress());

        if (mArraySites.get(i).getStartDate().equals("")) {
            myViewHolder.mTextStartDate.setText("NA");
        } else {
            myViewHolder.mTextStartDate.setText(mArraySites.get(i).getStartDate());
        }

        if (mArraySites.get(i).getEndDate().equals("")) {
            myViewHolder.mTextEndDate.setText("NA");
        } else {
            myViewHolder.mTextEndDate.setText(mArraySites.get(i).getEndDate());
        }

        if (mArraySites.get(i).getTotalAmount().equals("")) {
            myViewHolder.mTextToatlAmount.setText("Total Amount : NA");
        } else {
            myViewHolder.mTextToatlAmount.setText("Total Amount : " + mArraySites.get(i).getTotalAmount());
        }

        if (mArraySites.get(i).getGSTAmount().equals("")) {
            myViewHolder.mTextGst.setText("GST : NA");
        } else {
            myViewHolder.mTextGst.setText("GST : " + mArraySites.get(i).getGSTAmount());
        }


//        myViewHolder.mTextAttendee.setText("");


        myViewHolder.cardPropertyDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, QuotationListActivity.class);
                SessionManager mSession = new SessionManager(mContext);
                mSession.setPreferences(AppConstants.SITE_NAME, mArraySites.get(i).getCompanyName());
                mSession.setPreferences(AppConstants.EMPLOYEE_NAME, mArraySites.get(i).getEmployeeName());
                mSession.setPreferences(AppConstants.START_DATE, mArraySites.get(i).getStartDate());
                mSession.setPreferences(AppConstants.END_DATE, mArraySites.get(i).getEndDate());
                mSession.setPreferences(AppConstants.LOCATION, mArraySites.get(i).getCompanyAddress());
                mSession.setPreferences(AppConstants.GST_NO, mArraySites.get(i).getGSTAmount());
                mSession.setPreferences(AppConstants.AMOUNT, mArraySites.get(i).getTotalAmount());
                mSession.setPreferences(AppConstants.SITE_ID, mArraySites.get(i).getSitesID());
                mSession.setPreferences(AppConstants.TYPO_FRAGS_CHANGE, "VisitorSites");
                mContext.startActivity(intent);
            }
        });
        myViewHolder.img_icon.setImageDrawable(Common.setLabeledImageView(mContext, mArraySites.get(i).getCompanyName(), ""));

    }

    @Override
    public int getItemCount() {
        return mArraySites.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextName, mTextAddress, mTextEstimateDate, mTextAttendee, mTextToatlAmount,
                mTextGst, mTextStartDate, mTextEndDate;
        private CardView cardPropertyDetail;
        private ImageView img_icon;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mTextName = itemView.findViewById(R.id.text_row_property_list_item_name);
            mTextAddress = itemView.findViewById(R.id.text_row_property_list_item_address);
            mTextStartDate = itemView.findViewById(R.id.text_property_item_start_date);
            mTextEndDate = itemView.findViewById(R.id.text_property_item_end_date);
            mTextGst = itemView.findViewById(R.id.text_row_project_item_gst);
            mTextToatlAmount = itemView.findViewById(R.id.text_row_project_item_total_amount);
            mTextAttendee = itemView.findViewById(R.id.text_row_property_list_attendee);
            cardPropertyDetail = itemView.findViewById(R.id.cardPropertyDetail);
            img_icon = itemView.findViewById(R.id.img_icon);
        }
    }
}
