package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.GetEmployeeAttendance;
import com.tcc.R;

import java.util.ArrayList;

public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.ViewHolder> {


    Context mContext;
    ArrayList<GetEmployeeAttendance.Datum> mArrEmployee;

    public AttendanceAdapter(Context mContext, ArrayList<GetEmployeeAttendance.Datum> mArrEmployee) {
        this.mContext = mContext;
        this.mArrEmployee = mArrEmployee;
    }

    @NonNull
    @Override
    public AttendanceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_attendance, parent, false);

        return new AttendanceAdapter.ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull AttendanceAdapter.ViewHolder viewHolder, int i) {

        viewHolder.tvAttendaceDate.setText(String.format("Date : %s", mArrEmployee.get(i).getAttendanceDate()));
//        viewHolder.WorkingHrs.setText("" + mArrEmployee.get(i).);
        viewHolder.tvAttendence.setText(String.format("Training Attendance : %s", mArrEmployee.get(i).getAttendance()));

    }

    @Override
    public int getItemCount() {
        return mArrEmployee.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvAttendaceDate, WorkingHrs, tvAttendence;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvAttendaceDate = itemView.findViewById(R.id.tvAttendaceDate);
            WorkingHrs = itemView.findViewById(R.id.WorkingHrs);
            tvAttendence = itemView.findViewById(R.id.tvAttendence);
        }
    }
}
