package com.tcc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Activity.SiteAttendanceDetailActivity;
import com.tcc.Model.GetAttendanceModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;

public class SiteAttendanceAdapter extends RecyclerView.Adapter<SiteAttendanceAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<GetAttendanceModel.Datum> data;

    public SiteAttendanceAdapter(Context mContext, ArrayList<GetAttendanceModel.Datum> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @NonNull
    @Override
    public SiteAttendanceAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_siteattendance, parent, false);

        return new SiteAttendanceAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SiteAttendanceAdapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.card_siteattendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionManager manager = new SessionManager(mContext);
                Intent intent = new Intent(mContext, SiteAttendanceDetailActivity.class);
                manager.setPreferences(AppConstants.SITE_ID, data.get(i).getSitesID());
                manager.setPreferences(AppConstants.ATTENDANCE_DATE, data.get(i).getAttendanceDate());
                mContext.startActivity(intent);
            }
        });

        myViewHolder.siteAttendaceTitle.setText("Date:");
        myViewHolder.siteTitleValue.setVisibility(View.GONE);
        myViewHolder.mTextAbsent.setText(data.get(i).getAbsentount());
        myViewHolder.mTextPresent.setText(data.get(i).getPresentCount());
        myViewHolder.mTextLeave.setText(data.get(i).getHalfDayount());
        myViewHolder.mTextAddress.setText(data.get(i).getCompanyAddress());
        myViewHolder.mTextDate.setText(data.get(i).getAttendanceDate());


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CardView card_siteattendance;
        TextView siteAttendaceTitle, siteTitleValue, mTextDate, mTextAddress, mTextPresent, mTextAbsent, mTextLeave;
        LinearLayout ll_attendance;
        View v1site;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            card_siteattendance = itemView.findViewById(R.id.card_siteattendance);
            siteAttendaceTitle = itemView.findViewById(R.id.siteAttendaceTitle);
            siteTitleValue = itemView.findViewById(R.id.siteTitleValue);
            ll_attendance = itemView.findViewById(R.id.ll_dateattendance);

            mTextDate = itemView.findViewById(R.id.attendanceDateValue);
            mTextAddress = itemView.findViewById(R.id.attendanceAddressValue);
            mTextPresent = itemView.findViewById(R.id.presentValue);
            mTextAbsent = itemView.findViewById(R.id.AbsentValue);
            mTextLeave = itemView.findViewById(R.id.leaveValue);


        }
    }
}
