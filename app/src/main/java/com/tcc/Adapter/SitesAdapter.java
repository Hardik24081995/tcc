package com.tcc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Activity.QuotationListActivity;
import com.tcc.Model.GetSiteModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;

public class SitesAdapter extends RecyclerView.Adapter<SitesAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<GetSiteModel.Datum> mArrayList;

    public SitesAdapter(Context mContext, ArrayList<GetSiteModel.Datum> mArrayList) {
        this.mContext = mContext;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public SitesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.property_list_item, viewGroup, false);
        return new SitesAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        /*listViewHolder.cardPropertyDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, PropertyDetailActivity.class);
                mContext.startActivity(intent);
            }
        });*/

        myViewHolder.mTextName.setText(mArrayList.get(i).getCompanyName());
        myViewHolder.mTextAddress.setText(mArrayList.get(i).getCompanyAddress());

        if (mArrayList.get(i).getStartDate().equals("")) {
            myViewHolder.mTextStartDate.setText("NA");
        } else {
            myViewHolder.mTextStartDate.setText(mArrayList.get(i).getStartDate());
        }

        if (mArrayList.get(i).getEndDate().equals("")) {
            myViewHolder.mTextEndDate.setText("NA");
        } else {
            myViewHolder.mTextEndDate.setText(mArrayList.get(i).getEndDate());
        }

        if (mArrayList.get(i).getTotalAmount().equals("")) {
            myViewHolder.mTextToatlAmount.setText("NA");
        } else {
            myViewHolder.mTextToatlAmount.setText(mArrayList.get(i).getTotalAmount());
        }

        if (mArrayList.get(i).getGSTAmount().equals("")) {
            myViewHolder.mTextGst.setText("NA");
        } else {
            myViewHolder.mTextGst.setText(mArrayList.get(i).getGSTAmount());
        }


//        myViewHolder.mTextAttendee.setText("");


        myViewHolder.cardPropertyDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, QuotationListActivity.class);
                SessionManager mSession = new SessionManager(mContext);
                mSession.setPreferences(AppConstants.SITE_NAME, mArrayList.get(i).getCompanyName());
                mSession.setPreferences(AppConstants.EMPLOYEE_NAME, mArrayList.get(i).getEmployeeName());
                mSession.setPreferences(AppConstants.START_DATE, mArrayList.get(i).getStartDate());
                mSession.setPreferences(AppConstants.END_DATE, mArrayList.get(i).getEndDate());
                mSession.setPreferences(AppConstants.LOCATION, mArrayList.get(i).getCompanyAddress());
                mSession.setPreferences(AppConstants.GST_NO, mArrayList.get(i).getGSTAmount());
                mSession.setPreferences(AppConstants.AMOUNT, mArrayList.get(i).getTotalAmount());
                mSession.setPreferences(AppConstants.SITE_ID, mArrayList.get(i).getSitesID());
                mSession.setPreferences(AppConstants.TYPO_FRAGS_CHANGE, "MenuSites");
                mContext.startActivity(intent);
            }
        });
        myViewHolder.img_icon.setImageDrawable(Common.setLabeledImageView(mContext, mArrayList.get(i).getCompanyName(), ""));

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextName, mTextAddress, mTextEstimateDate, mTextAttendee, mTextToatlAmount,
                mTextGst, mTextStartDate, mTextEndDate;
        private CardView cardPropertyDetail;
        private ImageView img_icon;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mTextName = itemView.findViewById(R.id.text_row_property_list_item_name);
            mTextAddress = itemView.findViewById(R.id.text_row_property_list_item_address);
            // mTextEstimateDate=itemView.findViewById(R.id.text_row_projects_item_estimate_time);
            mTextStartDate = itemView.findViewById(R.id.text_property_item_start_date);
            mTextEndDate = itemView.findViewById(R.id.text_property_item_end_date);
            mTextGst = itemView.findViewById(R.id.text_row_project_item_gst);
            mTextToatlAmount = itemView.findViewById(R.id.text_row_project_item_total_amount);
            mTextAttendee = itemView.findViewById(R.id.text_row_property_list_attendee);
            cardPropertyDetail = itemView.findViewById(R.id.cardPropertyDetail);
            img_icon = itemView.findViewById(R.id.img_icon);
        }
    }
}
