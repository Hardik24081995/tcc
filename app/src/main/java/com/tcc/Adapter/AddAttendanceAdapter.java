package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.GetTeamEmployeeModel;
import com.tcc.R;

import java.util.ArrayList;

public class AddAttendanceAdapter extends RecyclerView.Adapter<AddAttendanceAdapter.ViewHolder> {

    Context mContext;
    ArrayList<GetTeamEmployeeModel.Datum> datumArrayList;

    public AddAttendanceAdapter(Context mContext, ArrayList<GetTeamEmployeeModel.Datum> datumArrayList) {
        this.mContext = mContext;
        this.datumArrayList = datumArrayList;
    }

    @NonNull
    @Override
    public AddAttendanceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_addattendance, parent, false);

        return new AddAttendanceAdapter.ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        holder.mTextName.setText(datumArrayList.get(position).getEmployeeName());
        holder.mTextDesignation.setText(datumArrayList.get(position).getUsertype());

        holder.buttonPresent.setOnClickListener(v -> {

            holder.buttonPresent.setBackgroundColor(mContext.getResources().getColor(R.color.green));
            holder.buttonPresent.setTextColor(mContext.getResources().getColor(R.color.color_white));
            holder.buttonAbsent.setBackgroundColor(mContext.getResources().getColor(R.color.color_white));
            holder.buttonAbsent.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
            holder.buttonHalfLeave.setBackgroundColor(mContext.getResources().getColor(R.color.color_white));
            holder.buttonHalfLeave.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));

            datumArrayList.get(position).setAttendance("P");
            addToJsonArray(datumArrayList.get(position), datumArrayList.get(position).getAttendance(), datumArrayList.get(position).getUserID(), datumArrayList.get(position).getSitesID());

        });

        holder.buttonAbsent.setOnClickListener(v -> {

            holder.buttonAbsent.setBackgroundColor(mContext.getResources().getColor(R.color.red));
            holder.buttonAbsent.setTextColor(mContext.getResources().getColor(R.color.color_white));
            holder.buttonPresent.setBackgroundColor(mContext.getResources().getColor(R.color.color_white));
            holder.buttonPresent.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
            holder.buttonHalfLeave.setBackgroundColor(mContext.getResources().getColor(R.color.color_white));
            holder.buttonHalfLeave.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));

            datumArrayList.get(position).setAttendance("A");
            addToJsonArray(datumArrayList.get(position), datumArrayList.get(position).getAttendance(), datumArrayList.get(position).getUserID(), datumArrayList.get(position).getSitesID());

        });

        holder.buttonHalfLeave.setOnClickListener(v -> {

            holder.buttonHalfLeave.setBackgroundColor(mContext.getResources().getColor(R.color.colorGray));
            holder.buttonHalfLeave.setTextColor(mContext.getResources().getColor(R.color.color_white));
            holder.buttonAbsent.setBackgroundColor(mContext.getResources().getColor(R.color.color_white));
            holder.buttonAbsent.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
            holder.buttonPresent.setBackgroundColor(mContext.getResources().getColor(R.color.color_white));
            holder.buttonPresent.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));

            datumArrayList.get(position).setAttendance("H");
            addToJsonArray(datumArrayList.get(position), datumArrayList.get(position).getAttendance(), datumArrayList.get(position).getUserID(), datumArrayList.get(position).getSitesID());

        });

    }

    protected void addToJsonArray(GetTeamEmployeeModel.Datum datum, String Attendance, String EmployeeID, String SitesID) {
    }

    @Override
    public int getItemCount() {
        return datumArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextName, mTextDesignation;
        private Button buttonPresent, buttonAbsent, buttonHalfLeave;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextName = itemView.findViewById(R.id.nameatt);
            mTextDesignation = itemView.findViewById(R.id.roleatt);

            buttonPresent = itemView.findViewById(R.id.buttonPresent);
            buttonAbsent = itemView.findViewById(R.id.buttonAbsent);
            buttonHalfLeave = itemView.findViewById(R.id.buttonHalfLeave);
        }
    }
}
