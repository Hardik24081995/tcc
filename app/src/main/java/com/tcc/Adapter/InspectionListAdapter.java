package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.GetInspectionListModel;
import com.tcc.R;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;

public class InspectionListAdapter extends RecyclerView.Adapter<InspectionListAdapter.ViewHolder> {

    Context mContext;
    ArrayList<GetInspectionListModel.Datum> mArrayInspection;

    public InspectionListAdapter(Context mContext, ArrayList<GetInspectionListModel.Datum> mArrayInspection) {
        this.mContext = mContext;
        this.mArrayInspection = mArrayInspection;
    }

    @NonNull
    @Override
    public InspectionListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View mView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_inspectionlist, viewGroup, false);

        return new InspectionListAdapter.ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull InspectionListAdapter.ViewHolder holder, int i) {

        SessionManager mSession = new SessionManager(mContext);

        holder.mTextSiteName.setText(mArrayInspection.get(i).getCompanyName());
        holder.mTextEmployeeName.setText(mArrayInspection.get(i).getEmployeeName());
        holder.mTextInspectionDate.setText(mArrayInspection.get(i).getInspectionDate());
        holder.mTextEmployeeType.setText(mArrayInspection.get(i).getUserType());

        holder.mInspectionCard.setOnClickListener(v -> {
            onInspectionSummary(mArrayInspection.get(i));
        });
    }

    protected void onInspectionSummary(GetInspectionListModel.Datum datum) {
    }

    @Override
    public int getItemCount() {
        return mArrayInspection.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTextSiteName, mTextEmployeeName, mTextInspectionDate, mTextEmployeeType;
        private CardView mInspectionCard;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextSiteName = itemView.findViewById(R.id.inspection_name);
            mTextEmployeeName = itemView.findViewById(R.id.employeename);
            mTextInspectionDate = itemView.findViewById(R.id.inspectiionDate);
            mTextEmployeeType = itemView.findViewById(R.id.typeEmployee);

            mInspectionCard = itemView.findViewById(R.id.inspectionCard);
        }
    }
}
