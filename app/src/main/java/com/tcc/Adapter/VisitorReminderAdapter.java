package com.tcc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Activity.ViewAllResponseActivity;
import com.tcc.Model.GetVisitorReminderModel;
import com.tcc.R;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;

public class VisitorReminderAdapter extends RecyclerView.Adapter<VisitorReminderAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<GetVisitorReminderModel.Datum> mArrayReminder;


    public VisitorReminderAdapter(Context mContext, ArrayList<GetVisitorReminderModel.Datum> mArrayReminder) {
        this.mContext = mContext;
        this.mArrayReminder = mArrayReminder;
    }

    @NonNull
    @Override
    public VisitorReminderAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_visitorreminder, parent, false);
        return new VisitorReminderAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VisitorReminderAdapter.MyViewHolder viewHolder, int i) {

        SessionManager mSession = new SessionManager(mContext);
        String Phone = mSession.getPreferences("Mobile", "");

        viewHolder.mImagePhone.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + Phone));
            mContext.startActivity(intent);
        });

        viewHolder.mTextMessage.setText(mArrayReminder.get(i).getMessage());
        viewHolder.mTextDate.setText(String.format("Date : %s", mArrayReminder.get(i).getReminderDate()));
        viewHolder.addResponse.setText(String.format("Added By : %s", mArrayReminder.get(i).getEmployeeName()));

        SessionManager manager = new SessionManager(mContext);

        viewHolder.mImageEmail.setOnClickListener(v -> onEmailClick());
        viewHolder.mImageSMS.setOnClickListener(v -> onSMSClick());

        viewHolder.allResponse.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, ViewAllResponseActivity.class);
            manager.setPreferences("ReminderID", mArrayReminder.get(i).getVisitorReminderID());
            mContext.startActivity(intent);
        });
        viewHolder.addResponse.setOnClickListener(v -> {
            onAddClick(mArrayReminder.get(i).getVisitorReminderID());
        });

    }

    protected void onAddClick(String visitorReminderID) {

    }

    protected void onEmailClick() {

    }

    protected void onSMSClick() {

    }

    @Override
    public int getItemCount() {
        return mArrayReminder.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgCustomer, mImageSMS, mImageEmail, mImagePhone, mImageQuotation;
        private TextView allResponse, addResponse, mTextMessage, mTextDate;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextMessage = itemView.findViewById(R.id.text_reminder_message);
            mTextDate = itemView.findViewById(R.id.text_reminder_date);
            allResponse = itemView.findViewById(R.id.allResponse);
            addResponse = itemView.findViewById(R.id.text_reminder_addResponse);
            mImageEmail = itemView.findViewById(R.id.image_row_visitor_email);
            mImagePhone = itemView.findViewById(R.id.image_row_visitor_call);
            mImageSMS = itemView.findViewById(R.id.image_row_visitor_sms);
        }
    }
}
