package com.tcc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Activity.TeamDetailsActivity;
import com.tcc.Model.GetTeamDefinitionModel;
import com.tcc.R;

import java.util.ArrayList;

public class TeamDefinitionAdapter extends RecyclerView.Adapter<TeamDefinitionAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<GetTeamDefinitionModel.Datum> arrTeam;

    public TeamDefinitionAdapter(Context mContext, ArrayList<GetTeamDefinitionModel.Datum> arrTeam) {
        this.mContext = mContext;
        this.arrTeam = arrTeam;
    }

    @NonNull
    @Override
    public TeamDefinitionAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View mView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_teamdefinition, parent, false);

        return new TeamDefinitionAdapter.MyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull TeamDefinitionAdapter.MyViewHolder holder, int i) {

        holder.mCardTeamDetails.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, TeamDetailsActivity.class);
            mContext.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return arrTeam.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextQualityManager, mTextCount, mTextStartDate, mTextEndDate, mTextFieldOperator, mTextOperationManager;
        private CardView mCardTeamDetails;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextFieldOperator = itemView.findViewById(R.id.FieldOperator);
            mTextOperationManager = itemView.findViewById(R.id.OperationManager);
            mTextQualityManager = itemView.findViewById(R.id.QualityManager);
            mTextCount = itemView.findViewById(R.id.TeamCount);
            mTextStartDate = itemView.findViewById(R.id.td_employee_start_date);
            mTextEndDate = itemView.findViewById(R.id.td_employee_end_date);

            mCardTeamDetails = itemView.findViewById(R.id.teamDetails);

        }
    }
}
