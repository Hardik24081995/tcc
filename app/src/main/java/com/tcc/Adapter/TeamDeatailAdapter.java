package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.GetTeamEmployeeModel;
import com.tcc.R;

import java.util.ArrayList;

public class TeamDeatailAdapter extends RecyclerView.Adapter<TeamDeatailAdapter.ViewHolder> {

    Context mContext;
    ArrayList<GetTeamEmployeeModel.Datum> mArrayTeam;

    public TeamDeatailAdapter(Context mContext, ArrayList<GetTeamEmployeeModel.Datum> mArrayTeam) {
        this.mContext = mContext;
        this.mArrayTeam = mArrayTeam;
    }

    @NonNull
    @Override
    public TeamDeatailAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_teamdetails, parent, false);

        return new TeamDeatailAdapter.ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull TeamDeatailAdapter.ViewHolder holder, int i) {

        holder.mTextEmployeeName.setText(mArrayTeam.get(i).getEmployeeName());

        holder.mImageShuffle.setOnClickListener(v -> {
            shufflEmployee(mArrayTeam.get(i));
        });

        holder.mImageDelete.setOnClickListener(v -> {
            deleteEmpployee(holder.getAdapterPosition(), mArrayTeam.get(i));
        });

        holder.mStartDate.setText(mArrayTeam.get(i).getStartDate() + " To " + mArrayTeam.get(i).getEndDate());

    }

    protected void deleteEmpployee(int adapterPosition, GetTeamEmployeeModel.Datum datum) {
    }

    protected void shufflEmployee(GetTeamEmployeeModel.Datum datum) {
    }

    @Override
    public int getItemCount() {
        return mArrayTeam.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImageShuffle, mImageDelete;
        private TextView mTextEmployeeName, mStartDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mImageShuffle = itemView.findViewById(R.id.shuffle);
            mImageDelete = itemView.findViewById(R.id.delete);

            mTextEmployeeName = itemView.findViewById(R.id.EmployeeName);
            mStartDate = itemView.findViewById(R.id.startDate);
        }
    }
}
