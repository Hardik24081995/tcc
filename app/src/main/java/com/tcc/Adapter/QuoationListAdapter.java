package com.tcc.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Activity.PropertyDetailActivity;
import com.tcc.Activity.SiteDetailsActivity;
import com.tcc.Model.GetQuotationListModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;

public class QuoationListAdapter extends RecyclerView.Adapter<QuoationListAdapter.ViewHolder> {

    Context mContext;
    ArrayList<GetQuotationListModel.Datum> mArrayQuoatation;

    public QuoationListAdapter(Context mContext, ArrayList<GetQuotationListModel.Datum> mArrayQuoatation) {
        this.mContext = mContext;
        this.mArrayQuoatation = mArrayQuoatation;
    }

    @NonNull
    @Override
    public QuoationListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_quoationlisting, parent, false);

        return new QuoationListAdapter.ViewHolder(mView);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull QuoationListAdapter.ViewHolder holder, int position) {

        String TypeO, url = "http://societyfy.in/TheCleaingCompany/assets/uploads/estimation/";
        SessionManager sessionManager = new SessionManager(mContext);
        sessionManager.setPreferences(AppConstants.QUOTATION_ID, mArrayQuoatation.get(position).getQuotationID());
        TypeO = sessionManager.getPreferences(AppConstants.TYPO_FRAGS_CHANGE, "");

        if (TypeO.equalsIgnoreCase("MenuSites")) {
            holder.mTextAccept.setVisibility(View.GONE);
            holder.mTextReject.setVisibility(View.GONE);
            holder.mTextConvert.setVisibility(View.GONE);
        }
        if (TypeO.equalsIgnoreCase("CustomerSites")) {
            holder.mTextConvert.setVisibility(View.GONE);
        }
        if (TypeO.equalsIgnoreCase("VisitorSites")) {
            holder.mTextAccept.setVisibility(View.GONE);
        }

        if (mArrayQuoatation.get(position).getQuotationStatus().equalsIgnoreCase("Pending")) {
            holder.response.setVisibility(View.VISIBLE);
            holder.mTextStatus.setText(String.format(": %s", mArrayQuoatation.get(position).getQuotationStatus()));
            holder.StatusImage.setColorFilter(mContext.getResources().getColor(R.color.colorPrimary));
            holder.linPayment.setVisibility(View.GONE);
        }
        if (mArrayQuoatation.get(position).getQuotationStatus().equalsIgnoreCase("Accept")) {
            holder.response.setVisibility(View.GONE);
            holder.linPayment.setVisibility(View.VISIBLE);
            holder.mTextStatus.setText(String.format(": %s", mArrayQuoatation.get(position).getQuotationStatus()));
            holder.StatusImage.setColorFilter(mContext.getResources().getColor(R.color.green));
        }
        if (mArrayQuoatation.get(position).getQuotationStatus().equalsIgnoreCase("Reject")) {
            holder.response.setVisibility(View.GONE);
            holder.linPayment.setVisibility(View.GONE);
            holder.mTextStatus.setText(String.format(": %s", mArrayQuoatation.get(position).getQuotationStatus()));
            holder.StatusImage.setColorFilter(mContext.getResources().getColor(R.color.red));
        }

        if (mArrayQuoatation.get(position).getReason().equalsIgnoreCase("")) {
            holder.reason.setVisibility(View.GONE);
        }
        if (!mArrayQuoatation.get(position).getReason().equalsIgnoreCase("")) {
            holder.reason.setVisibility(View.VISIBLE);
        }

        holder.mTextCompanyName.setText(String.format(": %s", mArrayQuoatation.get(position).getCompanyName()));
        holder.mTextAddress.setText(String.format(": %s", mArrayQuoatation.get(position).getAddress()));
        holder.mTextStartDate.setText(String.format(": %s", mArrayQuoatation.get(position).getStartDate()));
        holder.mTextEndDate.setText(String.format(": %s", mArrayQuoatation.get(position).getEstimateDate()));
        holder.mTextTotalAmount.setText(String.format(": %s", mArrayQuoatation.get(position).getTotalAmount()));
        holder.mTextGstAmount.setText(String.format(": %s", mArrayQuoatation.get(position).getGSTAmount()));
        holder.mTextReson.setText(String.format(": %s", mArrayQuoatation.get(position).getReason()));

        if (mArrayQuoatation.get(position).getIsVisit().equalsIgnoreCase("No")) {
            holder.mTextVisitStatusNo.setBackgroundColor(R.color.color_white);
        }
        if (mArrayQuoatation.get(position).getIsVisit().equalsIgnoreCase("Yes")) {
            holder.mTextVisitStatusYes.setText(": Yes");
            holder.mTextVisitStatusNo.setVisibility(View.GONE);
            holder.mTextVisitStatusYes.setGravity(Gravity.LEFT);
        }

        holder.mTextVisitStatusNo.setOnClickListener(v -> {
            changeStatus(mArrayQuoatation.get(position).getQuotationID(), "No");
        });

        holder.mTextVisitStatusYes.setOnClickListener(v -> {
            changeStatus(mArrayQuoatation.get(position).getQuotationID(), "Yes");
        });

        holder.linPayment.setOnClickListener(v -> {
            if (mArrayQuoatation.get(position).getQuotationStatus().equalsIgnoreCase("Accept") && TypeO.equalsIgnoreCase("CustomerSites")) {
                Intent intent = new Intent(mContext, PropertyDetailActivity.class);
                sessionManager.setPreferences(AppConstants.TEAM_COUNT, mArrayQuoatation.get(position).getTotalEmployee());
                sessionManager.setPreferences(AppConstants.FIELD_OPERATOR, mArrayQuoatation.get(position).getFieldOperator());
                sessionManager.setPreferences(AppConstants.QUALITY_MANAGER, mArrayQuoatation.get(position).getQualityManager());
                sessionManager.setPreferences(AppConstants.OPERATION_MANAGER, mArrayQuoatation.get(position).getOperationManager());
                sessionManager.setPreferences(AppConstants.GST_NO, mArrayQuoatation.get(position).getGSTNo());
                sessionManager.setPreferences(AppConstants.GST_AMOUNT, mArrayQuoatation.get(position).getGSTAmount());
                sessionManager.setPreferences(AppConstants.AMOUNT, mArrayQuoatation.get(position).getTotalAmount());
                sessionManager.setPreferences(AppConstants.START_DATE, mArrayQuoatation.get(position).getStartDate());
                sessionManager.setPreferences(AppConstants.END_DATE, mArrayQuoatation.get(position).getEndDate());
                sessionManager.setPreferences(AppConstants.QUOTATION_ID, mArrayQuoatation.get(position).getQuotationID());
                sessionManager.setPreferences("ENUMDOCTYPE", "ADD");
                mContext.startActivity(intent);
            }
            if (mArrayQuoatation.get(position).getQuotationStatus().equalsIgnoreCase("Accept") && TypeO.equalsIgnoreCase("MenuSites")) {
                Intent intent = new Intent(mContext, SiteDetailsActivity.class);
                sessionManager.setPreferences(AppConstants.TEAM_COUNT, mArrayQuoatation.get(position).getTotalEmployee());
                sessionManager.setPreferences(AppConstants.FIELD_OPERATOR, mArrayQuoatation.get(position).getFieldOperator());
                sessionManager.setPreferences(AppConstants.QUALITY_MANAGER, mArrayQuoatation.get(position).getQualityManager());
                sessionManager.setPreferences(AppConstants.OPERATION_MANAGER, mArrayQuoatation.get(position).getOperationManager());
                sessionManager.setPreferences(AppConstants.GST_NO, mArrayQuoatation.get(position).getGSTNo());
                sessionManager.setPreferences(AppConstants.START_DATE, mArrayQuoatation.get(position).getStartDate());
                sessionManager.setPreferences(AppConstants.END_DATE, mArrayQuoatation.get(position).getEndDate());
                sessionManager.setPreferences(AppConstants.QUOTATION_ID, mArrayQuoatation.get(position).getQuotationID());
                sessionManager.setPreferences(AppConstants.ADDRESS, mArrayQuoatation.get(position).getAddress());
                sessionManager.setPreferences(AppConstants.CUSTOMER_ID, mArrayQuoatation.get(position).getCustomerID());
                mContext.startActivity(intent);
            }
        });


        holder.mCard.setOnClickListener(v -> {
            if (mArrayQuoatation.get(position).getQuotationStatus().equalsIgnoreCase("Accept") && TypeO.equalsIgnoreCase("CustomerSites")) {
                Intent intent = new Intent(mContext, PropertyDetailActivity.class);
                sessionManager.setPreferences(AppConstants.TEAM_COUNT, mArrayQuoatation.get(position).getTotalEmployee());
                sessionManager.setPreferences(AppConstants.FIELD_OPERATOR, mArrayQuoatation.get(position).getFieldOperator());
                sessionManager.setPreferences(AppConstants.QUALITY_MANAGER, mArrayQuoatation.get(position).getQualityManager());
                sessionManager.setPreferences(AppConstants.OPERATION_MANAGER, mArrayQuoatation.get(position).getOperationManager());
                sessionManager.setPreferences(AppConstants.GST_NO, mArrayQuoatation.get(position).getGSTNo());
                sessionManager.setPreferences(AppConstants.GST_AMOUNT, mArrayQuoatation.get(position).getGSTAmount());
                sessionManager.setPreferences(AppConstants.AMOUNT, mArrayQuoatation.get(position).getTotalAmount());
                sessionManager.setPreferences(AppConstants.START_DATE, mArrayQuoatation.get(position).getStartDate());
                sessionManager.setPreferences(AppConstants.END_DATE, mArrayQuoatation.get(position).getEstimateDate());
                sessionManager.setPreferences(AppConstants.QUOTATION_ID, mArrayQuoatation.get(position).getQuotationID());
                sessionManager.setPreferences("ENUMDOCTYPE", "ADD");
                mContext.startActivity(intent);
            }
            if (mArrayQuoatation.get(position).getQuotationStatus().equalsIgnoreCase("Accept") && TypeO.equalsIgnoreCase("MenuSites")) {
                Intent intent = new Intent(mContext, SiteDetailsActivity.class);
                sessionManager.setPreferences(AppConstants.TEAM_COUNT, mArrayQuoatation.get(position).getTotalEmployee());
                sessionManager.setPreferences(AppConstants.FIELD_OPERATOR, mArrayQuoatation.get(position).getFieldOperator());
                sessionManager.setPreferences(AppConstants.QUALITY_MANAGER, mArrayQuoatation.get(position).getQualityManager());
                sessionManager.setPreferences(AppConstants.OPERATION_MANAGER, mArrayQuoatation.get(position).getOperationManager());
                sessionManager.setPreferences(AppConstants.GST_NO, mArrayQuoatation.get(position).getGSTNo());
                sessionManager.setPreferences(AppConstants.START_DATE, mArrayQuoatation.get(position).getStartDate());
                sessionManager.setPreferences(AppConstants.END_DATE, mArrayQuoatation.get(position).getEndDate());
                sessionManager.setPreferences(AppConstants.QUOTATION_ID, mArrayQuoatation.get(position).getQuotationID());
                sessionManager.setPreferences(AppConstants.ADDRESS, mArrayQuoatation.get(position).getAddress());
                sessionManager.setPreferences(AppConstants.CUSTOMER_ID, mArrayQuoatation.get(position).getCustomerID());
                mContext.startActivity(intent);
            }
        });

        holder.mImagePrint.setOnClickListener(v -> {
            if (mArrayQuoatation.get(position).getDocument().equalsIgnoreCase("")) {
                Toast.makeText(mContext, "Document Currently Unavailable", Toast.LENGTH_SHORT).show();
            } else {
                Intent viewIntent = new Intent("android.intent.action.VIEW",
                        Uri.parse(url + mArrayQuoatation.get(position).getDocument()));
                mContext.startActivity(viewIntent);
            }
        });

        holder.mTextReject.setOnClickListener(v -> onRejectClick(mArrayQuoatation.get(position)));
        holder.mTextAccept.setOnClickListener(v -> onAcceptClick(mArrayQuoatation.get(position), TypeO));
        holder.mTextConvert.setOnClickListener(v -> onConvertClicked(mArrayQuoatation.get(position), TypeO));

        ArrayList<GetQuotationListModel.Datum.Item> arrayItem =
                (ArrayList<GetQuotationListModel.Datum.Item>) mArrayQuoatation.get(position).getItem();

        QuotationServiceAdapter penaltyListEmployeeAdapter = new QuotationServiceAdapter(mContext, arrayItem);
        holder.mRecyclerView.setAdapter(penaltyListEmployeeAdapter);

    }

    protected void changeStatus(String quotationID, String status) {
    }

    protected void onConvertClicked(GetQuotationListModel.Datum datum, String typeO) {
    }

    protected void onAcceptClick(GetQuotationListModel.Datum datum, String typeO) {
    }

    protected void onRejectClick(GetQuotationListModel.Datum datum) {
    }

    @Override
    public int getItemCount() {
        return mArrayQuoatation.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CardView mCard;
        private TextView mTextCompanyName, mTextAddress, mTextStartDate, mTextEndDate, mTextTotalAmount,
                mTextGstAmount, mTextStatus, mTextAccept, mTextReject, mTextReson, mTextConvert, mTextVisitStatusYes, mTextVisitStatusNo;
        private RecyclerView mRecyclerView;
        private LinearLayout response, reason, linPayment;
        private ImageView StatusImage, mImagePrint;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mCard = itemView.findViewById(R.id.card_quotation);

            mTextCompanyName = itemView.findViewById(R.id.quotation_companyname);
            mTextAddress = itemView.findViewById(R.id.quotation_address);
            mTextStartDate = itemView.findViewById(R.id.quotation_startdate);
            mTextEndDate = itemView.findViewById(R.id.quotation_enddate);
            linPayment = itemView.findViewById(R.id.linPayment);
            mTextTotalAmount = itemView.findViewById(R.id.quotation_totalamount);
            mTextGstAmount = itemView.findViewById(R.id.quotation_totalgstamount);
            mTextStatus = itemView.findViewById(R.id.quotation_status);
            mTextAccept = itemView.findViewById(R.id.quotationaccept);
            mTextReject = itemView.findViewById(R.id.quotationreject);
            mTextReson = itemView.findViewById(R.id.quotation_reson);
            mTextConvert = itemView.findViewById(R.id.quotationconvert);
            mTextVisitStatusYes = itemView.findViewById(R.id.quotation_visitstatusyes);
            mTextVisitStatusNo = itemView.findViewById(R.id.quotation_visitstatusno);

            StatusImage = itemView.findViewById(R.id.img_status);
            mImagePrint = itemView.findViewById(R.id.img_pdf);

            mRecyclerView = itemView.findViewById(R.id.rv_quotationservices);

            response = itemView.findViewById(R.id.linearaccrej);
            reason = itemView.findViewById(R.id.resontoreject);

            RecyclerView.LayoutManager manager = new LinearLayoutManager(mContext, LinearLayout.VERTICAL, false);
            mRecyclerView.setLayoutManager(manager);
        }
    }
}
