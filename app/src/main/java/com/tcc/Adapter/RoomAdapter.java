package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.GetRoomAlocModel;
import com.tcc.R;

import java.util.ArrayList;

public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<GetRoomAlocModel.Datum> mArray;

    public RoomAdapter(Context mContext, ArrayList<GetRoomAlocModel.Datum> mArray) {
        this.mContext = mContext;
        this.mArray = mArray;
    }

    @NonNull
    @Override
    public RoomAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_room, parent, false);
        return new RoomAdapter.MyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull RoomAdapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.mTextName.setText("Room Number : " + mArray.get(i).getRoomNo());
        myViewHolder.mTextAddress.setText("Room Address : " + mArray.get(i).getRoomAddress());
        myViewHolder.mTextStart.setText("Start Date : " + mArray.get(i).getStartDate());
        myViewHolder.mStartEndDate.setText("End Date : " + mArray.get(i).getEndDate());

    }

    @Override
    public int getItemCount() {
        return mArray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView mTextName, mTextAddress, mTextStart, mStartEndDate;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mTextName = itemView.findViewById(R.id.tvRoomName);
            mTextAddress = itemView.findViewById(R.id.tvRoomAddress);
            mTextStart = itemView.findViewById(R.id.tvStartDate);
            mStartEndDate = itemView.findViewById(R.id.tvEndDate);
        }
    }
}
