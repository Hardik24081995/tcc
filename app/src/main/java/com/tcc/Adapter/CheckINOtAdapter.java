package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.CheckInOutListingModel;
import com.tcc.R;

import java.util.ArrayList;

public class CheckINOtAdapter extends RecyclerView.Adapter<CheckINOtAdapter.ViewHolder> {

    Context mContext;
    ArrayList<CheckInOutListingModel.Datum> mArayList;

    public CheckINOtAdapter(Context mContext, ArrayList<CheckInOutListingModel.Datum> mArayList) {
        this.mContext = mContext;
        this.mArayList = mArayList;
    }

    @NonNull
    @Override
    public CheckINOtAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_checkinout, parent, false);

        return new CheckINOtAdapter.ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull CheckINOtAdapter.ViewHolder holder, int i) {

        if (!mArayList.get(i).getCheckintime().equalsIgnoreCase("")) {
            String currentStringInTime = mArayList.get(i).getCheckintime();
            String[] CheckinTime = currentStringInTime.split(" ");
            holder.mTextCheckinTime.setText(CheckinTime[1]);
            holder.mTextDate.setText("Date : " + CheckinTime[0]);
            holder.mTextLocation.setText("Location :" + mArayList.get(i).getInAddress());
        }

        if (!mArayList.get(i).getCheckouttime().equalsIgnoreCase("")) {
            String currentStringOutTime = mArayList.get(i).getCheckouttime();
            String[] checkoutTime = currentStringOutTime.split(" ");
            holder.mTextCheckOutTime.setText(checkoutTime[1]);
            holder.mTextLocation.setText("Location :" + mArayList.get(i).getOutAddress());
        }

    }

    @Override
    public int getItemCount() {
        return mArayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTextDate, mTextCheckinTime, mTextCheckOutTime, mTextLocation;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextDate = itemView.findViewById(R.id.tvTodayDate);
            mTextCheckinTime = itemView.findViewById(R.id.tvCheckInTime);
            mTextCheckOutTime = itemView.findViewById(R.id.tvCheckOutTime);
            mTextLocation = itemView.findViewById(R.id.textLoc);
        }
    }
}
