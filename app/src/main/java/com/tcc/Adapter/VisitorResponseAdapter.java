package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.GetVisitorResponseModel;
import com.tcc.R;

import java.util.ArrayList;

public class VisitorResponseAdapter extends RecyclerView.Adapter<VisitorResponseAdapter.MyViewHolder> {


    Context mContext;
    ArrayList<GetVisitorResponseModel.Datum> mArrayResponse;

    public VisitorResponseAdapter(Context mContext, ArrayList<GetVisitorResponseModel.Datum> mArrayResponse) {
        this.mContext = mContext;
        this.mArrayResponse = mArrayResponse;
    }

    @NonNull
    @Override
    public VisitorResponseAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_reponse_screen, parent, false);
        return new VisitorResponseAdapter.MyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull VisitorResponseAdapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.mTextMsg.setText(mArrayResponse.get(i).getResponse());
        myViewHolder.mTextDate.setText(mArrayResponse.get(i).getResponseDate());
        myViewHolder.mTextAddedBy.setText(mArrayResponse.get(i).getEmployeeName());

    }

    @Override
    public int getItemCount() {
        return mArrayResponse.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mTextMsg, mTextDate, mTextAddedBy;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextMsg = itemView.findViewById(R.id.text_response_message);
            mTextDate = itemView.findViewById(R.id.text_response_Date);
            mTextAddedBy = itemView.findViewById(R.id.text_response_addedBy);
        }
    }
}
