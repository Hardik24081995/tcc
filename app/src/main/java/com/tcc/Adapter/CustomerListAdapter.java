package com.tcc.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.tcc.Activity.CustomerDetailActivity;
import com.tcc.Model.GetCustomerListModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;

public class CustomerListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    ArrayList<GetCustomerListModel.Datum> mArrayCustomer;
    private Activity mContext;
    private RecyclerView recyclerView;
    private boolean isLoading;
    // private OnLoadMoreListener mOnLoadMoreListener;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private TextDrawable.IBuilder mDrawableBuilder;
    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;

    public CustomerListAdapter(Activity mContext, ArrayList<GetCustomerListModel.Datum> mArrayCustomer) {
        this.mContext = mContext;
        this.mArrayCustomer = mArrayCustomer;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemview = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout
                    .customer_list_item, viewGroup, false);
            ListViewHolder viewHolder = new ListViewHolder(itemview);
            return viewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout
                    .loading_more_data_footer, viewGroup, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof ListViewHolder) {
            final ListViewHolder viewHolder = (ListViewHolder) holder;

            viewHolder.mRelativeImageBorder.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.user_image_border));

            viewHolder.mTextCompanyName.setText(mArrayCustomer.get(i).getCompanyName());

            viewHolder.imgCustomer.setImageDrawable(Common.setLabeledImageView(mContext, mArrayCustomer.get(i).getCompanyName(),
                    ""));
            viewHolder.txtName.setText(mArrayCustomer.get(i).getName());
            viewHolder.txtEmail.setText(mArrayCustomer.get(i).getEmailID());
            viewHolder.txtAddress.setText(mArrayCustomer.get(i).getAddress());
            viewHolder.txtMobile.setText(mArrayCustomer.get(i).getMobileNo());
            viewHolder.mTextSiteCount.setText(mArrayCustomer.get(i).getTotalSitesCount());

            viewHolder.txtMobile.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + mArrayCustomer.get(i).getMobileNo()));
                mContext.startActivity(intent);
            });


            viewHolder.itemView.setOnClickListener(v -> {
                Intent intentSetting = new Intent(mContext, CustomerDetailActivity.class);
                SessionManager manager = new SessionManager(mContext);
                manager.setPreferences("Name", mArrayCustomer.get(i).getName());
                manager.setPreferences("Mobile", mArrayCustomer.get(i).getMobileNo());
                manager.setPreferences("CustomerID", mArrayCustomer.get(i).getCustomerID());
                intentSetting.putExtra("CustomerID", mArrayCustomer.get(i).getCustomerID());
                manager.setPreferences("Address", mArrayCustomer.get(i).getAddress());
                manager.setPreferences("Email", mArrayCustomer.get(i).getEmailID());
                manager.setPreferences("CompanyName", mArrayCustomer.get(i).getCompanyName());

                mContext.startActivity(intentSetting);
            });

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mArrayCustomer.size();
    }

    /***
     * View Holder
     */
    static class ListViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout mRelativeImageBorder;
        private TextView txtAddress, txtMobile, txtEmail, txtName, mTextCompanyName, mTextSiteCount;
        private ImageView imgCustomer;

        public ListViewHolder(View view) {
            super(view);

            // Relative Layouts
            this.mRelativeImageBorder = itemView.findViewById(R.id.relative_row_customer_pic);

            imgCustomer = view.findViewById(R.id.image_row_customer_pic);

            txtName = view.findViewById(R.id.text_row_customer_name);
            txtEmail = view.findViewById(R.id.text_row_customer_email);
            txtMobile = view.findViewById(R.id.text_row_customer_mobile_no);
            txtAddress = view.findViewById(R.id.text_row_customer_address);
            mTextCompanyName = view.findViewById(R.id.tc_CompanyNameCustomer);
            mTextSiteCount = view.findViewById(R.id.text_row_customer_site_count);
        }
    }

    /***
     * Loading More data Progress bar view holder
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }
}
