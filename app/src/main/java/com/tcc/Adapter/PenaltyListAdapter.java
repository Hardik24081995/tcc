package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.PenaltyListModel;
import com.tcc.R;

import java.util.ArrayList;

public class PenaltyListAdapter extends RecyclerView.Adapter<PenaltyListAdapter.ViewHolder> {

    Context mContext;
    ArrayList<PenaltyListModel.Datum> mArraList;

    public PenaltyListAdapter(Context mContext, ArrayList<PenaltyListModel.Datum> mArraList) {
        this.mContext = mContext;
        this.mArraList = mArraList;
    }

    @NonNull
    @Override
    public PenaltyListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_penalty, parent, false);

        return new PenaltyListAdapter.ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull PenaltyListAdapter.ViewHolder holder, int i) {

        holder.mTextSiteName.setText(mArraList.get(i).getSitesName());
        holder.mTextReason.setText(mArraList.get(i).getReason());

        ArrayList<PenaltyListModel.Datum.Item> arrayItem = (ArrayList<PenaltyListModel.Datum.Item>) mArraList.get(i).getItem();

        PenaltyListEmployeeAdapter penaltyListEmployeeAdapter = new PenaltyListEmployeeAdapter(mContext, arrayItem);
        holder.mRecyclerView.setAdapter(penaltyListEmployeeAdapter);

    }

    @Override
    public int getItemCount() {
        return mArraList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTextSiteName, mTextReason;
        private RecyclerView mRecyclerView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextSiteName = itemView.findViewById(R.id.text_penalty_sitename);
            mTextReason = itemView.findViewById(R.id.text_penalty_reason);

            mRecyclerView = itemView.findViewById(R.id.penalty_employee_recycler);

            RecyclerView.LayoutManager manager = new LinearLayoutManager(mContext, LinearLayout.VERTICAL, false);
            mRecyclerView.setLayoutManager(manager);
        }
    }
}
