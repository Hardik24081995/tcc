package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.tcc.Model.GetInvoiceModel;
import com.tcc.R;
import com.tcc.utils.Common;

import java.util.ArrayList;


public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<GetInvoiceModel.Datum> mArrayList;
    pdfDownload pdfDownload;


    public InvoiceAdapter(Context mContext, ArrayList<GetInvoiceModel.Datum> mArrayList, pdfDownload pdfDownload1) {
        this.mContext = mContext;
        this.mArrayList = mArrayList;
        this.pdfDownload = pdfDownload1;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_invoice_list_item, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        /*listViewHolder.cardPropertyDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, PropertyDetailActivity.class);
                mContext.startActivity(intent);
            }
        });*/

        myViewHolder.txtCompanyName.setText(mArrayList.get(i).getCompanyName());
        myViewHolder.txtSiteName.setText(mArrayList.get(i).getSitesName());
        myViewHolder.txtStartDate.setText(mArrayList.get(i).getStartDate());
        myViewHolder.txtEndDate.setText(mArrayList.get(i).getEndDate());
//        myViewHolder.mTextAttendee.setText("");
        myViewHolder.txtSiteAddress.setText(mArrayList.get(i).getCompanyAddress());
        myViewHolder.txtQuotationNo.setText(mArrayList.get(i).getQuotationName());
        myViewHolder.txtInvoiceNo.setText(mArrayList.get(i).getInvoiceNo());

        myViewHolder.txtPDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdfDownload.downloadPDF(i, mArrayList.get(i));
            }
        });
        myViewHolder.imageRowCustomerPic.setImageDrawable(Common.setLabeledImageView(mContext, mArrayList.get(i).getCompanyName(), ""));
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public interface pdfDownload {
        void downloadPDF(int position, GetInvoiceModel.Datum data);

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        CircularImageView imageRowCustomerPic;
        TextView txtCompanyName;
        TextView txtSiteName;
        TextView txtSiteAddress;
        TextView txtStartDate;
        TextView txtEndDate;
        TextView txtQuotationNo;
        TextView txtInvoiceNo;
        TextView txtPDF;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageRowCustomerPic = itemView.findViewById(R.id.image_row_customer_pic);
            txtCompanyName = itemView.findViewById(R.id.txtCompanyName);
            txtSiteName = itemView.findViewById(R.id.txtSiteName);
            txtSiteAddress = itemView.findViewById(R.id.txtSiteAddress);
            txtStartDate = itemView.findViewById(R.id.txtStartDate);
            txtEndDate = itemView.findViewById(R.id.txtEndDate);
            txtQuotationNo = itemView.findViewById(R.id.txtQuotationNo);
            txtInvoiceNo = itemView.findViewById(R.id.txtInvoiceNo);
            txtPDF = itemView.findViewById(R.id.txtPDF);
        }
    }
}
