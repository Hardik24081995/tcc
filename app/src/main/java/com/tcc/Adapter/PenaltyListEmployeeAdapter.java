package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.PenaltyListModel;
import com.tcc.R;

import java.util.ArrayList;

public class PenaltyListEmployeeAdapter extends RecyclerView.Adapter<PenaltyListEmployeeAdapter.ViewHolder> {

    Context mContext;
    ArrayList<PenaltyListModel.Datum.Item> itemArrayList;

    public PenaltyListEmployeeAdapter(Context mContext, ArrayList<PenaltyListModel.Datum.Item> itemArrayList) {
        this.mContext = mContext;
        this.itemArrayList = itemArrayList;
    }

    @NonNull
    @Override
    public PenaltyListEmployeeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_penalty_employee, parent, false);

        return new PenaltyListEmployeeAdapter.ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull PenaltyListEmployeeAdapter.ViewHolder holder, int i) {

        holder.mTextEmployeeName.setText(itemArrayList.get(i).getEmployeeName());
        holder.mTextAmount.setText(String.format("₹ : %s", itemArrayList.get(i).getPenalty()));

    }

    @Override
    public int getItemCount() {
        return itemArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTextEmployeeName, mTextAmount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextEmployeeName = itemView.findViewById(R.id.text_penalty_employeename);
            mTextAmount = itemView.findViewById(R.id.text_penalty_amount);

        }
    }
}
