package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.GetTeamEmployeeModel;
import com.tcc.R;

import java.util.ArrayList;

public class SiteAttendanceDetailAdapter extends RecyclerView.Adapter<SiteAttendanceDetailAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<GetTeamEmployeeModel.Datum> datumArrayList;

    public SiteAttendanceDetailAdapter(Context mContext, ArrayList<GetTeamEmployeeModel.Datum> datumArrayList) {
        this.mContext = mContext;
        this.datumArrayList = datumArrayList;
    }

    @NonNull
    @Override
    public SiteAttendanceDetailAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View mView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_siteattendancedetail, parent, false);
        return new SiteAttendanceDetailAdapter.MyViewHolder(mView);

    }

    @Override
    public void onBindViewHolder(@NonNull final SiteAttendanceDetailAdapter.MyViewHolder holder, int i) {

        holder.mTextName.setText(datumArrayList.get(i).getEmployeeName());
        holder.mTextRole.setText(datumArrayList.get(i).getUsertype());

        if (datumArrayList.get(i).getAttendance().equalsIgnoreCase("P")) {
            holder.buttonPresent.setVisibility(View.VISIBLE);
        } else if (datumArrayList.get(i).getAttendance().equalsIgnoreCase("A")) {
            holder.buttonAbsent.setVisibility(View.VISIBLE);
        } else if (datumArrayList.get(i).getAttendance().equalsIgnoreCase("H")) {
            holder.buttonHalfLeave.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return datumArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        Button buttonPresent, buttonAbsent, buttonHalfLeave;
        private TextView mTextName, mTextRole;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextName = itemView.findViewById(R.id.nameatt);
            mTextRole = itemView.findViewById(R.id.roleatt);

            buttonPresent = itemView.findViewById(R.id.buttonPresent);
            buttonAbsent = itemView.findViewById(R.id.buttonAbsent);
            buttonHalfLeave = itemView.findViewById(R.id.buttonHalfLeave);

        }
    }
}
