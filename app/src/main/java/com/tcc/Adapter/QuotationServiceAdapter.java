package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.GetQuotationListModel;
import com.tcc.R;

import java.util.ArrayList;

public class QuotationServiceAdapter extends RecyclerView.Adapter<QuotationServiceAdapter.ViewHolder> {

    Context mContext;
    ArrayList<GetQuotationListModel.Datum.Item> itemArrayList;

    public QuotationServiceAdapter(Context mContext, ArrayList<GetQuotationListModel.Datum.Item> itemArrayList) {
        this.mContext = mContext;
        this.itemArrayList = itemArrayList;
    }

    @NonNull
    @Override
    public QuotationServiceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View mView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_quotation_services, viewGroup, false);

        return new QuotationServiceAdapter.ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull QuotationServiceAdapter.ViewHolder holder, int i) {

        holder.mTextUserType.setText(String.format("User Type : %s", itemArrayList.get(i).getUsertype()));
        holder.mTextDesc.setText(String.format("Description : %s", itemArrayList.get(i).getDescription()));
        holder.mTextRate.setText(String.format("Rate : %s", itemArrayList.get(i).getRate()));
        holder.mTextQty.setText(String.format("Quantity : %s", itemArrayList.get(i).getQty()));

    }

    @Override
    public int getItemCount() {
        return itemArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTextUserType, mTextDesc, mTextRate, mTextQty;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextUserType = itemView.findViewById(R.id.quotationservice_usertype);
            mTextDesc = itemView.findViewById(R.id.quotationservice_description);
            mTextRate = itemView.findViewById(R.id.quotationservice_rate);
            mTextQty = itemView.findViewById(R.id.quotationservice_qty);
        }
    }
}
