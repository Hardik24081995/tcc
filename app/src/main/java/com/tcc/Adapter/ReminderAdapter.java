package com.tcc.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.tcc.Activity.AddPaymentActivity;
import com.tcc.Model.GetCustomerReminderModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;

public class ReminderAdapter extends RecyclerView.Adapter<ReminderAdapter.MyViewHolder> {

    Activity mContext;
    ArrayList<GetCustomerReminderModel.Datum> mArrayList;

    private TextDrawable.IBuilder mDrawableBuilder;
    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;

    public ReminderAdapter(Activity mContext, ArrayList<GetCustomerReminderModel.Datum> mArrayList) {
        this.mContext = mContext;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public ReminderAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_reminder, parent, false);

        return new ReminderAdapter.MyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ReminderAdapter.MyViewHolder holder, int i) {

        SessionManager manager = new SessionManager(mContext);

        holder.text_reminder_name.setText(mArrayList.get(i).getEmployeeName());
        holder.text_row_reminder_phone.setText(mArrayList.get(i).getMobileNo());
        holder.text_row_reminder_date.setText(mArrayList.get(i).getReminderDate());
        holder.text_row_reminder_Description.setText(mArrayList.get(i).getMessage());

        holder.image_row_reminder_pic.setImageDrawable(Common.setLabeledImageView(mContext, mArrayList.get(i).getEmployeeName(), ""));

        holder.image_row_reminder_call.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + mArrayList.get(i).getMobileNo()));
            mContext.startActivity(intent);
        });

        holder.relative_visitor_action_quotation.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, AddPaymentActivity.class);
            mContext.startActivity(intent);
        });

        holder.image_row_reminder_email.setOnClickListener(v -> onEmailClick(mArrayList.get(i).getEmailID()));
        holder.image_row_reminder_sms.setOnClickListener(v -> onSMSClick(mArrayList.get(i).getMobileNo()));
        holder.itemView.setOnClickListener(v -> {

        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    protected void onEmailClick(String EmailID) {

    }

    protected void onSMSClick(String MobileNo) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView image_row_reminder_pic,
                image_row_reminder_email,
                image_row_reminder_call,
                image_row_reminder_sms;

        private RelativeLayout relative_visitor_action_quotation;

        private TextView text_reminder_name,
                text_row_reminder_phone,
                text_row_reminder_date,
                text_row_reminder_Description;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            image_row_reminder_pic = itemView.findViewById(R.id.image_row_reminder_pic);
            image_row_reminder_email = itemView.findViewById(R.id.image_row_reminder_email);
            image_row_reminder_call = itemView.findViewById(R.id.image_row_reminder_call);
            image_row_reminder_sms = itemView.findViewById(R.id.image_row_reminder_sms);

            text_reminder_name = itemView.findViewById(R.id.text_reminder_name);
            text_row_reminder_phone = itemView.findViewById(R.id.text_row_reminder_phone);
            text_row_reminder_date = itemView.findViewById(R.id.text_row_reminder_date);
            text_row_reminder_Description = itemView.findViewById(R.id.text_row_reminder_Description);

            relative_visitor_action_quotation = itemView.findViewById(R.id.relative_visitor_action_quotation);
        }
    }
}
