package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.NotificationModal;
import com.tcc.R;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    Context context;
    private ArrayList<NotificationModal.DataItem> listdata;

    public NotificationAdapter(Context context, ArrayList<NotificationModal.DataItem> listdata) {
        this.context = context;
        this.listdata = listdata;
    }


    @NonNull
    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_notification, parent, false);
        return new NotificationAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.MyViewHolder myViewHolder, int i) {
        NotificationModal.DataItem data = listdata.get(i);
        myViewHolder.payment.setText(data.getCustomerName());
        myViewHolder.description.setText(data.getDiscription());
        myViewHolder.date.setText(data.getCreatedDate());
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView payment, description, date;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            payment = itemView.findViewById(R.id.payment);
            description = itemView.findViewById(R.id.description);
            date = itemView.findViewById(R.id.date);
        }
    }
}
