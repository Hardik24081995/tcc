package com.tcc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Activity.QuotationListActivity;
import com.tcc.Model.GetSiteModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;


public class CustomerSiteListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    ArrayList<GetSiteModel.Datum> datumArrayList;
    private Context mContext;

    public CustomerSiteListAdapter(Context mContext, ArrayList<GetSiteModel.Datum> datumArrayList) {
        this.mContext = mContext;
        this.datumArrayList = datumArrayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_ITEM) {
            View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .property_list_item, parent, false);
            ListViewHolder viewHolder = new ListViewHolder(itemview);
            return viewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        ListViewHolder listViewHolder = (ListViewHolder) holder;

        ((ListViewHolder) holder).mTextName.setText(datumArrayList.get(position).getCompanyName());
        ((ListViewHolder) holder).mTextAddress.setText(datumArrayList.get(position).getCompanyAddress());

        if (datumArrayList.get(position).getStartDate().equals("")) {
            ((ListViewHolder) holder).mTextStartDate.setText("NA");
        } else {
            ((ListViewHolder) holder).mTextStartDate.setText(datumArrayList.get(position).getStartDate());
        }

        if (datumArrayList.get(position).getEndDate().equals("")) {
            ((ListViewHolder) holder).mTextEndDate.setText("NA");
        } else {
            ((ListViewHolder) holder).mTextEndDate.setText(datumArrayList.get(position).getEndDate());
        }

        if (datumArrayList.get(position).getTotalAmount().equals("")) {
            ((ListViewHolder) holder).mTextToatlAmount.setText("NA");
        } else {
            ((ListViewHolder) holder).mTextToatlAmount.setText("Total Amount : " + datumArrayList.get(position).getTotalAmount());
        }

        if (datumArrayList.get(position).getGSTAmount().equals("")) {
            ((ListViewHolder) holder).mTextGst.setText("NA");
        } else {
            ((ListViewHolder) holder).mTextGst.setText("GST Amount : " + datumArrayList.get(position).getGSTAmount());
        }


        listViewHolder.cardPropertyDetail.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, QuotationListActivity.class);
            SessionManager manager = new SessionManager(mContext);
            manager.setPreferences(AppConstants.SITE_ID, datumArrayList.get(position).getSitesID());
            manager.setPreferences(AppConstants.TYPO_FRAGS_CHANGE, "CustomerSites");
            mContext.startActivity(intent);
        });

        listViewHolder.img_icon.setImageDrawable(Common.setLabeledImageView(mContext, datumArrayList.get(position).getCompanyName(), ""));


    }

    @Override
    public int getItemCount() {
        return datumArrayList.size();
    }

    /***
     * View Holder
     */
    static class ListViewHolder extends RecyclerView.ViewHolder {

        private TextView mTextName, mTextAddress, mTextEstimateDate, mTextAttendee, mTextToatlAmount,
                mTextGst, mTextStartDate, mTextEndDate;
        private CardView cardPropertyDetail;
        private ImageView img_icon;

        public ListViewHolder(View view) {
            super(view);


            mTextName = view.findViewById(R.id.text_row_property_list_item_name);
            mTextAddress = view.findViewById(R.id.text_row_property_list_item_address);
            // mTextEstimateDate=view.findViewById(R.id.text_row_projects_item_estimate_time);
            mTextStartDate = view.findViewById(R.id.text_property_item_start_date);
            mTextEndDate = view.findViewById(R.id.text_property_item_end_date);
            mTextGst = view.findViewById(R.id.text_row_project_item_gst);
            mTextToatlAmount = view.findViewById(R.id.text_row_project_item_total_amount);
            mTextAttendee = view.findViewById(R.id.text_row_property_list_attendee);
            cardPropertyDetail = view.findViewById(R.id.cardPropertyDetail);
            img_icon = view.findViewById(R.id.img_icon);
        }
    }

    /***
     * Loading More data Progress bar view holder
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
        }
    }


    /***
     * Check if ATS is need to allow to change
     * Ats will change on ATSPercentage , If Customer as paid payment more or equal to ATS percentage then it allow
     * to change the status
     * @param CustomerProperty
     * @return
     */
   /* private boolean isATSAllow(CustomerProperty.Data CustomerProperty) {
        boolean status = false;
        double PropertyAmount = Double.parseDouble( CustomerProperty.getAmount());
        double PaymentRec = Double.parseDouble(CustomerProperty.getTotalPayment());

        double percentageValue = (double) ((PaymentRec * 100) / PropertyAmount);
        if (percentageValue >=Double.parseDouble( CustomerProperty.getATSPercentage()))
            status = true;
        return status;
    }*/


}
