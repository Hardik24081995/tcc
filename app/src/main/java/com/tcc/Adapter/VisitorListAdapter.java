package com.tcc.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.tcc.Activity.VisitorDetailActivity;
import com.tcc.Model.GetVisitorModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;

public class VisitorListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    ArrayList<GetVisitorModel.Datum> mArrayVisitor;
    private Activity mContext;
    private RecyclerView recyclerView;
    private boolean isLoading;
    // private OnLoadMoreListener mOnLoadMoreListener;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private TextDrawable.IBuilder mDrawableBuilder;
    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;

    public VisitorListAdapter(Activity mContext, ArrayList<GetVisitorModel.Datum> mArrayVisitor) {
        this.mContext = mContext;
        this.mArrayVisitor = mArrayVisitor;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemview = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout
                    .row_visitor_list_item, viewGroup, false);
            ListViewHolder viewHolder = new ListViewHolder(itemview);
            return viewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout
                    .loading_more_data_footer, viewGroup, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ListViewHolder) {
            final ListViewHolder viewHolder = (ListViewHolder) holder;

            viewHolder.mRelativeImageBorder.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.user_image_border));

            viewHolder.visitorListCard.setOnClickListener(v -> {

                Intent intent = new Intent(mContext, VisitorDetailActivity.class);
                SessionManager manager = new SessionManager(mContext);
                intent.putExtra("VisitorID", mArrayVisitor.get(position).getVisitorID());
                manager.setPreferences("VisitorID", mArrayVisitor.get(position).getVisitorID());
                manager.setPreferences("Name", mArrayVisitor.get(position).getName());
                manager.setPreferences("CompanyName", mArrayVisitor.get(position).getCompanyName());
                manager.setPreferences("Mobile", mArrayVisitor.get(position).getMobileNo());
                manager.setPreferences("Service", mArrayVisitor.get(position).getService());
                manager.setPreferences("ProposedDate", mArrayVisitor.get(position).getProposedDate());
                manager.setPreferences("WorkingDays", mArrayVisitor.get(position).getWorkingDays());
                manager.setPreferences("WorkingHours", mArrayVisitor.get(position).getWorkingHours());
                manager.setPreferences("NoOfMen", mArrayVisitor.get(position).getNoOfPowerRequire());
                manager.setPreferences("Address", mArrayVisitor.get(position).getLocation());
                manager.setPreferences("Email", mArrayVisitor.get(position).getEmailID());
                manager.setPreferences("LeadType", mArrayVisitor.get(position).getLeadType());
                manager.setPreferences("CityID", mArrayVisitor.get(position).getCityID());
                manager.setPreferences("StateID", mArrayVisitor.get(position).getStateID());
                //manager.setPreferences("GST", mArrayVisitor.get(position).get() );
                mContext.startActivity(intent);

            });

            viewHolder.imgCustomer.setImageDrawable(Common.setLabeledImageView(mContext, mArrayVisitor.get(position).getCompanyName(),
                    ""));
            viewHolder.txtName.setText(mArrayVisitor.get(position).getName());
            viewHolder.txtEmail.setText(mArrayVisitor.get(position).getEmailID());
            viewHolder.txtCompanyName.setText(mArrayVisitor.get(position).getCompanyName());
            viewHolder.txtAddress.setText(mArrayVisitor.get(position).getLocation());
            viewHolder.txtDate.setText(mArrayVisitor.get(position).getProposedDate());
            viewHolder.txtServiceName.setText(mArrayVisitor.get(position).getService());
            viewHolder.txtNoofMan.setText(mArrayVisitor.get(position).getNoOfPowerRequire());
            viewHolder.txtMobile.setText(mArrayVisitor.get(position).getMobileNo());
            viewHolder.txtLead.setText(mArrayVisitor.get(position).getLeadType());


            viewHolder.txtMobile.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + mArrayVisitor.get(position).getMobileNo()));
                mContext.startActivity(intent);
            });

            viewHolder.mImagePhone.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + mArrayVisitor.get(position).getMobileNo()));
                mContext.startActivity(intent);
            });

            viewHolder.mImageEmail.setOnClickListener(v -> onEmailClick(mArrayVisitor.get(position).getEmailID(), mArrayVisitor.get(position).getVisitorID()));
            viewHolder.mImageSMS.setOnClickListener(v -> onSMSClick(mArrayVisitor.get(position).getMobileNo()));

            viewHolder.itemView.setOnClickListener(v -> {

            });

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mArrayVisitor.size();
    }


    protected void onEmailClick(String Email, String ID) {

    }

    protected void onSMSClick(String Phone) {

    }

    /***
     * View Holder
     */
    static class ListViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout mRelativeImageBorder;
        private TextView txtAddress, txtMobile, txtEmail, txtName, txtCompanyName, txtServiceName, txtDate, txtNoofMan, txtLead, txtGstNo;
        private ImageView imgCustomer, mImageSMS, mImageEmail, mImagePhone;
        private CardView visitorListCard;

        public ListViewHolder(View view) {
            super(view);

            // Relative Layouts
            this.mRelativeImageBorder = itemView.findViewById(R.id.relative_row_visitor_pic);

            imgCustomer = view.findViewById(R.id.image_row_visitor_pic);

            visitorListCard = view.findViewById(R.id.visitorListCard);

            txtName = view.findViewById(R.id.text_row_visitor_name);
            txtEmail = view.findViewById(R.id.text_row_visitor_email);
            txtMobile = view.findViewById(R.id.text_row_visitor_mobile_no);
            txtAddress = view.findViewById(R.id.text_row_visitor_address);
            txtCompanyName = view.findViewById(R.id.tvCompanyNameVisitor);
            txtServiceName = view.findViewById(R.id.tvServiceName);
            txtDate = view.findViewById(R.id.tvDate);
            txtGstNo = view.findViewById(R.id.tv_GstNo);
            txtNoofMan = view.findViewById(R.id.tvNoOfMen);
            txtLead = view.findViewById(R.id.tvLeadType);

            mImageEmail = view.findViewById(R.id.image_row_visitor_email);
            mImagePhone = view.findViewById(R.id.image_row_visitor_call);
            mImageSMS = view.findViewById(R.id.image_row_visitor_sms);

        }
    }

    /***
     * Loading More data Progress bar view holder
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }
}
