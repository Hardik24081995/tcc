package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.GetCustomerProcessModel;
import com.tcc.R;

import java.util.ArrayList;

public class ProcessAdapter extends RecyclerView.Adapter<ProcessAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<GetCustomerProcessModel.Datum> mArrayList;

    public ProcessAdapter(Context mContext, ArrayList<GetCustomerProcessModel.Datum> mArrayList) {
        this.mContext = mContext;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public ProcessAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_process, parent, false);
        return new ProcessAdapter.MyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProcessAdapter.MyViewHolder holder, int i) {

        holder.mTextName.setText(mArrayList.get(i).getCustomerName());
        holder.mTextDate.setText(mArrayList.get(i).getCreatedDate());
        holder.mTextDecription.setText(mArrayList.get(i).getDiscription());
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextName, mTextDate, mTextDecription;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextName = itemView.findViewById(R.id.text_process_name);
            mTextDate = itemView.findViewById(R.id.text_process_date_time);
            mTextDecription = itemView.findViewById(R.id.text_process_description);
        }
    }
}
