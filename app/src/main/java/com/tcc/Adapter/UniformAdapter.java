package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.GetEmployeeUniformModel;
import com.tcc.R;

import java.util.ArrayList;

public class UniformAdapter extends RecyclerView.Adapter<UniformAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<GetEmployeeUniformModel.Datum> mArrUniform;

    public UniformAdapter(Context mContext, ArrayList<GetEmployeeUniformModel.Datum> mArrUniform) {
        this.mContext = mContext;
        this.mArrUniform = mArrUniform;
    }

    @NonNull
    @Override
    public UniformAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_uniform, parent, false);

        return new UniformAdapter.MyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull UniformAdapter.MyViewHolder holder, int position) {

        holder.tvUniformType.setText(String.format("Uniform Type : %s", mArrUniform.get(position).getUniformtype()));
        holder.tvUniformDate.setText(String.format("Date : %s", mArrUniform.get(position).getUniformDate()));

    }

    @Override
    public int getItemCount() {
        return mArrUniform.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvUniformType, tvUniformDate;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvUniformType = itemView.findViewById(R.id.tvUniformType);
            tvUniformDate = itemView.findViewById(R.id.tvUniformDate);
        }
    }
}
