package com.tcc.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.tcc.Activity.EmployeeDetailActivity;
import com.tcc.Model.GetEmployeListModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;

public class EmployeAdapter extends RecyclerView.Adapter<EmployeAdapter.MyViewHolder> {

    ArrayList<GetEmployeListModel.Datum> mArrayList;
    private Activity mContext;
    private TextDrawable.IBuilder mDrawableBuilder;
    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;

    public EmployeAdapter(Activity mContext, ArrayList<GetEmployeListModel.Datum> mArrayList) {
        this.mContext = mContext;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public EmployeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_employe, parent, false);

        return new EmployeAdapter.MyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeAdapter.MyViewHolder holder, int i) {

        holder.imgCustomer.setImageDrawable(Common.setLabeledImageView(mContext, mArrayList.get(i).getFirstName(), ""));
        holder.mTextName.setText(mArrayList.get(i).getFirstName() + " " + mArrayList.get(i).getLastName());
        holder.mTextPhone.setText(mArrayList.get(i).getMobileNo());
        holder.mTextPost.setText(mArrayList.get(i).getUserType());
        holder.mTextSalary.setText(mArrayList.get(i).getSalary());

        if (mArrayList.get(i).getAccountNo().equalsIgnoreCase("")) {
            holder.mImageDot.setVisibility(View.VISIBLE);
        } else if (!mArrayList.get(i).getAccountNo().equalsIgnoreCase("")) {
            holder.mImageDot.setVisibility(View.GONE);
        }

        holder.cardEmployee.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, EmployeeDetailActivity.class);
            SessionManager sessionManager = new SessionManager(mContext);
            sessionManager.setPreferences("Name", mArrayList.get(i).getFirstName() + " " + mArrayList.get(i).getLastName());
            sessionManager.setPreferences(AppConstants.EMPLOYEE_ID, "-1");
            sessionManager.setPreferences(AppConstants.EMPLOYEE_ID_FOR_ADD, mArrayList.get(i).getUserID());
            mContext.startActivity(intent);
        });

        holder.mImageDeactivate.setOnClickListener(v -> {

            SessionManager sessionManager = new SessionManager(mContext);
            if (sessionManager.getRole().getData().getEmployee().getIsEdit().equals("1")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        mContext);
                builder.setMessage("Would You Like To Deactivate Employee?");
                builder.setNegativeButton("NO",
                        (dialog, which) -> dialog.dismiss());
                builder.setPositiveButton("YES",
                        (dialog, which) -> deleteTeamMember(holder.getAdapterPosition(), mArrayList.get(i)));
                builder.show();
            } else {
                Common.setCustomToast(mContext, mContext.getString(R.string.not_access));
            }


        });

    }

    protected void deleteTeamMember(int adapterPosition, GetEmployeListModel.Datum datum) {
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgCustomer, mImageDot, mImageDeactivate;
        private CardView cardEmployee;
        private TextView mTextName, mTextPhone, mTextPost, mTextSalary;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imgCustomer = itemView.findViewById(R.id.img_employe);
            cardEmployee = itemView.findViewById(R.id.cardEmployee);
            mTextName = itemView.findViewById(R.id.tvEmployeeName);
            mTextPhone = itemView.findViewById(R.id.tvPhone);
            mTextPost = itemView.findViewById(R.id.tvPost);
            mTextSalary = itemView.findViewById(R.id.tvSalary);
            mImageDot = itemView.findViewById(R.id.image_bankdetails);

            mImageDeactivate = itemView.findViewById(R.id.imgDeactivate);
        }
    }
}
