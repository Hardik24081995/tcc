package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.GetEmployeeTrainingModel;
import com.tcc.R;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;

public class TrainingAdapter extends RecyclerView.Adapter<TrainingAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<GetEmployeeTrainingModel.Datum> datumArrayList;

    public TrainingAdapter(Context mContext, ArrayList<GetEmployeeTrainingModel.Datum> datumArrayList) {
        this.mContext = mContext;
        this.datumArrayList = datumArrayList;
    }


    @NonNull
    @Override
    public TrainingAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_training, parent, false);
        return new TrainingAdapter.MyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull TrainingAdapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.tvEmployeeName.setText(datumArrayList.get(i).getEmployeeName());
        myViewHolder.tvAttendance.setText(String.format("Training Name : %s", datumArrayList.get(i).getTraining()));
        myViewHolder.tvDateAndTime.setText(String.format("Date & Time : %s", datumArrayList.get(i).getTrainingDate()));

        SessionManager manager = new SessionManager(mContext);
        manager.setPreferences("TimeID", datumArrayList.get(i).getTrainingDateTimeID());

    }

    @Override
    public int getItemCount() {
        return datumArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvEmployeeName, tvAttendance, tvDateAndTime;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvEmployeeName = itemView.findViewById(R.id.tvTrainingName);
            tvAttendance = itemView.findViewById(R.id.tvAttendence);
            tvDateAndTime = itemView.findViewById(R.id.tvDateTime);

        }
    }
}
