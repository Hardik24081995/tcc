package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.CustomerPaymentModel;
import com.tcc.R;

import java.util.ArrayList;

public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<CustomerPaymentModel.Datum> mArrayList;

    public PaymentAdapter(Context mContext, ArrayList<CustomerPaymentModel.Datum> mArrayList) {
        this.mContext = mContext;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public PaymentAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_payment, parent, false);
        return new PaymentAdapter.MyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentAdapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.mTextDate.setText("Date : " + mArrayList.get(i).getPaymentDate());
        myViewHolder.mTextAmount.setText("Total Amount : " + mArrayList.get(i).getPaymentAmount());
        myViewHolder.mTextGSTAmount.setText("GSTAmount : " + mArrayList.get(i).getGSTAmount());
        myViewHolder.mTextType.setText("Amount Type : " + mArrayList.get(i).getAmountType());

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView mTextDate, mTextAmount, mTextGSTAmount, mTextType;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextDate = itemView.findViewById(R.id.payment_Date);
            mTextAmount = itemView.findViewById(R.id.payment_Amount);
            mTextGSTAmount = itemView.findViewById(R.id.payment_GSTAmount);
            mTextType = itemView.findViewById(R.id.payment_AmountType);
        }
    }
}
