package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.GetSalaryModal;
import com.tcc.R;

import java.util.ArrayList;

public class SalaryAdapter extends RecyclerView.Adapter<SalaryAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<GetSalaryModal.Datum> mArray;

    public SalaryAdapter(Context mContext, ArrayList<GetSalaryModal.Datum> mArray) {
        this.mContext = mContext;
        this.mArray = mArray;
    }

    @NonNull
    @Override
    public SalaryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_salary, parent, false);
        return new SalaryAdapter.MyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull SalaryAdapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.mTextAMount.setText("Amount : " + "₹" + mArray.get(i).getAmount());
        myViewHolder.mTextDesc.setText("Description : " + mArray.get(i).getDescription());
        myViewHolder.mTextDate.setText("Payment Date : " + mArray.get(i).getPaymentDate());
        // myViewHolder.mStartEndDate.setText("End Date : " + mArray.get(i).getEndDate());

    }

    @Override
    public int getItemCount() {
        return mArray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView mTextAMount, mTextDesc, mTextDate;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mTextAMount = itemView.findViewById(R.id.tvAmount);
            mTextDesc = itemView.findViewById(R.id.tvDesc);
            mTextDate = itemView.findViewById(R.id.tvDate);
            // mStartEndDate = itemView.findViewById(R.id.tvEndDate);
        }
    }
}
