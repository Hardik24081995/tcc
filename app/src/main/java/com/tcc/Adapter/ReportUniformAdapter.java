package com.tcc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Model.ReportUniformModal;
import com.tcc.R;

import java.util.ArrayList;

public class ReportUniformAdapter extends RecyclerView.Adapter<ReportUniformAdapter.MyViewHolder> {


    Context mContext;
    ArrayList<ReportUniformModal.DataItem> mArrayResponse;

    public ReportUniformAdapter(Context mContext, ArrayList<ReportUniformModal.DataItem> mArrayResponse) {
        this.mContext = mContext;
        this.mArrayResponse = mArrayResponse;
    }

    @NonNull
    @Override
    public ReportUniformAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_reponse_screen, parent, false);
        return new ReportUniformAdapter.MyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportUniformAdapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.mTextAddedBy.setText(mArrayResponse.get(i).getUniformtype());
        myViewHolder.mTextDate.setText(mArrayResponse.get(i).getUniformDate());
        myViewHolder.mTextMsg.setText(mArrayResponse.get(i).getEmployeeName());

    }

    @Override
    public int getItemCount() {
        return mArrayResponse.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mTextMsg, mTextDate, mTextAddedBy;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextMsg = itemView.findViewById(R.id.text_response_message);
            mTextDate = itemView.findViewById(R.id.text_response_Date);
            mTextAddedBy = itemView.findViewById(R.id.text_response_addedBy);
        }
    }
}
