package com.tcc.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.tcc.Activity.SiteAttendanceDetailActivity;
import com.tcc.Model.GetAttendanceModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.SessionManager;

import java.util.ArrayList;

public class MenuAttendanceAdapter extends RecyclerView.Adapter<MenuAttendanceAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<GetAttendanceModel.Datum> data;

    public MenuAttendanceAdapter(Context mContext, ArrayList<GetAttendanceModel.Datum> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @NonNull
    @Override
    public MenuAttendanceAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_siteattendance, parent, false);

        return new MenuAttendanceAdapter.MyViewHolder(mView);

    }

    @Override
    public void onBindViewHolder(@NonNull MenuAttendanceAdapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.ll_dateattendance.setVisibility(View.VISIBLE);
        myViewHolder.card_siteattendance.setOnClickListener(v -> {
            SessionManager manager = new SessionManager(mContext);
            Intent intent = new Intent(mContext, SiteAttendanceDetailActivity.class);
            manager.setPreferences(AppConstants.SITE_ID, data.get(i).getSitesID());
            manager.setPreferences(AppConstants.QUOTATION_ID, data.get(i).getQuotationID());
            manager.setPreferences(AppConstants.ATTENDANCE_DATE, data.get(i).getAttendanceDate());
            mContext.startActivity(intent);
        });

        myViewHolder.siteTitleValue.setText(data.get(i).getCompanyName());
        myViewHolder.mTextDate.setText(data.get(i).getAttendanceDate());
        myViewHolder.mTextAbsent.setText(data.get(i).getAbsentount());
        myViewHolder.mTextPresent.setText(data.get(i).getPresentCount());
        myViewHolder.mTextLeave.setText(data.get(i).getHalfDayount());
        myViewHolder.mTextAddress.setText(data.get(i).getCompanyAddress());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView siteAttendaceTitle, siteTitleValue;
        LinearLayout ll_dateattendance;
        TextView mTextDate, mTextAddress, mTextPresent, mTextAbsent, mTextLeave;
        CardView card_siteattendance;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            card_siteattendance = itemView.findViewById(R.id.card_siteattendance);
            siteAttendaceTitle = itemView.findViewById(R.id.siteAttendaceTitle);
            siteTitleValue = itemView.findViewById(R.id.siteTitleValue);
            ll_dateattendance = itemView.findViewById(R.id.ll_dateattendance);

            mTextDate = itemView.findViewById(R.id.attendanceDateValue);
            mTextAddress = itemView.findViewById(R.id.attendanceAddressValue);
            mTextPresent = itemView.findViewById(R.id.presentValue);
            mTextAbsent = itemView.findViewById(R.id.AbsentValue);
            mTextLeave = itemView.findViewById(R.id.leaveValue);
        }
    }
}
