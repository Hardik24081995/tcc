package com.tcc;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Activity mActivity;
    private ListView mListView;
    private Toolbar mToolbar;
    private ActionBarDrawerToggle mToggle;
    private TextView mTextHeader;
    private DrawerLayout mDrawerLayout;
    private ArrayList<GroupItem> mArrMenu;

    private ListViewAdapter mMenuAdapter;
    private ImageView mImageAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mActivity = MainActivity.this;
        mArrMenu = new ArrayList<>();

        getIds();
        setData();
        setToggleMenuToDrawer();
        prepareListData();
        setRegister();
    }


    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Tool Bar
            mToolbar = findViewById(R.id.toolbar_nav_drawer);

            mImageAdd = findViewById(R.id.image_custom_toolbar_add);

            // Drawer Layout
            mDrawerLayout = findViewById(R.id.drawer_layout);
            mListView = findViewById(R.id.list_view_main);
            mTextHeader = findViewById(R.id.text_nav_header);
            // Navigation Drawer View
            NavigationView mNavigationView = findViewById(R.id.nav_view);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            // ToDo: Sets the tool bar header properties
            mTextHeader.setText(getResources().getString(R.string.nav_menu_home));
            mTextHeader.setSelected(true);

            // ToDo: Sets the action bar
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Bind toggle button to drawer
     */
    private void setToggleMenuToDrawer() {
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
                hideKeyboard();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
                hideKeyboard();
            }
        };

        mDrawerLayout.setDrawerListener(mToggle);
        mToggle.syncState();
    }

    /**
     * Prepares the expandable list data
     */
    @SuppressLint("RestrictedApi")
    private void prepareListData() {

        GroupItem mGroupItemHome = new GroupItem();
        GroupItem mGroupItemVistor = new GroupItem();
        GroupItem mGroupItemCustomer = new GroupItem();
        GroupItem mGroupItemAttendance = new GroupItem();
        GroupItem mGroupItemSites = new GroupItem();
        GroupItem mGroupItemSettings = new GroupItem();
        GroupItem mGroupItemEmployee = new GroupItem();

        mArrMenu = new ArrayList<>();
        mGroupItemHome.title = getResources().getString(R.string.nav_menu_home);
        mGroupItemVistor.title = getResources().getString(R.string.nav_menu_visitor);
        mGroupItemCustomer.title = getResources().getString(R.string.nav_menu_customer);
//        mGroupItemAttendance.title = getResources().getString(R.string.nav_menu_attendance);
        mGroupItemSites.title = getResources().getString(R.string.nav_menu_sites);
        mGroupItemSettings.title = getResources().getString(R.string.nav_menu_settings);
        mGroupItemEmployee.title = getResources().getString(R.string.nav_menu_employee);


        /*mGroupItemAppointment.img = getResources().getDrawable(R.drawable.nav_appointment_icon);
        mGroupItemCheckIn.img = getResources().getDrawable(R.drawable.nav_menu_check_in_icon);
        mGroupItemHome.img = getResources().getDrawable(R.drawable.nav_home_icon);
        mGroupItemPatient.img = getResources().getDrawable(R.drawable.nav_patient_icon);
        //mGroupItemFollowUp.img = getResources().getDrawable(R.drawable.nav_follow_up_icon);
        mGroupItemSurgery.img = getResources().getDrawable(R.drawable.nav_menu_surgery_icon);
        mGroupItemPayment.img = getResources().getDrawable(R.drawable.nav_payment_icon);
        //  mGroupItemChangeStatus.img = getResources().getDrawable(R.drawable.ic_process_icon);
        mGroupItemSettings.img = getResources().getDrawable(R.drawable.nav_settings_icon);*/

        mArrMenu.add(mGroupItemHome);
        mArrMenu.add(mGroupItemVistor);
        mArrMenu.add(mGroupItemCustomer);
        mArrMenu.add(mGroupItemEmployee);
        mArrMenu.add(mGroupItemAttendance);
        mArrMenu.add(mGroupItemSites);
        mArrMenu.add(mGroupItemSettings);

        mMenuAdapter = new ListViewAdapter(mActivity);
        mMenuAdapter.setData(mArrMenu);
        mListView.setAdapter(mMenuAdapter);
        mMenuAdapter.notifyDataSetChanged();
    }

    /**
     * This method is used to hide the keyboard.
     */
    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /*
     * Set Register
     */
    private void setRegister() {
        mImageAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private static class GroupItem {
        String title;
        Drawable img;
    }

    private static class GroupHolder {
        ImageView mImageNavMenu;
        TextView mTextViewParentHeader;
    }

    private class ListViewAdapter extends BaseAdapter {

        private LayoutInflater inflater;
        private List<GroupItem> mMenuSearches;

        public ListViewAdapter(Activity mActivity) {
            inflater = LayoutInflater.from(mActivity);
        }

        public void setData(List<GroupItem> mMenuSearches) {
            this.mMenuSearches = mMenuSearches;
        }

        @Override
        public int getCount() {
            return mMenuSearches.size();
        }

        @Override
        public Object getItem(int position) {
            return mMenuSearches.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            GroupHolder holder;
            if (convertView == null) {
                holder = new GroupHolder();
                convertView = inflater.inflate(R.layout.row_list_menu_item, parent, false);

                holder.mImageNavMenu = convertView.findViewById(R.id.image_view_parent_nav_menu);
                holder.mTextViewParentHeader = convertView.findViewById(R.id.text_view_parent_nav_menu_name);
                convertView.setTag(holder);

                holder.mTextViewParentHeader.setText(mMenuSearches.get(position).title);

            } else {
                holder = (GroupHolder) convertView.getTag();
            }
            return convertView;
        }
    }
}
