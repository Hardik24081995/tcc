
package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


@SuppressWarnings("unused")
public class GetInvoiceModel {

    @Expose
    private List<Datum> data;
    @Expose
    private Long error;
    @Expose
    private String message;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Long getError() {
        return error;
    }

    public void setError(Long error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Datum {

        @SerializedName("CompanyAddress")
        private String companyAddress;
        @SerializedName("CompanyName")
        private String companyName;
        @SerializedName("CustomerID")
        private String customerID;
        @SerializedName("EndDate")
        private String endDate;
        @SerializedName("EstimateNo")
        private String estimateNo;
        @SerializedName("Invoice")
        private String invoice;
        @SerializedName("InvoiceID")
        private String invoiceID;
        @SerializedName("InvoiceNo")
        private String invoiceNo;
        @SerializedName("Name")
        private String name;
        @SerializedName("QuotationID")
        private String quotationID;
        @SerializedName("QuotationName")
        private String quotationName;
        @SerializedName("Rno")
        private String rno;
        @Expose
        private String rowcount;
        @SerializedName("SitesID")
        private String sitesID;
        @SerializedName("SitesName")
        private String sitesName;
        @SerializedName("StartDate")
        private String startDate;
        @SerializedName("Status")
        private String status;

        public String getCompanyAddress() {
            return companyAddress;
        }

        public void setCompanyAddress(String companyAddress) {
            this.companyAddress = companyAddress;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getCustomerID() {
            return customerID;
        }

        public void setCustomerID(String customerID) {
            this.customerID = customerID;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getEstimateNo() {
            return estimateNo;
        }

        public void setEstimateNo(String estimateNo) {
            this.estimateNo = estimateNo;
        }

        public String getInvoice() {
            return invoice;
        }

        public void setInvoice(String invoice) {
            this.invoice = invoice;
        }

        public String getInvoiceID() {
            return invoiceID;
        }

        public void setInvoiceID(String invoiceID) {
            this.invoiceID = invoiceID;
        }

        public String getInvoiceNo() {
            return invoiceNo;
        }

        public void setInvoiceNo(String invoiceNo) {
            this.invoiceNo = invoiceNo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getQuotationID() {
            return quotationID;
        }

        public void setQuotationID(String quotationID) {
            this.quotationID = quotationID;
        }

        public String getQuotationName() {
            return quotationName;
        }

        public void setQuotationName(String quotationName) {
            this.quotationName = quotationName;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

        public String getSitesID() {
            return sitesID;
        }

        public void setSitesID(String sitesID) {
            this.sitesID = sitesID;
        }

        public String getSitesName() {
            return sitesName;
        }

        public void setSitesName(String sitesName) {
            this.sitesName = sitesName;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    }


}
