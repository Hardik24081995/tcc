package com.tcc.Model;

public class PenaltyModel {
    private String OpetionSuggested;
    private String OpetionSuggestedID;
    private String status;
    private Boolean isChecked = false;

    public String getOpetionSuggested() {
        return OpetionSuggested;
    }

    public void setOpetionSuggested(String opetionSuggested) {
        OpetionSuggested = opetionSuggested;
    }

    public String getOpetionSuggestedID() {
        return OpetionSuggestedID;
    }

    public void setOpetionSuggestedID(String opetionSuggestedID) {
        OpetionSuggestedID = opetionSuggestedID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }
}
