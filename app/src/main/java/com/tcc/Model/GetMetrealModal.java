package com.tcc.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetMetrealModal {

    @SerializedName("data")
    private List<DataItem> data;

    @SerializedName("error")
    private int error;

    @SerializedName("message")
    private String message;

    public List<DataItem> getData() {
        return data;
    }

    public int getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public class DataItem {

        @SerializedName("Status")
        private String status;

        @SerializedName("MaterialID")
        private String materialID;

        @SerializedName("Rno")
        private String rno;

        @SerializedName("rowcount")
        private String rowcount;

        @SerializedName("Rate")
        private String rate;

        @SerializedName("HSNNo")
        private String hSNNo;

        @SerializedName("Material")
        private String material;

        public String getStatus() {
            return status;
        }

        public String getMaterialID() {
            return materialID;
        }

        public String getRno() {
            return rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public String getRate() {
            return rate;
        }

        public String getHSNNo() {
            return hSNNo;
        }

        public String getMaterial() {
            return material;
        }
    }
}