package com.tcc.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReportPaymentModal {

    @SerializedName("data")
    private List<DataItem> data;

    @SerializedName("error")
    private int error;

    @SerializedName("message")
    private String message;

    public List<DataItem> getData() {
        return data;
    }

    public int getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public class DataItem {

        @SerializedName("BankName")
        private String bankName;

        @SerializedName("Status")
        private String status;

        @SerializedName("SitesID")
        private String sitesID;

        @SerializedName("rowcount")
        private String rowcount;

        @SerializedName("AccountNo")
        private String accountNo;

        @SerializedName("PaymentDate")
        private String paymentDate;

        @SerializedName("SitesName")
        private String sitesName;

        @SerializedName("GSTAmount")
        private String gSTAmount;

        @SerializedName("QuotationID")
        private String quotationID;

        @SerializedName("PaymentAmount")
        private String paymentAmount;

        @SerializedName("CompanyName")
        private String companyName;

        @SerializedName("UTR")
        private String uTR;

        @SerializedName("Rno")
        private String rno;

        @SerializedName("AmountType")
        private String amountType;

        @SerializedName("CustomerPaymentID")
        private String customerPaymentID;

        @SerializedName("IFCCode")
        private String iFCCode;

        @SerializedName("BranchName")
        private String branchName;

        @SerializedName("ChequeNo")
        private String chequeNo;

        @SerializedName("RemainingGSTAmount")
        private String remainingGSTAmount;

        @SerializedName("PaymentMode")
        private String paymentMode;

        @SerializedName("RemainingAmount")
        private String remainingAmount;

        public String getBankName() {
            return bankName;
        }

        public String getStatus() {
            return status;
        }

        public String getSitesID() {
            return sitesID;
        }

        public String getRowcount() {
            return rowcount;
        }

        public String getAccountNo() {
            return accountNo;
        }

        public String getPaymentDate() {
            return paymentDate;
        }

        public String getSitesName() {
            return sitesName;
        }

        public String getGSTAmount() {
            return gSTAmount;
        }

        public String getQuotationID() {
            return quotationID;
        }

        public String getPaymentAmount() {
            return paymentAmount;
        }

        public String getCompanyName() {
            return companyName;
        }

        public String getUTR() {
            return uTR;
        }

        public String getRno() {
            return rno;
        }

        public String getAmountType() {
            return amountType;
        }

        public String getCustomerPaymentID() {
            return customerPaymentID;
        }

        public String getIFCCode() {
            return iFCCode;
        }

        public String getBranchName() {
            return branchName;
        }

        public String getChequeNo() {
            return chequeNo;
        }

        public String getRemainingGSTAmount() {
            return remainingGSTAmount;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public String getRemainingAmount() {
            return remainingAmount;
        }
    }
}