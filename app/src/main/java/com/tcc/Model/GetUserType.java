package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetUserType {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("UsertypeID")
        @Expose
        private String usertypeID;
        @SerializedName("Usertype")
        @Expose
        private String usertype;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("HSNNo")
        @Expose
        private String HSNNo;
        @SerializedName("Rate")
        @Expose
        private String Rate;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;

        public String getUsertypeID() {
            return usertypeID;
        }

        public void setUsertypeID(String usertypeID) {
            this.usertypeID = usertypeID;
        }

        public String getUsertype() {
            return usertype;
        }

        public void setUsertype(String usertype) {
            this.usertype = usertype;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

        public String getHSNNo() {
            return HSNNo;
        }

        public void setHSNNo(String HSNNo) {
            this.HSNNo = HSNNo;
        }

        public String getRate() {
            return Rate;
        }

        public void setRate(String rate) {
            Rate = rate;
        }
    }
}
