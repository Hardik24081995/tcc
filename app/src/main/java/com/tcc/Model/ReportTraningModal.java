package com.tcc.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReportTraningModal {

    @SerializedName("data")
    private List<DataItem> data;

    @SerializedName("error")
    private int error;

    @SerializedName("message")
    private String message;

    public List<DataItem> getData() {
        return data;
    }

    public int getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public class DataItem {

        @SerializedName("Training")
        private String training;

        @SerializedName("Status")
        private String status;

        @SerializedName("Rno")
        private String rno;

        @SerializedName("rowcount")
        private String rowcount;

        @SerializedName("UserID")
        private String userID;

        @SerializedName("TrainingTime")
        private String trainingTime;

        @SerializedName("TrainingDateTimeID")
        private String trainingDateTimeID;

        @SerializedName("TrainingEmployeeID")
        private String trainingEmployeeID;

        @SerializedName("EmployeeID")
        private String employeeID;

        @SerializedName("EmployeeName")
        private String employeeName;

        @SerializedName("TrainingDate")
        private String trainingDate;

        public String getTraining() {
            return training;
        }

        public String getStatus() {
            return status;
        }

        public String getRno() {
            return rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public String getUserID() {
            return userID;
        }

        public String getTrainingTime() {
            return trainingTime;
        }

        public String getTrainingDateTimeID() {
            return trainingDateTimeID;
        }

        public String getTrainingEmployeeID() {
            return trainingEmployeeID;
        }

        public String getEmployeeID() {
            return employeeID;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public String getTrainingDate() {
            return trainingDate;
        }
    }
}