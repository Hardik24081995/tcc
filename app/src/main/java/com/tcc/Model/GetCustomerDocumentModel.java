package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetCustomerDocumentModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("CustomerSitesDocumentID")
        @Expose
        private String customerSitesDocumentID;
        @SerializedName("SitesID")
        @Expose
        private String sitesID;
        @SerializedName("Title")
        @Expose
        private String title;
        @SerializedName("Document")
        @Expose
        private String document;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;
        @SerializedName("QuotationID")
        @Expose
        private String QuotationID;

        public String getQuotationID() {
            return QuotationID;
        }

        public void setQuotationID(String quotationID) {
            QuotationID = quotationID;
        }

        public String getCustomerSitesDocumentID() {
            return customerSitesDocumentID;
        }

        public void setCustomerSitesDocumentID(String customerSitesDocumentID) {
            this.customerSitesDocumentID = customerSitesDocumentID;
        }

        public String getSitesID() {
            return sitesID;
        }

        public void setSitesID(String sitesID) {
            this.sitesID = sitesID;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDocument() {
            return document;
        }

        public void setDocument(String document) {
            this.document = document;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

    }
}
