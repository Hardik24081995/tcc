package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CheckInOutListingModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("CheckincheckoutID")
        @Expose
        private String checkincheckoutID;
        @SerializedName("UserID")
        @Expose
        private String userID;
        @SerializedName("EmployeeName")
        @Expose
        private String employeeName;
        @SerializedName("Checkintime")
        @Expose
        private String checkintime;
        @SerializedName("Checkouttime")
        @Expose
        private String checkouttime;
        @SerializedName("Inlatitude")
        @Expose
        private String inlatitude;
        @SerializedName("Inlongitude")
        @Expose
        private String inlongitude;
        @SerializedName("InAddress")
        @Expose
        private String InAddress;
        @SerializedName("Outlatitude")
        @Expose
        private String outlatitude;
        @SerializedName("Outlongitude")
        @Expose
        private String outlongitude;
        @SerializedName("OutAddress")
        @Expose
        private String OutAddress;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;

        public String getCheckincheckoutID() {
            return checkincheckoutID;
        }

        public void setCheckincheckoutID(String checkincheckoutID) {
            this.checkincheckoutID = checkincheckoutID;
        }

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public void setEmployeeName(String employeeName) {
            this.employeeName = employeeName;
        }

        public String getCheckintime() {
            return checkintime;
        }

        public void setCheckintime(String checkintime) {
            this.checkintime = checkintime;
        }

        public String getCheckouttime() {
            return checkouttime;
        }

        public void setCheckouttime(String checkouttime) {
            this.checkouttime = checkouttime;
        }

        public String getInlatitude() {
            return inlatitude;
        }

        public void setInlatitude(String inlatitude) {
            this.inlatitude = inlatitude;
        }

        public String getInlongitude() {
            return inlongitude;
        }

        public void setInlongitude(String inlongitude) {
            this.inlongitude = inlongitude;
        }

        public String getOutlatitude() {
            return outlatitude;
        }

        public void setOutlatitude(String outlatitude) {
            this.outlatitude = outlatitude;
        }

        public String getOutlongitude() {
            return outlongitude;
        }

        public void setOutlongitude(String outlongitude) {
            this.outlongitude = outlongitude;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

        public String getInAddress() {
            return InAddress;
        }

        public void setInAddress(String inAddress) {
            InAddress = inAddress;
        }

        public String getOutAddress() {
            return OutAddress;
        }

        public void setOutAddress(String outAddress) {
            OutAddress = outAddress;
        }
    }
}
