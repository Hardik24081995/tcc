package com.tcc.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetModuleIDList {

    @SerializedName("data")
    private List<DataItem> data;

    @SerializedName("error")
    private int error;

    @SerializedName("message")
    private String message;

    public List<DataItem> getData() {
        return data;
    }

    public int getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public class DataItem {

        @SerializedName("ParentID")
        private String parentID;

        @SerializedName("ModuleID")
        private String moduleID;

        @SerializedName("Type")
        private String type;

        @SerializedName("ModuleName")
        private String moduleName;

        public String getParentID() {
            return parentID;
        }

        public String getModuleID() {
            return moduleID;
        }

        public String getType() {
            return type;
        }

        public String getModuleName() {
            return moduleName;
        }
    }
}