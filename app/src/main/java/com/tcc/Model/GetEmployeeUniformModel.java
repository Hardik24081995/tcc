package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetEmployeeUniformModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("EmployeeUniformID")
        @Expose
        private String employeeUniformID;
        @SerializedName("UserID")
        @Expose
        private String userID;
        @SerializedName("UniformDate")
        @Expose
        private String uniformDate;
        @SerializedName("Uniformtype")
        @Expose
        private String uniformtype;
        @SerializedName("EmployeeName")
        @Expose
        private String employeeName;
        @SerializedName("UniformTypeID")
        @Expose
        private String uniformTypeID;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;

        public String getEmployeeUniformID() {
            return employeeUniformID;
        }

        public void setEmployeeUniformID(String employeeUniformID) {
            this.employeeUniformID = employeeUniformID;
        }

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getUniformDate() {
            return uniformDate;
        }

        public void setUniformDate(String uniformDate) {
            this.uniformDate = uniformDate;
        }

        public String getUniformtype() {
            return uniformtype;
        }

        public void setUniformtype(String uniformtype) {
            this.uniformtype = uniformtype;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public void setEmployeeName(String employeeName) {
            this.employeeName = employeeName;
        }

        public String getUniformTypeID() {
            return uniformTypeID;
        }

        public void setUniformTypeID(String uniformTypeID) {
            this.uniformTypeID = uniformTypeID;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

    }
}
