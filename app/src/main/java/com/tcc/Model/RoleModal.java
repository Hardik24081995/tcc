package com.tcc.Model;

import com.google.gson.annotations.SerializedName;

public class RoleModal {

    @SerializedName("data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("Attendance")
        private Attendance attendance;

        @SerializedName("Training")
        private Training training;

        @SerializedName("Payment")
        private Payment payment;

        @SerializedName("Employee")
        private Employee employee;

        @SerializedName("Invoice")
        private Invoice invoice;

        @SerializedName("Customer")
        private Customer customer;

        @SerializedName("Inspection")
        private Inspection inspection;

        @SerializedName("Tickets")
        private Tickets tickets;

        @SerializedName("Uniform")
        private Uniform uniform;

        @SerializedName("Penlty")
        private Penlty penlty;
        @SerializedName("Sites")
        private Sites sites;

        public Attendance getAttendance() {
            return attendance;
        }

        public void setAttendance(Attendance attendance) {
            this.attendance = attendance;
        }

        public Training getTraining() {
            return training;
        }

        public void setTraining(Training training) {
            this.training = training;
        }

        public Payment getPayment() {
            return payment;
        }

        public void setPayment(Payment payment) {
            this.payment = payment;
        }

        public Employee getEmployee() {
            return employee;
        }

        public void setEmployee(Employee employee) {
            this.employee = employee;
        }

        public Invoice getInvoice() {
            return invoice;
        }

        public void setInvoice(Invoice invoice) {
            this.invoice = invoice;
        }

        public Customer getCustomer() {
            return customer;
        }

        public void setCustomer(Customer customer) {
            this.customer = customer;
        }

        public Inspection getInspection() {
            return inspection;
        }

        public void setInspection(Inspection inspection) {
            this.inspection = inspection;
        }

        public Tickets getTickets() {
            return tickets;
        }

        public void setTickets(Tickets tickets) {
            this.tickets = tickets;
        }

        public Uniform getUniform() {
            return uniform;
        }

        public void setUniform(Uniform uniform) {
            this.uniform = uniform;
        }

        public Penlty getPenlty() {
            return penlty;
        }

        public void setPenlty(Penlty penlty) {
            this.penlty = penlty;
        }

        public Sites getSites() {
            return sites;
        }

        public void setSites(Sites sites) {
            this.sites = sites;
        }
    }

    public class Employee {

        @SerializedName("is_view")
        private String isView;

        @SerializedName("is_insert")
        private String isInsert;

        @SerializedName("is_edit")
        private String isEdit;

        public String getIsView() {
            return isView;
        }

        public String getIsInsert() {
            return isInsert;
        }

        public String getIsEdit() {
            return isEdit;
        }
    }
}