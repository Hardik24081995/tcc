package com.tcc.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RoleModalResponse {

    @SerializedName("data")
    private List<DataItem> data;

    @SerializedName("error")
    private int error;

    @SerializedName("message")
    private String message;

    public List<DataItem> getData() {
        return data;
    }

    public int getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public class DataItem {

        @SerializedName("ParentID")
        private String parentID;

        @SerializedName("ModuleID")
        private String moduleID;

        @SerializedName("is_status")
        private String isStatus;

        @SerializedName("RoleMapID")
        private String roleMapID;

        @SerializedName("ModuleName")
        private String moduleName;

        @SerializedName("is_view")
        private String isView;

        @SerializedName("is_insert")
        private String isInsert;

        @SerializedName("is_export")
        private String isExport;

        @SerializedName("is_edit")
        private String isEdit;

        @SerializedName("RoleID")
        private String roleID;

        public String getParentID() {
            return parentID;
        }

        public String getModuleID() {
            return moduleID;
        }

        public String getIsStatus() {
            return isStatus;
        }

        public String getRoleMapID() {
            return roleMapID;
        }

        public String getModuleName() {
            return moduleName;
        }

        public String getIsView() {
            return isView;
        }

        public String getIsInsert() {
            return isInsert;
        }

        public String getIsExport() {
            return isExport;
        }

        public String getIsEdit() {
            return isEdit;
        }

        public String getRoleID() {
            return roleID;
        }
    }
}