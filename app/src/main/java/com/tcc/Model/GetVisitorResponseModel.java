package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetVisitorResponseModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("ResponseID")
        @Expose
        private String responseID;
        @SerializedName("ReminderID")
        @Expose
        private String reminderID;
        @SerializedName("Response")
        @Expose
        private String response;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;
        @SerializedName("EmployeeName")
        @Expose
        private String EmployeeName;
        @SerializedName("ResponseDate")
        @Expose
        private String ResponseDate;

        public String getEmployeeName() {
            return EmployeeName;
        }

        public void setEmployeeName(String employeeName) {
            EmployeeName = employeeName;
        }

        public String getResponseDate() {
            return ResponseDate;
        }

        public void setResponseDate(String responseDate) {
            ResponseDate = responseDate;
        }

        public String getResponseID() {
            return responseID;
        }

        public void setResponseID(String responseID) {
            this.responseID = responseID;
        }

        public String getReminderID() {
            return reminderID;
        }

        public void setReminderID(String reminderID) {
            this.reminderID = reminderID;
        }

        public String getResponse() {
            return response;
        }

        public void setResponse(String response) {
            this.response = response;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

    }
}
