package com.tcc.Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetVisitorModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


    public class Datum {

        @SerializedName("VisitorID")
        @Expose
        private String visitorID;
        @SerializedName("UserID")
        @Expose
        private String userID;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("EmployeeName")
        @Expose
        private String employeeName;
        @SerializedName("CompanyName")
        @Expose
        private String companyName;
        @SerializedName("EmailID")
        @Expose
        private String emailID;
        @SerializedName("MobileNo")
        @Expose
        private String mobileNo;
        @SerializedName("Location")
        @Expose
        private String location;
        @SerializedName("ServiceID")
        @Expose
        private String serviceID;
        @SerializedName("WorkingDays")
        @Expose
        private String workingDays;
        @SerializedName("WorkingHours")
        @Expose
        private String workingHours;
        @SerializedName("NoOfPowerRequire")
        @Expose
        private String noOfPowerRequire;
        @SerializedName("LeadType")
        @Expose
        private String leadType;
        @SerializedName("CityID")
        @Expose
        private String cityID;
        @SerializedName("StateID")
        @Expose
        private String stateID;
        @SerializedName("IsCustomer")
        @Expose
        private String isCustomer;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Service")
        @Expose
        private String service;
        @SerializedName("ProposedDate")
        @Expose
        private String proposedDate;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;

        public String getVisitorID() {
            return visitorID;
        }

        public void setVisitorID(String visitorID) {
            this.visitorID = visitorID;
        }

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public void setEmployeeName(String employeeName) {
            this.employeeName = employeeName;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getEmailID() {
            return emailID;
        }

        public void setEmailID(String emailID) {
            this.emailID = emailID;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getServiceID() {
            return serviceID;
        }

        public void setServiceID(String serviceID) {
            this.serviceID = serviceID;
        }

        public String getWorkingDays() {
            return workingDays;
        }

        public void setWorkingDays(String workingDays) {
            this.workingDays = workingDays;
        }

        public String getWorkingHours() {
            return workingHours;
        }

        public void setWorkingHours(String workingHours) {
            this.workingHours = workingHours;
        }

        public String getNoOfPowerRequire() {
            return noOfPowerRequire;
        }

        public void setNoOfPowerRequire(String noOfPowerRequire) {
            this.noOfPowerRequire = noOfPowerRequire;
        }

        public String getLeadType() {
            return leadType;
        }

        public void setLeadType(String leadType) {
            this.leadType = leadType;
        }

        public String getCityID() {
            return cityID;
        }

        public void setCityID(String cityID) {
            this.cityID = cityID;
        }

        public String getStateID() {
            return stateID;
        }

        public void setStateID(String stateID) {
            this.stateID = stateID;
        }

        public String getIsCustomer() {
            return isCustomer;
        }

        public void setIsCustomer(String isCustomer) {
            this.isCustomer = isCustomer;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getProposedDate() {
            return proposedDate;
        }

        public void setProposedDate(String proposedDate) {
            this.proposedDate = proposedDate;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

    }
}
