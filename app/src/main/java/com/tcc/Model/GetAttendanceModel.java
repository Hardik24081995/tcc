package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetAttendanceModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("SitesID")
        @Expose
        private String sitesID;
        @SerializedName("QuotationID")
        @Expose
        private String QuotationID;
        @SerializedName("AttendanceDate")
        @Expose
        private String attendanceDate;
        @SerializedName("CompanyName")
        @Expose
        private String companyName;
        @SerializedName("CompanyAddress")
        @Expose
        private String companyAddress;
        @SerializedName("PresentCount")
        @Expose
        private String presentCount;
        @SerializedName("Absentount")
        @Expose
        private String absentount;
        @SerializedName("HalfDayount")
        @Expose
        private String halfDayount;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;

        public String getSitesID() {
            return sitesID;
        }

        public void setSitesID(String sitesID) {
            this.sitesID = sitesID;
        }

        public String getQuotationID() {
            return QuotationID;
        }

        public void setQuotationID(String quotationID) {
            QuotationID = quotationID;
        }

        public String getAttendanceDate() {
            return attendanceDate;
        }

        public void setAttendanceDate(String attendanceDate) {
            this.attendanceDate = attendanceDate;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getCompanyAddress() {
            return companyAddress;
        }

        public void setCompanyAddress(String companyAddress) {
            this.companyAddress = companyAddress;
        }

        public String getPresentCount() {
            return presentCount;
        }

        public void setPresentCount(String presentCount) {
            this.presentCount = presentCount;
        }

        public String getAbsentount() {
            return absentount;
        }

        public void setAbsentount(String absentount) {
            this.absentount = absentount;
        }

        public String getHalfDayount() {
            return halfDayount;
        }

        public void setHalfDayount(String halfDayount) {
            this.halfDayount = halfDayount;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

    }
}
