package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetInspectionListModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("InspectionID")
        @Expose
        private String inspectionID;
        @SerializedName("UserID")
        @Expose
        private String userID;
        @SerializedName("InspectionDate")
        @Expose
        private String inspectionDate;
        @SerializedName("EmployeeName")
        @Expose
        private String employeeName;
        @SerializedName("UserType")
        @Expose
        private String userType;
        @SerializedName("SitesID")
        @Expose
        private String sitesID;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("CompanyName")
        @Expose
        private String companyName;
        @SerializedName("SitesName")
        @Expose
        private String sitesName;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;
        @SerializedName("Remarks")
        @Expose
        private String Remarks;
        @SerializedName("Image")
        @Expose
        private String Image;
        @SerializedName("Item")
        @Expose
        private List<Item> item = null;

        public String getInspectionID() {
            return inspectionID;
        }

        public void setInspectionID(String inspectionID) {
            this.inspectionID = inspectionID;
        }

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getInspectionDate() {
            return inspectionDate;
        }

        public void setInspectionDate(String inspectionDate) {
            this.inspectionDate = inspectionDate;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public void setEmployeeName(String employeeName) {
            this.employeeName = employeeName;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getSitesID() {
            return sitesID;
        }

        public void setSitesID(String sitesID) {
            this.sitesID = sitesID;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getSitesName() {
            return sitesName;
        }

        public void setSitesName(String sitesName) {
            this.sitesName = sitesName;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

        public String getRemarks() {
            return Remarks;
        }

        public void setRemarks(String remarks) {
            Remarks = remarks;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String image) {
            Image = image;
        }

        public List<Item> getItem() {
            return item;
        }

        public void setItem(List<Item> item) {
            this.item = item;
        }

        public class Item {

            @SerializedName("InspectionAnswerID")
            @Expose
            private String inspectionAnswerID;
            @SerializedName("InspectionID")
            @Expose
            private String inspectionID;
            @SerializedName("QuestionID")
            @Expose
            private String questionID;
            @SerializedName("Question")
            @Expose
            private String question;
            @SerializedName("Answer")
            @Expose
            private String answer;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getInspectionAnswerID() {
                return inspectionAnswerID;
            }

            public void setInspectionAnswerID(String inspectionAnswerID) {
                this.inspectionAnswerID = inspectionAnswerID;
            }

            public String getInspectionID() {
                return inspectionID;
            }

            public void setInspectionID(String inspectionID) {
                this.inspectionID = inspectionID;
            }

            public String getQuestionID() {
                return questionID;
            }

            public void setQuestionID(String questionID) {
                this.questionID = questionID;
            }

            public String getQuestion() {
                return question;
            }

            public void setQuestion(String question) {
                this.question = question;
            }

            public String getAnswer() {
                return answer;
            }

            public void setAnswer(String answer) {
                this.answer = answer;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }

        }

    }
}
