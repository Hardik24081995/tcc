package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetServicesModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("ServiceID")
        @Expose
        private String serviceID;
        @SerializedName("Service")
        @Expose
        private String service;
        @SerializedName("Qty")
        @Expose
        private String qty;
        @SerializedName("Rate")
        @Expose
        private String rate;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;
        @SerializedName("IsFixCost")
        @Expose
        private String IsFixCost;

        public String getIsFixCost() {
            return IsFixCost;
        }

        public void setIsFixCost(String isFixCost) {
            IsFixCost = isFixCost;
        }

        public String getServiceID() {
            return serviceID;
        }

        public void setServiceID(String serviceID) {
            this.serviceID = serviceID;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

    }
}
