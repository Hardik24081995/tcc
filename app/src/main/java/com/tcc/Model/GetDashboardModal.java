package com.tcc.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetDashboardModal {

    @SerializedName("data")
    private List<Data> data;

    @SerializedName("error")
    private int error;

    @SerializedName("message")
    private String message;

    public List<Data> getData() {
        return data;
    }

    public int getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }


    public class Data {

        @SerializedName("TotalCustomer")
        private String totalCustomer;

        @SerializedName("TotalVisitor")
        private String totalVisitor;

        @SerializedName("TotalCollection")
        private String totalCollection;

        @SerializedName("TotalActiveSite")
        private String totalActiveSite;

        @SerializedName("VisitorReminder")
        private String VisitorReminder;

        public String getVisitorReminder() {
            return VisitorReminder;
        }

        public void setVisitorReminder(String visitorReminder) {
            VisitorReminder = visitorReminder;
        }

        public String getTotalCustomer() {
            return totalCustomer;
        }

        public String getTotalVisitor() {
            return totalVisitor;
        }

        public String getTotalCollection() {
            return totalCollection;
        }

        public String getTotalActiveSite() {
            return totalActiveSite;
        }
    }
}