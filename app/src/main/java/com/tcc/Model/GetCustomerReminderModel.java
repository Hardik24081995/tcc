package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetCustomerReminderModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("CustomerPaymentID")
        @Expose
        private String customerPaymentID;
        @SerializedName("SitesID")
        @Expose
        private String sitesID;
        @SerializedName("PastDate")
        @Expose
        private String pastDate;
        @SerializedName("ReminderDate")
        @Expose
        private String reminderDate;
        @SerializedName("ReminderBy")
        @Expose
        private String reminderBy;
        @SerializedName("Message")
        @Expose
        private String message;
        @SerializedName("Amount")
        @Expose
        private String amount;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("EmployeeName")
        @Expose
        private String employeeName;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;
        @SerializedName("MobileNo")
        @Expose
        private String MobileNo;
        @SerializedName("EmailID")
        @Expose
        private String EmailID;
        @SerializedName("QuotationID")
        @Expose
        private String QuotationID;

        public String getQuotationID() {
            return QuotationID;
        }

        public void setQuotationID(String quotationID) {
            QuotationID = quotationID;
        }

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String mobileNo) {
            MobileNo = mobileNo;
        }

        public String getEmailID() {
            return EmailID;
        }

        public void setEmailID(String emailID) {
            EmailID = emailID;
        }

        public String getCustomerPaymentID() {
            return customerPaymentID;
        }

        public void setCustomerPaymentID(String customerPaymentID) {
            this.customerPaymentID = customerPaymentID;
        }

        public String getSitesID() {
            return sitesID;
        }

        public void setSitesID(String sitesID) {
            this.sitesID = sitesID;
        }

        public String getPastDate() {
            return pastDate;
        }

        public void setPastDate(String pastDate) {
            this.pastDate = pastDate;
        }

        public String getReminderDate() {
            return reminderDate;
        }

        public void setReminderDate(String reminderDate) {
            this.reminderDate = reminderDate;
        }

        public String getReminderBy() {
            return reminderBy;
        }

        public void setReminderBy(String reminderBy) {
            this.reminderBy = reminderBy;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public void setEmployeeName(String employeeName) {
            this.employeeName = employeeName;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

    }
}
