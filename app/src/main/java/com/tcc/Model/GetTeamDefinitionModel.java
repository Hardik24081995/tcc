package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetTeamDefinitionModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("TeamDefinitionID")
        @Expose
        private String teamDefinitionID;
        @SerializedName("SitesID")
        @Expose
        private String sitesID;
        @SerializedName("UserID")
        @Expose
        private String userID;
        @SerializedName("EmployeeName")
        @Expose
        private String employeeName;
        @SerializedName("UserType")
        @Expose
        private String userType;
        @SerializedName("StartDate")
        @Expose
        private String startDate;
        @SerializedName("EndDate")
        @Expose
        private String endDate;
        @SerializedName("FromSitesID")
        @Expose
        private String fromSitesID;
        @SerializedName("ToSitesID")
        @Expose
        private String toSitesID;
        @SerializedName("Type")
        @Expose
        private String type;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;
        @SerializedName("Designation")
        @Expose
        private String Designation;

        public String getDesignation() {
            return Designation;
        }

        public void setDesignation(String designation) {
            Designation = designation;
        }

        public String getTeamDefinitionID() {
            return teamDefinitionID;
        }

        public void setTeamDefinitionID(String teamDefinitionID) {
            this.teamDefinitionID = teamDefinitionID;
        }

        public String getSitesID() {
            return sitesID;
        }

        public void setSitesID(String sitesID) {
            this.sitesID = sitesID;
        }

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public void setEmployeeName(String employeeName) {
            this.employeeName = employeeName;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getFromSitesID() {
            return fromSitesID;
        }

        public void setFromSitesID(String fromSitesID) {
            this.fromSitesID = fromSitesID;
        }

        public String getToSitesID() {
            return toSitesID;
        }

        public void setToSitesID(String toSitesID) {
            this.toSitesID = toSitesID;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

    }
}
