package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConfigModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("ConfigID")
        @Expose
        private String configID;
        @SerializedName("SupportEmail")
        @Expose
        private String supportEmail;
        @SerializedName("TimeZone")
        @Expose
        private String timeZone;
        @SerializedName("MailFromName")
        @Expose
        private String mailFromName;
        @SerializedName("AppVersionIOS")
        @Expose
        private String appVersionIOS;
        @SerializedName("AppVersionAndroid")
        @Expose
        private String appVersionAndroid;
        @SerializedName("LeadType")
        @Expose
        private String leadType;
        @SerializedName("SGST")
        @Expose
        private String SGST;
        @SerializedName("CGST")
        @Expose
        private String CGST;

        @SerializedName("Online")
        @Expose
        private String Online;

        public String getOnline() {
            return Online;
        }

        public void setOnline(String online) {
            Online = online;
        }

        public String getSGST() {
            return SGST;
        }

        public void setSGST(String SGST) {
            this.SGST = SGST;
        }

        public String getCGST() {
            return CGST;
        }

        public void setCGST(String CGST) {
            this.CGST = CGST;
        }

        public String getConfigID() {
            return configID;
        }

        public void setConfigID(String configID) {
            this.configID = configID;
        }

        public String getSupportEmail() {
            return supportEmail;
        }

        public void setSupportEmail(String supportEmail) {
            this.supportEmail = supportEmail;
        }

        public String getTimeZone() {
            return timeZone;
        }

        public void setTimeZone(String timeZone) {
            this.timeZone = timeZone;
        }

        public String getMailFromName() {
            return mailFromName;
        }

        public void setMailFromName(String mailFromName) {
            this.mailFromName = mailFromName;
        }

        public String getAppVersionIOS() {
            return appVersionIOS;
        }

        public void setAppVersionIOS(String appVersionIOS) {
            this.appVersionIOS = appVersionIOS;
        }

        public String getAppVersionAndroid() {
            return appVersionAndroid;
        }

        public void setAppVersionAndroid(String appVersionAndroid) {
            this.appVersionAndroid = appVersionAndroid;
        }

        public String getLeadType() {
            return leadType;
        }

        public void setLeadType(String leadType) {
            this.leadType = leadType;
        }

    }
}
