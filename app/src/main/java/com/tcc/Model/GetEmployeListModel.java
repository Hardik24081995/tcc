package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetEmployeListModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose

    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public static class Datum {

        @SerializedName("UserID")
        @Expose
        private String userID;
        @SerializedName("EmailID")
        @Expose
        private String emailID;
        @SerializedName("MobileNo")
        @Expose
        private String mobileNo;
        @SerializedName("Address")
        @Expose
        private String address;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;
        @SerializedName("RoleID")
        @Expose
        private String roleID;
        @SerializedName("FirstName")
        @Expose
        private String firstName;
        @SerializedName("LastName")
        @Expose
        private String lastName;
        @SerializedName("Usertype")
        @Expose
        private String Usertype;
        @SerializedName("IsDeleted")
        @Expose
        private String isDeleted;
        @SerializedName("Salary")
        @Expose
        private String salary;
        @SerializedName("UsertypeID")
        @Expose
        private String UsertypeID;
        @SerializedName("WorkingHours")
        @Expose
        private String workingHours;
        @SerializedName("Documents")
        @Expose
        private String documents;
        @SerializedName("OfferLetter")
        @Expose
        private String offerLetter;
        @SerializedName("BankName")
        @Expose
        private String bankName;
        @SerializedName("BranchName")
        @Expose
        private String branchName;
        @SerializedName("AccountNo")
        @Expose
        private String accountNo;
        @SerializedName("IFSCCode")
        @Expose
        private String iFSCCode;

        public String getUsertypeID() {
            return UsertypeID;
        }

        public void setUsertypeID(String usertypeID) {
            UsertypeID = usertypeID;
        }

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getEmailID() {
            return emailID;
        }

        public void setEmailID(String emailID) {
            this.emailID = emailID;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

        public String getRoleID() {
            return roleID;
        }

        public void setRoleID(String roleID) {
            this.roleID = roleID;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getUserType() {
            return Usertype;
        }

        public void setUserType(String userType) {
            this.Usertype = userType;
        }

        public String getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(String isDeleted) {
            this.isDeleted = isDeleted;
        }

        public String getSalary() {
            return salary;
        }

        public void setSalary(String salary) {
            this.salary = salary;
        }

        public String getWorkingHours() {
            return workingHours;
        }

        public void setWorkingHours(String workingHours) {
            this.workingHours = workingHours;
        }

        public String getDocuments() {
            return documents;
        }

        public void setDocuments(String documents) {
            this.documents = documents;
        }

        public String getOfferLetter() {
            return offerLetter;
        }

        public void setOfferLetter(String offerLetter) {
            this.offerLetter = offerLetter;
        }

        public String getBankName() {
            return bankName;
        }

        public void setBankName(String bankName) {
            this.bankName = bankName;
        }

        public String getBranchName() {
            return branchName;
        }

        public void setBranchName(String branchName) {
            this.branchName = branchName;
        }

        public String getAccountNo() {
            return accountNo;
        }

        public void setAccountNo(String accountNo) {
            this.accountNo = accountNo;
        }

        public String getiFSCCode() {
            return iFSCCode;
        }

        public void setiFSCCode(String iFSCCode) {
            this.iFSCCode = iFSCCode;
        }
    }
}
