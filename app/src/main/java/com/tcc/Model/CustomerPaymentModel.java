package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerPaymentModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


    public class Datum {

        @SerializedName("CustomerPaymentID")
        @Expose
        private String customerPaymentID;
        @SerializedName("SitesID")
        @Expose
        private String sitesID;
        @SerializedName("PaymentAmount")
        @Expose
        private String paymentAmount;
        @SerializedName("GSTAmount")
        @Expose
        private String gSTAmount;
        @SerializedName("AmountType")
        @Expose
        private String amountType;
        @SerializedName("PaymentMode")
        @Expose
        private String paymentMode;
        @SerializedName("ChequeNo")
        @Expose
        private String chequeNo;
        @SerializedName("IFCCode")
        @Expose
        private String iFCCode;
        @SerializedName("UTR")
        @Expose
        private String uTR;
        @SerializedName("AccountNo")
        @Expose
        private String accountNo;
        @SerializedName("BankName")
        @Expose
        private String bankName;
        @SerializedName("BranchName")
        @Expose
        private String branchName;
        @SerializedName("PaymentDate")
        @Expose
        private String paymentDate;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;
        @SerializedName("QuotationID")
        @Expose
        private String QuotationID;
        @SerializedName("RemainingAmount")
        @Expose
        private String RemainingAmount;
        @SerializedName("RemainingGSTAmount")
        @Expose
        private String RemainingGSTAmount;

        public String getRemainingAmount() {
            return RemainingAmount;
        }

        public void setRemainingAmount(String remainingAmount) {
            RemainingAmount = remainingAmount;
        }

        public String getRemainingGSTAmount() {
            return RemainingGSTAmount;
        }

        public void setRemainingGSTAmount(String remainingGSTAmount) {
            RemainingGSTAmount = remainingGSTAmount;
        }

        public String getQuotationID() {
            return QuotationID;
        }

        public void setQuotationID(String quotationID) {
            QuotationID = quotationID;
        }

        public String getCustomerPaymentID() {
            return customerPaymentID;
        }

        public void setCustomerPaymentID(String customerPaymentID) {
            this.customerPaymentID = customerPaymentID;
        }

        public String getSitesID() {
            return sitesID;
        }

        public void setSitesID(String sitesID) {
            this.sitesID = sitesID;
        }

        public String getPaymentAmount() {
            return paymentAmount;
        }

        public void setPaymentAmount(String paymentAmount) {
            this.paymentAmount = paymentAmount;
        }

        public String getGSTAmount() {
            return gSTAmount;
        }

        public void setGSTAmount(String gSTAmount) {
            this.gSTAmount = gSTAmount;
        }

        public String getAmountType() {
            return amountType;
        }

        public void setAmountType(String amountType) {
            this.amountType = amountType;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getChequeNo() {
            return chequeNo;
        }

        public void setChequeNo(String chequeNo) {
            this.chequeNo = chequeNo;
        }

        public String getIFCCode() {
            return iFCCode;
        }

        public void setIFCCode(String iFCCode) {
            this.iFCCode = iFCCode;
        }

        public String getUTR() {
            return uTR;
        }

        public void setUTR(String uTR) {
            this.uTR = uTR;
        }

        public String getAccountNo() {
            return accountNo;
        }

        public void setAccountNo(String accountNo) {
            this.accountNo = accountNo;
        }

        public String getBankName() {
            return bankName;
        }

        public void setBankName(String bankName) {
            this.bankName = bankName;
        }

        public String getBranchName() {
            return branchName;
        }

        public void setBranchName(String branchName) {
            this.branchName = branchName;
        }

        public String getPaymentDate() {
            return paymentDate;
        }

        public void setPaymentDate(String paymentDate) {
            this.paymentDate = paymentDate;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

    }
}
