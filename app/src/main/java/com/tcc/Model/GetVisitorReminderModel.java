package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetVisitorReminderModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("VisitorReminderID")
        @Expose
        private String visitorReminderID;
        @SerializedName("VisitorID")
        @Expose
        private String visitorID;
        @SerializedName("PastDate")
        @Expose
        private String pastDate;
        @SerializedName("ReminderDate")
        @Expose
        private String reminderDate;
        @SerializedName("ReminderBy")
        @Expose
        private String reminderBy;
        @SerializedName("Message")
        @Expose
        private String message;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("EmployeeFirstName")
        @Expose
        private String EmployeeFirstName;

        @SerializedName("EmployeeLastName")
        @Expose
        private String EmployeeLastName;

        @SerializedName("EmployeeName")
        @Expose
        private String EmployeeName;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;

        public String getEmployeeName() {
            return EmployeeName;
        }

        public void setEmployeeName(String employeeName) {
            EmployeeName = employeeName;
        }

        public String getEmployeeFirstName() {
            return EmployeeFirstName;
        }

        public void setEmployeeFirstName(String employeeFirstName) {
            EmployeeFirstName = employeeFirstName;
        }

        public String getEmployeeLastName() {
            return EmployeeLastName;
        }

        public void setEmployeeLastName(String employeeLastName) {
            EmployeeLastName = employeeLastName;
        }

        public String getVisitorReminderID() {
            return visitorReminderID;
        }

        public void setVisitorReminderID(String visitorReminderID) {
            this.visitorReminderID = visitorReminderID;
        }

        public String getVisitorID() {
            return visitorID;
        }

        public void setVisitorID(String visitorID) {
            this.visitorID = visitorID;
        }

        public String getPastDate() {
            return pastDate;
        }

        public void setPastDate(String pastDate) {
            this.pastDate = pastDate;
        }

        public String getReminderDate() {
            return reminderDate;
        }

        public void setReminderDate(String reminderDate) {
            this.reminderDate = reminderDate;
        }

        public String getReminderBy() {
            return reminderBy;
        }

        public void setReminderBy(String reminderBy) {
            this.reminderBy = reminderBy;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }


        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

    }

}
