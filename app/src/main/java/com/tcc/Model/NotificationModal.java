package com.tcc.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationModal {

    @SerializedName("data")
    private List<DataItem> data;

    @SerializedName("error")
    private int error;

    @SerializedName("message")
    private String message;

    public List<DataItem> getData() {
        return data;
    }

    public int getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public class DataItem {

        @SerializedName("ActionID")
        private String actionID;

        @SerializedName("Rno")
        private String rno;

        @SerializedName("ActionType")
        private String actionType;

        @SerializedName("rowcount")
        private String rowcount;

        @SerializedName("CreatedDate")
        private String createdDate;

        @SerializedName("CustomerName")
        private String customerName;

        @SerializedName("Discription")
        private String discription;

        @SerializedName("CustomerType")
        private String customerType;

        @SerializedName("CustomerProcessID")
        private String customerProcessID;

        public String getActionID() {
            return actionID;
        }

        public String getRno() {
            return rno;
        }

        public String getActionType() {
            return actionType;
        }

        public String getRowcount() {
            return rowcount;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public String getCustomerName() {
            return customerName;
        }

        public String getDiscription() {
            return discription;
        }

        public String getCustomerType() {
            return customerType;
        }

        public String getCustomerProcessID() {
            return customerProcessID;
        }
    }
}