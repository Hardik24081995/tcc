package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetTeamEmployeeModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("TeamDefinitionID")
        @Expose
        private String teamDefinitionID;
        @SerializedName("SitesID")
        @Expose
        private String sitesID;
        @SerializedName("UserID")
        @Expose
        private String userID;
        @SerializedName("Attendance")
        @Expose
        private String attendance;
        @SerializedName("EmployeeName")
        @Expose
        private String employeeName;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;
        @SerializedName("MobileNo")
        @Expose
        private String mobileNo;
        @SerializedName("UsertypeID")
        @Expose
        private String UsertypeID;
        @SerializedName("Usertype")
        @Expose
        private String Usertype;
        @SerializedName("StartDate")
        @Expose
        private String StartDate;
        @SerializedName("EndDate")
        @Expose
        private String EndDate;
        @SerializedName("FromSitesID")
        @Expose
        private String FromSitesID;
        @SerializedName("ToSitesID")
        @Expose
        private String ToSitesID;
        @SerializedName("Type")
        @Expose
        private String Type;

        public String getTeamDefinitionID() {
            return teamDefinitionID;
        }

        public void setTeamDefinitionID(String teamDefinitionID) {
            this.teamDefinitionID = teamDefinitionID;
        }

        public String getSitesID() {
            return sitesID;
        }

        public void setSitesID(String sitesID) {
            this.sitesID = sitesID;
        }

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getAttendance() {
            return attendance;
        }

        public void setAttendance(String attendance) {
            this.attendance = attendance;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public void setEmployeeName(String employeeName) {
            this.employeeName = employeeName;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public String getUsertypeID() {
            return UsertypeID;
        }

        public void setUsertypeID(String usertypeID) {
            UsertypeID = usertypeID;
        }

        public String getUsertype() {
            return Usertype;
        }

        public void setUsertype(String usertype) {
            Usertype = usertype;
        }

        public String getStartDate() {
            return StartDate;
        }

        public void setStartDate(String startDate) {
            StartDate = startDate;
        }

        public String getEndDate() {
            return EndDate;
        }

        public void setEndDate(String endDate) {
            EndDate = endDate;
        }

        public String getFromSitesID() {
            return FromSitesID;
        }

        public void setFromSitesID(String fromSitesID) {
            FromSitesID = fromSitesID;
        }

        public String getToSitesID() {
            return ToSitesID;
        }

        public void setToSitesID(String toSitesID) {
            ToSitesID = toSitesID;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }
    }
}
