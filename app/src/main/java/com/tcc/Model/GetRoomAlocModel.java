package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetRoomAlocModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("EmployeeRoomID")
        @Expose
        private String employeeRoomID;
        @SerializedName("UserID")
        @Expose
        private String userID;
        @SerializedName("StartDate")
        @Expose
        private String startDate;
        @SerializedName("EndDate")
        @Expose
        private String endDate;
        @SerializedName("EmployeeName")
        @Expose
        private String employeeName;
        @SerializedName("RoomNo")
        @Expose
        private String roomNo;
        @SerializedName("RoomAddress")
        @Expose
        private String roomAddress;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;

        public String getEmployeeRoomID() {
            return employeeRoomID;
        }

        public void setEmployeeRoomID(String employeeRoomID) {
            this.employeeRoomID = employeeRoomID;
        }

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public void setEmployeeName(String employeeName) {
            this.employeeName = employeeName;
        }

        public String getRoomNo() {
            return roomNo;
        }

        public void setRoomNo(String roomNo) {
            this.roomNo = roomNo;
        }

        public String getRoomAddress() {
            return roomAddress;
        }

        public void setRoomAddress(String roomAddress) {
            this.roomAddress = roomAddress;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

    }
}
