package com.tcc.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReportUniformModal {

    @SerializedName("data")
    private List<DataItem> data;

    @SerializedName("error")
    private int error;

    @SerializedName("message")
    private String message;

    public List<DataItem> getData() {
        return data;
    }

    public int getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public class DataItem {

        @SerializedName("Status")
        private String status;

        @SerializedName("UniformDate")
        private String uniformDate;

        @SerializedName("Rno")
        private String rno;

        @SerializedName("Uniformtype")
        private String uniformtype;

        @SerializedName("rowcount")
        private String rowcount;

        @SerializedName("EmployeeUniformID")
        private String employeeUniformID;

        @SerializedName("UserID")
        private String userID;

        @SerializedName("EmployeeName")
        private String employeeName;

        @SerializedName("UniformTypeID")
        private String uniformTypeID;

        public String getStatus() {
            return status;
        }

        public String getUniformDate() {
            return uniformDate;
        }

        public String getRno() {
            return rno;
        }

        public String getUniformtype() {
            return uniformtype;
        }

        public String getRowcount() {
            return rowcount;
        }

        public String getEmployeeUniformID() {
            return employeeUniformID;
        }

        public String getUserID() {
            return userID;
        }

        public String getEmployeeName() {
            return employeeName;
        }

        public String getUniformTypeID() {
            return uniformTypeID;
        }
    }
}