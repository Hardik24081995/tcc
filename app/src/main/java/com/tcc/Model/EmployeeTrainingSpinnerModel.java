package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EmployeeTrainingSpinnerModel {


    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("TrainingDateTimeID")
        @Expose
        private String trainingDateTimeID;
        @SerializedName("TrainingID")
        @Expose
        private String trainingID;
        @SerializedName("Training")
        @Expose
        private String training;
        @SerializedName("TrainingDate")
        @Expose
        private String trainingDate;
        @SerializedName("TrainingTime")
        @Expose
        private String trainingTime;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;

        public String getTrainingDateTimeID() {
            return trainingDateTimeID;
        }

        public void setTrainingDateTimeID(String trainingDateTimeID) {
            this.trainingDateTimeID = trainingDateTimeID;
        }

        public String getTrainingID() {
            return trainingID;
        }

        public void setTrainingID(String trainingID) {
            this.trainingID = trainingID;
        }

        public String getTraining() {
            return training;
        }

        public void setTraining(String training) {
            this.training = training;
        }

        public String getTrainingDate() {
            return trainingDate;
        }

        public void setTrainingDate(String trainingDate) {
            this.trainingDate = trainingDate;
        }

        public String getTrainingTime() {
            return trainingTime;
        }

        public void setTrainingTime(String trainingTime) {
            this.trainingTime = trainingTime;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

    }
}
