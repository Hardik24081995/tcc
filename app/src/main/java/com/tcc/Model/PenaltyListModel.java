package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PenaltyListModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("PenaltyID")
        @Expose
        private String penaltyID;
        @SerializedName("PenaltyDate")
        @Expose
        private String penaltyDate;
        @SerializedName("SitesID")
        @Expose
        private String sitesID;
        @SerializedName("Reason")
        @Expose
        private String reason;
        @SerializedName("Penalty")
        @Expose
        private String penalty;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("SitesName")
        @Expose
        private String sitesName;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;
        @SerializedName("Item")
        @Expose
        private List<Item> item = null;

        public String getPenaltyID() {
            return penaltyID;
        }

        public void setPenaltyID(String penaltyID) {
            this.penaltyID = penaltyID;
        }

        public String getPenaltyDate() {
            return penaltyDate;
        }

        public void setPenaltyDate(String penaltyDate) {
            this.penaltyDate = penaltyDate;
        }

        public String getSitesID() {
            return sitesID;
        }

        public void setSitesID(String sitesID) {
            this.sitesID = sitesID;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getPenalty() {
            return penalty;
        }

        public void setPenalty(String penalty) {
            this.penalty = penalty;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getSitesName() {
            return sitesName;
        }

        public void setSitesName(String sitesName) {
            this.sitesName = sitesName;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

        public List<Item> getItem() {
            return item;
        }

        public void setItem(List<Item> item) {
            this.item = item;
        }

        public class Item {

            @SerializedName("PenaltyEmployeeID")
            @Expose
            private String penaltyEmployeeID;
            @SerializedName("UserID")
            @Expose
            private String userID;
            @SerializedName("EmployeeName")
            @Expose
            private String employeeName;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Penalty")
            @Expose
            private String Penalty;

            public String getPenalty() {
                return Penalty;
            }

            public void setPenalty(String penalty) {
                Penalty = penalty;
            }

            public String getPenaltyEmployeeID() {
                return penaltyEmployeeID;
            }

            public void setPenaltyEmployeeID(String penaltyEmployeeID) {
                this.penaltyEmployeeID = penaltyEmployeeID;
            }

            public String getUserID() {
                return userID;
            }

            public void setUserID(String userID) {
                this.userID = userID;
            }

            public String getEmployeeName() {
                return employeeName;
            }

            public void setEmployeeName(String employeeName) {
                this.employeeName = employeeName;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

        }

    }
}
