package com.tcc.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CollectionModal {

    @SerializedName("data")
    private List<DataItem> data;

    @SerializedName("error")
    private int error;

    @SerializedName("message")
    private String message;

    public List<DataItem> getData() {
        return data;
    }

    public int getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public class DataItem {

        @SerializedName("Rno")
        private String rno;

        @SerializedName("rowcount")
        private String rowcount;

        @SerializedName("ServiceName")
        private String serviceName;

        @SerializedName("GSTAmount")
        private String gSTAmount;

        @SerializedName("TotalAmount")
        private String totalAmount;

        public String getRno() {
            return rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public String getServiceName() {
            return serviceName;
        }

        public String getGSTAmount() {
            return gSTAmount;
        }

        public String getTotalAmount() {
            return totalAmount;
        }
    }
}