package com.tcc.Model;

import com.google.gson.annotations.SerializedName;

public class Uniform {

    @SerializedName("is_view")
    private String isView;

    @SerializedName("is_insert")
    private String isInsert;

    @SerializedName("is_edit")
    private String isEdit;

    public String getIsView() {
        return isView;
    }

    public String getIsInsert() {
        return isInsert;
    }

    public String getIsEdit() {
        return isEdit;
    }
}