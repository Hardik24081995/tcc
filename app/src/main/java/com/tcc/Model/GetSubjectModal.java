package com.tcc.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetSubjectModal {

    @SerializedName("data")
    private List<DataItem> data;

    @SerializedName("error")
    private int error;

    @SerializedName("message")
    private String message;

    public List<DataItem> getData() {
        return data;
    }

    public int getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public class DataItem {

        @SerializedName("Status")
        private String status;

        @SerializedName("Rno")
        private String rno;

        @SerializedName("rowcount")
        private String rowcount;

        @SerializedName("SubjectID")
        private String subjectID;

        @SerializedName("Subject")
        private String subject;

        public String getStatus() {
            return status;
        }

        public String getRno() {
            return rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public String getSubjectID() {
            return subjectID;
        }

        public String getSubject() {
            return subject;
        }
    }
}