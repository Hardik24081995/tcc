package com.tcc.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetQuotationListModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("QuotationID")
        @Expose
        private String quotationID;
        @SerializedName("CustomerID")
        @Expose
        private String customerID;
        @SerializedName("VisitorID")
        @Expose
        private String visitorID;
        @SerializedName("EstimateNo")
        @Expose
        private String estimateNo;
        @SerializedName("QuotationName")
        @Expose
        private String quotationName;
        @SerializedName("CompanyName")
        @Expose
        private String companyName;
        @SerializedName("Address")
        @Expose
        private String address;
        @SerializedName("GSTNo")
        @Expose
        private String gSTNo;
        @SerializedName("IsGST")
        @Expose
        private String isGST;
        @SerializedName("TotalAmount")
        @Expose
        private String totalAmount;
        @SerializedName("GSTAmount")
        @Expose
        private String gSTAmount;
        @SerializedName("CGST")
        @Expose
        private String cGST;
        @SerializedName("SGST")
        @Expose
        private String sGST;
        @SerializedName("ServiceID")
        @Expose
        private String serviceID;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("VisitorName")
        @Expose
        private String visitorName;
        @SerializedName("CustomerName")
        @Expose
        private String customerName;
        @SerializedName("ServiceName")
        @Expose
        private String serviceName;
        @SerializedName("Document")
        @Expose
        private String Document;
        @SerializedName("EstimateDate")
        @Expose
        private String estimateDate;
        @SerializedName("StartDate")
        @Expose
        private String startDate;
        @SerializedName("EndDate")
        @Expose
        private String endDate;
        @SerializedName("SitesID")
        @Expose
        private String sitesID;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;
        @SerializedName("QuotationStatus")
        @Expose
        private String QuotationStatus;
        @SerializedName("ReasonID")
        @Expose
        private String ReasonID;
        @SerializedName("Reason")
        @Expose
        private String Reason;
        @SerializedName("IsVisit")
        @Expose
        private String IsVisit;
        @SerializedName("TotalEmployee")
        @Expose
        private String TotalEmployee;
        @SerializedName("FieldOperator")
        @Expose
        private String FieldOperator;
        @SerializedName("QualityManager")
        @Expose
        private String QualityManager;
        @SerializedName("OperationManager")
        @Expose
        private String OperationManager;
        @SerializedName("Item")
        @Expose
        private List<Item> item = null;

        public String getQuotationStatus() {
            return QuotationStatus;
        }

        public void setQuotationStatus(String quotationStatus) {
            QuotationStatus = quotationStatus;
        }

        public String getReasonID() {
            return ReasonID;
        }

        public void setReasonID(String reasonID) {
            ReasonID = reasonID;
        }

        public String getReason() {
            return Reason;
        }

        public void setReason(String reason) {
            Reason = reason;
        }

        public String getQuotationID() {
            return quotationID;
        }

        public void setQuotationID(String quotationID) {
            this.quotationID = quotationID;
        }

        public String getCustomerID() {
            return customerID;
        }

        public void setCustomerID(String customerID) {
            this.customerID = customerID;
        }

        public String getVisitorID() {
            return visitorID;
        }

        public void setVisitorID(String visitorID) {
            this.visitorID = visitorID;
        }

        public String getEstimateNo() {
            return estimateNo;
        }

        public void setEstimateNo(String estimateNo) {
            this.estimateNo = estimateNo;
        }

        public String getQuotationName() {
            return quotationName;
        }

        public void setQuotationName(String quotationName) {
            this.quotationName = quotationName;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getGSTNo() {
            return gSTNo;
        }

        public void setGSTNo(String gSTNo) {
            this.gSTNo = gSTNo;
        }

        public String getIsGST() {
            return isGST;
        }

        public void setIsGST(String isGST) {
            this.isGST = isGST;
        }

        public String getDocument() {
            return Document;
        }

        public void setDocument(String document) {
            Document = document;
        }

        public String getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(String totalAmount) {
            this.totalAmount = totalAmount;
        }

        public String getGSTAmount() {
            return gSTAmount;
        }

        public void setGSTAmount(String gSTAmount) {
            this.gSTAmount = gSTAmount;
        }

        public String getCGST() {
            return cGST;
        }

        public void setCGST(String cGST) {
            this.cGST = cGST;
        }

        public String getSGST() {
            return sGST;
        }

        public void setSGST(String sGST) {
            this.sGST = sGST;
        }

        public String getServiceID() {
            return serviceID;
        }

        public void setServiceID(String serviceID) {
            this.serviceID = serviceID;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getVisitorName() {
            return visitorName;
        }

        public void setVisitorName(String visitorName) {
            this.visitorName = visitorName;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getServiceName() {
            return serviceName;
        }

        public void setServiceName(String serviceName) {
            this.serviceName = serviceName;
        }

        public String getEstimateDate() {
            return estimateDate;
        }

        public void setEstimateDate(String estimateDate) {
            this.estimateDate = estimateDate;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getSitesID() {
            return sitesID;
        }

        public void setSitesID(String sitesID) {
            this.sitesID = sitesID;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

        public String getIsVisit() {
            return IsVisit;
        }

        public void setIsVisit(String isVisit) {
            IsVisit = isVisit;
        }

        public String getTotalEmployee() {
            return TotalEmployee;
        }

        public void setTotalEmployee(String totalEmployee) {
            TotalEmployee = totalEmployee;
        }

        public String getFieldOperator() {
            return FieldOperator;
        }

        public void setFieldOperator(String fieldOperator) {
            FieldOperator = fieldOperator;
        }

        public String getQualityManager() {
            return QualityManager;
        }

        public void setQualityManager(String qualityManager) {
            QualityManager = qualityManager;
        }

        public String getOperationManager() {
            return OperationManager;
        }

        public void setOperationManager(String operationManager) {
            OperationManager = operationManager;
        }

        public List<Item> getItem() {
            return item;
        }

        public void setItem(List<Item> item) {
            this.item = item;
        }

        public class Item {

            @SerializedName("QuotationitemID")
            @Expose
            private String quotationitemID;
            @SerializedName("QuotationID")
            @Expose
            private String quotationID;
            @SerializedName("UsertypeID")
            @Expose
            private String usertypeID;
            @SerializedName("Description")
            @Expose
            private String description;
            @SerializedName("TotalNo")
            @Expose
            private String totalNo;
            @SerializedName("Qty")
            @Expose
            private String qty;
            @SerializedName("Rate")
            @Expose
            private String rate;
            @SerializedName("HSN_SAC")
            @Expose
            private String hSNSAC;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Usertype")
            @Expose
            private String usertype;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getQuotationitemID() {
                return quotationitemID;
            }

            public void setQuotationitemID(String quotationitemID) {
                this.quotationitemID = quotationitemID;
            }

            public String getQuotationID() {
                return quotationID;
            }

            public void setQuotationID(String quotationID) {
                this.quotationID = quotationID;
            }

            public String getUsertypeID() {
                return usertypeID;
            }

            public void setUsertypeID(String usertypeID) {
                this.usertypeID = usertypeID;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getTotalNo() {
                return totalNo;
            }

            public void setTotalNo(String totalNo) {
                this.totalNo = totalNo;
            }

            public String getQty() {
                return qty;
            }

            public void setQty(String qty) {
                this.qty = qty;
            }

            public String getRate() {
                return rate;
            }

            public void setRate(String rate) {
                this.rate = rate;
            }

            public String getHSNSAC() {
                return hSNSAC;
            }

            public void setHSNSAC(String hSNSAC) {
                this.hSNSAC = hSNSAC;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getUsertype() {
                return usertype;
            }

            public void setUsertype(String usertype) {
                this.usertype = usertype;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }

        }

    }
}
