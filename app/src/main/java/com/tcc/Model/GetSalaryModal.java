package com.tcc.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetSalaryModal {

    @SerializedName("data")
    private List<Datum> data;

    @SerializedName("error")
    private int error;

    @SerializedName("message")
    private String message;

    public List<Datum> getData() {
        return data;
    }

    public int getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public class Datum {

        @SerializedName("Status")
        private String status;

        @SerializedName("Rno")
        private String rno;

        @SerializedName("Description")
        private String description;

        @SerializedName("rowcount")
        private String rowcount;

        @SerializedName("EmployeepaymentID")
        private String employeepaymentID;

        @SerializedName("PaymentDate")
        private String paymentDate;

        @SerializedName("UserID")
        private String userID;

        @SerializedName("Amount")
        private String amount;

        @SerializedName("EmployeeName")
        private String employeeName;

        public String getStatus() {
            return status;
        }

        public String getRno() {
            return rno;
        }

        public String getDescription() {
            return description;
        }

        public String getRowcount() {
            return rowcount;
        }

        public String getEmployeepaymentID() {
            return employeepaymentID;
        }

        public String getPaymentDate() {
            return paymentDate;
        }

        public String getUserID() {
            return userID;
        }

        public String getAmount() {
            return amount;
        }

        public String getEmployeeName() {
            return employeeName;
        }
    }
}