package com.tcc.utils;

public class AppConstants {

    //Site Prefs
    public static final String COMPANY_NAME = "company_name";
    public static final String EMPLOYEE_NAME = "employee_name";
    public static final String SITE_NAME = "SiteName";
    public static final String SITE_ID = "siteID";
    public static final String ATTENDANCE_DATE = "Attendancedate";
    public static final String CLIENT_NAME = "ClientName";
    public static final String START_DATE = "Startdate";
    public static final String END_DATE = "EndDate";
    public static final String TEAM_COUNT = "teamcount";
    public static final String FIELD_OPERATOR = "fieldoperator";
    public static final String QUALITY_MANAGER = "qualitymanager";
    public static final String OPERATION_MANAGER = "operationmanager";
    public static final String LOCATION = "Location";
    public static final String GST_NO = "GSTNumber";
    public static final String AMOUNT = "amount";
    public static final String GST_AMOUNT = "gst_amunt";
    public static final String EMPLOYEE_ID = "EmployeeID";
    public static final String EMPLOYEE_ID_FOR_ADD = "employeeID";
    public static final String TOTAL_REMAINING_AMOUNT = "totalremainingamount";
    public static final String TOTAL_REMAINING_GST = "totalremaininggstamount";
    public static final String ADDRESS = "address";
    public static final String CUSTOMER_ID = "c_id";

    public static final String QUOTATION_ID = "QuotationID";
    public static final String TYPO_FRAGS_CHANGE = "null";
    public static final String WELCOME_FLAG = "welcome";

    public static final int INT_SPLASH_TIMEOUT = 3000;
    public static final int API_SUCCESS_ERROR = 200;
    public static final int TIME_INTERVAL = 2000;
    public static final String STR_TITLE = "Title";
    public static final int PIC_WIDTH = 512;
    public static final int PIC_HEIGHT = 512;
    public static final String TITLE = "Title";
    // Save the Error Log file in SD card in App Name folder
    public static final boolean IS_ERROR_LOG_SAVE = false;
    public static final String STR_EDIT = "Edit";
    public static final String STR_ADD = "Add";
    public static final String PDF_OPEN = "http://docs.google.com/gview?embedded=true&url=";
    public static final String BUNDLE_DEFUALT_IMAGE = "DefualtImage";
    public static final String BUNDLE_ADD_PATIENT_PHOTO_LOCAL = "AddPatientPhotoLocal";
    public static final String BUNDLE_APPOINTMENT_TYPE = "AppointmentType";
    public static final String STR_MEDICATION_EYE_DROP = "EyeDrops";
    // Other Special Character
    public static final String STR_COLON = " : ";
    public static final String STR_SPACE_HYPHEN = " - ";
    public static final String STR_HYPHEN = "-";
    public static final String STR_EMPTY_STRING = "";
    public static final String STR_EMPTY_SPACE = " ";
    public static final String STR_FORWARD_SLASH = "/";
    public static final String STR_COMMA_SEPARATOR = ", ";
    public static final String STR_DOT = ".";
    // Payment Status
    public static final String STR_STATUS_SUCCESS = "Success";
    public static final String STR_STATUS_FAILURE = "Failure";
    // For Storage
    public static final String STR_TEMP_FOLDER = "/.Tcc_temp";
    public static final String STR_TEMP_IMAGE = "temp_image";
    public static final String STR_MIME_TYPE = "text/html";
    public static final String STR_UTF = "utf-8";
    public static final String STR_PROVIDER = ".provider";
    public static final String STR_IMAGE = "image";
    public static final String STR_PDF = "pdf";
    // Storage Extension Types
    public static final String STR_EXT_JPG = ".jpg";
    public static final String STR_EXT_JPEG = ".jpeg";
    public static final String STR_EXT_PNG = ".png";
    public static final String STR_EXT_GIF = ".gif";
    public static final String STR_EXT_PDF = ".pdf";
    public static final String STR_EXT_TXT = ".txt";
    public static final String STR_EXT_DOC = ".doc";
    public static final String STR_EXT_DOCX = ".docx";
    public static final String STR_EXT_XLS = ".xls";
    public static final String STR_EXT_XLSX = ".xlsx";
    public static final String STR_EXT_PPT = ".ppt";
    public static final String STR_EXT_PPTX = ".pptx";
    public static final String STR_EXT_ALL = "*/*";
    // Attachment Data Types
    public static final String STR_DOC_TYPE_JPG = "image/jpg";
    public static final String STR_DOC_TYPE_GIF = "image/gif";
    public static final String STR_DOC_TYPE_PDF = "application/pdf";
    public static final String STR_DOC_TYPE_TXT = "text/plain";
    public static final String STR_DOC_TYPE_DOC = "application/msword";
    public static final String STR_DOC_TYPE_EXCEL = "application/vnd.ms-excel";
    public static final String STR_DOC_TYPE_PPT = "application/vnd.ms-powerpoint";
    public static int SNACK_BAR_RETRY = 101;
    public static String DEFAULT_VALUE = "-1";
    public static boolean IS_MATERIAL_DIALOG_EXIST;
    // Broadcast Receiver Keys
    public static String BROADCAST_TIME_SLOT = "broadcastTimeSlot";
    // Bundle Keys
    public static String BUNDLE_LOGIN_TYPE = "loginType";
    public static String BUNDLE_IS_FROM_SEARCH = "isFromSearch";
    public static String RECEPTIONIST_PATIENT = "ReceptionistPatient";
    public static String OPTOMETRIST_PRELIMINARY_EXAMINATION = "PreliminaryExamination";
    public static String OPTOMETRIST_PATIENT = "OptometristPatient";

    // Receptionist Bundle Keys
    public static String BUNDLE_PATIENT_ARRAY = "patientArray";
    public static String BUNDLE_PATIENT_NAME = "patientName";
    public static String BUNDLE_PATIENT_FIRST_NAME = "patientFirstName";
    public static String BUNDLE_PATIENT_LAST_NAME = "patientLastName";
    public static String BUNDLE_PATIENT_CODE = "patientCode";
    public static String BUNDLE_PATIENT_MOBILE = "patientMobileNo";
    public static String BUNDLE_MRD_NO = "mrdNo";
    public static String BUNDLE_PATIENT_APPOINTMENT_NO = "patientAppointmentNo";
    public static String BUNDLE_PATIENT_APPOINTMENT_ID = "patientAppointmentId";
    public static String BUNDLE_PATIENT_ID = "patientId";
    public static String BUNDLE_PATIENT_APPOINTMENT_DATE = "patientAppointmentDate";
    public static String BUNDLE_PATIENT_FOLLOW_UP_DATE = "patientFollowUpDate";
    public static String BUNDLE_PATIENT_TREATMENT_DATE = "patientTreatmentDate";
    public static String BUNDLE_PATIENT_APPOINTMENT_DOCTOR = "patientAppointmentDoctor";
    public static String BUNDLE_PATIENT_APPOINTMENT_COUNCILLOR = "patientAppointmentCouncillor";
    public static String BUNDLE_PATIENT_APPOINTMENT_STATUS = "patientAppointmentStatus";
    public static String BUNDLE_PIC_NAME = "ImageTitle";
    public static String BUNDLE_DOCUMENT_ID = "Document ID";
    public static String BUNDLE_CHANGE_STATUS = "Change Status";
    public static String BUNDLE_PATIENT_PAYMENT_AMOUNT = "patientPaymentAmount";
    public static String BUNDLE_PATIENT_PAYMENT_DATE = "patientPaymentDate";
    public static String BUNDLE_PATIENT_COUNCILLOR_DATE = "councillorDate";
    public static String BUNDLE_PATIENT_PAYMENT_APPOINTMENT = "patientPaymentWithAppointment";
    public static String BUNDLE_PAYMENT_EDIT_MODE = "editPatientPayment";
    public static String BUNDLE_PATIENT_PAYMENT_HISTORY_ID = "patientPaymentHistoryId";
    public static String BUNDLE_APPOINTMENT_STATUS = "appointmentStatus";
    public static String BUNDLE_APPOINTMENT_SEL_DATE = "appointmentSelectedDate";
    public static String BUNDLE_PATIENT_PROFILE = "patientProfile";
    public static String BUNDLE_ADD_PATIENT_USERDATA = "fromPAtient";
    public static String BUNDLE_VISION = "patientName";
    public static String BUNDLE_PATIENT_USER_ID = "UserID";
    public static String BUNDLE_PRELIMINARY_EXAMINATION_ID = "PreliminaryExaminationID";
    // Change Status Tab
    public static String EXAMINATION_BY_OPTOMETRIST = "Preliminary Examination Start By Optometrist";
    public static String EXAMINATION_BY_DOCTOR = "Examination Start By Doctor";
    //Change Status
    public static String STATUS_COMPLETE_BY_RECEPTIONIST = "Complete By Receptionist";
}
