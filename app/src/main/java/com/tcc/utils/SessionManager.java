package com.tcc.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.tcc.Model.GetModuleIDList;
import com.tcc.Model.RoleModal;
import com.tcc.Model.RoleModalResponse;

public class SessionManager {

    public static final String SOURCE_PATH = "sourcepath";
    public static final String IS_CHECKED = "checkinout";
    public static final String DATE = "date";
    private static final String PREF_NAME = "PREF_TCC";
    public final String APP_PASS_CODE = "app_pass_code";
    //    public final String CurrentTheme = "CurrentTheme";
    public final String KEY_LOGIN_USER_DATA = "loginUserData";
    public final String KEY_CONFIGURATION_DATA = "configurationData";
    public final String KEY_ROLE_DATA = "roleData";
    public final String KEY_MODULE_DATA = "moduleData";
    public final String KEY_ROLE = "rolee";
    public final String FIRSTNAME = "firstname";
    public final String LASTNAME = "lastname";
    public final String MOBILE = "mobile";
    private SharedPreferences settings;
    private SharedPreferences.Editor editor;


    public SessionManager(Context context) {
        Context mContext = context;
        settings = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
    }

    // Set the Access Token and Other Important Keys in Shared Preferences
    public void setPreferences(String Key, String Value) {
        editor.putString(Key, Value);
        editor.commit();
    }


    // Set the Access Token and Other Important Keys in Shared Preferences
    public void setPreferences(String Key, int Value) {
        editor.putInt(Key, Value);
        editor.apply();
    }

    // get the Access Token and Other Important Keys in Shared Preferences
    public int getPreferences(String Key, int Value) {
        return settings.getInt(Key, Value);
    }


    // get the Access Token and Other Important Keys in Shared Preferences
    public String getPreferences(String Key, String Value) {
        return settings.getString(Key, Value);
    }

    public void setPreferences(String Key, boolean Value) {
        editor.putBoolean(Key, Value);
        editor.apply();
    }

    // get the Access Token and Other Important Keys in Shared Preferences
    public boolean getPreferences(String Key, boolean Value) {
        return settings.getBoolean(Key, Value);
    }

    /**
     * Clear all the data stored in shared preferences
     */
    public void clearAllPreferences() {
        editor.clear().commit();
    }

    public void clearAllPreferences(String KEY_LOGIN_USER_DATA) {
        editor.clear().commit();
    }


    public GetModuleIDList getModuleList() {
        Gson gson = new Gson();
        String json = getPreferences(KEY_MODULE_DATA, "");
        GetModuleIDList moduleIDList = gson.fromJson(json, GetModuleIDList.class);
        return moduleIDList;
    }

    public RoleModalResponse getRoleList() {
        Gson gson = new Gson();
        String json2 = getPreferences(KEY_ROLE_DATA, "");
        RoleModalResponse roleList = gson.fromJson(json2, RoleModalResponse.class);
        return roleList;
    }

    public RoleModal getRole() {
        Gson gson = new Gson();
        RoleModal roleList = new RoleModal();
        String json2 = getPreferences(KEY_ROLE, "");
        roleList = gson.fromJson(json2, RoleModal.class);
        return roleList;
    }


}
