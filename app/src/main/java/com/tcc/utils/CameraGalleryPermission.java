package com.tcc.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saggi on 17/3/17.
 */

public class CameraGalleryPermission {

    public static final int INTERNAL_EXTERNAL_PERMISSION = 123;
    static List<String> permissionsList;

    public static boolean checkPermissionFragment(final Context context, Fragment fragment, ArrayList<String> permissionArrayList) {

        permissionsList = new ArrayList<String>();
        for (String strPermission : permissionArrayList)
            addPermission(context, strPermission);

        if (permissionsList.size() > 0) {
            //Fragment ProfiFragment = new ProfileFragment();
            fragment.requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    INTERNAL_EXTERNAL_PERMISSION);

            return false;
        } else
            return true;
    }

    public static boolean checkPermission(final Context context, ArrayList<String> permissionArrayList) {

        permissionsList = new ArrayList<String>();
        for (String strPermission : permissionArrayList)
            addPermission(context, strPermission);

        if (permissionsList.size() > 0) {

            ActivityCompat.requestPermissions((Activity) context, permissionsList.toArray(new String[permissionsList.size()]),
                    INTERNAL_EXTERNAL_PERMISSION);

            return false;
        } else
            return true;
    }

    private static boolean addPermission(Context context, String permission) {
        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            if (!ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permission))
                return false;
        }
        return true;
    }


}

