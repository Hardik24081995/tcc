package com.tcc.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.ColorInt;

import com.amulyakhare.textdrawable.TextDrawable;
import com.tcc.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Common {

    private static ProgressDialog mProgressDialog;

    /**
     * Insert Log
     *
     * @param mMessage - Message of the defined log
     */
    public static void insertLog(String mMessage) {
        Log.e("Tag", mMessage);
    }

    /**
     * This method should convert the date without time format to the given date format
     *
     * @param context    - Context
     * @param date       - Date
     * @param dateFormat - Date Format
     * @return - Return particular format for the given date
     */
    public static String convertDateWithoutTime(Context context, String date, String dateFormat) {
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy));
            Date d = format.parse(date);
            @SuppressLint("SimpleDateFormat") SimpleDateFormat serverFormat = new SimpleDateFormat(dateFormat);
            return serverFormat.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Open date & time picker
     *
     * @param context   - Context
     * @param mTextView - Text View to set the date & time
     */
    public static void openDateTimePicker(final Context context, final TextView mTextView) {

        int mYear, mMonth, mDay, mHour, mMinute;

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        c.set(year, monthOfYear, dayOfMonth);
                        new TimePickerDialog(context, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                            @SuppressLint("SimpleDateFormat")
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                c.set(Calendar.MINUTE, minute);
                                mTextView.setText(new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss)).
                                        format(c.getTime()));
                            }
                        }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false).show();

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        datePickerDialog.show();
    }

    /**
     * Open date & time picker
     *
     * @param context   - Context
     * @param mTextView - Text View to set the date & time
     */
    public static void openTimePicker(final Context context, final TextView mTextView) {

        int mYear, mMonth, mDay, mHour, mMinute;

        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        mTextView.setText(new SimpleDateFormat(context.getResources().getString(R.string.date_format_hh_mm_a)).
                                format(c.getTime())
                                .replace(context.getResources().getString(R.string.hour_format_small_am), context.getResources().getString(R.string.hour_format_am))
                                .replace(context.getResources().getString(R.string.hour_format_small_pm), context.getResources().getString(R.string.hour_format_pm)));
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    /**
     * Open the date picker
     *
     * @param context   - Context
     * @param mTextView - Text View to set the date
     */
    public static void openDatePicker(final Context context, final TextView mTextView) {

        int mYear, mMonth, mDay, mHour, mMinute;

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        c.set(year, monthOfYear, dayOfMonth);
                        mTextView.setText(new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy)).
                                format(c.getTime()));

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        datePickerDialog.show();
    }


    /**
     * Open the Past date picker
     *
     * @param context   - Context
     * @param mTextView - Text View to set the date
     */
    public static void openPastDatePicker(final Context context, final EditText mTextView) {

        int mYear, mMonth, mDay, mHour, mMinute;

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        c.set(year, monthOfYear, dayOfMonth);
                        mTextView.setText(new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy)).
                                format(c.getTime()));

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        datePickerDialog.show();
    }

    /*
     * This method is fetching the absolute path of the image file
     * if you want to upload other kind of files like .pdf, .docx
     * you need to make changes on this method only
     * Rest part will be the same
     * */
    public static String getRealPathFromURI(Uri contentUri, Activity mActivity) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = mActivity.managedQuery(contentUri, proj, null, null, null);

        if (cursor == null)
            return null;

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    // to scale the bitmap
    public static Bitmap decodeFile(File f, int WIDTH, int HIGHT) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // The new size we want to scale to
            final int REQUIRED_WIDTH = WIDTH;
            final int REQUIRED_HIGHT = HIGHT;
            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_WIDTH
                    && o.outHeight / scale / 2 >= REQUIRED_HIGHT)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {

        }
        return null;
    }

    /**
     * Open the Past date picker
     *
     * @param context   - Context
     * @param mTextView - Text View to set the date
     */
    public static void openPastDateTimePicker(final Context context, final EditText mTextView) {
        final Calendar calendar;
        try {
            calendar = Calendar.getInstance();
            DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {

                @SuppressLint("SimpleDateFormat")
                @Override
                public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

                    calendar.set(year, monthOfYear, dayOfMonth);
                    new TimePickerDialog(context, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                        @SuppressLint("SimpleDateFormat")
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            calendar.set(Calendar.MINUTE, minute);
                            mTextView.setText(new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss)).
                                    format(calendar.getTime()));
                        }
                    }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false).show();

                }
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
            datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
            datePickerDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Open the Past date picker
     *
     * @param context - Context
     * @param -       Text View to set the date
     */
    public static void openEditAllDateTimePicker(final Context context, final EditText mTextView) {

        int mYear, mMonth, mDay, mHour, mMinute;

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        c.set(year, monthOfYear, dayOfMonth);
                        new TimePickerDialog(context, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                            @SuppressLint("SimpleDateFormat")
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                c.set(Calendar.MINUTE, minute);
                                mTextView.setText(new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss)).
                                        format(c.getTime()));
                            }
                        }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false).show();

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

    /**
     * Open date picker for all dates like past, present & future dates are visible to select
     *
     * @param context   - Context
     * @param mTextView - Text View to set the date
     */
    public static void openDatePickerForAllDates(final Context context, final TextView mTextView) {

        int mYear, mMonth, mDay, mHour, mMinute;

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        c.set(year, monthOfYear, dayOfMonth);
                        mTextView.setText(new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy)).
                                format(c.getTime()));

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public static void openAllDateTimePicker(final Context context, final TextView mTextView) {

        int mYear, mMonth, mDay, mHour, mMinute;

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        c.set(year, monthOfYear, dayOfMonth);
                        new TimePickerDialog(context, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                            @SuppressLint("SimpleDateFormat")
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                c.set(Calendar.MINUTE, minute);
                                mTextView.setText(new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss)).
                                        format(c.getTime()));
                            }
                        }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false).show();

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    /**
     * This method should convert the date and time from the server date
     *
     * @param context - Context
     * @param Date    - String date
     * @return - Return date in string format
     */
    public static String convertDateTimeToServer(Context context, String Date) {
        String strDate = Date;
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss));
            java.util.Date newDate = format.parse(Date);
            strDate = format.format(newDate);

            @SuppressLint("SimpleDateFormat") SimpleDateFormat formatDate = new SimpleDateFormat(context.getResources().getString(R.string.date_format_yyyy_mm_dd_hh_mm_ss));
            strDate = formatDate.format(newDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strDate;
    }

    /**
     * This method should convert the date and time from the server date
     *
     * @param context - Context
     * @param Date    - String date
     * @return - Return date in string format
     */
    public static String convertDateUsingDateFormat(Context context, String Date, String mServerDateFormat, String mOutputDateFormat) {
        String strDate = Date;
        try {
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat format = new SimpleDateFormat(mServerDateFormat);
            java.util.Date newDate = format.parse(Date);
            strDate = format.format(newDate);

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat formatDate = new SimpleDateFormat(mOutputDateFormat);
            strDate = formatDate.format(newDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strDate;
    }

    /**
     * This method should convert the date from the server date in the given format
     *
     * @param context - Context
     * @param Date    - String date
     * @return - Return date in string format
     */
    public static String convertDateToServer(Context context, String Date) {
        String strDate = Date;
        try {

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat format = new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy));
            java.util.Date newDate = format.parse(Date);
            strDate = format.format(newDate);

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat formatDate = new SimpleDateFormat(context.getResources().getString(R.string.date_format_yyyy_mm_dd));
            strDate = formatDate.format(newDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strDate;
    }

    /**
     * This method should convert the date from the server date in the given format
     *
     * @param context - Context
     * @param Date    - String date
     * @return - Return date in string format
     */
    public static String convertDateToServerMinus(Context context, String Date) {
        String strDate = Date;
        try {

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat format = new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));
            java.util.Date newDate = format.parse(Date);
            strDate = format.format(newDate);

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat formatDate = new SimpleDateFormat(context.getResources().getString(R.string.date_format_yyyy_mm_dd));
            strDate = formatDate.format(newDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strDate;
    }

    @SuppressLint("SimpleDateFormat")
    public static String convertTime(Context context, String mTime) {
        String mConvertedTime = mTime;
        try {
            // ToDo: Split time
            String[] time = mConvertedTime.split("-");
            String mSplitTime = time[0];

            // ToDO: Convert split time to 24 hours and return the string
            final SimpleDateFormat sdf = new SimpleDateFormat(context.getResources().getString(R.string.date_format_hh_mm_aa));
            final Date dateObj = sdf.parse(mSplitTime);
            mConvertedTime = new SimpleDateFormat(context.getResources().getString(R.string.date_format_hh_mm_capital)).format(dateObj);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return mConvertedTime;
    }

    /**
     * Sets the current date & time
     *
     * @param context - Context
     * @return - Return date in date format
     */
    public static String setCurrentDateTime(Context context) {
        Date date = Calendar.getInstance().getTime();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat df = new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss));
        return df.format(date);
    }

    /**
     * Sets the current date & time
     *
     * @param context - Context
     * @return - Return date in date format
     */
    public static String setCheckCurrentDateTime(Context context) {
        Date date = Calendar.getInstance().getTime();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat df = new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy_kk_mm_ss_hyphen));
        return df.format(date);
    }


    /**
     * Set Convert String Date one format to another Format
     *
     * @param mActivity          -Activity
     * @param date               -String Date
     * @param input_date         - Input Date Format
     * @param output_date_format - Output Date Format
     * @return -Return String
     */
    public static String setConvertDateFormat(Activity mActivity, String date, String input_date, String output_date_format) {
        String strDate = date;
        try {

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat format = new SimpleDateFormat(input_date);
            Date newDate = format.parse(date);
            strDate = format.format(newDate);

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat formatDate = new SimpleDateFormat(output_date_format);
            strDate = formatDate.format(newDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strDate;
    }

    /**
     * Sets the toast message
     *
     * @param context - Context
     * @param message - Message for showing the toast
     */
    public static void setCustomToast(Context context, String message) {
        final Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, 5000);
    }


    /**
     * This method should check the string is null or empty
     *
     * @param context - Context
     * @param mValue  - Value
     * @return - Return value if string is not null otherwise return Na is string gets null
     */
    public static String isEmptyString(Context context, String mValue) {
        if (mValue != null && mValue.length() == 0) {
            mValue = context.getResources().getString(R.string.no_value_available);
        }
        return mValue;
    }

    public static void showLoadingDialog(Context context, String message) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();
    }

    public static void hideDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    /**
     * This method should set the labeled image view
     *
     * @param context    - Context
     * @param mFirstName - Label First Name
     * @param mLastName  - Label Last Name
     * @return - Return Drawable Builder
     */
    public static Drawable setLabeledImageView(Context context, String mFirstName, String mLastName) {
        TextDrawable.IBuilder mDrawableBuilder = TextDrawable.builder().round();

        String LastName = TextUtils.isEmpty(mLastName) ? AppConstants.STR_EMPTY_STRING : String.valueOf(mLastName.charAt(0)).toUpperCase();
        return mDrawableBuilder.build(String.valueOf(mFirstName.charAt(0)).toUpperCase() + LastName, setThemeColor(context));
    }

    /**
     * Sets the theme color
     *
     * @param context - Context
     * @return - Return color as per the theme applied
     */
    public static int setThemeColor(Context context) {
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
        @ColorInt int mColor = typedValue.data;
        return mColor;
    }
}
