package com.tcc.utils;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.tcc.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AppUtil {
    private Context mContext;

    public AppUtil(Context Context) {
        mContext = Context;
    }

    /***
     * Display message
     *
     * @param view - View
     * @param Message - Message
     */
    public static void displaySnackBarWithMessage(View view, String Message) {
        try {
            Snackbar.make(view, Message, Snackbar.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String convertDate(Context context, String date) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            Date d = format.parse(date);
            SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd");
            return serverFormat.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /***
     *  Create new image file with timestamp
     * @return - Return time stamp for image file
     */
    public static File currentTimeStampFile() {
        File file = new File(Environment.getExternalStorageDirectory() + AppConstants.STR_TEMP_FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }
        return new File(file, AppConstants.STR_TEMP_IMAGE + AppConstants.STR_EXT_JPEG);
    }

    /**
     * Display snack bar for network error
     *
     * @param View - View
     */
    public void displayNoInternetSnackBar(View View, final AppCallbackListener listener) {
        final Snackbar snackbar = Snackbar
                .make(View, mContext.getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);

        snackbar.setAction("Retry", new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View view) {
                snackbar.dismiss();
                listener.onAppCallback(AppConstants.SNACK_BAR_RETRY);
            }
        });

        // Changing message text color
        snackbar.setActionTextColor(Color.RED);

        // Changing action button text color
        android.view.View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(mContext.getResources().getColor(R.color.colorWhite));
        snackbar.show();
    }

    /***
     * Display snack bar for network error
     *
     * @param View - View
     */
    public void displayNoInternetSnackBar(View View) {
        try {
            Snackbar snackbar = Snackbar
                    .make(View, mContext.getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG)
                    /*.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    })*/;

            // Changing message text color
            snackbar.setActionTextColor(Color.RED);

            // Changing action button text color
            android.view.View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * To check the internet connection
     *
     * @return
     */
    public boolean getConnectionState() {
        ConnectivityManager cm = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        if (ni == null)
            return false;
        else
            return true;
    }


}
