package com.tcc.utils;

import android.content.Context;
import android.provider.Settings;

/**
 * Application file is used for all Configuration that are used in the Application
 */
public class ApplicationConfiguration {

    public static final String host = "api.linkedin.com";
    public static final String url = "https://" + host
            + "/v1/people/~:" +
            "(id,email-address,formatted-name,phone-numbers,picture-url,public-profile-url,picture-urls::(original))";
    public static int MAX_PASSWORD_LENGTH = 6;
    //Linked IN
    public static String LINKEDIN_CLIENT_ID = "";
    public static String LINKEDIN_CLIENT_SECRET = "";
    //Twitter
    public static String TWITTER_CONSUMER_KEY = "";
    public static String TWITTER_CONSUMER_SECRET = "";
    //Pinterest
    public static String PINTEREST_APP_ID = "";
    private Context mContext;

    public ApplicationConfiguration(Context Ctx) {
        mContext = Ctx;

    }

    /***
     * Get the User Type
     * @param mContext
     * @return
     */
    public static String getUserType(Context mContext) {
       /* SessionManager mSharepref = new SessionManager(mContext);
        LoginModel User = mSharepref.getPreferences();
        if(User.getIsAdmin() ==  1){
            return DeviceType.ADMIN.toString();
        }else{
            return DeviceType.EMPLOYEE.toString();
        }*/
        return "";
    }

    /***
     * get the Unique identification for devices
     * @return
     */
    public String getDeviceID() {
        return Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static enum UserRegType {
        Regular,
        Facebook,
        Google,
        LinkedIn,
        Twitter,
        Pinterest,
        Instagram
    }

    public static enum DeviceType {
        ADMIN {
            public String toString() {
                return "Admin Android";
            }
        },
        EMPLOYEE {
            public String toString() {
                return "Employee Android";
            }
        };
    }

    public static enum PAYMENT_TYPE {
        Cheque,
        Online
    }

    public static enum USER_TYPE {
        ADMIN,
        EMPLOYEE
    }

    public static enum PROPERTY_STATUS {
        Verified,
        ATS,
        SD
    }

    public static enum FilterType {
        Daily,
        Weekly,
        Monthly,
        Yearly,
        Total
    }
}
