package com.tcc.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.DocumentAdapter;
import com.tcc.Model.GetCustomerDocumentModel;
import com.tcc.Model.RemoveDocModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerDocumentFragment extends Fragment {

    RelativeLayout mRelativeNoDataFound;
    RecyclerView rv_document;
    DocumentAdapter documentAdapter;
    ArrayList<GetCustomerDocumentModel.Datum> myArrayList;
    SessionManager mSession;
    String SitesID, QuotationID, UserID;
    private Activity mActivity;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private View mView;
    private boolean isBackFromB;

    public CustomerDocumentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_document, container, false);
        isBackFromB = false;

        myArrayList = new ArrayList<>();
        mSession = new SessionManager(getActivity());
        UserID = GetJsonData.getLoginData(getActivity(), WebFields.LOGIN.RESPONSE_USERID);
        SitesID = mSession.getPreferences(AppConstants.SITE_ID, "");
        QuotationID = mSession.getPreferences(AppConstants.QUOTATION_ID, "");

        getID();
        getSiteDocument();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getSiteDocument();
            mSwipeRefreshLayout.setRefreshing(false);
        });

        return mView;
    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        if (isBackFromB) {
            //Refresh your stuff here
            getSiteDocument();
        }

    }

    private void getSiteDocument() {
        myArrayList.clear();
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getCustomerDocumentJson("-1", "1", SitesID, QuotationID));

            Call<GetCustomerDocumentModel> call = RetrofitClient.createService(ApiInterface.class).getCustomerDocumentAPI(body);
            call.enqueue(new Callback<GetCustomerDocumentModel>() {
                @Override
                public void onResponse(@NonNull Call<GetCustomerDocumentModel> call, @NonNull Response<GetCustomerDocumentModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayList.addAll(response.body().getData());
                            if (response.body() != null) {
                                mRelativeNoDataFound.setVisibility(View.GONE);
                                rv_document.setVisibility(View.VISIBLE);
                                setAdapter();
                            } else {
                                mRelativeNoDataFound.setVisibility(View.VISIBLE);
                                rv_document.setVisibility(View.GONE);
                            }

                        } else {
                            mRelativeNoDataFound.setVisibility(View.VISIBLE);
                            rv_document.setVisibility(View.GONE);
//                           // Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetCustomerDocumentModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        documentAdapter = new DocumentAdapter(getActivity(), myArrayList) {
            @Override
            protected void onImageDelete(int adapterPosition, GetCustomerDocumentModel.Datum datum) {
                super.onImageDelete(adapterPosition, datum);
                removeDoc(adapterPosition, datum);
            }
        };
        rv_document.setHasFixedSize(true);
        rv_document.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rv_document.setAdapter(documentAdapter);
    }

    private void removeDoc(int adapterPosition, GetCustomerDocumentModel.Datum datum) {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.removeDocumentJson(UserID, datum.getCustomerSitesDocumentID()));

            Call<RemoveDocModel> call = RetrofitClient.createService(ApiInterface.class).deleteDocumentAPI(body);
            call.enqueue(new Callback<RemoveDocModel>() {
                @Override
                public void onResponse(@NonNull Call<RemoveDocModel> call, @NonNull Response<RemoveDocModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayList.remove(adapterPosition);
                            documentAdapter.notifyDataSetChanged();
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<RemoveDocModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getID() {
        rv_document = mView.findViewById(R.id.rv_documents);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipeToRefresh);
        mRelativeNoDataFound = mView.findViewById(R.id.relative_no_data_available);
    }


}
