package com.tcc.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.NotificationAdapter;
import com.tcc.Model.NotificationModal;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment {

    View mView;
    ArrayList<NotificationModal.DataItem> myArrayList;
    RecyclerView rv_notify;
    String UserID;
    SwipeRefreshLayout mSwipeRefreshLayout;
    NotificationAdapter adapter;
    private RelativeLayout mRelativeNoDataFound, mRelativeNoInternet;
    private Button mBtnRetry;

    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_notification, container, false);

        myArrayList = new ArrayList<>();
        UserID = GetJsonData.getLoginData(getActivity(), WebFields.LOGIN.RESPONSE_USERID);
        getIds();
        getNotificationList();
        setAdapter();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            myArrayList.clear();
            adapter.notifyDataSetChanged();
            getNotificationList();
            mSwipeRefreshLayout.setRefreshing(false);
        });
        return mView;
    }

    private void getIds() {
        mSwipeRefreshLayout = mView.findViewById(R.id.swipeToRefresh);
        rv_notify = mView.findViewById(R.id.recycler_view);
        mRelativeNoDataFound = mView.findViewById(R.id.relative_no_data_available);
        mRelativeNoInternet = mView.findViewById(R.id.relative_no_internet);
        mBtnRetry = mView.findViewById(R.id.button_retry);
    }

    private void setAdapter() {
        adapter = new NotificationAdapter(getContext(), myArrayList);
        rv_notify.setHasFixedSize(true);
        rv_notify.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_notify.setAdapter(adapter);
    }


    private void getNotificationList() {
        try {
            Common.showLoadingDialog(getActivity(), "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getNotification(UserID, "-1", "1"));

            Call<NotificationModal> call = RetrofitClient.createService(ApiInterface.class).getNotification(body);
            call.enqueue(new Callback<NotificationModal>() {
                @Override
                public void onResponse(@NonNull Call<NotificationModal> call, @NonNull Response<NotificationModal> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayList.addAll(response.body().getData());
                            if (myArrayList.isEmpty()) {
                                mRelativeNoDataFound.setVisibility(View.VISIBLE);
                                rv_notify.setVisibility(View.GONE);
                            } else {
                                adapter.notifyDataSetChanged();
                            }


                        } else {
                            mSwipeRefreshLayout.setEnabled(false);
                            mRelativeNoDataFound.setVisibility(View.VISIBLE);
                            rv_notify.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<NotificationModal> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    // Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
