package com.tcc.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.tcc.Adapter.CustomerListAdapter;
import com.tcc.Model.GetCustomerListModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerFragment extends Fragment {

    ArrayList<GetCustomerListModel.Datum> mArrayCustomer;
    AlertDialog alertDialog;
    RelativeLayout relative_no_data_available;
    private Activity mActivity;
    private View mView;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RelativeLayout mRelativeNoDataFound, mRelativeNoInternet;
    private Button mBtnRetry;
    private CustomerListAdapter mCustomerListAdapter;
    private FloatingActionButton mFabSearch;
    private EditText text_searchname, text_email;
    private Button btn_customersearch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_customer, container, false);

        mActivity = getActivity();
        mArrayCustomer = new ArrayList<>();

        getIds();
        setRegiListener();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mCustomerListAdapter);

        getCustomerList();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getCustomerList();
            mSwipeRefreshLayout.setRefreshing(false);
        });

        return mView;
    }


    private void getCustomerList() {
        mArrayCustomer.clear();
        try {
            Common.showLoadingDialog(getActivity(), "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getCustomerListJson("-1", "1", "", "", "-1", "-1"));

            Call<GetCustomerListModel> call = RetrofitClient.createService(ApiInterface.class).getCustomerAPI(body);
            call.enqueue(new Callback<GetCustomerListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetCustomerListModel> call, @NonNull Response<GetCustomerListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayCustomer.addAll(response.body().getData());
                            if (response.body() != null) {
                                relative_no_data_available.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                                setAdapter();
                            } else {
                                relative_no_data_available.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            }

                        } else {

                            relative_no_data_available.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                            //  Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetCustomerListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void getIds() {
        mRecyclerView = mView.findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) mView.findViewById(R.id.swipeToRefresh);
        mRelativeNoDataFound = mView.findViewById(R.id.relative_no_data_available);
        mRelativeNoInternet = mView.findViewById(R.id.relative_no_internet);
        mBtnRetry = mView.findViewById(R.id.button_retry);
        mFabSearch = mView.findViewById(R.id.floating_customer_search);
        relative_no_data_available = mView.findViewById(R.id.relative_no_data_available);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorBlack);
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mCustomerListAdapter = new CustomerListAdapter(mActivity, mArrayCustomer);
        mRecyclerView.setAdapter(mCustomerListAdapter);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void setRegiListener() {

        mFabSearch.setOnClickListener(v -> {

            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            dialogBuilder.setTitle("Search Customer");
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.layout_customer_search, null);
            dialogBuilder.setView(dialogView);

            alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.setCancelable(true);

            text_searchname = dialogView.findViewById(R.id.edt_customer_name);
            text_email = dialogView.findViewById(R.id.edt_customer_email);
            btn_customersearch = dialogView.findViewById(R.id.btn_customer_search);

            btn_customersearch.setOnClickListener(v1 -> {

//                if (!text_email.getText().toString().trim().equalsIgnoreCase("")) {
//                    if (!text_email.getText().toString().trim().matches(emailPattern)) {
//                        text_email.setError("Enter Valid Email");
//                    } else {
//                        String customerName = text_searchname.getText().toString().trim();
//                        String customerEmail = text_email.getText().toString().trim();
//                        getCustomerList(customerName, customerEmail);
//                    }
//                } else {
                String customerName = text_searchname.getText().toString().trim();
                String customerEmail = text_email.getText().toString().trim();
                getCustomerList(customerName, customerEmail);
//                }
            });
        });
    }

    private void getCustomerList(String customerName, String customerEmail) {
        try {
            Common.showLoadingDialog(getActivity(), "Loading");
            mArrayCustomer.clear();
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getCustomerListJson("-1", "1", customerName, customerEmail, "-1", "-1"));

            Call<GetCustomerListModel> call = RetrofitClient.createService(ApiInterface.class).getCustomerAPI(body);
            call.enqueue(new Callback<GetCustomerListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetCustomerListModel> call, @NonNull Response<GetCustomerListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayCustomer.addAll(response.body().getData());
                            alertDialog.dismiss();
                            if (mArrayCustomer.isEmpty()) {
                                mRelativeNoDataFound.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                setAdapter();
                            }

                        } else {
                            // Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetCustomerListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
