package com.tcc.fragment;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Activity.EmailActivityDialog;
import com.tcc.Adapter.FollowUPReminderAdapter;
import com.tcc.Model.AddVisitorResponseModel;
import com.tcc.Model.GetVisitorReminderModel;
import com.tcc.Model.SMSModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FollowUPFragment extends Fragment {

    View mView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView rv_visitorReminder;
    RelativeLayout relative_no_data_available;
    FollowUPReminderAdapter mAdapter;
    ArrayList<GetVisitorReminderModel.Datum> mArrayReminder;
    EditText edtMessage_EmailDialog;
    String filter;
    private String VisitorID, ReminderID, UserID, Phone, Email;
    private boolean isBackFromB;

    public FollowUPFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_visitor_reminder, container, false);


        mArrayReminder = new ArrayList<>();
        isBackFromB = false;


        filter = getArguments().getString("filter");
        getID();
        getData();
        setRegister();
        getVisitorReminder();
        return mView;
    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }


    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        //Refresh your stuff here
        if (isBackFromB) {
            getVisitorReminder();
        }
    }

    private void getID() {
        rv_visitorReminder = mView.findViewById(R.id.rv_visitorReminder);
        relative_no_data_available = mView.findViewById(R.id.relative_no_data_available);

        mSwipeRefreshLayout = (SwipeRefreshLayout) mView.findViewById(R.id.swipeToRefresh);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorBlack);
    }

    private void getData() {
        SessionManager manager = new SessionManager(getActivity());
        String UserData = manager.getPreferences(manager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(getActivity(), WebFields.LOGIN.RESPONSE_USERID);
        VisitorID = manager.getPreferences("VisitorID", "");
        ReminderID = manager.getPreferences("ReminderID", "");
        Phone = manager.getPreferences("Mobile", "");
        Email = manager.getPreferences("Email", "");
    }

    private void setData() {

        mAdapter = new FollowUPReminderAdapter(getActivity(), mArrayReminder);
        rv_visitorReminder.setHasFixedSize(true);
        rv_visitorReminder.setLayoutManager(new LinearLayoutManager(getActivity()));

        mAdapter = new FollowUPReminderAdapter(getActivity(), mArrayReminder) {
            @Override
            protected void onEmailClick() {
                super.onEmailClick();

                Intent intent = new Intent(getActivity(), EmailActivityDialog.class);
                intent.putExtra("ID", VisitorID);
                intent.putExtra("Actionuser", "Visitor");
                intent.putExtra("Email", Email);
                startActivity(intent);
            }

            @Override
            protected void onSMSClick() {
                super.onSMSClick();
                alterDialogForSendSMS();
            }

            @Override
            protected void onAddClick(String visitorReminderID) {
                super.onSMSClick();
                alterDialogForResponse(visitorReminderID);
            }
        };

        rv_visitorReminder.setAdapter(mAdapter);

    }

    private void setRegister() {
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getVisitorReminder();
            mSwipeRefreshLayout.setRefreshing(false);
        });
    }


    private void alterDialogForResponse(String visitorReminderID) {

        try {
            Dialog dialog = new Dialog(getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialogue_addresponse);

            // Edit Text
            edtMessage_EmailDialog = dialog.findViewById(R.id.edit_dialog_addresponse);

            // Button
            Button mButtonSend = dialog.findViewById(R.id.button_dialog_addresponse);

            // ToDo: Button Yes Click Listener
            mButtonSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (checkValidate()) {
                        DoAddResponse(visitorReminderID);
                        dialog.dismiss();
                    }
                }

                private boolean checkValidate() {
                    boolean status = true;

                    if (edtMessage_EmailDialog.getText().toString().trim().isEmpty()) {
                        status = false;
                        edtMessage_EmailDialog.setError("Enter Message");
                    }
                    return status;
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void DoAddResponse(String visitorReminderID) {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.addVisitorResponseJson(UserID, visitorReminderID, edtMessage_EmailDialog.getText().toString()));

            Call<AddVisitorResponseModel> call = RetrofitClient.createService(ApiInterface.class).addVisitorResponseAPI(body);
            call.enqueue(new Callback<AddVisitorResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<AddVisitorResponseModel> call, @NonNull Response<AddVisitorResponseModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddVisitorResponseModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(getActivity(), t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void alterDialogForSendSMS() {
        try {
            Dialog dialog = new Dialog(getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_sms_reminder);

            // Text View
            TextView mTextMobileNo = dialog.findViewById(R.id.text_dialog_sms_mobile_no);

            // Edit Text
            EditText edtMessage_EmailDialog = dialog.findViewById(R.id.edit_dialog_sms_message);

            // Sets up the data
            mTextMobileNo.setText(Phone);

            // Button
            Button mButtonSend = dialog.findViewById(R.id.button_dialog_sms_send);

            // ToDo: Button Yes Click Listener
            mButtonSend.setOnClickListener(v -> {
                if (checkValidationForSMS(edtMessage_EmailDialog.getText().toString())) {
                    sendEmail(mTextMobileNo.getText().toString(), edtMessage_EmailDialog.getText().toString());
                    dialog.dismiss();
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendEmail(String phone, String message) {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.sendSMSJson(phone, message));

            Call<SMSModel> call = RetrofitClient.createService(ApiInterface.class).sendSMSAPI(body);

            call.enqueue(new Callback<SMSModel>() {
                @Override
                public void onResponse(Call<SMSModel> call, Response<SMSModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            Toast.makeText(getActivity(), "Message Sent Successfully", Toast.LENGTH_SHORT).show();
                        } else {
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<SMSModel> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkValidationForSMS(String msg) {
        boolean status = true;

        if (msg.equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(getActivity(), "Enter Message", Toast.LENGTH_SHORT).show();
        }

        return status;
    }

    private void getVisitorReminder() {
        mArrayReminder.clear();
        String roleID = GetJsonData.getLoginData(requireContext(), WebFields.LOGIN.RESPONSE_ROLE_ID);

        String UserID = "-1";
        if (!roleID.equals("-1")) {
            UserID = GetJsonData.getLoginData(requireContext(), WebFields.LOGIN.RESPONSE_USERID);
        }

        try {
            Common.showLoadingDialog(getActivity(), "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getFollowUp("-1", "1", UserID, filter));

            Call<GetVisitorReminderModel> call = RetrofitClient.createService(ApiInterface.class).getVisitorReminderAPI(body);
            call.enqueue(new Callback<GetVisitorReminderModel>() {
                @Override
                public void onResponse(@NonNull Call<GetVisitorReminderModel> call, @NonNull Response<GetVisitorReminderModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayReminder.addAll(response.body().getData());
                            if (mArrayReminder.isEmpty()) {
                                relative_no_data_available.setVisibility(View.VISIBLE);
                                rv_visitorReminder.setVisibility(View.GONE);
                            } else {
                                relative_no_data_available.setVisibility(View.GONE);
                                rv_visitorReminder.setVisibility(View.VISIBLE);
                                setData();
                            }

                        } else {
                            relative_no_data_available.setVisibility(View.VISIBLE);
                            rv_visitorReminder.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetVisitorReminderModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //   Common.setCustomToast(getActivity(), t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
