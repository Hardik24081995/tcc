package com.tcc.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.tcc.Activity.HomeActivity;
import com.tcc.Model.GetUserType;
import com.tcc.Model.ProfileModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.AppPermissions;
import com.tcc.utils.AppUtil;
import com.tcc.utils.CameraGalleryPermission;
import com.tcc.utils.Common;
import com.tcc.utils.ImageUtils.Compressor;
import com.tcc.utils.ImageUtils.ImageUtil;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    public static final int REQUEST_CAMERA = 101, REQUEST_GALLERY = 102;
    public String mFilePath = "", mImagePath;
    Activity mActivity;
    SessionManager mSessionManager;
    String UserID;
    Spinner spinnerDesignation;
    ArrayList<GetUserType.Datum> designationList = new ArrayList<>();
    ArrayList<String> DesignationString = new ArrayList<>();
    String userType = "";
    private Button submitProfile;
    private ImageView imgProfileImage;
    private EditText firstName, lastName, mobileNo, Salary, Address;
    private int SelectedDesignationIndex = 0;
    private String DesignationID = "", Name;
    private String chosenTask;
    private File destination;
    private Uri mImageUri;
    private File mPath;
    private Bitmap mBitmap;
    private String imagename = "";
    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinnerDesignation:
                    SelectedDesignationIndex = adapterView.getSelectedItemPosition();

                    if (SelectedDesignationIndex != 0) {
                        DesignationID = String.valueOf((Integer.parseInt(designationList.get(SelectedDesignationIndex - 1).getUsertypeID())));
                        String Name = (designationList.get(SelectedDesignationIndex - 1).getUsertype());
                        Common.insertLog("mStateId::> " + DesignationID);
                        Common.insertLog("mStateName::> " + Name);
                    }
                    break;


            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_profile, container, false);

        mActivity = getActivity();


        mSessionManager = new SessionManager(mActivity);
        String UserData = mSessionManager.getPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);
        String FirstName = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_FIRST_NAME);
        String LastName = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_LAST_NAME);
        String Phone = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_MOBILE_NO);
        String PhotoURL = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_PHOTOURL);
        String salary = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_SALARY);
        String address = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_ADDRESS);
        userType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.USER_TYPE);

        try {
            imagename = PhotoURL.substring(PhotoURL.lastIndexOf('/') + 1);
        } catch (Exception e) {

        }


        imgProfileImage = mView.findViewById(R.id.imgProfileImage);
        firstName = mView.findViewById(R.id.firstName);
        lastName = mView.findViewById(R.id.lastName);
        Salary = mView.findViewById(R.id.Salary);
        Address = mView.findViewById(R.id.Address);
        mobileNo = mView.findViewById(R.id.mobileNo);
        submitProfile = mView.findViewById(R.id.submitProfile);
        spinnerDesignation = mView.findViewById(R.id.spinnerDesignation);
        spinnerDesignation.setOnItemSelectedListener(onItemSelectedListener);
        View view = mView.findViewById(R.id.view);

        firstName.setText(FirstName);
        lastName.setText(LastName);
        Salary.setText(salary);
        Address.setText(address);
        mobileNo.setText(Phone);

        imgProfileImage.setImageDrawable(Common.setLabeledImageView(getActivity(), FirstName, LastName));

       /* if (!userType.equals("Admin")) {
            getDesignation();
            spinnerDesignation.setVisibility(View.VISIBLE);
            view.setVisibility(View.VISIBLE);
        }*/


        if (PhotoURL.equalsIgnoreCase("")) {
            imgProfileImage.setImageDrawable(Common.setLabeledImageView(getActivity(), FirstName, LastName));
        } else {
            Picasso.with(mActivity)
                    .load(PhotoURL)
                    .placeholder(R.drawable.demo_user_pic)
                    .into(imgProfileImage);
        }

        imgProfileImage.setOnClickListener(v -> openImageUpload());
        submitProfile.setOnClickListener(v ->
                doSubmit());

        return mView;
    }

    private void getDesignation() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getUserType());

            Call<GetUserType> call = RetrofitClient.createService(ApiInterface.class).getUserType(body);
            call.enqueue(new Callback<GetUserType>() {
                @Override
                public void onResponse(@NonNull Call<GetUserType> call, @NonNull Response<GetUserType> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            designationList.addAll(response.body().getData());
                            setTrainingAdapter();
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetUserType> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTrainingAdapter() {
        try {
            DesignationString.add(0, "Select UserType");
            if (designationList.size() > 0) {
                for (GetUserType.Datum Services : designationList) {
                    DesignationString.add(Services.getUsertype());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, DesignationString) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == SelectedDesignationIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerDesignation.setAdapter(adapter);

            for (int i = 0; i < designationList.size(); i++) {
                if (designationList.get(i).getUsertype().equals(userType)) {
                    spinnerDesignation.setSelection(i + 1);
                    DesignationID = designationList.get(i).getUsertypeID();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doSubmit() {
        try {
            Common.showLoadingDialog(getActivity(), "Loading");
            MultipartBody.Part body = null;
            RequestBody imagenameparams = null;
            if (mImagePath != null) {
                File file = new File(mImagePath);
                final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                body = MultipartBody.Part.createFormData(WebFields.REGISTER_USER.REQUEST_IMAGE,
                        file.getName(), requestFile);
            } else {
                if (!imagename.equals("")) {
                    imagenameparams = RequestBody.create(MediaType.parse("text/plain"), imagename);
                }
            }

            RequestBody method = RequestBody.create(MediaType.parse("text/plain"), WebFields.REGISTER_USER.MODE);

            RequestBody userID = RequestBody.create(MediaType.parse("text/plain"), UserID);
            RequestBody FirstName = RequestBody.create(MediaType.parse("text/plain"), firstName.getText().toString());
            RequestBody LastName = RequestBody.create(MediaType.parse("text/plain"), lastName.getText().toString());
            RequestBody MobileNo = RequestBody.create(MediaType.parse("text/plain"), mobileNo.getText().toString());
            RequestBody salary = RequestBody.create(MediaType.parse("text/plain"), Salary.getText().toString());
            RequestBody Adress = RequestBody.create(MediaType.parse("text/plain"), Address.getText().toString());
            RequestBody ID = RequestBody.create(MediaType.parse("text/plain"), DesignationID);
            RequestBody UsertypeID = RequestBody.create(MediaType.parse("text/plain"), DesignationID);

            Call<ProfileModel> callRepos = new RetrofitClient().createService(ApiInterface.class).EditProfileAPI(method,
                    FirstName, LastName, MobileNo, userID, Adress, ID, salary, UsertypeID, imagenameparams, body);

            callRepos.enqueue(new Callback<ProfileModel>() {
                @Override
                public void onResponse(@NonNull Call<ProfileModel> call, @NonNull Response<ProfileModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            Common.setCustomToast(mActivity, mMessage);

                            saveDataToSharedPreferences(jsonObject);

                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ProfileModel> call, @NonNull Throwable t) {
                    Common.insertLog("Add UploadPics error " + t.getMessage());
//                    Common.hideDialog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveDataToSharedPreferences(JSONObject jsonObject) {
        try {
            if (jsonObject.has(WebFields.DATA)) {
                String mUserData = jsonObject.getString(WebFields.DATA);
                mSessionManager.setPreferences(mSessionManager.KEY_LOGIN_USER_DATA, mUserData);

                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);
                Objects.requireNonNull(getActivity()).finishAffinity();

            } else {
                mSessionManager.setPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Open popup for camera and gallery
     */
    private void openImageUpload() {

        final CharSequence[] menus = {"Capture Image", "Gallery",
                "Cancel"};

        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        builder.setTitle("Select Options");
        builder.setItems(menus, (dialog, pos) -> {

            boolean result = CameraGalleryPermission.checkPermission(mActivity,
                    AppPermissions.ReadWriteExternalStorageRequiredPermission());

            if (pos == 0) {
                chosenTask = "Capture Image";
                if (result) {
                    openCameraIntent();
                }
            } else if (pos == 1) {
                chosenTask = "Gallery";
                if (result) {
                    openGalleryIntent();
                }
            } else if (pos == 2) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /**
     * Open the camera intent
     */
    private void openCameraIntent() {
        destination = AppUtil.currentTimeStampFile();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
        } else {
            File file = new File(Uri.fromFile(destination).getPath());
            Uri photoUri = FileProvider.getUriForFile(mActivity, mActivity.getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }

        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        if (intent.resolveActivity(mActivity.getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    /**
     * Open the gallery intent
     */
    private void openGalleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Show only images, no videos or anything else
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_GALLERY);
    }

    /**
     * Request camera permission dynamically
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case CameraGalleryPermission.INTERNAL_EXTERNAL_PERMISSION:
                Map<String, Integer> perms = new HashMap<>();
                perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                if (perms.get(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    if (chosenTask.equals("Capture Image")) {
                        openCameraIntent();
                    } else if (chosenTask.equals("Gallery")) {
                        openGalleryIntent();
                    }
                } else {
                    Toast.makeText(mActivity, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode,resultCode,data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                    CropImage.activity(Uri.fromFile(destination)).start(mActivity, this);
                    break;
                case REQUEST_GALLERY:
                    mImageUri = data.getData();

                    CropImage.activity(mImageUri).start(mActivity, this);

                    mFilePath = Common.getRealPathFromURI(mImageUri, mActivity);
                    if (mFilePath == null)
                        mFilePath = mImageUri.getPath(); // from File Manager

                    if (mFilePath != null) {
                        File mFile = new File(mFilePath);
                        mBitmap = Common.decodeFile(mFile, AppConstants.PIC_WIDTH,
                                AppConstants.PIC_HEIGHT);
                        mPath = mFile;
                    }
                    break;
                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    mImageUri = result.getUri();
                    onSelectFromGalleryResult(mImageUri);
                    break;
            }
        }
    }

    /**
     * Sets the cropped image from the gallery or camera intent
     */
    private void onSelectFromGalleryResult(Uri dataUri) {
        Bitmap bm = null;
        String imageName = null;
        if (dataUri != null) {
            try {
                //  bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), dataUri);
                File file = new File(dataUri.getPath());
                imageName = file.getName();
                mImagePath = dataUri.getPath();
                Compressor compressor = new Compressor(mActivity);
                if (ImageUtil.convertURItoFile(mActivity, dataUri) != null)
                    bm = compressor.compressToBitmap(ImageUtil.convertURItoFile(mActivity, dataUri));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        if (bm != null) {
            bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        }

        imgProfileImage.setImageBitmap(bm);
    }
}
