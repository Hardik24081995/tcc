package com.tcc.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.SessionManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class SiteInfoFragment extends Fragment {


    View mView;
    SessionManager mSession;
    private TextView mTextClientName, mTextCompanyName, mTextStartDate, mTextEndDate, mTextLocation, mTextGSTNumber;


    public SiteInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_site_info, container, false);
        mSession = new SessionManager(getActivity());

        mTextClientName = mView.findViewById(R.id.sites_ClientName);
        mTextCompanyName = mView.findViewById(R.id.sites_company_name);
        mTextStartDate = mView.findViewById(R.id.sites_StartDate);
        mTextEndDate = mView.findViewById(R.id.sites_EndDate);
        mTextGSTNumber = mView.findViewById(R.id.sites_GST_NUMBER);
        mTextLocation = mView.findViewById(R.id.sites_Address);

        mTextClientName.setText(mSession.getPreferences(AppConstants.EMPLOYEE_NAME, ""));
        mTextLocation.setText(mSession.getPreferences(AppConstants.LOCATION, ""));
        mTextGSTNumber.setText(mSession.getPreferences(AppConstants.GST_NO, ""));
        mTextStartDate.setText(mSession.getPreferences(AppConstants.START_DATE, ""));
        mTextEndDate.setText(mSession.getPreferences(AppConstants.END_DATE, ""));
        mTextCompanyName.setText(mSession.getPreferences(AppConstants.SITE_NAME, ""));


        return mView;
    }

}
