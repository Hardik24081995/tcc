package com.tcc.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.tcc.Activity.AboutUsActivity;
import com.tcc.Activity.LoginActivity;
import com.tcc.Activity.PenaltyListingActivity;
import com.tcc.Activity.TicketListingActivity;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.SessionManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment {

    private Activity mActivity;
    private View mView;
    private CardView Changepass, Notification, Privacy, Terms, About, Logout, ticket, penalty;
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent intent = new Intent();

            switch (v.getId()) {
                case R.id.card1:
                    intent = new Intent(mActivity, AboutUsActivity.class);
                    intent.putExtra(AppConstants.TITLE, mActivity.getString(R.string.text_change_password));
                    startActivity(intent);
                    break;
                case R.id.card2:
                    intent = new Intent(mActivity, AboutUsActivity.class);
                    intent.putExtra(AppConstants.TITLE, mActivity.getString(R.string.text_notification));
                    startActivity(intent);
                    break;

                case R.id.card3:
                    intent = new Intent(mActivity, AboutUsActivity.class);
                    intent.putExtra(AppConstants.TITLE, mActivity.getString(R.string.text_privancy_polices));
                    startActivity(intent);
                    break;
                case R.id.card4:
                    intent = new Intent(mActivity, AboutUsActivity.class);
                    intent.putExtra(AppConstants.TITLE, mActivity.getString(R.string.text_term_policy));
                    startActivity(intent);
                    break;
                case R.id.card5:
                    intent = new Intent(mActivity, AboutUsActivity.class);
                    intent.putExtra(AppConstants.TITLE, mActivity.getString(R.string.text_about));
                    startActivity(intent);
                    break;
                case R.id.card6:
                    SessionManager sessionManager = new SessionManager(getActivity());
                    sessionManager.clearAllPreferences();
                    sessionManager.clearAllPreferences(sessionManager.KEY_LOGIN_USER_DATA);
                    sessionManager.clearAllPreferences(sessionManager.KEY_CONFIGURATION_DATA);
                    sessionManager.setPreferences("IsFirst", false);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mActivity.finishAffinity();
                    startActivity(new Intent(mActivity, LoginActivity.class));
                    break;
                case R.id.card7:
                    intent = new Intent(mActivity, PenaltyListingActivity.class);
                    startActivity(intent);
                    break;
                case R.id.card8:
                    intent = new Intent(mActivity, TicketListingActivity.class);
                    startActivity(intent);
                    break;
            }
        }
    };


    public SettingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_setting, container, false);

        mActivity = getActivity();

        getIds();
        setRgister();
        return mView;
    }

    /**
     * this Method Declare
     */
    private void getIds() {
        Changepass = mView.findViewById(R.id.card1);
        Notification = mView.findViewById(R.id.card2);
        Privacy = mView.findViewById(R.id.card3);
        Terms = mView.findViewById(R.id.card4);
        About = mView.findViewById(R.id.card5);
        Logout = mView.findViewById(R.id.card6);
        ticket = mView.findViewById(R.id.card8);
        penalty = mView.findViewById(R.id.card7);
    }

    private void setRgister() {
        Changepass.setOnClickListener(clickListener);
        Notification.setOnClickListener(clickListener);
        Privacy.setOnClickListener(clickListener);
        Terms.setOnClickListener(clickListener);
        About.setOnClickListener(clickListener);
        Logout.setOnClickListener(clickListener);
        ticket.setOnClickListener(clickListener);
        penalty.setOnClickListener(clickListener);
    }
}
