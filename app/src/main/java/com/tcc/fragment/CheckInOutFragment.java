package com.tcc.fragment;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tcc.Adapter.CheckINOtAdapter;
import com.tcc.Model.AddCheckinModel;
import com.tcc.Model.CheckInOutListingModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class CheckInOutFragment extends Fragment implements LocationListener {

    private static final int REQUEST_LOCATION = 1;
    protected Context context;
    protected String latitude, longitude;
    double lat;
    double longi;
    Activity mActivity;
    View mView;
    String currentDate, isChecked, storedDate;
    SessionManager mSessionManager;
    RecyclerView recyclerView;
    ArrayList<CheckInOutListingModel.Datum> mArrayList;
    LocationManager locationManager;
    private TextView tvTodayDate, tvCheckIn, tvCheckInTime, tvCheckOut, tvCheckOutTime;
    private Button btn_checkin, btn_checkout;

    public CheckInOutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_check_in_out, container, false);

        mActivity = getActivity();
        mSessionManager = new SessionManager(mActivity);
        mArrayList = new ArrayList<>();
        isChecked = mSessionManager.getPreferences(SessionManager.IS_CHECKED, "");
        storedDate = mSessionManager.getPreferences(SessionManager.DATE, "");

        currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        mSessionManager.setPreferences(SessionManager.DATE, currentDate);

        ActivityCompat.requestPermissions(mActivity,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        initView();
        return mView;
    }

    private void initView() {
        getID();
        getCheckInOutList();

        if (storedDate.equalsIgnoreCase(currentDate)) {
            if (isChecked.equalsIgnoreCase("CheckOut")) {
                btn_checkin.setVisibility(View.GONE);
                btn_checkout.setVisibility(View.GONE);
                tvCheckIn.setVisibility(View.VISIBLE);
                tvCheckInTime.setVisibility(View.VISIBLE);
                tvCheckInTime.setText(mSessionManager.getPreferences("CheckInTime", ""));
                tvCheckOut.setVisibility(View.VISIBLE);
                tvCheckOutTime.setVisibility(View.VISIBLE);
                tvCheckOutTime.setText(mSessionManager.getPreferences("CheckOutTime", ""));
            } else if (isChecked.equalsIgnoreCase("CheckIn")) {
                btn_checkout.setVisibility(View.VISIBLE);
                btn_checkin.setVisibility(View.GONE);
                tvCheckIn.setVisibility(View.VISIBLE);
                tvCheckInTime.setVisibility(View.VISIBLE);
                tvCheckInTime.setText(mSessionManager.getPreferences("CheckInTime", ""));
            } else {
                btn_checkin.setVisibility(View.VISIBLE);
                btn_checkout.setVisibility(View.GONE);
            }
        } else {
            btn_checkin.setVisibility(View.VISIBLE);
            btn_checkout.setVisibility(View.GONE);
        }

        tvTodayDate.setText("Date : " + currentDate);

        btn_checkin.setOnClickListener(v -> {

            locationManager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                OnGPS();
            } else {
                getLocation();
            }

            mSessionManager.setPreferences(SessionManager.IS_CHECKED, "CheckIn");

            String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

            tvCheckIn.setVisibility(View.VISIBLE);
            tvCheckInTime.setVisibility(View.VISIBLE);
            tvCheckInTime.setText(currentTime);
            mSessionManager.setPreferences("CheckInTime", currentTime);
            btn_checkout.setVisibility(View.VISIBLE);
            btn_checkin.setVisibility(View.GONE);

            try {
                updateCheckinTime();
            } catch (IOException e) {
                e.printStackTrace();
            }

        });

        btn_checkout.setOnClickListener(v -> {

            locationManager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                OnGPS();
            } else {
                getLocation();
            }

            mSessionManager.setPreferences(SessionManager.IS_CHECKED, "CheckOut");

            String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

            tvCheckOut.setVisibility(View.VISIBLE);
            tvCheckOutTime.setVisibility(View.VISIBLE);
            tvCheckOutTime.setText(currentTime);
            mSessionManager.setPreferences("CheckOutTime", currentTime);
            btn_checkout.setVisibility(View.GONE);

            try {
                updateCheckoutTime();
            } catch (IOException e) {
                e.printStackTrace();
            }

        });

    }

    private void OnGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(
                mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGPS != null) {

                lat = locationGPS.getLatitude();
                longi = locationGPS.getLongitude();

                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);
            } else {
                Toast.makeText(mActivity, "Unable to find location.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void updateCheckinTime() throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(mActivity, Locale.getDefault());

        addresses = geocoder.getFromLocation(lat, longi, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5


        try {
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

            String newAddress = address + " " + city + " " + state + " " + country + " " + postalCode;

            Common.showLoadingDialog(getActivity(), "Loading");
            SessionManager mSession = new SessionManager(mActivity);
            String UserData = mSession.getPreferences(mSession.KEY_LOGIN_USER_DATA, "");
            String UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);
            String newDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
            String newTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

            String DateTime = newDate + " " + newTime;

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.addCheckInJson(UserID, DateTime, latitude, longitude, newAddress));

            Call<AddCheckinModel> call = RetrofitClient.createService(ApiInterface.class).addCheckInTimeAPI(body);
            call.enqueue(new Callback<AddCheckinModel>() {
                @Override
                public void onResponse(@NonNull Call<AddCheckinModel> call, @NonNull Response<AddCheckinModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mSessionManager.setPreferences("CheckInCheckOutID", response.body().getData().get(0).getID());
                            Common.hideDialog();
                        } else {
                            Common.hideDialog();

                        }
                    } catch (JSONException e) {
                        Common.hideDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddCheckinModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.hideDialog();
                    Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void updateCheckoutTime() throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(mActivity, Locale.getDefault());

        addresses = geocoder.getFromLocation(lat, longi, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();

        String newAddress = address + " " + city + " " + state + " " + country + " " + postalCode;

        try {
            Common.showLoadingDialog(getActivity(), "Loading");
            SessionManager mSession = new SessionManager(mActivity);
            String UserData = mSession.getPreferences(mSession.KEY_LOGIN_USER_DATA, "");
            String UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);
            String newDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
            String newTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
            String CheckInCheckOutID = mSessionManager.getPreferences("CheckInCheckOutID", "");

            String DateTime = newDate + " " + newTime;

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.addCheckOutJson(UserID, DateTime, latitude, longitude, CheckInCheckOutID, newAddress));

            Call<AddCheckinModel> call = RetrofitClient.createService(ApiInterface.class).addCheckOutTimeAPI(body);
            call.enqueue(new Callback<AddCheckinModel>() {
                @Override
                public void onResponse(@NonNull Call<AddCheckinModel> call, @NonNull Response<AddCheckinModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            Common.hideDialog();
                            getCheckInOutList();
                        } else {
                            Common.hideDialog();
                        }
                    } catch (JSONException e) {
                        Common.hideDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddCheckinModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.hideDialog();
                    Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCheckInOutList() {
        try {
            mArrayList.clear();
            Common.showLoadingDialog(getActivity(), "Loading");
            SessionManager mSession = new SessionManager(mActivity);
            String UserData = mSession.getPreferences(mSession.KEY_LOGIN_USER_DATA, "");
            String UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getCheckInOutJson("-1", "1", UserID));

            Call<CheckInOutListingModel> call = RetrofitClient.createService(ApiInterface.class).checkInAPI(body);
            call.enqueue(new Callback<CheckInOutListingModel>() {
                @Override
                public void onResponse(@NonNull Call<CheckInOutListingModel> call, @NonNull Response<CheckInOutListingModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayList.addAll(response.body().getData());
                            Common.hideDialog();
                            setAdapter();
                        } else {
                            Common.hideDialog();
                        }
                    } catch (JSONException e) {
                        Common.hideDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CheckInOutListingModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.hideDialog();
                    Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        CheckINOtAdapter mAdapter = new CheckINOtAdapter(getActivity(), mArrayList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(mAdapter);
    }

    private void getID() {
        tvTodayDate = mView.findViewById(R.id.tvTodayDate);
        tvCheckIn = mView.findViewById(R.id.tvCheckIn);
        tvCheckInTime = mView.findViewById(R.id.tvCheckInTime);
        tvCheckOut = mView.findViewById(R.id.tvCheckOut);
        tvCheckOutTime = mView.findViewById(R.id.tvCheckOutTime);
        btn_checkin = mView.findViewById(R.id.btn_checkin);
        btn_checkout = mView.findViewById(R.id.btn_checkout);
        recyclerView = mView.findViewById(R.id.recyclerView);
    }

    @Override
    public void onLocationChanged(Location location) {

        lat = location.getLatitude();
        longi = location.getLongitude();

        latitude = String.valueOf(location.getLatitude());
        longitude = String.valueOf(location.getLongitude());

    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("Latitude", "disable");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("Latitude", "enable");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("Latitude", "status");
    }
}
