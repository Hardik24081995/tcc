package com.tcc.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.tcc.R;
import com.tcc.utils.SessionManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerInfoProfileFragment extends Fragment {

    String Name, Mobile, Address, Email, CompanyName;
    private Activity mActivity;
    private TextView mTextName, mTextEmail, mTextPhone, mTextLocation, mTextCompanyName;
    private View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_information_profile, container,
                false);
        mActivity = getActivity();
        getIds();
        setData();
        return mView;
    }

    private void getIds() {
        mTextName = mView.findViewById(R.id.text_customer_name);
        mTextEmail = mView.findViewById(R.id.text_customer_email);
        mTextPhone = mView.findViewById(R.id.text_customer_mobile);
        mTextLocation = mView.findViewById(R.id.text_customer_address);
        mTextCompanyName = mView.findViewById(R.id.companyName);

        SessionManager manager = new SessionManager(getActivity());
        Name = manager.getPreferences("Name", "");
        Mobile = manager.getPreferences("Mobile", "");
        CompanyName = manager.getPreferences("CompanyName", "");
        Address = manager.getPreferences("Address", "");
        Email = manager.getPreferences("Email", "");
    }

    /**
     * Set Data
     */
    private void setData() {
        mTextName.setText(Name);
        mTextEmail.setText(Email);
        mTextPhone.setText(Mobile);
        mTextLocation.setText(Address);
        mTextCompanyName.setText(CompanyName);
    }
}
