package com.tcc.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.PaymentAdapter;
import com.tcc.Model.CustomerPaymentModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerPaymentFragment extends Fragment {

    View mView;
    RelativeLayout relative_no_data_available;
    PaymentAdapter mAdapter;
    SessionManager sessionManager;
    String QuotationID, SiteID;
    ArrayList<CustomerPaymentModel.Datum> myArrayList;
    private Activity mActivity;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean isBackFromB;


    public CustomerPaymentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_payment, container, false);
        isBackFromB = false;
        myArrayList = new ArrayList<>();

        mActivity = getActivity();
        sessionManager = new SessionManager(mActivity);
        QuotationID = sessionManager.getPreferences(AppConstants.QUOTATION_ID, "");
        SiteID = sessionManager.getPreferences(AppConstants.SITE_ID, "");

        getID();
        getPaymentList();


        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getPaymentList();
            mSwipeRefreshLayout.setRefreshing(false);
        });

        return mView;
    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        if (isBackFromB) {
            //Refresh your stuff here

            getPaymentList();
        }

    }

    private void getPaymentList() {
        myArrayList.clear();
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getCustomerPaymentJson("-1", "1", SiteID, QuotationID));

            Call<CustomerPaymentModel> call = RetrofitClient.createService(ApiInterface.class).getCustomerPaymentAPI(body);
            call.enqueue(new Callback<CustomerPaymentModel>() {
                @Override
                public void onResponse(@NonNull Call<CustomerPaymentModel> call, @NonNull Response<CustomerPaymentModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayList.addAll(response.body().getData());
                            if (myArrayList.isEmpty()) {
                                relative_no_data_available.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                relative_no_data_available.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                                setAdapter();

                                for (int i = 0; i < myArrayList.size(); i++) {
                                    sessionManager.setPreferences(AppConstants.TOTAL_REMAINING_AMOUNT, myArrayList.get(i).getRemainingAmount());
                                    sessionManager.setPreferences(AppConstants.TOTAL_REMAINING_GST, myArrayList.get(i).getRemainingGSTAmount());
                                }
                            }
                        } else {
                            relative_no_data_available.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                            sessionManager.setPreferences(AppConstants.TOTAL_REMAINING_GST, "");
                            sessionManager.setPreferences(AppConstants.TOTAL_REMAINING_AMOUNT, "");
//                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CustomerPaymentModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    // Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        mAdapter = new PaymentAdapter(getActivity(), myArrayList);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);

    }

    private void getID() {
        mRecyclerView = mView.findViewById(R.id.rv_payment);
        relative_no_data_available = mView.findViewById(R.id.relative_no_data_available_payment);
        mSwipeRefreshLayout = (SwipeRefreshLayout) mView.findViewById(R.id.swipeToRefresh);
    }

}
