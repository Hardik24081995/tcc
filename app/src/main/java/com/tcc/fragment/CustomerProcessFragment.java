package com.tcc.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.ProcessAdapter;
import com.tcc.Model.GetCustomerProcessModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerProcessFragment extends Fragment {

    Activity mActivity;
    View mView;
    ProcessAdapter mAdapter;
    RecyclerView rv_process;
    SessionManager manager;
    RelativeLayout mRelativeNoDataFound;
    ArrayList<GetCustomerProcessModel.Datum> myArrayList;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean isBackFromB;

    public CustomerProcessFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_process, container, false);
        isBackFromB = false;

        mActivity = getActivity();
        myArrayList = new ArrayList<>();
        manager = new SessionManager(mActivity);

        getID();
        getProcess();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getProcess();
            mSwipeRefreshLayout.setRefreshing(false);
        });


        return mView;
    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        if (isBackFromB) {
            //Refresh your stuff here
            getProcess();
        }

    }

    private void getProcess() {
        myArrayList.clear();
        try {
            String CustomerID = manager.getPreferences("CustomerID", "");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getCustomerProcessJson("-1", "1", CustomerID));

            Call<GetCustomerProcessModel> call = RetrofitClient.createService(ApiInterface.class).getCustomerProcessAPI(body);
            call.enqueue(new Callback<GetCustomerProcessModel>() {
                @Override
                public void onResponse(@NonNull Call<GetCustomerProcessModel> call, @NonNull Response<GetCustomerProcessModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayList.addAll(response.body().getData());
                            if (response.body() != null) {
                                mRelativeNoDataFound.setVisibility(View.GONE);
                                rv_process.setVisibility(View.VISIBLE);
                                setAdapter();
                            } else {
                                mRelativeNoDataFound.setVisibility(View.VISIBLE);
                                rv_process.setVisibility(View.GONE);
                            }

                        } else {

                            mRelativeNoDataFound.setVisibility(View.VISIBLE);
                            rv_process.setVisibility(View.GONE);
//                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetCustomerProcessModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {

        mAdapter = new ProcessAdapter(getActivity(), myArrayList);
        rv_process.setHasFixedSize(true);
        rv_process.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_process.setAdapter(mAdapter);
    }

    private void getID() {
        rv_process = mView.findViewById(R.id.rv_process);
        mSwipeRefreshLayout = (SwipeRefreshLayout) mView.findViewById(R.id.swipeToRefresh);
        mRelativeNoDataFound = mView.findViewById(R.id.relative_no_data_available);
    }

}
