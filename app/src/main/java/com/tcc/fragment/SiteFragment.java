package com.tcc.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.tcc.Adapter.SitesAdapter;
import com.tcc.Model.GetSiteModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SiteFragment extends Fragment {

    Activity mActivity;
    RelativeLayout mRelativeNoDataFound;
    View mView;
    SitesAdapter mAdapter;
    RecyclerView rv_sites;
    ImageView mImageAdd;
    ArrayList<GetSiteModel.Datum> myArrayList;
    AlertDialog alertDialog;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private FloatingActionButton mFabSearch;

    public SiteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_site, container, false);

        myArrayList = new ArrayList<>();
        mActivity = getActivity();

        getID();
        setRegister();
        getSite();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getSite();
            mSwipeRefreshLayout.setRefreshing(false);
        });

        return mView;
    }

    private void setRegister() {

        mFabSearch.setOnClickListener(v -> {

            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            dialogBuilder.setTitle("Search Site");
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.layout_sitesearch, null);
            dialogBuilder.setView(dialogView);

            alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.setCancelable(true);

            EditText text_searchname = dialogView.findViewById(R.id.edt_customer_name);
            EditText text_email = dialogView.findViewById(R.id.edt_customer_email);
            Button btn_customersearch = dialogView.findViewById(R.id.btn_customer_search);

            btn_customersearch.setOnClickListener(v1 -> {
                String customerName = text_searchname.getText().toString().trim();
                String customerEmail = text_email.getText().toString().trim();
                if (customerEmail.equalsIgnoreCase("")) {
                    customerEmail = customerName;
                    getSites(customerName, customerEmail);
                } else {
                    getSites(customerName, customerEmail);
                }
            });
        });
    }

    private void getSites(String customerName, String customerEmail) {
        myArrayList.clear();
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getSiteListJson("-1", "1", "-1", "-1", "-1"
                    , "", customerEmail));
            Common.showLoadingDialog(getActivity(), "Loading");
            Call<GetSiteModel> call = RetrofitClient.createService(ApiInterface.class).getSiteAPI(body);
            call.enqueue(new Callback<GetSiteModel>() {
                @Override
                public void onResponse(@NonNull Call<GetSiteModel> call, @NonNull Response<GetSiteModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayList.addAll(response.body().getData());
                            if (myArrayList.isEmpty()) {
                                mRelativeNoDataFound.setVisibility(View.VISIBLE);
                                rv_sites.setVisibility(View.GONE);
                                alertDialog.dismiss();
                            } else {
                                mRelativeNoDataFound.setVisibility(View.GONE);
                                rv_sites.setVisibility(View.VISIBLE);
                                setAdapter();
                                alertDialog.dismiss();
                            }

                        } else {
                            mRelativeNoDataFound.setVisibility(View.VISIBLE);
                            rv_sites.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetSiteModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    // Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getSite() {
        myArrayList.clear();
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getSiteListJson("-1", "1", "-1", "-1", "-1"
                    , "", ""));
            Common.showLoadingDialog(getActivity(), "Loading");
            Call<GetSiteModel> call = RetrofitClient.createService(ApiInterface.class).getSiteAPI(body);
            call.enqueue(new Callback<GetSiteModel>() {
                @Override
                public void onResponse(@NonNull Call<GetSiteModel> call, @NonNull Response<GetSiteModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayList.addAll(response.body().getData());
                            if (myArrayList.isEmpty()) {
                                mRelativeNoDataFound.setVisibility(View.VISIBLE);
                                rv_sites.setVisibility(View.GONE);
                            } else {
                                mRelativeNoDataFound.setVisibility(View.GONE);
                                rv_sites.setVisibility(View.VISIBLE);
                                setAdapter();
                            }

                        } else {
                            mRelativeNoDataFound.setVisibility(View.VISIBLE);
                            rv_sites.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetSiteModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setAdapter() {
        SitesAdapter sitesAdapter = new SitesAdapter(getContext(), myArrayList);
        rv_sites.setHasFixedSize(true);
        rv_sites.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_sites.setAdapter(sitesAdapter);
    }

    private void getID() {

        rv_sites = mView.findViewById(R.id.rv_sites);
        mImageAdd = mView.findViewById(R.id.image_custom_toolbar_add);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipeToRefresh);
        mRelativeNoDataFound = mView.findViewById(R.id.relative_no_data_available);

        mFabSearch = mView.findViewById(R.id.floating_site_search);
    }

}
