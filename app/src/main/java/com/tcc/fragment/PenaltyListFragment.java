package com.tcc.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.PenaltyListAdapter;
import com.tcc.Model.PenaltyListModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PenaltyListFragment extends Fragment {

    Activity mActivity;
    View mView;
    PenaltyListAdapter mCustomerListAdapter;
    ArrayList<PenaltyListModel.Datum> datumArrayList = new ArrayList<>();
    private TextView mTextHeader;
    private ImageView mImageBack, mImageAdd;
    private RecyclerView mRecyclerView;
    private RelativeLayout mRelativeNoDataFound;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean isBackFromB;

    public PenaltyListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_penalty_list, container, false);

        initView();
        return mView;
    }

    private void initView() {

        mActivity = getActivity();
        isBackFromB = false;

        getID();
        setData();
        getPenalty();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            try {
                datumArrayList.clear();
                mCustomerListAdapter.notifyDataSetChanged();
                getPenalty();
                mSwipeRefreshLayout.setRefreshing(false);
            } catch (Exception e) {

            }


        });

    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        //Refresh your stuff here
        if (isBackFromB) {
            //Refresh your stuff here
            datumArrayList.clear();
            mCustomerListAdapter.notifyDataSetChanged();
            getPenalty();
        }
    }

    private void getPenalty() {
        try {

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getPenatyJson("-1", "1"));
            Common.showLoadingDialog(mActivity, "Loading");
            Call<PenaltyListModel> call = RetrofitClient.createService(ApiInterface.class).getPenaltyAPI(body);
            call.enqueue(new Callback<PenaltyListModel>() {
                @Override
                public void onResponse(@NonNull Call<PenaltyListModel> call, @NonNull Response<PenaltyListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            //  Common.setCustomToast(mActivity, mMessage);
                            datumArrayList.addAll(response.body().getData());

                            if (datumArrayList.isEmpty()) {
                                mRelativeNoDataFound.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                mRelativeNoDataFound.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                                setAdapter();
                            }
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PenaltyListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mCustomerListAdapter = new PenaltyListAdapter(mActivity, datumArrayList);
        mRecyclerView.setAdapter(mCustomerListAdapter);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void setData() {

    }

    private void getID() {
        mRecyclerView = mView.findViewById(R.id.rv_penaltyListing);
        mRelativeNoDataFound = mView.findViewById(R.id.relative_no_data_available);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipeToRefresh);
    }
}
