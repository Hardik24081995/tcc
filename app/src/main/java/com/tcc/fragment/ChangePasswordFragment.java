package com.tcc.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.tcc.Activity.LoginActivity;
import com.tcc.Model.ChangePasswordModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordFragment extends Fragment {

    Activity mActivity;
    View mView;
    Button submit;
    SessionManager mSessionManager;
    String UserID;
    private int buttonState = 1;
    private EditText mEditCurrent, mEditNew, mEditConfirm;
    private String strCurrent, strNew, strConfirm;


    public ChangePasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_change_password, container, false);
        mActivity = getActivity();

        mSessionManager = new SessionManager(mActivity);
        String UserData = mSessionManager.getPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");

        getID();
        setRegister();
        return mView;
    }

    private void getID() {

        mEditCurrent = mView.findViewById(R.id.changepass_currentPassword);
        mEditNew = mView.findViewById(R.id.changepass_NewPassword);
        mEditConfirm = mView.findViewById(R.id.changepass_ConfirmPassword);
        submit = mView.findViewById(R.id.button_change_password_submit);

        UserID = GetJsonData.getLoginData(getActivity(), WebFields.LOGIN.RESPONSE_USERID);
    }

    private void setRegister() {
        submit.setOnClickListener(v -> {
            if (checkValidation()) {
                doChangePassword();
            }
        });
    }

    private boolean checkValidation() {
        boolean status = true;

        strCurrent = mEditCurrent.getText().toString().trim();
        strNew = mEditNew.getText().toString().trim();
        strConfirm = mEditConfirm.getText().toString().trim();

        if (strCurrent.isEmpty()) {
            status = false;
            mEditCurrent.setError("Enter Current Password");
        }
        if (strNew.isEmpty()) {
            status = false;
            mEditNew.setError("Enter New Password");
        }
        if (!strConfirm.equals(strNew)) {
            status = false;
            mEditConfirm.setError("Password Not Match");
        }

        return status;
    }


    private void doChangePassword() {
        try {
            Common.showLoadingDialog(getActivity(), "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.setChangePassword(UserID, strCurrent, strNew));

            Call<ChangePasswordModel> call = RetrofitClient.createService(ApiInterface.class).changePasswordAPI(body);
            call.enqueue(new Callback<ChangePasswordModel>() {
                @Override
                public void onResponse(@NonNull Call<ChangePasswordModel> call, @NonNull Response<ChangePasswordModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            Common.setCustomToast(getActivity(), mMessage);
                            SessionManager sessionManager = new SessionManager(mActivity);
                            sessionManager.clearAllPreferences();
                            sessionManager.clearAllPreferences(sessionManager.KEY_LOGIN_USER_DATA);
                            sessionManager.clearAllPreferences(sessionManager.KEY_CONFIGURATION_DATA);

                            Intent intent = new Intent();
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            mActivity.finishAffinity();
                            startActivity(new Intent(mActivity, LoginActivity.class));

                        } else {
                            Common.setCustomToast(getActivity(), mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ChangePasswordModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(getActivity(), t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
