package com.tcc.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.SalaryAdapter;
import com.tcc.Model.GetSalaryModal;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmployeeSalaryFragment extends Fragment {

    Activity mActivity;
    SwipeRefreshLayout mSwipeRefreshLayout;
    View mView;
    SalaryAdapter mAdapter;
    RecyclerView rv_room;
    RelativeLayout relative_no_data_available;
    ArrayList<GetSalaryModal.Datum> mArraListRoom;
    private boolean isBackFromB;

    public EmployeeSalaryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_room, container, false);
        isBackFromB = false;

        mActivity = getActivity();
        mArraListRoom = new ArrayList<>();
        getRoomAlloc();
        rv_room = mView.findViewById(R.id.rv_room);
        relative_no_data_available = mView.findViewById(R.id.relative_no_data_available);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipeToRefresh);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorBlack);

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getRoomAlloc();
            mSwipeRefreshLayout.setRefreshing(false);
        });

        return mView;
    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        if (isBackFromB) {
            //Refresh your stuff here
            getRoomAlloc();
        }

    }

    private void getRoomAlloc() {
        mArraListRoom.clear();
        try {
            SessionManager mSession = new SessionManager(mActivity);
            String UserData = mSession.getPreferences(mSession.KEY_LOGIN_USER_DATA, "");
            String UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);
            String UserIDforAdd = mSession.getPreferences(AppConstants.EMPLOYEE_ID_FOR_ADD, "");

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getPaymentList("-1", "1", UserIDforAdd));

            Call<GetSalaryModal> call = RetrofitClient.createService(ApiInterface.class).getPaymentAPI(body);
            call.enqueue(new Callback<GetSalaryModal>() {
                @Override
                public void onResponse(@NonNull Call<GetSalaryModal> call, @NonNull Response<GetSalaryModal> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArraListRoom.addAll(response.body().getData());
                            if (mArraListRoom.isEmpty()) {
                                relative_no_data_available.setVisibility(View.VISIBLE);
                                rv_room.setVisibility(View.GONE);
                            } else {
                                relative_no_data_available.setVisibility(View.GONE);
                                rv_room.setVisibility(View.VISIBLE);
                                setAdapter();
                            }
                        } else {
                            relative_no_data_available.setVisibility(View.VISIBLE);
                            rv_room.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetSalaryModal> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setAdapter() {
        mAdapter = new SalaryAdapter(getActivity(), mArraListRoom);
        rv_room.setHasFixedSize(true);
        rv_room.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_room.setAdapter(mAdapter);
    }

}
