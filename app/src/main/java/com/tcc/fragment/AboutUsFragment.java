package com.tcc.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.fragment.app.Fragment;

import com.tcc.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUsFragment extends Fragment {


    View mView;

    public AboutUsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_about_us, container, false);
        WebView webView = (WebView) mView.findViewById(R.id.webView);
        webView.loadUrl("http://societyfy.in/TheCleaingCompany/api/service/getPage?PageName=AboutUS");
        return mView;
    }

}
