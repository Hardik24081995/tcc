package com.tcc.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.tcc.Activity.TeamDetailsActivity;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.SessionManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class SiteTeamDefinitionFragment extends Fragment {

    Activity mActivity;
    View mView;
    SessionManager mSessionManager;
    private TextView mTextQualityManager, mTextCount, mTextStartDate, mTextEndDate, mTextFieldOperator, mTextOperationManager;
    private CardView mCardTeamDetails;

    public SiteTeamDefinitionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_team_definition, container, false);

        mActivity = getActivity();
        mSessionManager = new SessionManager(mActivity);

        initView();
        return mView;
    }

    private void initView() {
        getID();

        mTextFieldOperator.setText(mSessionManager.getPreferences(AppConstants.FIELD_OPERATOR, ""));
        mTextQualityManager.setText(mSessionManager.getPreferences(AppConstants.QUALITY_MANAGER, ""));
        mTextOperationManager.setText(mSessionManager.getPreferences(AppConstants.OPERATION_MANAGER, ""));
        mTextCount.setText(mSessionManager.getPreferences(AppConstants.TEAM_COUNT, ""));
        mTextStartDate.setText(mSessionManager.getPreferences(AppConstants.START_DATE, ""));
        mTextEndDate.setText(mSessionManager.getPreferences(AppConstants.END_DATE, ""));
    }


    private void getID() {
        mTextFieldOperator = mView.findViewById(R.id.FieldOperator);
        mTextOperationManager = mView.findViewById(R.id.OperationManager);
        mTextQualityManager = mView.findViewById(R.id.QualityManager);
        mTextCount = mView.findViewById(R.id.TeamCount);
        mTextStartDate = mView.findViewById(R.id.td_employee_start_date);
        mTextEndDate = mView.findViewById(R.id.td_employee_end_date);

        mCardTeamDetails = mView.findViewById(R.id.teamDetails);
        mCardTeamDetails.setOnClickListener(v -> {
            Intent intent = new Intent(mActivity, TeamDetailsActivity.class);
            startActivity(intent);
        });
    }

}
