package com.tcc.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Activity.EmailActivityDialog;
import com.tcc.Adapter.ReminderAdapter;
import com.tcc.Model.GetCustomerReminderModel;
import com.tcc.Model.SMSModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerReminderFragment extends Fragment {


    RecyclerView rv_reminder;
    ReminderAdapter reminderAdapter;
    RelativeLayout relative_no_data_available;
    SessionManager manager;
    ArrayList<GetCustomerReminderModel.Datum> myArrayList;
    String siteID, QuotationID;
    private Activity mActivity;
    private View mView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean isBackFromB;


    public CustomerReminderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_reminder, container, false);
        isBackFromB = false;

        mActivity = getActivity();
        myArrayList = new ArrayList<>();
        manager = new SessionManager(mActivity);
        siteID = manager.getPreferences(AppConstants.SITE_ID, "");
        QuotationID = manager.getPreferences(AppConstants.QUOTATION_ID, "");


        GetID();
        getReeminderList();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getReeminderList();
            mSwipeRefreshLayout.setRefreshing(false);
        });


        return mView;
    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }


    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        if (isBackFromB) {
            //Refresh your stuff here
            getReeminderList();
        }

    }

    private void getReeminderList() {
        myArrayList.clear();
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getCustomerReminderListJson("-1", "1", siteID, QuotationID));

            Call<GetCustomerReminderModel> call = RetrofitClient.createService(ApiInterface.class).getCustomerReminderAPI(body);
            call.enqueue(new Callback<GetCustomerReminderModel>() {
                @Override
                public void onResponse(@NonNull Call<GetCustomerReminderModel> call, @NonNull Response<GetCustomerReminderModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayList.addAll(response.body().getData());
                            if (myArrayList.isEmpty()) {
                                relative_no_data_available.setVisibility(View.VISIBLE);
                                rv_reminder.setVisibility(View.GONE);
                            } else {
                                relative_no_data_available.setVisibility(View.GONE);
                                rv_reminder.setVisibility(View.VISIBLE);
                                setAdapter();
                            }
                        } else {
                            relative_no_data_available.setVisibility(View.VISIBLE);
                            rv_reminder.setVisibility(View.GONE);
//                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetCustomerReminderModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //   Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void GetID() {
        rv_reminder = mView.findViewById(R.id.rv_reminder);
        relative_no_data_available = mView.findViewById(R.id.relative_no_data_available_reminder);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipeToRefresh);
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv_reminder.setLayoutManager(mLayoutManager);

        reminderAdapter = new ReminderAdapter(mActivity, myArrayList) {
            @Override
            protected void onEmailClick(String EmailID) {
                super.onEmailClick(EmailID);

                Intent intent = new Intent(mActivity, EmailActivityDialog.class);
                intent.putExtra("ID", "1");
                intent.putExtra("Actionuser", "Customer");
                intent.putExtra("Email", EmailID);
                startActivity(intent);
            }

            @Override
            protected void onSMSClick(String MobileNo) {
                super.onSMSClick(MobileNo);
                alterDialogForSendSMS(MobileNo);
            }
        };
        rv_reminder.setAdapter(reminderAdapter);
        rv_reminder.setVisibility(View.VISIBLE);
    }

    public void alterDialogForSendSMS(String MobileNo) {
        try {
            Dialog dialog = new Dialog(getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_sms_reminder);

            // Text View
            TextView mTextMobileNo = dialog.findViewById(R.id.text_dialog_sms_mobile_no);

            // Edit Text
            EditText edtMessage_EmailDialog = dialog.findViewById(R.id.edit_dialog_sms_message);

            // Sets up the data
            mTextMobileNo.setText(MobileNo);

            // Button
            Button mButtonSend = dialog.findViewById(R.id.button_dialog_sms_send);

            // ToDo: Button Yes Click Listener
            mButtonSend.setOnClickListener(v -> {
                if (checkValidationForSMS(edtMessage_EmailDialog.getText().toString())) {
                    sendEmail(mTextMobileNo.getText().toString(), edtMessage_EmailDialog.getText().toString());
                    dialog.dismiss();
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendEmail(String phone, String message) {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.sendSMSJson(phone, message));

            Call<SMSModel> call = RetrofitClient.createService(ApiInterface.class).sendSMSAPI(body);

            call.enqueue(new Callback<SMSModel>() {
                @Override
                public void onResponse(Call<SMSModel> call, Response<SMSModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            Toast.makeText(getActivity(), "Message Sent Successfully", Toast.LENGTH_SHORT).show();
                        } else {
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<SMSModel> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkValidationForSMS(String msg) {
        boolean status = true;

        if (msg.equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(getActivity(), "Enter Message", Toast.LENGTH_SHORT).show();
        }

        return status;
    }

}
