package com.tcc.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.tcc.Adapter.EmployeAdapter;
import com.tcc.Model.DeativateEmployeeModel;
import com.tcc.Model.GetEmployeListModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmployeFragment extends Fragment {

    View mView;
    AlertDialog alertDialog;
    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView rv_employe;
    RelativeLayout mRelative_no_data;
    EmployeAdapter mAdapter;
    ArrayList<GetEmployeListModel.Datum> mArrayList;
    String UserID;
    SessionManager mSessionManager;
    private boolean isBackFromB;
    // Load More Listener Variables
    private int currentPageIndex = 1, lastFetchRecord = 0, totalRecords = 0;
    private boolean isRefresh = false, isLoadMore = false;

    private boolean isFirstTime = true;

    private FloatingActionButton mFabSearch;

    public EmployeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_employe, container, false);
        mArrayList = new ArrayList<>();
        mSessionManager = new SessionManager(getActivity());
        UserID = GetJsonData.getLoginData(getActivity(), WebFields.LOGIN.RESPONSE_USERID);
        isBackFromB = false;

        getID();
        getEmployeeList();

        return mView;

    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isBackFromB) {
            getEmployeeList();
        }

    }

    private void getID() {
        rv_employe = mView.findViewById(R.id.rv_employe);
        mRelative_no_data = mView.findViewById(R.id.relative_no_data_available);

        mSwipeRefreshLayout = mView.findViewById(R.id.swipeToRefresh);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorBlack);

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getEmployeeList();
            mSwipeRefreshLayout.setRefreshing(false);
        });

        mFabSearch = mView.findViewById(R.id.floating_employee_search);
        mFabSearch.setOnClickListener(v -> {

            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            dialogBuilder.setTitle("Search Employee");
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.layout_employee_search, null);
            dialogBuilder.setView(dialogView);

            alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.setCancelable(true);

            TextView text_searchname = dialogView.findViewById(R.id.edt_employee_name);
            TextView text_searchnumber = dialogView.findViewById(R.id.edt_employee_phone);
            Button btn_customersearch = dialogView.findViewById(R.id.btn_employee_search);

            btn_customersearch.setOnClickListener(v1 -> {

                String customerName = text_searchname.getText().toString().trim();
                String customerEmail = text_searchnumber.getText().toString().trim();
                getEmployeeSearch(customerName, customerEmail);

            });
        });

        rv_employe.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv_employe.setLayoutManager(mLayoutManager);
        mAdapter = new EmployeAdapter(getActivity(), mArrayList);
        rv_employe.setAdapter(mAdapter);

        if (rv_employe.getVisibility() == View.GONE)
            displayData();
    }

    private void getEmployeeSearch(String customerName, String customerEmail) {
        try {
            Common.showLoadingDialog(getActivity(), "Loading");
            mArrayList.clear();
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getEmployeeListJson("-1", "1", customerName, customerEmail, "-1"));

            Call<GetEmployeListModel> call = RetrofitClient.createService(ApiInterface.class).getEmployeAPI(body);
            call.enqueue(new Callback<GetEmployeListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetEmployeListModel> call, @NonNull Response<GetEmployeListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayList.addAll(response.body().getData());
                            alertDialog.dismiss();
                            if (mArrayList.isEmpty()) {
                                mRelative_no_data.setVisibility(View.VISIBLE);
                                rv_employe.setVisibility(View.GONE);
                            } else {
                                mRelative_no_data.setVisibility(View.GONE);
                                rv_employe.setVisibility(View.VISIBLE);
                                setEmployeeAdapter();
                            }

                        } else {
                            mRelative_no_data.setVisibility(View.VISIBLE);
                            rv_employe.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetEmployeListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(getActivity(), t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getEmployeeList() {
        mArrayList.clear();
        try {
            Common.showLoadingDialog(getActivity(), "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getEmployeeListJson("-1", "1", "", "", "-1"));

            Call<GetEmployeListModel> call = RetrofitClient.createService(ApiInterface.class).getEmployeAPI(body);
            call.enqueue(new Callback<GetEmployeListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetEmployeListModel> call, @NonNull Response<GetEmployeListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayList.addAll(response.body().getData());
//                            parseResponseForEventList(response.body().getData());
                            if (mArrayList.isEmpty()) {
                                mRelative_no_data.setVisibility(View.VISIBLE);
                                rv_employe.setVisibility(View.GONE);
                            } else {
                                mRelative_no_data.setVisibility(View.GONE);
                                rv_employe.setVisibility(View.VISIBLE);
                                setEmployeeAdapter();
                            }

                        } else {
                            mRelative_no_data.setVisibility(View.VISIBLE);
                            rv_employe.setVisibility(View.GONE);
                            //Common.setCustomToast(getActivity(), mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetEmployeListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(getActivity(), t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void parseResponseForEventList(List<GetEmployeListModel.Datum> data) {
        try {

            ArrayList<GetEmployeListModel.Datum> outwards = new ArrayList<>();
            outwards.addAll(data);

            if (mArrayList != null && mArrayList.size() > 0 &&
                    mAdapter != null) {
                mArrayList.clear();
                mArrayList.addAll(outwards);
                mAdapter.notifyDataSetChanged();

            } else {
                mArrayList.clear();
                mArrayList = outwards;//new ArrayList<Inventory>

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                rv_employe.setLayoutManager(mLayoutManager);

                mAdapter = new EmployeAdapter(getActivity(), mArrayList);
                rv_employe.setAdapter(mAdapter);
                totalRecords = data.size();
                // TotalRecords = 1;
//                setAddClickListener();

                if (rv_employe.getVisibility() == View.GONE)
                    displayData();
            }

        } catch (Exception e) {

//            RecyclerProgressView.stopProgress();
            try {
//                mAppUtils.displaySnackBarWithMessage(getActivity().findViewById(android.R.id.content),
//                        getResources().getString(R.string.Error_Msg_Try_Later));
            } catch (Exception ee) {
            }
            displayData();

        }
    }

    private void setEmployeeAdapter() {
        mAdapter = new EmployeAdapter(getActivity(), mArrayList) {
            @Override
            protected void deleteTeamMember(int adapterPosition, GetEmployeListModel.Datum datum) {
                super.deleteTeamMember(adapterPosition, datum);
                deactivateEmployee(adapterPosition, datum);
            }
        };
        rv_employe.setHasFixedSize(true);
        rv_employe.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_employe.setAdapter(mAdapter);
    }

    private void deactivateEmployee(int adapterPosition, GetEmployeListModel.Datum datum) {
        try {
            Common.showLoadingDialog(getActivity(), "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.DeactivateEmployeeJson(datum.getUserID(), UserID));

            Call<DeativateEmployeeModel> call = RetrofitClient.createService(ApiInterface.class).deactivateEmployeeAPI(body);
            call.enqueue(new Callback<DeativateEmployeeModel>() {
                @Override
                public void onResponse(@NonNull Call<DeativateEmployeeModel> call, @NonNull Response<DeativateEmployeeModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayList.remove(adapterPosition);
                            mAdapter.notifyDataSetChanged();
                            Toast.makeText(getActivity(), "Employee Deactivated", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DeativateEmployeeModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //   Common.setCustomToast(getActivity(), t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * Display RecyclerView after binding the data and hide the Data
     */
    private void displayData() {
        final LinearLayout mProgressView = (LinearLayout) mView.findViewById(R.id.llayoutProgressViewScreen);
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(50);

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                //RecyclerProgressView.stopProgress();

                rv_employe.animate()
                        .alpha(0.f)
                        .setDuration(80)
                        .start();
                mProgressView.setVisibility(View.GONE);

                rv_employe.setAlpha(0.f);
                rv_employe.setVisibility(View.VISIBLE);
                rv_employe.animate()
                        .alpha(1.f)
                        .setDuration(80)
                        .start();

            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });

        mProgressView.startAnimation(fadeOut);
    }

}
