package com.tcc.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.tcc.Activity.EmailActivityDialog;
import com.tcc.Adapter.VisitorListAdapter;
import com.tcc.Model.GetServicesModel;
import com.tcc.Model.GetVisitorModel;
import com.tcc.Model.SMSModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class VisitorFragment extends Fragment {

    SwipeRefreshLayout mSwipeRefreshLayout;
    AlertDialog alertDialog;
    ArrayList<GetVisitorModel.Datum> mArrayVisitor;
    ArrayList<GetServicesModel.Datum> mArrayServiceList;
    String ServiceName;
    private Activity mActivity;
    private View mView;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshView;
    private RelativeLayout mRelativeNoDataFound, mRelativeNoInternet;
    private Button mBtnRetry;
    private EditText text_searchname, text_email, textMobile, textLead, edt_visitor_phone, edt_visitor_company;
    private Button btn_visitorsearch;
    private VisitorListAdapter mCustomerListAdapter;
    private FloatingActionButton mFabSearch;
    private Spinner spinner_services_search;
    private int mSelectedServiceIndex = 0;
    private ArrayList<String> mArrServices;
    private boolean isBackFromB;

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinner_Service:
                    mSelectedServiceIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedServiceIndex != 0) {
                        ServiceName = String.valueOf((Integer.parseInt(mArrayServiceList.get(mSelectedServiceIndex - 1).getServiceID())));
                        String mCountryName = (mArrayServiceList.get(mSelectedServiceIndex - 1).getService());
                        Common.insertLog("mServiceId::> " + ServiceName);
                        Common.insertLog("mServiceName::> " + mCountryName);
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_customer, container, false);
        mActivity = getActivity();
        mArrayVisitor = new ArrayList<>();
        mArrayServiceList = new ArrayList<>();
        mArrServices = new ArrayList<>();

        getIds();
        getServices();
        setRegiListener();

        isBackFromB = false;

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mCustomerListAdapter);


        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mArrayVisitor.clear();
            mCustomerListAdapter.notifyDataSetChanged();
            GetVisitorList();
            mSwipeRefreshLayout.setRefreshing(false);
        });


        GetVisitorList();
        return mView;
    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isBackFromB) {//Do something
            mArrayVisitor.clear();
            GetVisitorList();
        }
    }

    private void GetVisitorList() {
        try {
            Common.showLoadingDialog(getActivity(), "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getVisitorListJson("-1", "1", "", "", "-1", "-1", "All", "", "", "-1"));

            Call<GetVisitorModel> call = RetrofitClient.createService(ApiInterface.class).getVisitorAPI(body);
            call.enqueue(new Callback<GetVisitorModel>() {
                @Override
                public void onResponse(@NonNull Call<GetVisitorModel> call, @NonNull Response<GetVisitorModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayVisitor.addAll(response.body().getData());
                            if (mArrayVisitor.isEmpty()) {
                                mRelativeNoDataFound.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                setAdapter();
                            }


                        } else {
                            mSwipeRefreshLayout.setEnabled(false);
                            mRelativeNoDataFound.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetVisitorModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    // Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getIds() {
        mSwipeRefreshLayout = mView.findViewById(R.id.swipeToRefresh);
        mRecyclerView = mView.findViewById(R.id.recycler_view);
        mSwipeRefreshView = mView.findViewById(R.id.swipe_container_recycler_view_screen);
        mRelativeNoDataFound = mView.findViewById(R.id.relative_no_data_available);
        mRelativeNoInternet = mView.findViewById(R.id.relative_no_internet);
        mBtnRetry = mView.findViewById(R.id.button_retry);
        mFabSearch = mView.findViewById(R.id.floating_customer_search);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorBlack);

    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mCustomerListAdapter = new VisitorListAdapter(mActivity, mArrayVisitor) {
            @Override
            protected void onEmailClick(String Email, String ID) {
                super.onEmailClick(Email, ID);

                Intent intent = new Intent(mActivity, EmailActivityDialog.class);
                intent.putExtra("ID", ID);
                intent.putExtra("Actionuser", "Visitor");
                intent.putExtra("Email", Email);
                startActivity(intent);
            }

            @Override
            protected void onSMSClick(String Phone) {
                super.onSMSClick(Phone);
                alterDialogForSendSMS(Phone);
            }
        };
        mRecyclerView.setAdapter(mCustomerListAdapter);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    /***
     * Show Alert for Send SMS
     */

    public void alterDialogForSendSMS(String Phone) {
        try {
            Dialog dialog = new Dialog(getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_sms_reminder);

            // Text View
            TextView mTextMobileNo = dialog.findViewById(R.id.text_dialog_sms_mobile_no);

            // Edit Text
            EditText edtMessage_EmailDialog = dialog.findViewById(R.id.edit_dialog_sms_message);

            // Sets up the data
            mTextMobileNo.setText(Phone);

            // Button
            Button mButtonSend = dialog.findViewById(R.id.button_dialog_sms_send);

            // ToDo: Button Yes Click Listener
            mButtonSend.setOnClickListener(v -> {
                if (checkValidationForSMS(edtMessage_EmailDialog.getText().toString())) {
                    sendEmail(mTextMobileNo.getText().toString(), edtMessage_EmailDialog.getText().toString());
                    dialog.dismiss();
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendEmail(String phone, String message) {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.sendSMSJson(phone, message));

            Call<SMSModel> call = RetrofitClient.createService(ApiInterface.class).sendSMSAPI(body);

            call.enqueue(new Callback<SMSModel>() {
                @Override
                public void onResponse(Call<SMSModel> call, Response<SMSModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            Toast.makeText(mActivity, "Message Sent Successfully", Toast.LENGTH_SHORT).show();
                        } else {
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<SMSModel> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkValidationForSMS(String msg) {
        boolean status = true;

        if (msg.equalsIgnoreCase("")) {
            status = false;
            Toast.makeText(mActivity, "Enter Message", Toast.LENGTH_SHORT).show();
        }

        return status;
    }

    private void getServices() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getVisitorServiceJson());

            Call<GetServicesModel> call = RetrofitClient.createService(ApiInterface.class).getVisitorServiceAPI(body);

            call.enqueue(new Callback<GetServicesModel>() {
                @Override
                public void onResponse(Call<GetServicesModel> call, Response<GetServicesModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            mArrayServiceList.addAll(response.body().getData());
                            setServiceAdapter();

                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<GetServicesModel> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setServiceAdapter() {
        try {
            mArrServices.add(0, getString(R.string.spinner_select_services));
            if (mArrayServiceList.size() > 0) {
                for (GetServicesModel.Datum Services : mArrayServiceList) {
                    mArrServices.add(Services.getService());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, mArrServices) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedServiceIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_services_search.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setRegiListener() {


        mFabSearch.setOnClickListener(v -> {

            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            dialogBuilder.setTitle("Search Visitor");
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.layout_visitor_search, null);
            dialogBuilder.setView(dialogView);

            alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.setCancelable(true);

            text_searchname = dialogView.findViewById(R.id.edt_visitor_name);
            text_email = dialogView.findViewById(R.id.edt_visitor_email);
            edt_visitor_company = dialogView.findViewById(R.id.edt_visitor_company);
            edt_visitor_phone = dialogView.findViewById(R.id.edt_visitor_phone);
            textLead = dialogView.findViewById(R.id.edt_visitor_lead);
            btn_visitorsearch = dialogView.findViewById(R.id.btn_visitor_search);
            spinner_services_search = dialogView.findViewById(R.id.spinner_services_search);

            spinner_services_search.setOnItemSelectedListener(onItemSelectedListener);

            btn_visitorsearch.setOnClickListener(v1 -> {
                if (!text_email.getText().toString().trim().equalsIgnoreCase("")) {
                    if (!text_email.getText().toString().trim().matches(emailPattern)) {
                        text_email.setError("Enter Valid Email");
                    } else {
                        String visitorName = text_searchname.getText().toString().trim();
                        String visitorEmail = text_email.getText().toString().trim();
                        String visitorCompnay = edt_visitor_company.getText().toString().trim();
                        String visitorPhone = edt_visitor_phone.getText().toString().trim();
                        getVisitorList(visitorName, visitorEmail, visitorCompnay, visitorPhone);
                    }
                } else {
                    String visitorName = text_searchname.getText().toString().trim();
                    String visitorEmail = text_email.getText().toString().trim();
                    String visitorCompnay = edt_visitor_company.getText().toString().trim();
                    String visitorPhone = edt_visitor_phone.getText().toString().trim();
                    getVisitorList(visitorName, visitorEmail, visitorCompnay, visitorPhone);
                }
            });
        });
    }

    private void getVisitorList(String visitorName, String visitorEmail, String visitorCompany, String visitorPhone) {
        try {
            mArrayVisitor.clear();
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getVisitorListJson("-1", "1", visitorName, visitorEmail, "-1", "-1", "All", visitorCompany, visitorName, "-1"));

            Call<GetVisitorModel> call = RetrofitClient.createService(ApiInterface.class).getVisitorAPI(body);
            call.enqueue(new Callback<GetVisitorModel>() {
                @Override
                public void onResponse(@NonNull Call<GetVisitorModel> call, @NonNull Response<GetVisitorModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayVisitor.addAll(response.body().getData());
                            alertDialog.dismiss();
                            setAdapter();

                        } else {

                            // mSwipeRefreshLayout.setEnabled(false);
                            mRelativeNoDataFound.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetVisitorModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
