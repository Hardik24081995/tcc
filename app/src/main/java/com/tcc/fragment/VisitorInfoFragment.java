package com.tcc.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.tcc.R;
import com.tcc.utils.SessionManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class VisitorInfoFragment extends Fragment {

    View mView;
    private TextView mTextName, mTextCompanyName, mTextPhone, mTextEmail, mTextAddress, mTextLeadType, mTextNoOfMen, mTextWorkingDays, mTextWorkingHours,
            mTextDate, mTextServices;
    private String Name, CompanyName, Mobile, Services, ProposedDate, WorkingDays, WorkingHours, NoOfMen, Address, Email, Lead;

    public VisitorInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_visitor_info, container, false);


        getID();
        getData();
        setData();

        return mView;
    }

    private void getID() {
        mTextName = mView.findViewById(R.id.text_visitor_name);
        mTextCompanyName = mView.findViewById(R.id.text_visitor_comapny_name);
        mTextPhone = mView.findViewById(R.id.text_visitor_phone);
        mTextEmail = mView.findViewById(R.id.text_visitor_email);
        mTextAddress = mView.findViewById(R.id.text_visitor_address);
        mTextLeadType = mView.findViewById(R.id.text_visitor_lead_type);
        mTextNoOfMen = mView.findViewById(R.id.text_visitor_no_of_power);
        mTextWorkingDays = mView.findViewById(R.id.text_visitor_working_days);
        mTextWorkingHours = mView.findViewById(R.id.text_visitor_working_hours);
        mTextDate = mView.findViewById(R.id.text_visitor_date);
        mTextServices = mView.findViewById(R.id.text_visitor_service);

    }

    private void getData() {
        SessionManager manager = new SessionManager(getActivity());
        Name = manager.getPreferences("Name", "");
        CompanyName = manager.getPreferences("CompanyName", "");
        Mobile = manager.getPreferences("Mobile", "");
        Services = manager.getPreferences("Service", "");
        ProposedDate = manager.getPreferences("ProposedDate", "");
        WorkingDays = manager.getPreferences("WorkingDays", "");
        WorkingHours = manager.getPreferences("WorkingHours", "");
        NoOfMen = manager.getPreferences("NoOfMen", "");
        Address = manager.getPreferences("Address", "");
        Email = manager.getPreferences("Email", "");
        Lead = manager.getPreferences("LeadType", "");
    }

    private void setData() {
        mTextName.setText(Name);
        mTextCompanyName.setText(CompanyName);
        mTextPhone.setText(Mobile);
        mTextAddress.setText(Address);
        mTextDate.setText(ProposedDate);
        mTextLeadType.setText(Lead);
        mTextServices.setText(Services);
        mTextWorkingDays.setText(WorkingDays);
        mTextWorkingHours.setText(WorkingHours);
        mTextNoOfMen.setText(NoOfMen);
        mTextEmail.setText(Email);

    }

}
