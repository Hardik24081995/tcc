package com.tcc.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.TrainingAdapter;
import com.tcc.Model.GetEmployeeTrainingModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmployeeTrainingFragment extends Fragment {

    Activity mActivity;
    View mView;
    TrainingAdapter mAdapter;
    RecyclerView rv_training;
    RelativeLayout mRelativeNoDataFound;
    ArrayList<GetEmployeeTrainingModel.Datum> mArrayTraining = new ArrayList<>();
    SessionManager mSessionManager;
    String EmployeeID;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean isBackFromB;

    public EmployeeTrainingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_training, container, false);
        isBackFromB = false;
        mActivity = getActivity();

        mSessionManager = new SessionManager(mActivity);
        EmployeeID = mSessionManager.getPreferences(AppConstants.EMPLOYEE_ID_FOR_ADD, "");


        getID();
        getEmployeeTrainging();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getEmployeeTrainging();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        return mView;
    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        if (isBackFromB) {
            //Refresh your stuff here
            getEmployeeTrainging();
        }

    }

    private void getID() {
        rv_training = mView.findViewById(R.id.rv_training);
        mSwipeRefreshLayout = (SwipeRefreshLayout) mView.findViewById(R.id.swipeToRefresh);
        mRelativeNoDataFound = mView.findViewById(R.id.relative_no_data_available);
    }

    private void getEmployeeTrainging() {
        mArrayTraining.clear();
        try {
            Common.showLoadingDialog(getActivity(), "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getEmployeeTrainingJson("-1", "1", EmployeeID));

            Call<GetEmployeeTrainingModel> call = RetrofitClient.createService(ApiInterface.class).getEmployeeTrainingAPI(body);
            call.enqueue(new Callback<GetEmployeeTrainingModel>() {
                @Override
                public void onResponse(@NonNull Call<GetEmployeeTrainingModel> call, @NonNull Response<GetEmployeeTrainingModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayTraining.addAll(response.body().getData());
                            if (response.body() != null) {
                                mRelativeNoDataFound.setVisibility(View.GONE);
                                rv_training.setVisibility(View.VISIBLE);
                                setAdapter();
                            } else {
                                mRelativeNoDataFound.setVisibility(View.VISIBLE);
                                rv_training.setVisibility(View.GONE);
                            }

                        } else {

                            mRelativeNoDataFound.setVisibility(View.VISIBLE);
                            rv_training.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetEmployeeTrainingModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //   Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {

        mAdapter = new TrainingAdapter(getActivity(), mArrayTraining);
        rv_training.setHasFixedSize(true);
        rv_training.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_training.setAdapter(mAdapter);

    }

}
