package com.tcc.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.UniformAdapter;
import com.tcc.Model.GetEmployeeUniformModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmployeeUniformFragment extends Fragment {

    SessionManager mSessionManager;
    SwipeRefreshLayout mSwipeRefreshLayout;
    View mView;
    UniformAdapter mAdapter;
    RecyclerView rv_uniform;
    RelativeLayout relative_no_data_available;
    ArrayList<GetEmployeeUniformModel.Datum> mArrUniform;
    private String UserID, UserIDforAdd;
    private boolean isBackFromB;

    public EmployeeUniformFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_uniform, container, false);
        mArrUniform = new ArrayList<>();
        isBackFromB = false;

        mSessionManager = new SessionManager(getActivity());
        String UserData = mSessionManager.getPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(getActivity(), WebFields.LOGIN.RESPONSE_USERID);
        UserIDforAdd = mSessionManager.getPreferences(AppConstants.EMPLOYEE_ID_FOR_ADD, "");

        rv_uniform = mView.findViewById(R.id.rv_uniform);
        relative_no_data_available = mView.findViewById(R.id.relative_no_data_available);
        mSwipeRefreshLayout = (SwipeRefreshLayout) mView.findViewById(R.id.swipeToRefresh);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorBlack);

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getUniformList();
            mSwipeRefreshLayout.setRefreshing(false);
        });

        getUniformList();

        return mView;
    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();

        if (isBackFromB) {
            //Refresh your stuff here
            getUniformList();
        }
    }

    private void getUniformList() {
        mArrUniform.clear();
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getEmployeeUniformListJson("-1", "1", UserIDforAdd));

            Call<GetEmployeeUniformModel> call = RetrofitClient.createService(ApiInterface.class).getEmployeeUniformListAPI(body);
            call.enqueue(new Callback<GetEmployeeUniformModel>() {
                @Override
                public void onResponse(@NonNull Call<GetEmployeeUniformModel> call, @NonNull Response<GetEmployeeUniformModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrUniform.addAll(response.body().getData());
                            if (mArrUniform.isEmpty()) {
                                relative_no_data_available.setVisibility(View.VISIBLE);
                                rv_uniform.setVisibility(View.GONE);
                            } else {
                                relative_no_data_available.setVisibility(View.GONE);
                                rv_uniform.setVisibility(View.VISIBLE);
                                setAdapter();
                            }
                        } else {
                            relative_no_data_available.setVisibility(View.VISIBLE);
                            rv_uniform.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetEmployeeUniformModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    // Common.setCustomToast(getActivity(), t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        mAdapter = new UniformAdapter(getActivity(), mArrUniform);
        rv_uniform.setHasFixedSize(true);
        rv_uniform.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_uniform.setAdapter(mAdapter);
    }

}
