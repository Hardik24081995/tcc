package com.tcc.fragment;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.tcc.Adapter.InvoiceAdapter;
import com.tcc.Adapter.SitesAdapter;
import com.tcc.Model.GetInvoiceModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvoiceFragment extends Fragment implements InvoiceAdapter.pdfDownload {

    Activity mActivity;
    RelativeLayout mRelativeNoDataFound;
    TextView text_no_data_available;
    View mView;
    SitesAdapter mAdapter;
    RecyclerView rv_sites;
    ImageView mImageAdd;
    ArrayList<GetInvoiceModel.Datum> myArrayList;
    AlertDialog alertDialog;
    int downloadedSize = 0, totalsize;
    float per = 0;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private FloatingActionButton mFabSearch;

    public InvoiceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_invoice, container, false);

        myArrayList = new ArrayList<>();
        mActivity = getActivity();

        getID();
        setRegister();
        getInvoice();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getInvoice();
            mSwipeRefreshLayout.setRefreshing(false);
        });

        return mView;
    }

    private void setRegister() {

        mFabSearch.setOnClickListener(v -> {

            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            dialogBuilder.setTitle("Search Site");
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.layout_sitesearch, null);
            dialogBuilder.setView(dialogView);

            alertDialog = dialogBuilder.create();
            alertDialog.show();
            alertDialog.setCancelable(true);

            EditText text_searchname = dialogView.findViewById(R.id.edt_customer_name);
            EditText text_email = dialogView.findViewById(R.id.edt_customer_email);
            Button btn_customersearch = dialogView.findViewById(R.id.btn_customer_search);

           /* btn_customersearch.setOnClickListener(v1 -> {
                String customerName = text_searchname.getText().toString().trim();
                String customerEmail = text_email.getText().toString().trim();
                if (customerEmail.equalsIgnoreCase("")) {
                    customerEmail = customerName;
                    getSites(customerName, customerEmail);
                } else {
                    getSites(customerName, customerEmail);
                }
            });*/
        });
    }


    private void getInvoice() {
        myArrayList.clear();
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getInvoiceListJson("-1", "1", "-1"));
            Common.showLoadingDialog(getActivity(), "Loading");
            Call<GetInvoiceModel> call = RetrofitClient.createService(ApiInterface.class).getInvoceAPI(body);
            call.enqueue(new Callback<GetInvoiceModel>() {
                @Override
                public void onResponse(@NonNull Call<GetInvoiceModel> call, @NonNull Response<GetInvoiceModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayList.addAll(response.body().getData());
                            if (myArrayList.isEmpty()) {
                                mRelativeNoDataFound.setVisibility(View.VISIBLE);
                                rv_sites.setVisibility(View.GONE);
                            } else {
                                mRelativeNoDataFound.setVisibility(View.GONE);
                                rv_sites.setVisibility(View.VISIBLE);
                                setAdapter();
                            }

                        } else {
                            mRelativeNoDataFound.setVisibility(View.VISIBLE);
                            rv_sites.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetInvoiceModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setAdapter() {
        InvoiceAdapter sitesAdapter = new InvoiceAdapter(getContext(), myArrayList, this);
        rv_sites.setHasFixedSize(true);
        rv_sites.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_sites.setAdapter(sitesAdapter);
    }

    private void getID() {

        rv_sites = mView.findViewById(R.id.rv_sites);
        mImageAdd = mView.findViewById(R.id.image_custom_toolbar_add);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipeToRefresh);
        mRelativeNoDataFound = mView.findViewById(R.id.relative_no_data_available);
        text_no_data_available = mView.findViewById(R.id.text_no_data_available);
        mFabSearch = mView.findViewById(R.id.floating_site_search);
    }

    @Override
    public void downloadPDF(int position, GetInvoiceModel.Datum data) {

        String path = "http://societyfy.in/TheCleaingCompany/assets/uploads/invoice/";

        String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
        String fileName = "filename.pdf";
        destination += fileName;
        final Uri uri = Uri.parse("file://" + destination);

        String url = path + data.getInvoice(); //paste url here

        if (data.getInvoice().equalsIgnoreCase("")) {
            Toast.makeText(getContext(), "Document Currently Unavailable", Toast.LENGTH_SHORT).show();
        } else {
            Intent viewIntent = new Intent("android.intent.action.VIEW",
                    Uri.parse(url));
            getContext().startActivity(viewIntent);
        }

/*        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription("Downloading....");
        request.setTitle(" TITLE ");
        request.setDestinationUri(uri);

        final DownloadManager manager = (DownloadManager) getContext().getSystemService(Context.DOWNLOAD_SERVICE);
        final long downloadId = manager.enqueue(request);

        final String finalDestination = destination;
        BroadcastReceiver onComplete = new BroadcastReceiver() {
            public void onReceive(Context ctxt, Intent intent) {
                Log.d("Update status", "Download completed");
                Toast.makeText(getContext(), "Download Complete", Toast.LENGTH_LONG).show();

                File file = new File(finalDestination);
                Uri apkURI = FileProvider.getUriForFile(getContext().getApplicationContext(), getContext().getPackageName() + ".provider", file);
                new Thread(new Runnable() {
                    public void run() {
                        Uri path = Uri.fromFile(new File(finalDestination));
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                intent.setDataAndType(apkURI, "application/pdf");
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            } else {
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.setDataAndType(path, "application/pdf");
                            }
                            startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                    text_no_data_available
                                    .setError("PDF Reader application is not installed in your device");
                      }
                    }
                }).start();
                getContext().unregisterReceiver(this);
            }
        };*/

        //getContext().registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        // downloadAndOpenPDF(path +data.getInvoice());

    }

    void downloadAndOpenPDF(String url) {


        new Thread(new Runnable() {
            public void run() {
                File file = downloadFile(url);
                Uri path = Uri.fromFile(file);
                Uri apkURI = FileProvider.getUriForFile(getContext().getApplicationContext(), getContext().getPackageName() + ".provider", file);
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        intent.setDataAndType(apkURI, "application/pdf");
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    } else {
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.setDataAndType(path, "application/pdf");
                    }
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                          /*//  text_no_data_available
                                    .setError("PDF Reader application is not installed in your device");
*/
                }
            }
        }).start();

    }

    File downloadFile(String dwnload_file_path) {
        File file = null;
        try {

            URL url = new URL(dwnload_file_path);
            HttpURLConnection urlConnection = (HttpURLConnection) url
                    .openConnection();

            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);

            // connect
            urlConnection.connect();

            // set the path where we want to save the file
            File SDCardRoot = Environment.getExternalStorageDirectory();
            // create a new file, to save the downloaded file
            file = new File(SDCardRoot, "test.pdf");

            FileOutputStream fileOutput = new FileOutputStream(file);

            // Stream used for reading the data from the internet
            InputStream inputStream = urlConnection.getInputStream();

            // this is the total size of the file which we are
            // downloading
            totalsize = urlConnection.getContentLength();
            setText("Starting PDF download...");

            // create a buffer...
            byte[] buffer = new byte[1024 * 1024];
            int bufferLength = 0;

            while ((bufferLength = inputStream.read(buffer)) > 0) {
                fileOutput.write(buffer, 0, bufferLength);
                downloadedSize += bufferLength;
                per = ((float) downloadedSize / totalsize) * 100;
                setText("Total PDF File size  : "
                        + (totalsize / 1024)
                        + " KB\n\nDownloading PDF " + (int) per
                        + "% complete");
            }
            // close the output stream when complete //
            fileOutput.close();
            setText("Download Complete. Open PDF Application installed in the device.");

        } catch (final MalformedURLException e) {
            setTextError("Some error occured. Press back and try again.",
                    Color.RED);
        } catch (final IOException e) {
            setTextError("Some error occured. Press back and try again.",
                    Color.RED);
        } catch (final Exception e) {
            setTextError(
                    "Failed to download image. Please check your internet connection.",
                    Color.RED);
        }
        return file;
    }

    void setTextError(final String message, final int color) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {

                text_no_data_available.setText(message);
            }
        });

    }

    void setText(final String txt) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                text_no_data_available.setText(txt);
            }
        });

    }


}

