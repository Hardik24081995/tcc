package com.tcc.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Activity.InspectionSummaryActivity;
import com.tcc.Adapter.InspectionListAdapter;
import com.tcc.Model.GetInspectionListModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class InspectionFragment extends Fragment {


    Activity mActivity;
    View mView;
    RelativeLayout mRelativeNoDataFound;
    InspectionListAdapter mAdapter;
    ArrayList<GetInspectionListModel.Datum> mArrayInspection;
    SessionManager mSession;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean isBackFromB;

    public InspectionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_survey, container, false);
        mActivity = getActivity();
        mSession = new SessionManager(mActivity);
        mArrayInspection = new ArrayList<>();

        isBackFromB = false;

        initView();
        return mView;
    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isBackFromB) {
            getInspectionListing();
        }

    }

    private void initView() {
        getid();
        getInspectionListing();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorBlack);
    }

    private void getInspectionListing() {
        try {
            mArrayInspection.clear();
            Common.showLoadingDialog(getActivity(), "Loading");
            SessionManager mSession = new SessionManager(mActivity);
            String UserData = mSession.getPreferences(mSession.KEY_LOGIN_USER_DATA, "");
            String UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getInspectionJson("-1", "1", UserID));

            Call<GetInspectionListModel> call = RetrofitClient.createService(ApiInterface.class).getInspectionAPI(body);
            call.enqueue(new Callback<GetInspectionListModel>() {
                @Override
                public void onResponse(@NonNull Call<GetInspectionListModel> call, @NonNull Response<GetInspectionListModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayInspection.addAll(response.body().getData());
                            if (mArrayInspection.isEmpty()) {
                                mRelativeNoDataFound.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                setAdapter();
                            }
                        }
                    } catch (JSONException e) {
                        Common.hideDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetInspectionListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.hideDialog();
                    Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new InspectionListAdapter(mActivity, mArrayInspection) {
            @Override
            protected void onInspectionSummary(GetInspectionListModel.Datum datum) {
                super.onInspectionSummary(datum);
                for (int pos = 0; pos < mArrayInspection.size(); pos++) {
                    if (mArrayInspection.get(pos).getInspectionID().equalsIgnoreCase(datum.getInspectionID())) {
                        Intent intent = new Intent(mActivity, InspectionSummaryActivity.class);
                        mSession.setPreferences("inspectionDate", datum.getInspectionDate());
                        mSession.setPreferences("siteName", datum.getCompanyName());
                        mSession.setPreferences("employeeName", datum.getEmployeeName());
                        mSession.setPreferences("employeeType", datum.getUserType());
                        mSession.setPreferences("remarks", datum.getRemarks());
                        mSession.setPreferences("image", datum.getImage());
                        mSession.setPreferences("id", String.valueOf(pos));
                        startActivity(intent);
                    }
                }
            }
        };
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void getid() {
        mRecyclerView = mView.findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) mView.findViewById(R.id.swipeToRefresh);
        mRelativeNoDataFound = mView.findViewById(R.id.relative_no_data_available);

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mArrayInspection.clear();
            getInspectionListing();
            mSwipeRefreshLayout.setRefreshing(false);
        });
    }

}
