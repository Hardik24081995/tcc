package com.tcc.fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import com.tcc.Activity.BarchartActivity;
import com.tcc.Activity.ReportTraningActivity;
import com.tcc.Activity.ReportUniformActivity;
import com.tcc.R;
import com.tcc.utils.AppUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ReportsFragment extends Fragment {

    Boolean isBackFromB;
    View mView;
    Button btnPayment, btnTraining, btnUniform;
    EditText edtStartDate, edtEndDate;
    private int sYear, sMonth, sDay;
    private int lYear, lMonth, lDay;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragments_reports, container, false);

        isBackFromB = false;
        getIds();


        return mView;
    }

    private void getIds() {
        btnPayment = mView.findViewById(R.id.btnPayment);
        btnTraining = mView.findViewById(R.id.btnTraining);
        btnUniform = mView.findViewById(R.id.btnUniform);
        edtStartDate = mView.findViewById(R.id.edtStartDate);
        edtEndDate = mView.findViewById(R.id.edtEndDate);

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c);
        edtStartDate.setText(formattedDate);

        Date c2 = Calendar.getInstance().getTime();
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate2 = df2.format(c2);
        edtEndDate.setText(formattedDate2);

        btnTraining.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(requireContext(), ReportTraningActivity.class);
                String sdate = AppUtil.convertDate(requireContext(), edtStartDate.getText().toString());
                String ldate = AppUtil.convertDate(requireContext(), edtEndDate.getText().toString());
                i.putExtra("sDate", sdate);
                i.putExtra("lDate", ldate);
                startActivity(i);
            }
        });

        btnUniform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(requireContext(), ReportUniformActivity.class);
                String sdate = AppUtil.convertDate(requireContext(), edtStartDate.getText().toString());
                String ldate = AppUtil.convertDate(requireContext(), edtEndDate.getText().toString());
                i.putExtra("sDate", sdate);
                i.putExtra("lDate", ldate);
                startActivity(i);
            }
        });

        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(requireContext(), BarchartActivity.class);
                String sdate = AppUtil.convertDate(requireContext(), edtStartDate.getText().toString());
                String ldate = AppUtil.convertDate(requireContext(), edtEndDate.getText().toString());
                i.putExtra("sDate", sdate);
                i.putExtra("lDate", ldate);
                startActivity(i);
            }
        });

        edtStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectStartDate();
            }
        });

        edtEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectEndDate();
            }
        });
    }

    private void selectStartDate() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        sYear = c.get(Calendar.YEAR);
        sMonth = c.get(Calendar.MONTH);
        sDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(),
                (view, year, monthOfYear, dayOfMonth) -> edtStartDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), sYear, sMonth, sDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void selectEndDate() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        lYear = c.get(Calendar.YEAR);
        lMonth = c.get(Calendar.MONTH);
        lDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(),
                (view, year, monthOfYear, dayOfMonth) -> edtEndDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), lYear, lMonth, lDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

}
