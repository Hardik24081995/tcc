package com.tcc.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.CustomerSiteListAdapter;
import com.tcc.Model.GetSiteModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerSiteFragment extends Fragment {

    Activity mActivity;
    SessionManager mSessionManager;
    RelativeLayout mRelativeNoDataFound;
    View mView;
    CustomerSiteListAdapter mAdapter;
    RecyclerView rv_sites;
    ImageView mImageAdd;
    ArrayList<GetSiteModel.Datum> myArrayList;
    String CustomerID;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean isBackFromB;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_property_profile, container, false);
        isBackFromB = false;

        mActivity = getActivity();
        myArrayList = new ArrayList<>();
        mSessionManager = new SessionManager(mActivity);
        CustomerID = mSessionManager.getPreferences("CustomerID", "");

        getID();
        getSite();

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            myArrayList.clear();
            getSite();
            mSwipeRefreshLayout.setRefreshing(false);
        });


        return mView;
    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        if (isBackFromB) {
            //Refresh your stuff here
            myArrayList.clear();
            getSite();
        }
    }

    private void getSite() {
        try {
            Common.showLoadingDialog(getActivity(), "Loading");
//            mArrayCustomer.clear();
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getSiteListJson("-1", "1", "-1", CustomerID, "-1"
                    , "", ""));

            Call<GetSiteModel> call = RetrofitClient.createService(ApiInterface.class).getSiteAPI(body);
            call.enqueue(new Callback<GetSiteModel>() {
                @Override
                public void onResponse(@NonNull Call<GetSiteModel> call, @NonNull Response<GetSiteModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayList.addAll(response.body().getData());
//                            alertDialog.dismiss();
                            if (response.body() == null) {
                                mRelativeNoDataFound.setVisibility(View.VISIBLE);
                                rv_sites.setVisibility(View.GONE);
                            } else {
                                mRelativeNoDataFound.setVisibility(View.GONE);
                                rv_sites.setVisibility(View.VISIBLE);
                                setAdapter();
                            }

                        } else {
                            mRelativeNoDataFound.setVisibility(View.VISIBLE);
                            rv_sites.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetSiteModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv_sites.setLayoutManager(mLayoutManager);
        mAdapter = new CustomerSiteListAdapter(mActivity, myArrayList);
        rv_sites.setAdapter(mAdapter);
        rv_sites.setVisibility(View.VISIBLE);
    }

    private void getID() {

        rv_sites = mView.findViewById(R.id.rv_sites);
        mImageAdd = mView.findViewById(R.id.image_custom_toolbar_add);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipeToRefresh);
        mRelativeNoDataFound = mView.findViewById(R.id.relative_no_data_available_customeer);
    }

}
