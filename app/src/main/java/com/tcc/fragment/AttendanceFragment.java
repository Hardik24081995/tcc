package com.tcc.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.AttendanceAdapter;
import com.tcc.Model.GetEmployeeAttendance;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AttendanceFragment extends Fragment {

    Activity mActivity;
    View mView;
    AttendanceAdapter mAdapter;
    RecyclerView mRecyclerView;
    ArrayList<GetEmployeeAttendance.Datum> rowsArrayList = new ArrayList<>();
    SwipeRefreshLayout swipeRefreshLayout;
    RelativeLayout mRelativeNoDataFound;
    SessionManager manager;
    String UserID, UserIDforAdd;

    public AttendanceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_attendance, container, false);

        mActivity = getActivity();
        manager = new SessionManager(mActivity);
        String UserData = manager.getPreferences(manager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USERID);
        UserIDforAdd = manager.getPreferences(AppConstants.EMPLOYEE_ID_FOR_ADD, "");

        getID();
        getEmployeeList();

        swipeRefreshLayout.setOnRefreshListener(() -> {
            getEmployeeList();
            swipeRefreshLayout.setRefreshing(false);
        });

        return mView;
    }

    private void getEmployeeList() {
        rowsArrayList.clear();
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getEmployeeAttendanceJson("-1", "1", UserIDforAdd));

            Call<GetEmployeeAttendance> call = RetrofitClient.createService(ApiInterface.class).getEmployeeAttendanceAPI(body);
            call.enqueue(new Callback<GetEmployeeAttendance>() {
                @Override
                public void onResponse(@NonNull Call<GetEmployeeAttendance> call, @NonNull Response<GetEmployeeAttendance> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            rowsArrayList.addAll(response.body().getData());

                            if (rowsArrayList.isEmpty()) {
                                mRelativeNoDataFound.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            } else {
                                mRelativeNoDataFound.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                                setAdapter();
                            }

                        } else {
                            mRelativeNoDataFound.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                            //  Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetEmployeeAttendance> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    // Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        AttendanceAdapter sitesAdapter = new AttendanceAdapter(getContext(), rowsArrayList);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(sitesAdapter);
    }

    private void getID() {
        swipeRefreshLayout = mView.findViewById(R.id.swipeToRefresh);
        mRecyclerView = mView.findViewById(R.id.rv_attendance);
        mRelativeNoDataFound = mView.findViewById(R.id.relative_no_data_available);
    }

}
