package com.tcc.fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.gson.Gson;
import com.tcc.Activity.HomeActivity;
import com.tcc.Model.CityModel;
import com.tcc.Model.GetDashboardModal;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.AppUtil;
import com.tcc.utils.ApplicationConfiguration;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements AdapterView.OnItemSelectedListener {


    View mView;
    Activity mActivity;
    ArrayList<CityModel.Datum> mArrayCityList = new ArrayList<>();
    String[] country = {"All", "Jamnagar", "Gwalior", "Mumbai", "Ahmedabad"};
    LineChart mLineChart;
    SessionManager sessionManager;
    // @BindView(R.id.about) edtitext user;
    int selectedRadiobutton;
    ArrayList<String> severityStringList = new ArrayList<>();
    ArrayList<String> mArrCity = new ArrayList<>();
    LinearLayout mLinearFullView, linear_home_total_customer, linear_home_total_sites, linear_home_total_visitor, linear_home_tota_collection, linear_home_followup;
    TextView text_home_total_customer, text_total_site, text_home_total_visitor, text_home_total_collection, text_home_total_followup;
    private Spinner spinnerCity;
    private RadioGroup mRadioGroupHome;
    private String mFilePath, CityID = "";
    private int mSelectedCityIndex = 0, mCityId;
    // private BarChart chart;
    //LineChartView chart;
    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinnerCity:
                    mSelectedCityIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedCityIndex != 0) {
                        CityID = String.valueOf((Integer.parseInt(mArrayCityList.get(mSelectedCityIndex - 1).getCityID())));
                        String mCityName = (mArrayCityList.get(mSelectedCityIndex - 1).getCityName());
                        Common.insertLog("mCityId::> " + CityID);
                        Common.insertLog("mCityName::> " + mCityName);
                    }
                    break;


            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(mView);
        mActivity = requireActivity();
        sessionManager = new SessionManager(mActivity);

        String token = sessionManager.getPreferences("token", "");
        Log.d("tokan", token);

        spinnerCity = (Spinner) mView.findViewById(R.id.spinnerCity);
        spinnerCity.setOnItemSelectedListener(this);


        getIds();
        // setChartData();
        selectedRadiobutton = R.id.dailyRadio;
        setTheme();
        callToCityAPI(-1);
        callDashboardCountAPI(ApplicationConfiguration.FilterType.Daily.toString());
        setRegListeners();
        return mView;
    }

    private void getIds() {
        mLineChart = mView.findViewById(R.id.barchart);

        mLinearFullView = mView.findViewById(R.id.mLinearFullView);
        //  chart = new LineChartView(requireContext());
        //  mLinearFullView.addView(chart);
        linear_home_total_customer = mView.findViewById(R.id.linear_home_total_customer);
        linear_home_total_sites = mView.findViewById(R.id.linear_home_total_sites);
        linear_home_total_visitor = mView.findViewById(R.id.linear_home_total_visitor);
        linear_home_tota_collection = mView.findViewById(R.id.linear_home_tota_collection);
        text_home_total_customer = mView.findViewById(R.id.text_home_total_customer);
        text_home_total_visitor = mView.findViewById(R.id.text_home_total_visitor);
        text_home_total_collection = mView.findViewById(R.id.text_home_total_collection);
        text_total_site = mView.findViewById(R.id.text_total_site);
        linear_home_followup = mView.findViewById(R.id.linear_home_followup);
        text_home_total_followup = mView.findViewById(R.id.text_home_total_followup);

        mRadioGroupHome = mView.findViewById(R.id.radio_group_home);
        spinnerCity = mView.findViewById(R.id.spinnerCity);
        spinnerCity.setOnItemSelectedListener(onItemSelectedListener);

        // setChartDATAT();


        // chart.setDrawLegend(false);
    }

    /*private void setChartDATAT(){
        chart.setInteractive(true);

       // chart.setContainerScrollEnabled(true,null);
       // ChartData.setAxisXBottom(Axis axisX);
       // ColumnChartData.setStacked(boolean isStacked);
       // Line.setStrokeWidth(int strokeWidthDp);

        List<PointValue> values = new ArrayList<PointValue>();
        values.add(new PointValue(0, 2));
        values.add(new PointValue(1, 4));
        values.add(new PointValue(2, 3));
        values.add(new PointValue(3, 4));

        //In most cased you can call data model methods in builder-pattern-like manner.
        Line line = new Line(values).setColor(Color.BLUE).setCubic(true);
        List<Line> lines = new ArrayList<Line>();
        lines.add(line);

        LineChartData data = new LineChartData();
        data.setLines(lines);

        chart.setLineChartData(data);
    }*/

    /**
     * Sets up the theme
     */
    private void setTheme() {
        try {
            linear_home_total_customer.setBackground(mActivity.getResources().getDrawable(R.drawable.sky_blue_circle));
            linear_home_tota_collection.setBackground(mActivity.getResources().getDrawable(R.drawable.sky_blue_circle));
            linear_home_total_sites.setBackground(mActivity.getResources().getDrawable(R.drawable.sky_blue_circle));
            linear_home_total_visitor.setBackground(mActivity.getResources().getDrawable(R.drawable.sky_blue_circle));
            linear_home_followup.setBackground(mActivity.getResources().getDrawable(R.drawable.sky_blue_circle));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {

        linear_home_followup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton radioButton6 = mView.findViewById(selectedRadiobutton);
                Fragment fragment = new FollowUPFragment();
                Bundle b = new Bundle();
                b.putString("filter", radioButton6.getText().toString());
                fragment.setArguments(b);
                ((HomeActivity) getActivity()).changeFragment(fragment, "FOLLOW UP");
                // changeFragment(new ProfileTabFragment(), "test");
            }
        });


        linear_home_total_visitor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new VisitorFragment();
                ((HomeActivity) getActivity()).changeFragment(fragment, mActivity.getString(R.string.nav_menu_visitor));
            }
        });

        linear_home_total_sites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new SiteFragment();
                ((HomeActivity) getActivity()).changeFragment(fragment, mActivity.getString(R.string.nav_menu_sites));
            }
        });

        linear_home_total_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new CustomerFragment();
                ((HomeActivity) getActivity()).changeFragment(fragment, mActivity.getString(R.string.nav_menu_customer));
            }
        });

        /*linear_home_total_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new CustomerFragment();
                ((HomeActivity) getActivity()).changeFragment(fragment, mActivity.getString(R.string.nav_menu_customer));
            }
        });*/

        linear_home_tota_collection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton radioButton6 = mView.findViewById(selectedRadiobutton);
                Fragment fragment = new CollectionFragment();
                Bundle b = new Bundle();
                b.putString("filter", radioButton6.getText().toString());
                fragment.setArguments(b);
                ((HomeActivity) getActivity()).changeFragment(fragment, "Collection");
                // changeFragment(new ProfileTabFragment(), "test");
            }
        });


        try {
            mRadioGroupHome.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    selectedRadiobutton = checkedId;
                    checkRadioButtonSelected(checkedId);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        mLineChart.clear();
        setChartData();
//        Toast.makeText(getActivity(),country[position] , Toast.LENGTH_LONG).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {


    }

    /**
     * This method should call the City API
     */
    private void callToCityAPI(int mStateId) {
        try {
            if (mArrCity.size() > 0)
                mArrCity.clear();
            if (mArrayCityList.size() > 0)
                mArrayCityList.clear();

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCityListJson(String.valueOf(mStateId)));

            Call<CityModel> call = RetrofitClient.createService(ApiInterface.class).getCityListAPI(body);
            call.enqueue(new Callback<CityModel>() {
                @Override
                public void onResponse(@NonNull Call<CityModel> call, @NonNull Response<CityModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                mArrayCityList.addAll(response.body().getData());
                                setCityAdapter();
                            }
                        } else {
                            setCityAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CityModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setCityAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setCityAdapter() {
        try {
            mArrCity.add(0, getString(R.string.spinner_select_city));

            if (mArrayCityList.size() > 0) {
                for (CityModel.Datum cityModel : mArrayCityList) {
                    mArrCity.add(cityModel.getCityName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrCity) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCityIndex) {
                        // Set spinner selected popup item's text color
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerCity.setAdapter(adapter);

            for (int i = 0; i < mArrCity.size(); i++) {
                if (mArrCity.get(i).equals("Ahmedabad")) {
                    spinnerCity.setSelection(i);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setChartData() {
        mLineChart.setDrawGridBackground(true);
        mLineChart.getDescription().setEnabled(true);
        mLineChart.setDrawBorders(false);

        mLineChart.getAxisLeft().setEnabled(true);
        mLineChart.getAxisRight().setDrawAxisLine(true);
        mLineChart.getAxisRight().setDrawGridLines(true);
        mLineChart.getXAxis().setDrawAxisLine(true);
        mLineChart.getXAxis().setDrawGridLines(true);
        mLineChart.getXAxis().setGranularity(1f); // minimum axis-step (interval) is 1

        // enable touch gestures
        mLineChart.setTouchEnabled(true);

        // enable scaling and dragging
        mLineChart.setDragEnabled(true);
        mLineChart.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mLineChart.setPinchZoom(true);
        Legend l = mLineChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);

        getData(mLineChart);
    }

    public void getData(LineChart lineChart) {
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        ArrayList<Entry> LeftAct = new ArrayList<>();
        ArrayList<Entry> RightAct = new ArrayList<>();

        final ArrayList<String> xAxisLabel = new ArrayList<>();


        for (int l = 0; l < 10; l++) {

            float a = 10;
            float pos = l;

            if (l > 3 && l < 6) {

                if (l % 2 == 0)
                    a = Float.parseFloat(String.valueOf(15.0));
            }

            LeftAct.add(new Entry(pos, a));



            /*for (int i=0;i<mArrHistory.get(l).getPreExamination().size();i++)
            {
                float Left=0;
                if (!mArrHistory.get(l).getPreExamination().get(i).getLNCT()
                        .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){

                    Left= Float.parseFloat(mArrHistory.get(l).getPreExamination().get(i).getLNCT());
                }
                LeftAct.add(new Entry(pos,Left));

                float right=0;
                if (!mArrHistory.get(l).getPreExamination().get(i).
                        getRNCT().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){

                    right= Float.parseFloat(mArrHistory.get(l).getPreExamination().get(i).getRNCT());
                }
                RightAct.add(new Entry(pos,right));

                xAxisLabel.add(mArrHistory.get(l).getTreatmentDate());

                // Date.add(new Entry(pos,mArrHistory.get(l).getTreatmentDate()));
            }*/
        }




      /*  LineDataSet d = new LineDataSet(LeftAct, name);
        d.setLineWidth(3.0f);
        d.setCircleRadius(5f);

        d.setColor(Color.RED);//
        d.setCircleColor(Color.RED);
        dataSets.add(d);
        d.setAxisDependency(YAxis.AxisDependency.LEFT);*/

        /*LineDataSet right = new LineDataSet(RightAct,"Right ACT");
        right.setLineWidth(3.0f);
        right.setCircleRadius(5f);

        right.setColor(Color.GREEN);
        right.setCircleColor(Color.GREEN);
        dataSets.add(right);
        right.setAxisDependency(YAxis.AxisDependency.LEFT);*/

/*
        lineChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xAxisLabel));

        LineData data = new LineData(dataSets);
        lineChart.setData(data);

        lineChart.invalidate();
        lineChart.animateXY(1500, 1000); // animate horizontal and vertical 3000 milliseconds*/
    }


    /**
     * Check radio button selected or not
     *
     * @param checkedId - Checked Id
     */
    private void checkRadioButtonSelected(int checkedId) {
        if (checkedId == R.id.dailyRadio) {
            callDashboardCountAPI(ApplicationConfiguration.FilterType.Daily.toString());
        } else if (checkedId == R.id.weeklyRadio) {
            callDashboardCountAPI(ApplicationConfiguration.FilterType.Weekly.toString());
        } else if (checkedId == R.id.monthlyRadio) {
            callDashboardCountAPI(ApplicationConfiguration.FilterType.Monthly.toString());
        } else if (checkedId == R.id.yearlyRadio) {
            callDashboardCountAPI(ApplicationConfiguration.FilterType.Yearly.toString());
        } else if (checkedId == R.id.totalRadio) {
            callDashboardCountAPI(ApplicationConfiguration.FilterType.Total.toString());
        }
    }

    private void callDashboardCountAPI(String filter) {
        try {
            String roleID = GetJsonData.getLoginData(requireContext(), WebFields.LOGIN.RESPONSE_ROLE_ID);

            String UserID = "-1";
            if (!roleID.equals("-1")) {
                UserID = GetJsonData.getLoginData(requireContext(), WebFields.LOGIN.RESPONSE_USERID);
            }

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getDashboardCount(UserID, filter));

            Call<GetDashboardModal> call = RetrofitClient.createService(ApiInterface.class).getDashboardData(body);

            call.enqueue(new Callback<GetDashboardModal>() {
                @Override
                public void onResponse(Call<GetDashboardModal> call, Response<GetDashboardModal> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            parseResponseForDashboardCount(response.body().getData().get(0));

                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<GetDashboardModal> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * Prase the response for Dashboard Count
     *
     * @param mJsonObj - Json Object
     */
    @SuppressLint("SetTextI18n")
    private void parseResponseForDashboardCount(GetDashboardModal.Data mJsonObj) {
        try {

            String TotalVisitor = mJsonObj.getTotalVisitor();
            String TotalSites = mJsonObj.getTotalActiveSite();
            String TotalColllection = mJsonObj.getTotalCollection();
            String TotalCustomer = mJsonObj.getTotalCustomer();
            String TotalReminder = mJsonObj.getVisitorReminder();


            if (TotalVisitor.equals("-1")) {
                text_home_total_visitor.setText("0");
            } else {
                text_home_total_visitor.setText(AppConstants.STR_EMPTY_STRING + TotalVisitor);
            }

            if (TotalCustomer.equals("-1")) {
                text_home_total_customer.setText("0");
            } else {
                text_home_total_customer.setText(AppConstants.STR_EMPTY_STRING + TotalCustomer);
            }

            if (TotalColllection.equals("-1")) {
                text_home_total_collection.setText("0");
            } else {
                text_home_total_collection.setText(AppConstants.STR_EMPTY_STRING + TotalColllection);
            }

            if (TotalSites.equals("-1")) {
                text_total_site.setText("0");
            } else {
                text_total_site.setText(AppConstants.STR_EMPTY_STRING + TotalSites);
            }

            if (TotalReminder.equals("-1")) {
                text_home_total_followup.setText("0");
            } else {
                text_home_total_followup.setText(AppConstants.STR_EMPTY_STRING + TotalReminder);
            }


        } catch (Exception e) {
            // ErrorLog.SaveErrorLog(e);
            AppUtil.displaySnackBarWithMessage(mLinearFullView, getString(R.string.Error_Msg_Try_Later));
        }
    }

}
