package com.tcc.fragment;


import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;
import com.tcc.Adapter.FollowUPReminderAdapter;
import com.tcc.Model.CollectionModal;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.GetJsonData;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CollectionFragment extends Fragment {

    View mView;
    PieChart pieChart;
    RelativeLayout relative_no_data_available;
    FollowUPReminderAdapter mAdapter;
    ArrayList<CollectionModal.DataItem> mArrayCollection;
    String filter;
    private String VisitorID, ReminderID, UserID, Phone, Email;
    private boolean isBackFromB;

    public CollectionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_collection, container, false);


        mArrayCollection = new ArrayList<>();
        isBackFromB = false;


        filter = getArguments().getString("filter");
        getID();
        getData();
        setRegister();
        getCollection();
        return mView;
    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }


    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        //Refresh your stuff here
        if (isBackFromB) {
            getCollection();
        }
    }

    private void getID() {
        pieChart = mView.findViewById(R.id.pieChart);
        relative_no_data_available = mView.findViewById(R.id.relative_no_data_available);

    }

    private void getData() {
        SessionManager manager = new SessionManager(getActivity());
        String UserData = manager.getPreferences(manager.KEY_LOGIN_USER_DATA, "");
        UserID = GetJsonData.getLoginData(getActivity(), WebFields.LOGIN.RESPONSE_USERID);
        VisitorID = manager.getPreferences("VisitorID", "");
        ReminderID = manager.getPreferences("ReminderID", "");
        Phone = manager.getPreferences("Mobile", "");
        Email = manager.getPreferences("Email", "");
    }

    private void setData() {

        ArrayList arrayAmount = new ArrayList();
        ArrayList<PieEntry> entries = new ArrayList<>();
        //  ArrayList<Str> arrayService = new ArrayList();

        for (int i = 0; i < mArrayCollection.size(); i++) {
            float j = i;
            // arrayService.add(mArrayCollection.get(i).getServiceName());
            entries.add(new PieEntry(Float.parseFloat(mArrayCollection.get(i).getTotalAmount()), mArrayCollection.get(i).getServiceName()));

        }
      /*  entries.add(new PieEntry(155f, "one"));
        entries.add(new PieEntry(255f, "two"));
        entries.add(new PieEntry(355f, "three"));
        entries.add(new PieEntry(455f, "four"));*/

        PieDataSet dataSet = new PieDataSet(entries, "");
        //PieDataSet dataSet2 = new PieDataSet(arrayService, dataSet);
       /* ArrayList<IPieDataSet> dataSets = new ArrayList<>();
        dataSets.add(dataSet);*/
        PieData data = new PieData(dataSet);


        //  data.setValueFormatter(new PercentFormatter());
        // data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        data.setValueTextSize(13f);
        // pieChart.setDragDecelerationEnabled(true);
        pieChart.getDescription().setEnabled(false);
        //  pieChart.setUsePercentValues(true);
        pieChart.setData(data);
        pieChart.setCenterText("Amount");
        pieChart.setEntryLabelTextSize(12);
        pieChart.setHoleRadius(75);


        pieChart.animateXY(5000, 5000);
    }

    private void setRegister() {

    }


    private void getCollection() {
        String roleID = GetJsonData.getLoginData(requireContext(), WebFields.LOGIN.RESPONSE_ROLE_ID);

        String UserID = "-1";
        if (!roleID.equals("-1")) {
            UserID = GetJsonData.getLoginData(requireContext(), WebFields.LOGIN.RESPONSE_USERID);
        }

        try {
            Common.showLoadingDialog(getActivity(), "Loading");
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getCOllection(filter));

            Call<CollectionModal> call = RetrofitClient.createService(ApiInterface.class).getCollection(body);
            call.enqueue(new Callback<CollectionModal>() {
                @Override
                public void onResponse(@NonNull Call<CollectionModal> call, @NonNull Response<CollectionModal> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            mArrayCollection.addAll(response.body().getData());
                            if (mArrayCollection.isEmpty()) {
                                relative_no_data_available.setVisibility(View.VISIBLE);
                                pieChart.setVisibility(View.GONE);
                            } else {
                                relative_no_data_available.setVisibility(View.GONE);
                                pieChart.setVisibility(View.VISIBLE);
                                setData();
                            }

                        } else {
                            relative_no_data_available.setVisibility(View.VISIBLE);
                            pieChart.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CollectionModal> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //   Common.setCustomToast(getActivity(), t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
