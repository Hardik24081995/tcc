package com.tcc.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.VisitorSiteAdapter;
import com.tcc.Model.GetSiteModel;
import com.tcc.R;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisitorSitesFragment extends Fragment {

    Activity mActivity;
    RelativeLayout mRelativeNoDataFound;
    View mView;
    VisitorSiteAdapter sitesAdapter;
    RecyclerView rv_sites;
    ImageView mImageAdd;
    ArrayList<GetSiteModel.Datum> myArrayList;
    SessionManager manager;
    String VisitorID;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean isBackFromB;

    public VisitorSitesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_visitor_sites, container, false);
        isBackFromB = false;

        myArrayList = new ArrayList<>();
        mActivity = getActivity();
        manager = new SessionManager(mActivity);
        VisitorID = manager.getPreferences("VisitorID", "");

        getID();
        getSite();


        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getSite();
            mSwipeRefreshLayout.setRefreshing(false);
        });

        return mView;

    }

    private void getSite() {
        myArrayList.clear();
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getSiteListJson("-1", "1", "-1", "-1", VisitorID,
                    "", ""));

            Call<GetSiteModel> call = RetrofitClient.createService(ApiInterface.class).getSiteAPI(body);
            call.enqueue(new Callback<GetSiteModel>() {
                @Override
                public void onResponse(@NonNull Call<GetSiteModel> call, @NonNull Response<GetSiteModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            myArrayList.addAll(response.body().getData());
                            if (myArrayList.isEmpty()) {
                                mRelativeNoDataFound.setVisibility(View.VISIBLE);
                                rv_sites.setVisibility(View.GONE);
                            } else {
                                mRelativeNoDataFound.setVisibility(View.GONE);
                                rv_sites.setVisibility(View.VISIBLE);
                                setAdapter();
                            }

                        } else {
                            mRelativeNoDataFound.setVisibility(View.VISIBLE);
                            rv_sites.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetSiteModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    // Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setAdapter() {
        sitesAdapter = new VisitorSiteAdapter(getContext(), myArrayList);
        rv_sites.setHasFixedSize(true);
        rv_sites.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_sites.setAdapter(sitesAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();
        isBackFromB = true;
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        //Refresh your stuff here
        if (isBackFromB) {
            getSite();
        }
    }

    private void getID() {

        rv_sites = mView.findViewById(R.id.rv_sites);
        mImageAdd = mView.findViewById(R.id.image_custom_toolbar_add);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipeToRefresh);
        mRelativeNoDataFound = mView.findViewById(R.id.relative_no_data_available);
    }

}
