package com.tcc.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tcc.Adapter.SiteAttendanceAdapter;
import com.tcc.Model.GetAttendanceModel;
import com.tcc.R;
import com.tcc.utils.AppConstants;
import com.tcc.utils.Common;
import com.tcc.utils.SessionManager;
import com.tcc.webServices.APICommonMethods;
import com.tcc.webServices.ApiInterface;
import com.tcc.webServices.RetrofitClient;
import com.tcc.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SiteAttendanceFragment extends Fragment {

    View mView;
    Activity mActivity;
    SessionManager manager;
    SiteAttendanceAdapter mAdapter;
    RecyclerView rv_attendancesite;
    ArrayList<GetAttendanceModel.Datum> arrayList = new ArrayList<>();
    RelativeLayout relative_no_data_available;
    String SitisID, QuotationID;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public SiteAttendanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_site_attendance, container, false);

        mActivity = getActivity();

        manager = new SessionManager(mActivity);
        SitisID = manager.getPreferences(AppConstants.SITE_ID, "");
        QuotationID = manager.getPreferences(AppConstants.QUOTATION_ID, "");
        getID();
        getAttendance();


        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAttendance();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        return mView;
    }

    private void getAttendance() {
        arrayList.clear();
        try {
            Common.showLoadingDialog(getActivity(), "Loading");
//            mArrayCustomer.clear();
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.getAtendance("-1", "1", SitisID, QuotationID));

            Call<GetAttendanceModel> call = RetrofitClient.createService(ApiInterface.class).getAttendanceAPI(body);
            call.enqueue(new Callback<GetAttendanceModel>() {
                @Override
                public void onResponse(@NonNull Call<GetAttendanceModel> call, @NonNull Response<GetAttendanceModel> response) {
                    Common.hideDialog();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            arrayList.addAll(response.body().getData());
//                            alertDialog.dismiss();
                            if (arrayList.isEmpty()) {
                                relative_no_data_available.setVisibility(View.VISIBLE);
                                rv_attendancesite.setVisibility(View.GONE);
                            } else {
                                relative_no_data_available.setVisibility(View.GONE);
                                rv_attendancesite.setVisibility(View.VISIBLE);
                                setAdapter();
                            }
                        } else {
                            relative_no_data_available.setVisibility(View.VISIBLE);
                            rv_attendancesite.setVisibility(View.GONE);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetAttendanceModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //  Common.setCustomToast(mActivity, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        SiteAttendanceAdapter mAdapter = new SiteAttendanceAdapter(getActivity(), arrayList);
        rv_attendancesite.setHasFixedSize(true);
        rv_attendancesite.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_attendancesite.setAdapter(mAdapter);
    }

    private void getID() {
        rv_attendancesite = mView.findViewById(R.id.rv_attendancesite);
        relative_no_data_available = mView.findViewById(R.id.relative_no_data_available);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipeToRefresh);
    }

}
